# -*- coding: utf-8 -*-
# -*- mode: python -*-

import contextlib
import logging
import os
import sys

LOGGER = logging.getLogger('H2D2-tools.pyinstaller')

pkg_path = os.path.join(os.environ['INRS_DEV'], 'H2D2-tools', 'script')
sys.path.append(pkg_path)

@contextlib.contextmanager
def pushd(new_dir):
    """
    https://stackoverflow.com/questions/6194499/pushd-through-os-system
    """
    previous_dir = os.getcwd()
    LOGGER.debug("pushd to %s" % new_dir)
    os.chdir(new_dir)
    try:
        LOGGER.debug("pop to %s" % previous_dir)
        yield
    finally:
        os.chdir(previous_dir)

block_cipher = None

items = []
specs = [
    'DADataAnalyzer/DADataAnalyzer.spec',
    'ProbeTracer/PTApp.spec',
    'ConvergenceCompare/CCApp.spec',
    'ConvergenceTracer/CTApp.spec',
    ]
for spec in specs:
    LOGGER.info('Analyzing %s', spec)
    try:
        pth = os.path.join(pkg_path, spec)
        with open(pth, 'rb') as f:
            code = compile(f.read(), spec, 'exec')
    except FileNotFoundError as e:
        raise SystemExit('spec "{}" not found'.format(spec))

    with pushd( os.path.dirname(pth) ):
        glbs = globals()
        exec(code, glbs)
        a = glbs['a']
        a.spec_file = pth       # inject full spec file path
        items.append(a)

if len(items) > 1:
    LOGGER.info('Merging %s', [a.name for a in items])
    args = ( (a, a.name, a.name) for a in items )
    MERGE(*args)

for a in items:
    LOGGER.info('Packaging %s', a.name)
    pkg = a.name
    pth = os.path.dirname(a.spec_file)
    with pushd(pth):
        pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
        exe = EXE(pyz,
                  a.scripts,
                  a.dependencies,
                  exclude_binaries=True,
                  name=pkg,
                  debug=False,
                  strip=False,
                  upx=True,
                  console=True)
        coll = COLLECT(exe,
                       a.binaries,
                       a.zipfiles,
                       a.datas,
                       strip=False,
                       upx=True,
                       name=pth)
