@echo off

set BIN_DIR=%~dp0
set BIN_DIR=%BIN_DIR:~0,-1%

if exist "%BIN_DIR%\..\script" (
   set BIN_DIR="%BIN_DIR%\..\script\ConvergenceTracer"
)

pushd "%BIN_DIR%"
if exist CTApp.exe (
   CTApp.exe %*
) else (
   python CTApp.py %*
)
popd
