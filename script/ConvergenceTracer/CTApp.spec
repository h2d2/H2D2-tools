# -*- coding: utf-8 -*-
# -*- mode: python -*-
import os
import sys
sys.setrecursionlimit(5000)

pkg_path = '.'
sup_path = '..'

def getDatasAndBinaries():
    hiddens  = [ 'wx.xml' ]
    datas    = []
    binaries = []
    return hiddens, datas, binaries

def analyze():
    block_cipher = None
    hiddens, datas, binaries = getDatasAndBinaries()

    a = Analysis(['CTApp.py'],
                 pathex=[pkg_path, sup_path],
                 binaries=binaries,
                 datas=datas,
                 hiddenimports=hiddens,
                 hookspath=[],
                 runtime_hooks=[],
                 excludes=[],
                 win_no_prefer_redirects=False,
                 win_private_assemblies=False,
                 cipher=block_cipher)
    a.name = 'CTApp'        # inject name
    return a

a = analyze()
