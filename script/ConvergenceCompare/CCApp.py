#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from __future__ import absolute_import
from __future__ import print_function

import os
import sys
selfDir = os.path.dirname( os.path.abspath(__file__) )
supPath = os.path.normpath(os.path.join(selfDir, '..'))
if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import wx
import CCFrame

class CCApp(wx.App):
    def __init__(self, *args, **kwargs):
        super(CCApp, self).__init__(*args, **kwargs)

    def OnInit(self):
        frm = CCFrame.CCFrame(None, -1, "")
        self.SetTopWindow(frm)
        frm.Show()
        return 1

if __name__ == "__main__":
    app = CCApp(False)
    app.MainLoop()
