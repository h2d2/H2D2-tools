# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import matplotlib
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg

import numpy
import wx

from CTCommon import CTEntry
from CTCommon import CTUtil

class ParallelIterator:
    def __init__(self, it1, it2):
        self.iter1 = it1
        self.iter2 = it2
        self.entr1 = None
        self.entr2 = None

    def __iter__(self):
        return self

    @staticmethod
    def eq_entries(l, r):
        if (not l and not r): return True
        if (not l): return False
        if (not r): return False
        if (l.stime != r.stime): return False
        if (l.iseq  != r.iseq):  return False
#        if (l.inod  != r.inod):  return False  # hndl might be different
        if (l.itsq  != r.itsq):  return False
#        if (l.algo  != r.algo):  return False  # don't discriminate on algo
#        if (l.iter  != r.iter):  return False  # or iter, only on itsq
        return True

    @staticmethod
    def lt_entries(l, r):
        if (not l): return True
        if (not r): return False
        if (l.stime > r.stime): return False
        if (l.stime < r.stime): return True
        if (l.iseq  > r.iseq):  return False
        if (l.iseq  < r.iseq):  return True
#        if (l.inod  > r.inod):  return False
#        if (l.inod  < r.inod):  return True
        if (l.itsq  < r.itsq):  return True
#        if (l.algo  > r.algo):  return False
#        if (l.algo  < r.algo):  return True
        return (l.iter  < r.iter)

    @staticmethod
    def get_next_entry(iter):
        entry = None
        while True:
            entry = next(iter)
            if isinstance(entry, CTEntry.Entry): break
        return entry

    def __next__(self):
        if  (self.eq_entries(self.entr1, self.entr2)):
            self.entr1 = self.get_next_entry(self.iter1)
            self.entr2 = self.get_next_entry(self.iter2)
        elif (self.lt_entries(self.entr1, self.entr2)):
            self.entr1 = self.get_next_entry(self.iter1)
        elif (self.lt_entries(self.entr2, self.entr1)):
            self.entr2 = self.get_next_entry(self.iter2)
        else:
            raise AttributeError

        if  (self.eq_entries(self.entr1, self.entr2)):
            return self.entr1, self.entr2
        elif (self.lt_entries(self.entr1, self.entr2)):
            return self.entr1, None
        elif (self.lt_entries(self.entr2, self.entr1)):
            return None, self.entr2

class Plot(matplotlib.figure.Figure):
    def __init__(self, parent, *args, **kwargs):
        sp_param  = matplotlib.figure.SubplotParams(left=0.10, bottom=0.15, right=0.98, top=0.96)
        super(Plot, self).__init__(subplotpars=sp_param)
        self.canvas = FigureCanvasWxAgg(parent, -1, self)

        self.ax = self.add_subplot(111)

        self.sourcex = 'ipas'
        self.__reset()

    def __reset(self):
        self.convhist = []
        self.entry_last = None

        self.vpx1 = 0.0
        self.vpx2 = 100
        self.vpy1 = 1.0e-2
        self.vpy2 = 1.0e+6

        self.__init_figure()

    def __init_figure(self):
        self.ax.clear()
        self.ax.set_xlim  (self.vpx1, self.vpx2)
        self.ax.set_xlabel('Iteration')

        self.ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
        self.ax.axhline(1.0, linestyle='-', color='darkgrey', alpha=0.7)
        self.ax.set_axisbelow(True)
        self.ax.set_yscale('log')
        self.ax.set_ylim  (self.vpy1, self.vpy2)
        self.ax.set_ylabel('Stopping Criteria')

    def __draw_one_entity(self, e1, e2, color = None):
        if (e2.stime != e1.stime):
            x = e1.x
            y = self.vpy2
            self.ax.axvline(x, color='red', linestyle='dashed', label=str(e2.stime), alpha=0.8)
            self.ax.text(x+1, y*0.6, str(e2.stime), size='x-small')
        else:
            x = numpy.array( [float(e1.x), float(e2.x)] )
            y = numpy.array( [e1.cria,  e2.cria] )
            mkt, mkc = e2.get_marker()
            stl, clr = e2.get_line()
            self.ax.semilogy(x, y, linestyle=stl, color=color, marker=mkt, markerfacecolor=mkc, picker=True, pickradius=5)

    def __draw_pair(self, e11, e12, e21, e22):
        if (not e11): e11 = e12
        if (not e21): e21 = e22
        if (e12): self.__draw_one_entity(e11, e12, color = CTUtil.get_clr(0))
        if (e22): self.__draw_one_entity(e21, e22, color = CTUtil.get_clr(1))

    def __read_one_file(self, file, convhist):
        entry = CTEntry.Entry()
        done = False
        while (not done):
            where = file.tell()
            if entry.read(file):
                convhist.append(entry)
            else:
                done = True
                file.seek(where)

    def load_data_from_file(self, fnames):
        self.__reset()

        for f in fnames:
            self.convhist.append( CTEntry.AlgoConvergence() )
            file = open(f, 'r')
            self.__read_one_file(file, self.convhist[-1])
            file.close()

        x = 0
        pIt = ParallelIterator( self.convhist[0].__iter__(), self.convhist[1].__iter__() )
        for e1, e2 in pIt:
            x += 1
            if (e1): e1.x = x
            if (e2): e2.x = x

        for c in self.convhist: c.filter_bbox()
        for c in self.convhist: c.count()

        viewCtr = self.get_view_ctrx()
        viewRng = self.get_view_rngx()
        self.set_view_x(viewCtr, viewRng)

    def draw_plot(self):
        self.fill_plot()
        self.redraw()

    def fill_plot(self):
        self.__init_figure()
        if (len(self.convhist) == 0): return

        e11 = None
        e21 = None
        pIt = ParallelIterator( self.convhist[0].__iter__(), self.convhist[1].__iter__() )
        for e12, e22 in pIt:
            in12 = (e12 and e12.x >= self.vpx1 and e12.x <= self.vpx2)
            in22 = (e22 and e22.x >= self.vpx1 and e22.x <= self.vpx2)
            if (in12 or in22):
                self.__draw_pair(e11, e12, e21, e22)
            e11, e21 = e12, e22

    def get_canvas(self):
        return self.canvas

    def get_data_spnx(self):
        """Data span in x"""
        x1 = self.convhist[0].stts.nitr
        x2 = self.convhist[1].stts.nitr
        return 0, min(x1,x2)

    def get_data_spny(self):
        """Data span in y"""
        r1 = self.convhist[0].get_rangey()
        r2 = self.convhist[1].get_rangey()
        return min(r1[0], r2[0]), max(r1[1], r2[1])

    def __get_entry_cb(self, e):
        self.pick_e = e

    def get_entry(self, line, x):
        if not x: return None
        fltr = CTEntry.Filter( 'x > %i and x < %i' % (x-1, x+1) )
        self.pick_e = None
        if (not self.pick_e):
            self.convhist[0].filter_on_entries(self.__get_entry_cb, fltr.filter)
        if (not self.pick_e):
            self.convhist[1].filter_on_entries(self.__get_entry_cb, fltr.filter)
        if (not self.pick_e): return ''
        ipas = self.pick_e.ipas
        self.__ipas = ipas

        fltr = CTEntry.Filter( 'typename == "AlgoTimeStep" and ipas == %i' % (ipas) )
        self.pick_e = None
        self.convhist[0].filter_on_container(self.__get_entry_cb, fltr.filter)
        try:
            s0 = self.pick_e.get_stats()
        except:
            s0 = ''

        self.pick_e = None
        self.convhist[1].filter_on_container(self.__get_entry_cb, fltr.filter)
        try:
            s1 = self.pick_e.get_stats()
        except:
            s1 = ''

        return (s0, s1)

    def get_stats(self):
        return (self.convhist[0].get_stats(), self.convhist[1].get_stats())

    def get_view_ctrx(self):
        return (self.vpx1 + self.vpx2) / 2

    def get_view_ctry(self):
        return (self.vpy1 + self.vpy2) / 2

    def get_view_rngx(self):
        return (self.vpx2 - self.vpx1)

    def get_view_rngy(self):
        return (self.vpy2 - self.vpy1)

    def get_view_vptx(self):
        return (self.vpx1, self.vpx2)

    def get_view_vpty(self):
        return (self.vpy1, self.vpy2)

    def redraw(self):
        self.ax.set_xlim(self.vpx1, self.vpx2)
        self.ax.set_ylim(self.vpy1, self.vpy2)
        self.canvas.draw()

    def set_sourcex(self, attrib):
        pass

    def set_view_x(self, viewCtr, viewRng):
        try:
            dataSpan = self.get_data_spnx()
            self.vpx1 = max(viewCtr-0.50*viewRng, dataSpan[0])
        except:
            self.vpx1 = viewCtr-0.50*viewRng
        self.vpx2 = self.vpx1 + viewRng
#        self.vpx2 = max(self.vpx1+viewRng, viewRng)

    def set_view_y(self, viewCtr, viewRng):
        try:
            dataSpan = self.get_data_spny()
            self.vpy1 = max(viewCtr-0.50*viewRng, dataSpan[0])
        except:
            self.vpy1 = viewCtr-0.50*viewRng
        self.vpy2 = self.vpy1 + viewRng
#        self.vpy2 = max(self.vpy1+viewRng, viewRng)

    def set_view_ctrx(self, viewCtr):
        viewRng = self.get_view_rngx()
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_ctry(self, viewCtr):
        viewRng = self.get_view_rngy()
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_view_rngx(self, viewRng):
        viewCtr = self.get_view_ctrx()
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_rngy(self, viewRng):
        viewCtr = self.get_view_ctry()
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_view_vptx(self, xmin, xmax):
        viewCtr = 0.5*(xmax + xmin)
        viewRng = xmax - xmin
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_vpty(self, ymin, ymax):
        viewCtr = 0.5*(ymax + ymin)
        viewRng = ymax - ymin
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def on_update(self):
        pass

if __name__ == '__main__':
    app = wx.PySimpleApp()

    frame = wx.Frame(None, -1, "")
    plot = Plot(frame)
    plot.read('t1.log', 't2.log')
    plot.draw_plot()

    frame.Show()
    app.MainLoop()
