@echo off

set BIN_DIR=%~dp0
set BIN_DIR=%BIN_DIR:~0,-1%

if exist "%BIN_DIR%\..\script" (
   set BIN_DIR="%BIN_DIR%\..\script\ConvergenceCompare"
)

pushd "%BIN_DIR%"
if exist CCApp.exe (
   CCApp.exe %*
) else (
   python CCApp.py %*
)
popd
