# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import CTCommon.CTCurve as CTCurve

class Curves(CTCurve.Curves):
    def __init__(self):
        super(Curves, self).__init__()

    def __read_file_one_entry(self):
        line = self.file.readline()
        if (not line): raise EOFError
        nlin, ncol, time = line.split()
        nlin = int(nlin)
        ncol = int(ncol)
        time = float(time)

        if (len(self.curves) == 0):
            for i in range(nlin):
                self.curves.append( CTCurve.Curve(i) )

        for i in range(nlin):
            line = self.file.readline()
            val = float(line)
            self.curves[i].add_entry(time, val)
            self.ymin = min(self.ymin, val)
            self.ymax = max(self.ymax, val)

        self.xmin = min(self.xmin, time)
        self.xmax = max(self.xmax, time)

    def read_file_to_eof(self):
        try:
            while (True):
                where = self.file.tell()
                self.__read_file_one_entry()
        except EOFError:
            self.file.seek(where)

    def __read_table_one_entry(self, data):
        time = data[0]
        nlin = len(data[1])

        if (len(self.curves) == 0):
            for i in range(nlin):
                self.curves.append( CTCurve.Curve(i) )

        for d, i in zip(data[1], list(range(nlin))):
            self.curves[i].add_entry(time, d)
            self.ymin = min(self.ymin, d)
            self.ymax = max(self.ymax, d)

        self.xmin = min(self.xmin, time)
        self.xmax = max(self.xmax, time)

    def read_table_to_eod(self, data):
        for d in data:
            self.__read_table_one_entry(d)

if __name__ == '__main__':
    plot = CTCurve.Curve()
    plot.load_data_from_file('p1.prb')
    plot.fill_plot(None)

