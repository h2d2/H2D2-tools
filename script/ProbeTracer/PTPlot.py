# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from CTCommon import CTPlot
try:
    from . import PTCurve
except ImportError:
    import PTCurve

class Plot(CTPlot.Plot):
    def __init__(self, parent, *args, **kwargs):
        super(Plot, self).__init__(parent, *args, **kwargs)

    def load_data_from_file(self, fname):
        self.reset()
        if fname and len(fname) > 0:
            self.xmin =  1.0e99
            self.xmax = -1.0e99
            self.ymin =  1.0e99
            self.ymax = -1.0e99

            for f in fname:
                crv = PTCurve.Curves()
                self.curves.append(crv)
                crv.load_data_from_file(f)

                rx = crv.get_data_rangex()
                ry = crv.get_data_rangey()
                self.xmin = min(self.xmin, rx[0])
                self.xmax = max(self.xmax, rx[1])
                self.ymin = min(self.ymin, ry[0])
                self.ymax = max(self.ymax, ry[1])

        self.reset_view()

    def load_data_from_table(self, data):
        self.reset()
        if data and len(data) > 0:
            self.xmin =  1.0e99
            self.xmax = -1.0e99
            self.ymin =  1.0e99
            self.ymax = -1.0e99

            for d in data:
                crv = PTCurve.Curves()
                self.curves.append(crv)
                crv.load_data_from_table(d)

                rx = crv.get_data_rangex()
                ry = crv.get_data_rangey()
                self.xmin = min(self.xmin, rx[0])
                self.xmax = max(self.xmax, rx[1])
                self.ymin = min(self.ymin, ry[0])
                self.ymax = max(self.ymax, ry[1])

        self.reset_view()


if __name__ == '__main__':
    import wx
    app = wx.App()

    frame = wx.Frame(None, -1, "")
    plot = Plot(frame)
    plot.load_data_from_file( ['p1.prb'] )
    plot.draw_plot()

    frame.Show()
    app.MainLoop()
