# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import numpy as np

from CTCommon.FEMesh import FEMesh, FE1DRegularGrid, FE2DRegularGrid
from IPImageProcessor.GDAL import GDLBasemap

class DAFrameGrid:
    cntrInt = 0xFF00
    cntrXtr = 0x0000

    def __init__(self, grid, parent=None, srsGrid=None, srsProj=None, shortName=None, longName=None):
        assert isinstance(grid, (FEMesh, FE1DRegularGrid, FE2DRegularGrid))
        self.grid   = grid
        self.parent = parent
        self.srsGrid = srsGrid
        self.srsProj = srsProj
        if srsGrid and srsProj:
            self.grid2proj = GDLBasemap.GDLCoordinateTransformation(srsGrid, srsProj)
            self.proj2grid = GDLBasemap.GDLCoordinateTransformation(srsProj, srsGrid)
        elif parent:
            self.grid2proj = parent.grid2proj
            self.proj2grid = parent.proj2grid
            self.srsGrid = self.grid2proj.getSourceCS() if self.grid2proj else None
            self.srsProj = self.proj2grid.getSourceCS() if self.proj2grid else None
        else:
            self.grid2proj = None
            self.proj2grid = None
        self.shortName = shortName
        self.longName  = longName
        self.bboxProj  = None

        if parent:
            self.idx = DAFrameGrid.cntrInt
            DAFrameGrid.cntrInt += 1
        else:
            self.idx = DAFrameGrid.cntrXtr
            DAFrameGrid.cntrXtr += 1

    @staticmethod
    def resetCounters():
        DAFrameGrid.cntrInt = 0xFF00
        DAFrameGrid.cntrXtr = 0x0000

    def __xtrBbox(self):
        """
        Bounding box for projected coord, without stretching & margins
        """
        X, Y = self.getCoordinates()
        xmin = np.min(X)
        xmax = np.max(X)
        ymin = np.min(Y)
        ymax = np.max(Y)
        return (xmin, ymin, xmax, ymax)

    def __str__(self):
        return self.getShortName()

    def getGrid(self):
        return self.grid

    def getParent(self):
        return self.parent

    def getShortName(self):
        if self.shortName: return self.shortName

        files = self.grid.getFiles()
        if files:
            if len(files) == 1:
                name = os.path.basename(files[0])
            else:
                name = ' '.join( (os.path.basename(files[0]), '...') )
        else:
            name = 'Grid name not set'
        return '#%04x %s' % (self.idx, name)

    def getLongName(self):
        if self.longName: return self.longName

        files = self.grid.getFiles()
        name = '; '.join(files) if files else 'Grid name not set'
        return '#%04x %s' % (self.idx, name)

    def getCoordinates(self):
        """
        Specialize getCoordinates to return projected coordinates.
        Raw coord can be retrieved by a call to the parent method
        FEMesh.getCoordinates
        """
        X, Y = self.grid.getCoordinates()
        if self.grid2proj:
            X, Y = self.grid2proj.TransformPoints(X, Y)
        return X, Y

    def getBbox(self, stretchfactor=0, margins=(0, 0)):
        """
        Specialize getBbox to the bounding box for projected coordinates.
        BBox for raw coord can be retrieved by a call to the parent method
        FEMesh.getBbox
        """
        if not self.bboxProj:
            self.bboxProj = self.__xtrBbox()
        xmin, ymin, xmax, ymax = self.bboxProj
        dx = (xmax-xmin)*stretchfactor + margins[0]
        dy = (ymax-ymin)*stretchfactor + margins[1]
        return (xmin-dx, ymin-dy, xmax+dx, ymax+dy)

    def getGridSRS(self):
        return self.srsGrid

    def getProjSRS(self):
        return self.srsProj

    def getProjectionFromProjToGrid(self):
        return self.proj2grid

    def getProjectionFromGridToProj(self):
        return self.grid2proj

    def hasProjection(self):
        return self.grid2proj is not None

    def setProjectSRS(self, srsProj):
        if self.srsGrid:
            srsGrid = self.srsGrid
            self.grid2proj = GDLBasemap.GDLCoordinateTransformation(srsGrid, srsProj)
            self.proj2grid = GDLBasemap.GDLCoordinateTransformation(srsProj, srsGrid)

    def projectToGrid(self, X, Y):
        """
        Projet X, Y coord form project to grid coordinates
        """
        if self.proj2grid:
            X, Y = self.proj2grid.TransformPoints(X, Y)
        return X, Y

    def projectToProj(self, X, Y):
        """
        Projet X, Y coord form grid coordinates to project
        """
        if self.grid2proj:
            X, Y = self.grid2proj.TransformPoints(X, Y)
        return X, Y

    def __getattr__(self, name):
        """
        Transfert all unknown calls to the grid
        """
        return getattr(self.grid, name)
