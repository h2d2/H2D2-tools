# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import traceback

from pubsub import pub
from matplotlib.lines         import Line2D
from matplotlib.backend_bases import MouseButton
import mplcursors
import wx

if __name__ == '__main__':
    import os
    import sys
    selfDir = os.path.dirname (os.path.abspath(__file__))
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)
    supPath = os.path.normpath(os.path.join(supPath, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

from IPImageProcessor       import IPRaster
from IPImageProcessor.GDAL  import GDLBasemap
from CTCommon.FEMesh        import FEMesh
from DAFrameGrid            import DAFrameGrid
from DALayers.DALayer       import DALayer, DALayerItemCS
from DALayers.DADlgParamLineString import DADlgParamLineString, markers

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Layer.Linestring")

hlp="""\
Click on first point to select start point of linestring
CTRL-click on second point to select lass point of linestring

ALT  to switch momentarly out of zoom-pan mode
CTRL to add point
"""

class DAToolNodeDataCursor:
    """
    Encapsulate a mplcursors.Cursor.
    The mplcursors.Cursor will be constructed on activate() and
    removed on deactivate()
    """
    def __init__(self, CS, formatter=None, xytext=(5,5), hover=False):
        self.CS     = CS
        self.fmtr   = formatter
        self.xytext = xytext
        self.cursor = None
        self.hover  = hover
        self.activate()

    def __del__(self):
        try:
            self.deactivate()
        except RuntimeError:        # canvas might be already dead
            pass

    def activate(self):
        if not self.cursor:
            self.cursor = mplcursors.Cursor([self.CS], hover=self.hover)
        if self.fmtr:
            self.cursor.connect("add", self.fmtr)

    def deactivate(self):
        if self.cursor:
            self.cursor.remove()
            self.cursor = None

class DALayerLineString(DALayer):
    count = 0
    MODIF_ALT  = ['alt', 'alt+control']
    MODIF_CTRL = ['control', 'alt+control']

    def __init__(self, page, field, cbOnGenGrid=None):
        super().__init__()
        self.onGenGrid = cbOnGenGrid if cbOnGenGrid else self.__dummyCbOnGenGrid
        self.pane = page
        self.prnt = field.getGrid()
        # ---  Gen grid with only nodes connected to elements
        self.grid = self.prnt.genCompactGrid( connec=self.prnt.getConnectivities() )

        self.fig       = self.pane.get_figure()
        self.ax        = self.pane.get_axes()
        self.canvas    = self.fig.canvas
        self.toolbar   = self.pane.get_toolbar()
        self.oldCursor = None

        self.__create_menus()
        self.pane.Bind(wx.EVT_MENU, self.onMenuXeqReset,    self.mnu_un_rst)
        self.pane.Bind(wx.EVT_MENU, self.onMenuXeqInverse,  self.mnu_xq_inv)
        self.pane.Bind(wx.EVT_MENU, self.onMenuXeqRestrict, self.mnu_xq_lmt)
        self.pane.Bind(wx.EVT_MENU, self.onMenuXeqReset,    self.mnu_xq_rst)
        self.pane.Bind(wx.EVT_MENU, self.onMenuIdB, self.mnu_id_bnd)
        self.pane.Bind(wx.EVT_MENU, self.onMenuIdS, self.mnu_id_sri)
        self.pane.Bind(wx.EVT_MENU, self.onMenuIdL, self.mnu_id_lst)
        self.pane.Bind(wx.EVT_MENU, self.onMenuTpl, self.mnu_xy_tpl)
        self.pane.Bind(wx.EVT_MENU, self.onMenuLst, self.mnu_xy_lst)
        self.pane.Bind(wx.EVT_MENU, self.onMenuWKT, self.mnu_xy_wkt)

        self.mplCIDs = []
        canvas = self.canvas
        self.mplCIDs.append( canvas.mpl_connect('motion_notify_event', self.onMouseMove) )
        self.mplCIDs.append( canvas.mpl_connect('button_press_event',  self.onMouseClick) )
        self.mplCIDs.append( canvas.mpl_connect('button_release_event',self.onMouseUnclick) )
        self.mplCIDs.append( canvas.mpl_connect('scroll_event',        self.onMouseScroll) )
        self.mplCIDs.append( canvas.mpl_connect('pick_event',          self.onPick) )
        self.mplCIDs.append( canvas.mpl_connect('key_press_event',     self.onKeyPress) )
        self.mplCIDs.append( canvas.mpl_connect('key_release_event',   self.onKeyRelease) )

        DALayerLineString.count += 1
        self.title  = 'LineString editor #%03i' % DALayerLineString.count
        self.marker = 'circle'
        self.elem_color = '#8080FF'
        self.node_color = '#00DFDF'
        self.has_cursor = False

        self.ptIdx  = -1
        self.ptIdxs = []
        self.cs = None
        self.dc = None
        self.ov = Line2D([], [], linestyle='solid', marker='s', color='r')
        self.bndCount = 0

    def __create_menus(self):
        self.mnuSel = wx.Menu()
        self.mnu_xq_inv = wx.MenuItem(self.mnuSel, wx.ID_ANY, 'Inverse selection', '', wx.ITEM_NORMAL)
        self.mnuSel.Append(self.mnu_xq_inv)
        self.mnu_xq_lmt = wx.MenuItem(self.mnuSel, wx.ID_ANY, 'Restrict to selection', '', wx.ITEM_NORMAL)
        self.mnuSel.Append(self.mnu_xq_lmt)
        self.mnu_xq_rst = wx.MenuItem(self.mnuSel, wx.ID_ANY, 'Reset', '', wx.ITEM_NORMAL)
        self.mnuSel.Append(self.mnu_xq_rst)
        self.mnuSel.AppendSeparator()
        self.mnu_id = wx.Menu()
        self.mnu_id_bnd = wx.MenuItem(self.mnu_id, wx.ID_ANY, 'Pick as boundary', 'Copy nodes indexes to clipboard as H2D2 boundary', wx.ITEM_NORMAL)
        self.mnu_id.Append(self.mnu_id_bnd)
        self.mnu_id_sri = wx.MenuItem(self.mnu_id, wx.ID_ANY, 'Pick id',      'Copy nodes indexes to clipboard as a space separated serie', wx.ITEM_NORMAL)
        self.mnu_id.Append(self.mnu_id_sri)
        self.mnu_id_lst = wx.MenuItem(self.mnu_id, wx.ID_ANY, 'Pick [id]',    'Copy nodes indexes to clipboard as a list', wx.ITEM_NORMAL)
        self.mnu_id.Append(self.mnu_id_lst)
        self.mnuSel.AppendSubMenu(self.mnu_id, 'Indexes')
        self.mnu_xy = wx.Menu()
        self.mnu_xy_tpl = wx.MenuItem(self.mnu_xy, wx.ID_ANY, 'Pick (x,y)',   'Copy x,y location to clipboard as tuples', wx.ITEM_NORMAL)
        self.mnu_xy.Append(self.mnu_xy_tpl)
        self.mnu_xy_lst = wx.MenuItem(self.mnu_xy, wx.ID_ANY, 'Pick [(x,y)]', 'Copy x,y location to clipboard as a list of tuples', wx.ITEM_NORMAL)
        self.mnu_xy.Append(self.mnu_xy_lst)
        self.mnu_xy_wkt = wx.MenuItem(self.mnu_xy, wx.ID_ANY, 'Pick as WKT',  'Copy x, y location to clipboard as WKT', wx.ITEM_NORMAL)
        self.mnu_xy.Append(self.mnu_xy_wkt)
        self.mnuSel.AppendSubMenu(self.mnu_xy, 'xy')

        self.mnuUnsel = wx.Menu()
        self.mnu_un_rst = wx.MenuItem(self.mnuUnsel, wx.ID_ANY, 'Reset', '', wx.ITEM_NORMAL)
        self.mnuUnsel.Append(self.mnu_un_rst)

    def __del__(self):
        DALayerLineString.delete(self)

    @staticmethod
    def delete(self):
        try:
            for cid in self.mplCIDs:
                self.canvas.mpl_disconnect(cid)
            if self.dc:
                self.dc.deactivate()
        except:
            pass
        finally:
            self.mplCIDs = []
            self.dc = None

    def __dummyCbOnGenGrid(self, event):
        pass

    def __str__(self):
        return self.title

    def __copyToClipboard(self, txt):
        clipdata = wx.TextDataObject(txt)
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()

    def __dataCursorPathFormatter(self, selection):
        try:
            idx = selection.index
            idx = self.grid.getNode(idx).getGlobalIndex()
            sid = '%i' % idx
            selection.annotation.set_text(sid)
        except Exception as e:
            errMsg = '%s\n%s' % (str(e), traceback.format_exc())
            LOGGER.error(errMsg)

    def __redrawOverlay(self):
        if len(self.ptIdxs) > 1:
            grid = self.grid.genLineString(self.ptIdxs[0], self.ptIdxs[-1])
            X, Y = grid.getCoordinates()
        elif len(self.ptIdxs) == 1:
            node = self.grid.getNode(self.ptIdxs[0])
            X, Y = node.getCoordinates()
        else:
            X, Y = [], []
        self.ov.set_data(X, Y)
        self.canvas.draw()

    def __genIpFig(self):
        kwargs = {}
        kwargs['marker'] = markers[self.marker]
        kwargs['markerfacecolor']  = self.node_color
        kwargs['color']  = self.elem_color
        kwargs['picker'] = True
        kwargs['pickradius'] = 5

        self.items = []
        cs = self.ax.drawMesh(self.grid, color = self.elem_color)
        self.items.append( DALayerItemCS(cs, IPRaster.IPPlotType.SUB_MESH, 'Sub-grid') )
        cs = self.ax.drawNodesMesh(self.grid, **kwargs)[0]
        self.items.append( DALayerItemCS(cs, IPRaster.IPPlotType.NODES,  'Nodes') )
        self.items.append( DALayerItemCS(self.ov, IPRaster.IPPlotType.NODES,  'Selection') )

        # ---  Keep nodes
        self.cs = cs
        # ---  Add data cursor for nodes
        if self.has_cursor:
            self.dc = DAToolNodeDataCursor(self.cs, xytext=(5,5), hover=True, formatter=self.__dataCursorPathFormatter)

        self.pane.add_layer(self)
        self.ov.set_data([], [])

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.__genIpFig()
        self.pane.redraw()

    def draw(self):
        busyOnEntry = wx.IsBusy()
        if busyOnEntry: wx.EndBusyCursor()

        dlg = DADlgParamLineString(self.pane)
        dlg.set_title (self.title)
        dlg.set_marker(self.marker)
        #dlg.set_element_color(self.elem_color)
        dlg.set_node_color(self.node_color)
        #dlg.set_has_cursor(self.has_cursor)

        dlgOK = True # (dlg.ShowModal() == wx.ID_OK)
        if dlgOK:
            self.title  = dlg.title
            self.marker = dlg.marker
            #self.elem_color = dlg.element_color
            self.node_color = dlg.node_color
            # self.has_cursor = dlg.has_cursor
        dlg.Destroy()
        if busyOnEntry: wx.BeginBusyCursor()

        errMsg = ''
        if dlgOK:
            try:
                self.__xeqDraw()
            except Exception as e:
                import traceback
                errMsg = '\n'.join( ('Drawing the skin of a skin/L2 mesh?', str(e), traceback.format_exc()) )

        if errMsg:
            dlg = wx.MessageDialog(self.pane, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()

    def getSourceGrid(self):
        return self.prnt

    def getGrid(self):
        if len(self.ptIdxs) != 2:
            pub.sendMessage('statusbar.flashmessage', message='Invalid list of nodes')
            return

        wx.BeginBusyCursor()
        try:
            grid = self.grid.genLineString(self.ptIdxs[0], self.ptIdxs[1])
            return grid
        finally:
            wx.EndBusyCursor()

    def __formatLSForBoundary(self, nodes):
        txt = []
        for i in range(0, len(nodes), 50):
            imin = i
            imax = min(i+50, len(nodes))
            nods = ['%8d' % (n+1) for n in nodes[imin:imax]]
            line = ' '.join(nods)
            line = '$__boundary:_%06d_%06d$ %s' % (self.count, self.bndCount, line)
            txt.append(line)
        return txt

    def onMenuXeqReset(self, event):
        self.ptIdxs = []
        self.ptIdx  = -1
        self.grid = self.prnt.genCompactGrid( connec=self.prnt.getConnectivities() )
        self.__xeqDraw()

    def onMenuXeqRestrict(self, event):
        if len(self.ptIdxs) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of nodes')
            return
        self.grid = self.getGrid()
        self.__xeqDraw()

    def onMenuXeqInverse(self, event):
        if len(self.ptIdxs) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of nodes')
            return
        self.ptIdxs = list( reversed(self.ptIdxs) )
        no0 = self.ptIdxs[0]
        if len(self.grid.getElementsOfNode(no0)) == 1:
            self.ptIdxs[0] = self.grid.getNode(-1).getLocalIndex()
        self.__redrawOverlay()

    def onMenuIdB(self, event):
        if len(self.ptIdxs) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of nodes')
            return
        self.bndCount += 1
        grid = self.getGrid()
        lst = [ n.getGlobalIndex() for n in grid.iterNodes() ]
        txt = self.__formatLSForBoundary(lst)
        self.__copyToClipboard('\n'.join(txt))

    def onMenuIdS(self, event):
        if len(self.ptIdxs) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of nodes')
            return
        grid = self.getGrid()
        fmt = '{i:d}'
        lst = [ fmt.format(i=n.getGlobalIndex()) for n in grid.iterNodes() ]
        txt = ' '.join(lst)
        self.__copyToClipboard('%s' % txt)

    def onMenuIdL(self, event):
        if len(self.ptIdxs) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of nodes')
            return
        grid = self.getGrid()
        fmt = '{i:d}'
        lst = [ fmt.format(i=n.getGlobalIndex()) for n in grid.iterNodes() ]
        txt = ', '.join(lst)
        self.__copyToClipboard('[%s]' % txt)

    def onMenuTpl(self, event):
        X, Y = self.ov.get_data()
        if len(X) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of nodes')
            return
        fmt = '({x:f}, {y:f})'
        lst = [ fmt.format(x=x, y=y) for x, y in zip(X, Y) ]
        txt = ', '.join(lst)
        self.__copyToClipboard('%s' % txt)

    def onMenuLst(self, event):
        X, Y = self.ov.get_data()
        if len(X) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of nodes')
            return
        fmt = '({x:f}, {y:f})'
        lst = [ fmt.format(x=x, y=y) for x, y in zip(X, Y) ]
        txt = ', '.join(lst)
        self.__copyToClipboard('[%s]' % txt)

    def onMenuWKT(self, event):
        X, Y = self.ov.get_data()
        if len(X) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of nodes')
            return
        if len(X) == 1:
            fmt = '{x:f} {y:f}'
            txt = fmt.format(x=X[0], y=Y[0])
            self.__copyToClipboard('POINT(%s)' % txt)
        else:
            fmt = '({x:f} {y:f})'
            lst = [ fmt.format(x=x, y=y) for x, y in zip(X, Y) ]
            txt = ', '.join(lst)
            self.__copyToClipboard('MULTIPOINT(%s)' % txt)

    def __onMouseClickRight(self, event):
        """
        Display pop-up menu
        """
        #print('DALayerLineString.__onMouseClickRight', event.key)
        if len(self.ptIdxs) < 2:
            self.pane.PopupMenu(self.mnuUnsel)
        else:
            self.pane.PopupMenu(self.mnuSel)

    def onMouseClick(self, event):
        if self.toolbar.mode: return        # not in NONE mode
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.xdata  is None: return
        if event.button == MouseButton.LEFT:
            return
        if event.button == MouseButton.RIGHT:
            return self.__onMouseClickRight(event)

    def __onMouseUnclickLeft(self, event):
        if self.ptIdx == -1:
            print('DALayerLineString.__onMouseUnclickLeft: key = ', event.key)
            if event.key not in DALayerLineString.MODIF_CTRL:
                self.ptIdxs = []
                self.ov.set_data([], [])
        else:
            if self.ptIdx in self.ptIdxs:
                self.ptIdxs.remove(self.ptIdx)
                self.__redrawOverlay()
            else:
                self.ptIdxs.append(self.ptIdx)
                self.__redrawOverlay()

        event.canvas.draw()
        self.ptIdx = -1

    def onMouseUnclick(self, event):
        if self.toolbar.mode: return        # not in NONE mode
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.xdata  is None: return
        if event.button == MouseButton.LEFT:
            return self.__onMouseUnclickLeft(event)
        else:
            return

    def onMouseScroll(self, event):
        """
        Reset the pick event generated by scroll
        """
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        self.ptIdx = -1

    def onMouseMove(self, event):
        """
        Reset the pick event generated by move
        """
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        self.ptIdx = -1

    def onKeyPress(self, event):
        """
        """
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        #print('DALayerLineString.onKeyPress', event.key)
        if event.key in DALayerLineString.MODIF_ALT and not self.oldCursor:
            self.oldCursor = event.canvas.GetCursor()
            newCursor = wx.Cursor(wx.CURSOR_ARROW)
            event.canvas.SetCursor(newCursor)
            self.toolbar.saveState()
            self.toolbar.resetState()

    def onKeyRelease(self, event):
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        #print('DALayerLineString.onKeyRelease', event.key)
        if event.key in DALayerLineString.MODIF_ALT:
            if self.oldCursor:
                event.canvas.SetCursor(self.oldCursor)
                self.oldCursor = None
                self.toolbar.restoreState()

    def onPick(self, event):
        """
        Set ptIdx to the picked item
        """
        #print('DALayerLineString.onPick', event)
        #print('DALayerLineString.onPick', event.artist)
        if self.toolbar.mode: return        # not in NONE mode
        if event.canvas is not self.canvas: return
        if not isinstance(event.artist, Line2D): return
        if len(event.ind) == 0: return
        if len(event.ind) != 1: return
        artist = event.artist
        marker = artist.get_marker()
        #print('DALayerLineString.onPick: marker ', marker)
        #print('DALayerLineString.onPick: ind ', event.ind)
        if marker and marker != 'None':
            self.ptIdx = event.ind[0]


if __name__ == "__main__":
    import wx.lib.agw.flatnotebook as fnb
    from DAFrameField import DAFrameField
    from DAPanels.DAPnl2DFigure     import DAPnl2DFigure
    LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer")

    class TestFrame(wx.Frame):
        def __init__(self, *args, **kwds):
            # begin wxGlade: TestFrame.__init__
            kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
            wx.Frame.__init__(self, *args, **kwds)
            self.SetSize((800, 600))
            self.nbk = fnb.FlatNotebook(self, wx.ID_ANY, style=fnb.FNB_X_ON_TAB)

            self.__set_properties()
            self.__do_layout()

            self.fld = self.__genField()

            w, h = self.nbk.GetClientSize()
            w, h = self.nbk.ConvertPixelsToDialog((w,h))
            self.bbox = self.fld.getGrid().getBbox(stretchfactor=0.02)
            self.size = (float(w)/80, float(h)/80)
            self.screen = GDLBasemap.GDLGeoTransform()
            self.screen.setWindow(self.bbox)
            self.screen.setViewportFromSize(self.size, 80)
            self.screen.resizeWindowToViewport()

            self.fig = IPRaster.IPFigure()
            self.fig.createFigure(size=self.size, use_pyplot=False)
            ax = self.fig.addSubPlot(1, 1, 1) #, facecolor='w')
            self.fig.adjustSubPlots(hspace=0.0, wspace=0.0, bottom=0.0, top=1.0, left=0.0, right=1.0)
            ax.setup(window=self.screen.getWindow(), aspect_equal=True)
            ax.init()
            ax.removeAxis()

            self.win = DAPnl2DFigure(parent=self.nbk, fig=self.fig, axes=ax)
            indx = self.nbk.GetPageCount()
            self.nbk.InsertPage(indx, self.win, 'Edit')
            self.nbk.SetSelection(indx)
            self.win.toolbar.Show()
            self.win.show(True)

            self.lyr = DALayerLineString(self.win, self.fld)
            self.lyr.draw()
            # end wxGlade

        def __set_properties(self):
            self.SetTitle("LineString Unit Test")

        def __do_layout(self):
            # begin wxGlade: TestFrame.__do_layout
            szr_main = wx.BoxSizer(wx.VERTICAL)
            szr_main.Add(self.nbk, 1, wx.EXPAND, 0)
            self.SetSizer(szr_main)
            self.Layout()
            # end wxGlade

        def __genField(self):
            p = 'E:/inrs-dev/tutorials/tutorial3'
            f = os.path.join(p, 'test.cor'), os.path.join(p, 'test.ele')
            mesh  = FEMesh(files=f, withLocalizer=True)
            grid  = DAFrameGrid(mesh)
            field = DAFrameField.as2DFiniteElementField(grid=grid)
            return field

        def edit(self):
            pass

    class TestApp(wx.App):
        def OnInit(self):
            self.frame = TestFrame(None, wx.ID_ANY, "")
            self.SetTopWindow(self.frame)
            self.frame.Show()
            return True

    formatter = logging.Formatter('%(asctime)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    #import time
    #waiting = True
    #while waiting:
    #    time.sleep(1000)

    app = TestApp(0)
    app.MainLoop()
