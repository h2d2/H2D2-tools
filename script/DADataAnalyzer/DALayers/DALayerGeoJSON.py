# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2021
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx

from . import DALayer
from IPImageProcessor import IPPlotter

from . import DADlgParamGeoJSON

class DALayerGeoJSON(DALayer.DALayer):
    count = 0

    def __init__(self, page, data = [], title=''):
        super().__init__()
        self.pane = page
        self.data = data

        self.fig  = self.pane.get_figure()
        self.ax   = self.pane.get_axes()

        DALayerGeoJSON.count += 1
        self.title = 'Patches #%03d' % DALayerGeoJSON.count
        if title: self.title += ' @ %s' % title
        self.faceColor = '#6699CC'
        self.edgeColor = '#999999'
        self.alpha     = 0.5

    def __genIpFig(self):
        kwargs = {'fc' : self.faceColor,
                  'ec' : self.edgeColor,
                  'alpha': self.alpha }
        CSS = self.ax.drawGeoJSON(self.data, **kwargs)
        self.items = [ DALayer.DALayerItemCS(cs, IPPlotter.IPPlotType.PATCH, 'Patch #%03d' % i)
                       for i, cs in enumerate(CSS) ]
        self.pane.add_layer(self)

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.__genIpFig()
        self.pane.redraw()

    def draw(self):
        busyOnEntry = wx.IsBusy()
        if busyOnEntry: wx.EndBusyCursor()

        dlg = DADlgParamGeoJSON.DADlgParamGeoJSON(self.pane)
        dlg.set_title (self.title)
        dlg.set_face_color(self.faceColor)
        dlg.set_edge_color(self.edgeColor)
        dlg.set_alpha_value(self.alpha)

        dlgOK = (dlg.ShowModal() == wx.ID_OK)
        if dlgOK:
            self.title = dlg.title
            self.faceColor = dlg.face_color
            self.edgeColor = dlg.edge_color
            self.alpha     = dlg.alpha_value
        dlg.Destroy()
        if busyOnEntry: wx.BeginBusyCursor()

        errMsg = ''
        if dlgOK:
            try:
                self.__xeqDraw()
            except Exception as e:
                import traceback
                errMsg = '\n'.join( (str(e), traceback.format_exc()) )

        if errMsg:
            dlg = wx.MessageDialog(self.pane, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()

    def __str__(self):
        return self.title

if __name__ == "__main__":
    pass
    # dummy = DALayerGeoJSON(None, 1.0, None)
