#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2021
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import collections.abc

import wx
import wx.lib.colourselect as  wxcsel

class DADlgParamGeoJSON(wx.Dialog):
    def __init__(self, *args, **kwds):
        kwds['style'] = wx.DEFAULT_DIALOG_STYLE
        super().__init__(*args, **kwds)

        self.szr_val_staticbox = wx.StaticBox(self, wx.ID_ANY, 'Values')
        self.szr_top_staticbox = wx.StaticBox(self, wx.ID_ANY, 'Name')
        self.stx_title = wx.StaticText(self, wx.ID_ANY, 'Layer name ')
        self.txt_title = wx.TextCtrl  (self, wx.ID_ANY, '')
        self.stx_fcclr = wx.StaticText(self, wx.ID_ANY, 'Face color')
        self.stx_ecclr = wx.StaticText(self, wx.ID_ANY, 'Edge color')
        self.stx_alpha  = wx.StaticText(self, wx.ID_ANY, 'Alfa blending')

        self.btn_fcclr = wxcsel.ColourSelect(self, wx.ID_ANY, ' '*25)
        self.btn_ecclr = wxcsel.ColourSelect(self, wx.ID_ANY, ' '*25)
        self.spn_alpha  = wx.SpinCtrlDouble(self, wx.ID_ANY, min=0.0, max=1.0, initial=1.0, inc=0.01, style=wx.TE_PROCESS_TAB)

        self.btn_ok     = wx.Button(self, wx.ID_OK, '')
        self.btn_cancel = wx.Button(self, wx.ID_CANCEL, '')

        self.__set_properties()
        self.__do_layout()

        self.Bind(wxcsel.EVT_COLOURSELECT, self.on_btn_color)
        self.Bind(wx.EVT_BUTTON, self.on_btn_ok,  self.btn_ok)
        self.Bind(wx.EVT_BUTTON, self.on_btn_cancel, self.btn_cancel)

        self.cb = None

    def __set_properties(self):
        self.SetTitle('MPL Patch parameters')
        self.txt_title.SetToolTip ('Name of the layer')
        self.btn_ok.SetToolTip    ('Apply the changes')
        self.btn_cancel.SetToolTip('Cancel changes and close the dialog box')

        self.btn_ok.SetDefault()

        sz = self.txt_title.GetTextExtent("M"*120)
        self.txt_title.GetSizeFromTextSize(sz.x, sz.y*2)

        self.btn_fcclr.SetValue('#6699CC')
        self.btn_ecclr.SetValue('#999999')
        self.spn_alpha.SetValue(0.5)

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)
        szr_btn  = wx.BoxSizer(wx.HORIZONTAL)

        szr_top  = wx.StaticBoxSizer(self.szr_top_staticbox, wx.HORIZONTAL)
        szr_top.Add(self.stx_title, 1, wx.EXPAND, 0)
        szr_top.Add(self.txt_title, 3, wx.EXPAND, 0)

        szr_val  = wx.StaticBoxSizer(self.szr_val_staticbox, wx.VERTICAL)
        szr_fcclr = wx.BoxSizer(wx.HORIZONTAL)
        szr_fcclr.Add(self.stx_fcclr, 3, wx.ALIGN_BOTTOM, 0)
        szr_fcclr.Add(self.btn_fcclr, 2, wx.EXPAND, 0)
        szr_val.Add(szr_fcclr, 1, wx.EXPAND, 0)
        szr_ecclr = wx.BoxSizer(wx.HORIZONTAL)
        szr_ecclr.Add(self.stx_ecclr, 3, wx.ALIGN_BOTTOM, 0)
        szr_ecclr.Add(self.btn_ecclr, 2, wx.EXPAND, 0)
        szr_val.Add(szr_ecclr, 1, wx.EXPAND, 0)
        szr_alpha = wx.BoxSizer(wx.HORIZONTAL)
        szr_alpha.Add(self.stx_alpha, 3, wx.ALIGN_BOTTOM, 0)
        szr_alpha.Add(self.spn_alpha, 2, wx.EXPAND, 0)
        szr_val.Add(szr_alpha, 1, wx.EXPAND, 0)

        szr_data = wx.BoxSizer(wx.VERTICAL)
        szr_data.Add(szr_top, 0, wx.EXPAND, 0)
        szr_data.Add(szr_val, 0, wx.EXPAND, 0)

        szr_main.Add(szr_data, 0, 0, 0)
        szr_btn.AddStretchSpacer(prop=4)
        szr_btn.Add(self.btn_ok, 0, wx.EXPAND, 0)
        szr_btn.Add(self.btn_cancel, 0, wx.EXPAND, 0)
        szr_main.Add(szr_btn, 1, wx.EXPAND, 0)

        self.SetSizer(szr_main)
        szr_main.Fit(self)
        self.Layout()

    def on_btn_color(self, event):
        clr = event.GetValue().GetAsString(flags=wx.C2S_HTML_SYNTAX)

    def get_title(self):
        return self.txt_title.GetValue()

    def set_title(self, t):
        self.txt_title.SetValue(t)

    title = property(get_title, set_title)

    def get_face_color(self):
        clr = self.btn_fcclr.GetValue().GetAsString(flags=wx.C2S_HTML_SYNTAX)
        return clr

    def set_face_color(self, c):
        self.btn_fcclr.SetValue(c)

    face_color = property(get_face_color, set_face_color)

    def get_edge_color(self):
        clr = self.btn_ecclr.GetValue().GetAsString(flags=wx.C2S_HTML_SYNTAX)
        return clr

    def set_edge_color(self, c):
        self.btn_ecclr.SetValue(c)

    edge_color = property(get_edge_color, set_edge_color)

    def get_alpha_value(self):
        val = self.spn_alpha.GetValue()
        return val

    def set_alpha_value(self, v):
        self.spn_alpha.SetValue(v)

    alpha_value = property(get_alpha_value, set_alpha_value)

    def set_callback(self, cb):
        if not isinstance(cb, collections.abc.Callable): raise TypeError('Callable expected')
        self.cb = cb

    def on_btn_ok(self, event):
        if self.cb:
            self.cb(self)
        else:
            event.Skip()

    def on_btn_cancel(self, event):
        self.Destroy()


if __name__ == '__main__':
    def cb(dlg):
        print((dlg.title))
        print((dlg.values))

    app = wx.App(False)
    dlg = DADlgParamGeoJSON(None, -1, '')
    dlg.set_callback(cb)
    app.SetTopWindow(dlg)
    dlg.Show()
    app.MainLoop()
