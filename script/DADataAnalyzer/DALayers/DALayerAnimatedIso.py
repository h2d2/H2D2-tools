# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2022
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import threading
from matplotlib.animation import FuncAnimation
import wx

from . import DALayer
from . import DADlgParamIso
from . import DAAnimator
from IPImageProcessor import IPRaster
from IPImageProcessor.IPColorBar import IPDraggableColorBarWithFrame

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Layer.AnimatedIso")

class DALayerAnimatedIso(DALayer.DALayer):
    count = 0

    def __init__(self, page, field, times, dops, dfls, names):
        super().__init__()
        self.pane  = page
        self.field = field
        self.times = times
        self.dops  = dops
        self.dfls  = dfls
        self.names = names

        self.anim   = None
        self.thread = None
        self.thread_i = -1

        self.cache = {}
        self.isModified = False

        self.fig   = self.pane.get_figure()
        self.ax    = self.pane.get_axes()
        self.nav   = None
        self.inav  = -1

        DALayerAnimatedIso.count += 1
        self.field.getDataAtStep(tsteps=times[0], dops=self.dops)
        self.title = 'Animated Iso #%03i: %s @ %s' % (DALayerAnimatedIso.count, names[0], str(field))
        self.vmin, self.vmax, self.nval, self.mask = (self.field.getDataMin(), self.field.getDataMax(), 21, True)
        self.cmap, self.cmin, self.cmax = ('jet', 0.0, 1.0)
        self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar = (False, False, False, False, True)

        self.min0,  self.max0,  self.nvl0  = self.vmin, self.vmax, self.nval
        self.cmap0, self.cmin0, self.cmax0 = self.cmap, self.cmin, self.cmax

    def __genIpFig(self, data):
        kwargs = { 'mask' : self.mask }
        kwargs.update( IPRaster.IPAxes.genKwLimits(self.vmin, self.vmax, self.nval, self.log_scale) )
        kwargs.update( IPRaster.IPAxes.genKwCmap  (self.cmap, self.cmin, self.cmax) )

        items = []
        if self.iso_line:
            cs = self.ax.drawContourLine(self.field.getGrid(), data, **kwargs)
            items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.CONTOUR_LINE, 'Isoline') )
        else:
            kwargs.update( IPRaster.IPAxes.genKwExtend(self.open_top, self.open_btm) )
            cs = self.ax.drawContourFill(self.field.getGrid(), data, **kwargs)
            items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.CONTOUR_FILL, 'Isovalue') )
        if not self.hide_bar:
            cb = IPDraggableColorBarWithFrame(cs, self.ax,
                                              fraction=0.05,
                                              ticks=kwargs['levels'],
                                              format='%.3e',
                                              label=self.title,
                                              keyAction=False)
            items.append( DALayer.DALayerItemCB(cb, 'Colorbar') )
        return items

    def __getStep(self, i):
        LOGGER.trace('DALayerAnimatedIso.__getStep(%d)' % i)
        t = self.times[i]
        if t not in self.cache:
            data  = self.field.getDataAtStep(tsteps=(i,), dops=self.dops)
            items = self.__genIpFig(data)
            for item in items:
                item.detach()
            self.cache[t] = items[0]
        return self.cache[t]

    def __getStepForAnimation(self, i):
        LOGGER.trace('DALayerAnimatedIso.__getStepForAnimation(%d)' % i)
        item = self.__getStep(i)
        self.remove(self.items[0])
        self.insert(0, item)
        self.isModified = True
        return item.cs.collections      # animation requires the artist sequence

    def __threadFigAtStep(self):
        assert self.thread.is_alive()
        _ = self.__getStep(self.thread_i)
        self.thread = None

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
            self.cache = {}
        data = self.field.getDataActual()
        self.items = self.__genIpFig(data)
        self.inav = 0
        time = self.times[0]                    # first time
        self.cache[time] = self.items[0]        # cache iso cs
        self.pane.add_layer(self)
        self.pane.redraw()

    def draw(self):
        busyOnEntry = wx.IsBusy()
        if busyOnEntry: wx.EndBusyCursor()

        dlg = DADlgParamIso.DADlgParamIso(self.pane)
        dlg.set_title (self.title)
        dlg.set_values(self.vmin, self.vmax, self.nval, self.cmap,  self.cmin,  self.cmax,
                       self.min0, self.max0, self.nvl0, self.cmap0, self.cmin0, self.cmax0)
        dlg.set_checks(self.iso_line, self.log_scale, self.open_top, self.open_btm, self.mask, self.hide_bar)

        dlgOK = (dlg.ShowModal() == wx.ID_OK)
        if dlgOK:
            self.title = dlg.title
            self.vmin, self.vmax, self.nval, self.cmap, self.cmin, self.cmax = dlg.values
            self.iso_line, self.log_scale, self.open_top, self.open_btm, self.mask, self.hide_bar = dlg.options
        dlg.Destroy()
        if busyOnEntry: wx.BeginBusyCursor()

        errMsg = ''
        if dlgOK:
            try:
                self.__xeqDraw()
                self.activate(True)
            except Exception as e:
                import traceback
                errMsg = '\n'.join( (str(e), traceback.format_exc()) )

        if errMsg:
            dlg = wx.MessageDialog(self.pane, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()

    def onAnimatorChange(self, d: int, i: int, t: float):
        """
        onAnimatorChange is a callback for the DAAnimator

        Args:
            d (int): increment (delta)
            i (int): index
            t (float): time
        """
        # ---  Wait for task to finish
        if self.thread and self.thread_i == i:
            self.thread.join()

        # ---  Load and redraw
        item = self.__getStep(i)
        self.remove(self.items[0])
        self.insert(0, item)
        self.isModified = True
        self.pane.setState(modified=True)
        self.pane.redraw()

        # ---  Pre-emptive load
        if i+d >= 0 and i+d < len(self.times):
            if self.thread:
                self.thread.join()
            self.thread_i = i + d
            self.thread = threading.Thread(target=self.__threadFigAtStep)
            self.thread.start()

    def __genAnim(self):
        kwargs = {}
        kwargs['frames']   = len(self.times)
        kwargs['interval'] = 200
        kwargs['repeat']   = True
        kwargs['blit']     = True
        self.anim = FuncAnimation(self.fig, self.__getStepForAnimation, **kwargs)

    def onAnimatorRun(self, *args):
        if self.anim:
            self.anim.resume()
        else:
            self.__genAnim()

    def onAnimatorStop(self, *args):
        self.anim.pause()

    def onAnimatorSave(self, *args):
        pass

    def onAnimatorClose(self):
        self.activate(False)

    def hasAnimator(self):
        return True

    def update(self):
        """
        For dynamic items, items with a processing step on draw.
        Return True if changes where made and a redraw if necessary.
        """
        isModified = self.isModified
        self.isModified = isModified
        return isModified

    def activate(self, active):
        if active:
            if not self.nav:
                self.nav = DAAnimator.DAAnimator(self.pane,
                                                 on_change=self.onAnimatorChange,
                                                 on_run=self.onAnimatorRun,
                                                 on_pause=self.onAnimatorStop,
                                                 on_close=self.onAnimatorClose,
                                                 on_save=self.onAnimatorSave)
                self.nav.setTimes(self.times)
                self.nav.setIndex(self.inav)
        else:
            if self.anim:
                self.onAnimatorStop()
            if self.nav:
                self.inav = self.nav.getIndex()
                nav = self.nav          # Protection against recursion:
                self.nav = None         # onBtnStop will call onAnimatorClose
                nav.onBtnStop(None)     # who will call activate.
        super().activate(active)

    def __str__(self):
        return self.title

if __name__ == "__main__":
    dummy = DALayerAnimatedIso(None, None, 1.0, None, [0, 1])
