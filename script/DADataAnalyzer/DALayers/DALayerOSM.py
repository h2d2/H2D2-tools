# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import itertools
import logging
import os
import threading
import traceback
import tilemapbase
import numpy as np

from . import DALayer
from CTCommon              import CTCache
from IPImageProcessor      import IPRaster
from IPImageProcessor.GDAL import GDLBasemap

EPSG_WGS84 = 4326
EPSG_OSM   = 3857

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Layer.OSM")

from PIL import Image as _Image
from PIL import ImageDraw as _ImageDraw
class PlotterWithStop(tilemapbase.Plotter):
    def __init__(self, *args, **kwargs):
        self.doStop = kwargs.pop('doStop', self.__defaultDoStop)
        super().__init__(*args, **kwargs)

    def __defaultDoStop(self):
        return False

    def as_one_image_coarse(self, allow_large = False):
        """Use these settings to assemble tiles into a single image.  Will always
        return an image which is a whole number of tiles width and height; the
        exact extent will be a sub-rectangle of the image.

        :param allow_large: If False (default) then don't use more than 128
          tiles.  A guard against spamming the tile server.tile_provider

        :return: A :class:`PIL.Image` instance.
        """
        zoom_on_entry = self._zoom
        try:
            self._zoom -= 1
            if not allow_large:
                self._check_download_size()
            size = 2 * self._tile_provider.tilesize
            smax = size-1
            xs = size * (self.xtilemax + 1 - self.xtilemin)
            ys = size * (self.ytilemax + 1 - self.ytilemin)
            out = _Image.new("RGBA", (xs, ys))
            xr  = range(self.xtilemin, self.xtilemax + 1)
            yr  = range(self.ytilemin, self.ytilemax + 1)
            for x, y in itertools.product(xr, yr):
                if self.doStop():
                    break
                tile = self._tile_provider.get_tile(x, y, self.zoom)
                tile = tile.resize((size, size), _Image.BILINEAR)
                #t2 = _ImageDraw.Draw(tile)
                #t2.line((   0,    0,    0,smax)) # , fill='black', width=1)
                #t2.line((   0, smax, smax,smax)) # , fill='black', width=1)
                #t2.line((smax, smax, smax,   0)) # , fill='black', width=1)
                #t2.line((smax,    0,    0,   0)) # , fill='black', width=1)
                #t2.text((100, 100), 'x=%d' % x)
                #t2.text((100, 120), 'y=%d' % y)
                #t2.text((100, 140), 'z=%d' % self.zoom)
                xo = (x - self.xtilemin) * size
                yo = (y - self.ytilemin) * size
                out.paste(tile, (xo, yo))
        finally:
            self._zoom = zoom_on_entry
        return out

    def as_one_image(self, allow_large = False):
        """Use these settings to assemble tiles into a single image.  Will always
        return an image which is a whole number of tiles width and height; the
        exact extent will be a sub-rectangle of the image.

        :param allow_large: If False (default) then don't use more than 128
          tiles.  A guard against spamming the tile server.tile_provider

        :return: A :class:`PIL.Image` instance.
        """
        if not allow_large:
            self._check_download_size()
        size = self._tile_provider.tilesize
        xs = size * (self.xtilemax + 1 - self.xtilemin)
        ys = size * (self.ytilemax + 1 - self.ytilemin)
        out = _Image.new("RGBA", (xs, ys))
        xr  = range(self.xtilemin, self.xtilemax + 1)
        yr  = range(self.ytilemin, self.ytilemax + 1)
        for x, y in itertools.product(xr, yr):
            if self.doStop():
                break
            tile = self._tile_provider.get_tile(x, y, self.zoom)
            #t2 = _ImageDraw.Draw(tile)
            #t2.line((  0,   0,   0,255)) # , fill='black', width=1)
            #t2.line((  0, 255, 255,255)) # , fill='black', width=1)
            #t2.line((255, 255, 255,  0)) # , fill='black', width=1)
            #t2.line((255,   0,   0,  0)) # , fill='black', width=1)
            #t2.text((100, 100), 'x=%d' % x)
            #t2.text((100, 120), 'y=%d' % y)
            #t2.text((100, 140), 'z=%d' % self.zoom)
            xo = (x - self.xtilemin) * size
            yo = (y - self.ytilemin) * size
            out.paste(tile, (xo, yo))
        return out


class DALayerOSM(DALayer.DALayer):
    count = 0

    def __init__(self, page, srs_proj=None, title=''):
        super().__init__()

        self.pane   = page
        self.bbprj  = None
        self.img    = None   # image
        self.tiles  = None   # tilemapbase.tiles
        self.tbbosm = ()     # tiles bbox in osm coord
        self.tbbprj = ()     # tiles bbox in proj coord
        self.tzoom  = -1     # tiles zoom level
        self.proj2osm = None
        self.osm2proj = None
        self.thread   = None
        self.threadStop = False

        self.ax   = self.pane.get_axes()

        DALayerOSM.count += 1
        self.title = 'OpenStreetMap #%03i' % DALayerOSM.count
        if title: self.title += ' @ %s' % title

        srs_osm = GDLBasemap.GDLSpatialReference()
        err = srs_osm.ImportFromEPSG(EPSG_OSM)
        if err != 0:
            raise RuntimeError('Projection "EPSG:%d" could not be loaded' % EPSG_OSM)
        self.proj2osm = GDLBasemap.GDLCoordinateTransformation(srs_proj, srs_osm)
        self.osm2proj = GDLBasemap.GDLCoordinateTransformation(srs_osm, srs_proj)

        osmCachePath = CTCache.getCachePath(subPath='osm')
        osmCacheFile = os.path.join(osmCachePath, 'tilemapbase_cache.db')
        tilemapbase.start_logging()
        tilemapbase.init(cache_filename=osmCacheFile, create=True)
        self.tiles = tilemapbase.tiles.build_OSM()

    def __genImg(self):
        size = self.pane.GetSize()

        bbprj = self.pane.getBBox()                                         # ll-ur in proj coord
        xyosm = self.proj2osm.TransformPoints(bbprj[0::2], bbprj[1::2])     # x-y   in osm coord
        bbosm = np.array(xyosm).transpose()                                 # ll-ur in osm coord

        e = tilemapbase.Extent.from_3857(bbosm[0][0], bbosm[1][0], bbosm[1][1], bbosm[0][1])
        #p = tilemapbase.Plotter(e, self.tiles, width=size[0], height=size[1])
        p = PlotterWithStop(e, self.tiles, width=size[0], height=size[1], doStop=lambda: self.threadStop)

        img = p.as_one_image()
        if not self.threadStop:
            self.img    = img
            tbb = self.__getTileBBox(p)                     # ul-lr in osm coord
            self.tbbosm = tbb[0], tbb[3], tbb[2], tbb[1]    # ll-ur in osm coord
            self.tbbprj = self.osm2proj.TransformPoint(tbb[0], tbb[3])[0:2] + self.osm2proj.TransformPoint(tbb[2], tbb[1])[0:2]
            self.tzoom  = p.zoom
            self.bbprj  = bbprj

    def __getTileBBox(self, plotter):
        scale = 2 ** plotter.zoom
        xt0, yt0 = plotter.extent.project(plotter.xtilemin / scale, plotter.ytilemin / scale)
        xt1, yt1 = plotter.extent.project((plotter.xtilemax + 1) / scale, (plotter.ytilemax + 1) / scale)
        return (xt0, yt0, xt1, yt1)

    def __threadDraw(self):
        assert self.thread.is_alive()
        self.__genImg()
        if not self.threadStop:
            cs = self.ax.drawImage(self.img, window=self.tbbprj)
            item = DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.IMAGE, 'OSM')
            if self.items:
                self.pane.del_layer(self)
            self.items = [ item ]
            self.pane.add_layer(self)
            self.pane.redraw()
        self.thread = None

    def draw(self):
        if self.thread:
            self.threadStop = True
            self.thread.join()
        self.threadStop = False
        self.thread = threading.Thread(target=self.__threadDraw)
        self.thread.start()

    def update(self):
        """
        For dynamic items, items with a processing step on draw.
        Return True if changes where made and a redraw if necessary.
        """
        isModified = False
        if not self.isVisible() or not self.items[0].isVisible():
            return isModified
        # ---  Get actual tiles bbox
        size  = self.pane.GetSize()
        bbprj = self.pane.getBBox()                                         # ll-ur in proj coord
        xyosm = self.proj2osm.TransformPoints(bbprj[0::2], bbprj[1::2])     # x-y   in osm coord
        bbosm = np.array(xyosm).transpose()                                 # ll-ur in osm coord
        e = tilemapbase.Extent.from_3857(bbosm[0][0], bbosm[1][0], bbosm[1][1], bbosm[0][1])
        p = tilemapbase.Plotter(e, self.tiles, width=size[0], height=size[1])
        tbb = self.__getTileBBox(p)                     # ul-lr in osm coord
        tbbosm = tbb[0], tbb[3], tbb[2], tbb[1]         # ll-ur in osm coord
        # ---  Control
        doUpdate  = False
        doUpdate |= (p.zoom != self.tzoom)
        doUpdate |= (tbbosm[0] < self.tbbosm[0])
        doUpdate |= (tbbosm[1] < self.tbbosm[1])
        doUpdate |= (tbbosm[2] > self.tbbosm[2])
        doUpdate |= (tbbosm[3] > self.tbbosm[3])
        # --- XEQ
        try:
            if doUpdate:
                self.draw()
                isModified = True
            elif bbprj != self.bbprj:
                self.bbprj = bbprj
                isModified = True
        except Exception as e:
            errMsg = '%s\n%s' % (str(e), traceback.format_exc())
            LOGGER.error(errMsg)
        return isModified

    def __str__(self):
        return self.title

if __name__ == "__main__":
    dummy = DALayerOSM(None, 1.0, None)
