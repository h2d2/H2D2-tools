# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import wx

from . import DALayer
from . import DADlgParamSrf3D
from . import DANavigator
from IPImageProcessor import IPRaster
from IPImageProcessor.IPColorBar import IPDraggableColorBarWithFrame

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Layer.Srf3D")

class DALayerSrf3D(DALayer.DALayer):
    count = 0

    def __init__(self, page, field, names):
        super().__init__()
        self.pane  = page
        self.field = field
        self.names = names

        self.fig   = self.pane.get_figure()
        self.ax    = self.pane.get_axes()
        self.nav   = None

        DALayerSrf3D.count += 1
        self.title = '3D Surface #%03i: %s @ %s' % (DALayerSrf3D.count, names[0], str(field))
        self.vmin, self.vmax, self.nval = (self.field.getDataMin(), self.field.getDataMax(), 21)
        self.cmap, self.cmin, self.cmax = ('jet', 0.0, 1.0)
        self.log_scale, self.open_top, self.open_btm, self.hide_bar = (False, False, False, True)

        self.min0,  self.max0,  self.nvl0  = self.vmin, self.vmax, self.nval
        self.cmap0, self.cmin0, self.cmax0 = self.cmap, self.cmin, self.cmax

    def __genIpFig(self):
        kwargs = {}
        # kwargs.update( IPRaster.IPAxes.genKwLimits(self.vmin, self.vmax, self.nval, self.log_scale) )
        kwargs.update( IPRaster.IPAxes.genKwCmap  (self.cmap, self.cmin, self.cmax) )

        self.items = []
        cs = self.ax.drawSurface3D(self.field.getGrid(), self.field.getDataActual(), **kwargs)
        self.ax.set_zlim3d(self.vmin, self.vmax)
        self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.SURFACE_3D, 'Surface 3D') )
        if not self.hide_bar:
            cb = IPDraggableColorBarWithFrame(cs, self.ax,
                                              fraction=0.05,
                                              ticks=kwargs['levels'],
                                              format='%.3e',
                                              label=self.title,
                                              keyAction=False)
            self.items.append( DALayer.DALayerItemCB(cb, 'Colorbar') )
        self.pane.add_layer(self)

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.__genIpFig()
        self.pane.redraw()

    def draw(self):
        busyOnEntry = wx.IsBusy()
        if busyOnEntry: wx.EndBusyCursor()

        dlg = DADlgParamSrf3D.DADlgParamSrf3D(self.pane)
        dlg.set_title (self.title)
        dlg.set_values(self.vmin, self.vmax, self.nval, self.cmap,  self.cmin,  self.cmax,
                       self.min0, self.max0, self.nvl0, self.cmap0, self.cmin0, self.cmax0)
        dlg.set_checks(self.log_scale, self.open_top, self.open_btm, self.hide_bar)

        dlgOK = (dlg.ShowModal() == wx.ID_OK)
        if dlgOK:
            self.title = dlg.title
            self.vmin, self.vmax, self.nval, self.cmap, self.cmin, self.cmax = dlg.values
            self.log_scale, self.open_top, self.open_btm, self.hide_bar = dlg.options
        dlg.Destroy()
        if busyOnEntry: wx.BeginBusyCursor()

        errMsg = ''
        if dlgOK:
            try:
                self.__xeqDraw()
            except Exception as e:
                import traceback
                errMsg = '\n'.join( ('Drawing the skin of a skin/L2 mesh?', str(e), traceback.format_exc()) )

        if errMsg:
            dlg = wx.MessageDialog(self.pane, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()

    def onNavigatorChange(self, x, y, v):
        self.pane.doCenter(x, y)

    def onNavigatorClose(self):
        self.activate(False)

    def hasNavigator(self):
        return True

    def activate(self, active):
        if active:
            if not self.nav:
                self.nav = DANavigator.DANavigator(self.pane, on_change=self.onNavigatorChange, on_close=self.onNavigatorClose)
                data = self.field.getDataActual()
                grid = self.field.getGrid()
                self.nav.setData(grid, data)
        else:
            self.nav = None


    def __str__(self):
        return self.title

if __name__ == "__main__":
    dummy = DALayerSrf3D(None, 1.0, None, [0, 1])
