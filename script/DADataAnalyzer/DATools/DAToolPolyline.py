#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2021
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath( os.path.join(selfDir, '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)
    supPath = os.path.normpath( os.path.join(supPath, '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import enum
import logging
import math
from pubsub import pub
import numpy as np
import wx
from matplotlib.backend_bases import MouseButton

from DATools.DADlgPosition import DADlgPosition

LOGGER = logging.getLogger('H2D2.Tools.DataAnalyzer.Tools.ToolPolyline')

class DAToolPolyline:
    STATUS = enum.Flag('STATUS', names='none append move edit')

    def __init__(self, window, axes, cbOnChange=None):
        self.parent = window
        self.cbOnChange = cbOnChange if cbOnChange else self.__dummyCbOnChange

        self.canvas = axes.figure.canvas
        self.cidMoD = self.canvas.mpl_connect('button_press_event',   self.onMouseDown)
        self.cidMoU = self.canvas.mpl_connect('button_release_event', self.onMouseUp)
        self.cidMoM = self.canvas.mpl_connect('motion_notify_event',  self.onMouseMove)
        self.cidKyD = self.canvas.mpl_connect('key_press_event',      self.onKeyPress)
        self.cidKyR = self.canvas.mpl_connect('key_release_event',    self.onKeyRelease)
        self.cidPck = self.canvas.mpl_connect('pick_event',           self.onPicking)

        picks = axes.plot([], [], linestyle=' ' , marker=' ', color='red', picker=True, pickradius=5)
        lines = axes.plot([], [], linestyle='-' , marker='o', color='red', picker=True, pickradius=5) # , markersize=3)
        slcts = axes.plot([], [], linestyle='-' , marker='o', color='red') # , markersize=3)
        assert len(lines) == 1
        self.polyPck = picks[0]
        self.polyDsp = lines[0]
        self.polySlc = slcts[0]

        self.mnuSave = wx.Menu()
        self.mnu_len = wx.MenuItem(self.mnuSave, wx.ID_ANY, 'Pick length',  'Copy length to clipboard', wx.ITEM_NORMAL)
        self.mnu_tpl = wx.MenuItem(self.mnuSave, wx.ID_ANY, 'Pick (x,y)',   'Copy x,y location to clipboard as tuple', wx.ITEM_NORMAL)
        self.mnu_lst = wx.MenuItem(self.mnuSave, wx.ID_ANY, 'Pick [(x,y)]', 'Copy x,y location to clipboard as list of tuple', wx.ITEM_NORMAL)
        self.mnu_wkt = wx.MenuItem(self.mnuSave, wx.ID_ANY, 'Pick as WKT',  'Copy x, y location to clipboard as WKT', wx.ITEM_NORMAL)
        #self.mnu_jsn = wx.MenuItem(self.mnuSave, wx.ID_ANY, 'Pick as JSON', 'Copy x, y location to clipboard as GeoJSON', wx.ITEM_NORMAL)
        self.mnuSave.Append(self.mnu_len)
        self.mnuSave.Append(self.mnu_tpl)
        self.mnuSave.Append(self.mnu_lst)
        self.mnuSave.Append(self.mnu_wkt)

        self.mnuLine = wx.Menu()
        self.mnuLineSplit = wx.MenuItem(self.mnuLine, wx.ID_ANY, 'Split',   'Split the line', wx.ITEM_NORMAL)
        self.mnuLine.Append(self.mnuLineSplit)

        self.mnuPoint = wx.Menu()
        self.mnuPointMid = wx.MenuItem(self.mnuPoint, wx.ID_ANY, 'As mid point',    'Set the point as the middle point', wx.ITEM_NORMAL)
        self.mnuPointVal = wx.MenuItem(self.mnuPoint, wx.ID_ANY, 'Position...',     'Set the coordinates', wx.ITEM_NORMAL)
        self.mnuPointDel = wx.MenuItem(self.mnuPoint, wx.ID_ANY, 'Delete',          'Delete the point', wx.ITEM_NORMAL)
        self.mnuPointXtd = wx.MenuItem(self.mnuPoint, wx.ID_ANY, 'Extend polyline', 'Extend the polyline', wx.ITEM_NORMAL)
        self.mnuPoint.Append(self.mnuPointMid)
        self.mnuPoint.Append(self.mnuPointVal)
        self.mnuPoint.AppendSeparator()
        self.mnuPoint.Append(self.mnuPointDel)
        self.mnuPoint.AppendSeparator()
        self.mnuPoint.Append(self.mnuPointXtd)

        self.parent.Bind(wx.EVT_MENU, self.onMenuLen, self.mnu_len)
        self.parent.Bind(wx.EVT_MENU, self.onMenuTpl, self.mnu_tpl)
        self.parent.Bind(wx.EVT_MENU, self.onMenuLst, self.mnu_lst)
        self.parent.Bind(wx.EVT_MENU, self.onMenuWKT, self.mnu_wkt)
        #self.parent.Bind(wx.EVT_MENU, self.onMenuJSN, self.mnu_jsn)

        self.parent.Bind(wx.EVT_MENU, self.onMenuLineSplit, self.mnuLineSplit)
        self.parent.Bind(wx.EVT_MENU, self.onMenuPointMid, self.mnuPointMid)
        self.parent.Bind(wx.EVT_MENU, self.onMenuPointVal, self.mnuPointVal)
        self.parent.Bind(wx.EVT_MENU, self.onMenuPointDel, self.mnuPointDel)
        self.parent.Bind(wx.EVT_MENU, self.onMenuPointXtd, self.mnuPointXtd)

        self.xd = []        # Vertex position, data
        self.yd = []
        self.xm = []        # Vertex position, mouse i.e. screen
        self.ym = []
        self.xs = []        # Selected points, data
        self.ys = []
        self.reversed = False
        self.stts = DAToolPolyline.STATUS.append
        self.pkPntId = -1
        self.pkLinId = -1

    @staticmethod
    def delete(self):
        self.xd = []
        self.yd = []
        self.__drawPoly()
        self.xs = []
        self.ys = []
        self.__drawSlct()
        try:
            self.polyDsp.remove()
            self.polyPck.remove()
            self.polySlc.remove()
        except ValueError:
            pass
        try:
            self.canvas.draw_idle()
            self.canvas.mpl_disconnect(self.cidMoD)
            self.canvas.mpl_disconnect(self.cidMoU)
            self.canvas.mpl_disconnect(self.cidMoM)
            self.canvas.mpl_disconnect(self.cidKyD)
            self.canvas.mpl_disconnect(self.cidKyR)
            self.canvas.mpl_disconnect(self.cidPck)
            self.parent.Unbind(wx.EVT_CONTEXT_MENU)
        except RuntimeError as e:
            LOGGER.warn(str(e))
        pub.sendMessage('statusbar.setmessage', message='')

    def __dummyCbOnChange(self, poly):
        pass

    def __copyToClipboard(self, txt):
        clipdata = wx.TextDataObject(txt)
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()

    def __inAppend(self):
        return self.stts == DAToolPolyline.STATUS.append

    def __inMove(self):
        return self.stts == DAToolPolyline.STATUS.move

    def __inEdit(self):
        return self.stts == DAToolPolyline.STATUS.edit

    def __moveSelectedPoint(self, xd, yd):
        # ---  Point in pixels
        transformed_path = self.polyDsp._get_transformed_path()
        affine = transformed_path.get_affine()
        xm, ym = affine.transform((xd, yd))
        # ---  Move
        self.xd[self.pkPntId] = xd
        self.yd[self.pkPntId] = yd
        self.xm[self.pkPntId] = xm
        self.ym[self.pkPntId] = ym
        self.__drawPoly()

    def __drawPoly(self):
        self.polyDsp.set_xdata(self.xd)
        self.polyDsp.set_ydata(self.yd)
        self.polyDsp.set_color('red' if self.__inAppend() else 'blue')
        self.polyPck.set_xdata(self.xd)
        self.polyPck.set_ydata(self.yd)
        self.canvas.draw_idle()
        #self.canvas.draw()

    def __drawSlct(self):
        self.polySlc.set_xdata(self.xs)
        self.polySlc.set_ydata(self.ys)
        self.canvas.draw_idle()
        #self.canvas.draw()

    def __reverse(self):
        self.xd.reverse()
        self.yd.reverse()
        self.xm.reverse()
        self.ym.reverse()
        self.xs.reverse()
        self.ys.reverse()
        self.xs.reverse()
        self.ys.reverse()
        self.reversed = not self.reversed

    def getLength(self):
        xy = tuple( zip(self.xd, self.yd) )
        l = 0.0
        for (x1, y1), (x2, y2) in zip(xy[:-1], xy[1:]):
            l += math.hypot(x2-x1, y2-y1)
        return l

    def getPoints(self):
        """
        getPoints [summary]

        Return the polyline vertexes as list of (x, y) tuples

        Returns:
            list: list of tuple positions
        """
        return list( zip(self.xd, self.yd) )

    def setPoints(self, xy):
        # ---  Points in mouse coord (screen coord)
        transformed_path = self.polyDsp._get_transformed_path()
        affine = transformed_path.get_affine()
        xym = [ affine.transform((x, y)) for x, y in xy ]
        # ---  Set values
        self.stts = DAToolPolyline.STATUS.edit if xy else DAToolPolyline.STATUS.append
        self.xd = [ _xy[0] for _xy in xy ]
        self.yd = [ _xy[1] for _xy in xy ]
        self.xm = [ _xy[0] for _xy in xym ]
        self.ym = [ _xy[1] for _xy in xym ]
        self.xs = []
        self.ys = []
        self.__drawPoly()
        self.__drawSlct()

    def showPopupMenu(self, mnu):
        self.parent.PopupMenu(mnu)

    def onMenuLen(self, event):
        if len(self.xd) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of points')
            return
        fmt = '{l:f}'
        txt = fmt.format(l=self.getLength())
        self.__copyToClipboard('%s' % txt)

    def onMenuTpl(self, event):
        if len(self.xd) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of points')
            return
        fmt = '({x:f}, {y:f})'
        lst = [ fmt.format(x=x, y=y) for x, y in zip(self.xd, self.yd) ]
        txt = ', '.join(lst)
        self.__copyToClipboard('%s' % txt)

    def onMenuLst(self, event):
        if len(self.xd) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of points')
            return
        fmt = '({x:f}, {y:f})'
        lst = [ fmt.format(x=x, y=y) for x, y in zip(self.xd, self.yd) ]
        txt = ', '.join(lst)
        self.__copyToClipboard('[%s]' % txt)

    def onMenuWKT(self, event):
        if len(self.xd) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of points')
            return
        fmt = '{x:f} {y:f}'
        if len(self.xd) == 0:
            self.__copyToClipboard('LINESTRING EMPTY')
        elif len(self.xd) == 1:
            txt = fmt.format(x=self.xd[0], y=self.yd[0])
            self.__copyToClipboard('POINT(%s)' % txt)
        else:
            lst = [ fmt.format(x=x, y=y) for x, y in zip(self.xd, self.yd) ]
            txt = ', '.join(lst)
            self.__copyToClipboard('LINESTRING(%s)' % txt)

    def onMenuLineSplit(self, event):
        LOGGER.trace('DAToolPolyline.onMenuLineSplit: %s', event)
        assert self.pkLinId != -1
        # ---  Mid point in data
        xdm = 0.5*(self.xd[self.pkLinId]+self.xd[self.pkLinId+1])
        ydm = 0.5*(self.yd[self.pkLinId]+self.yd[self.pkLinId+1])
        # ---  Mid point in pixels
        transformed_path = self.polyDsp._get_transformed_path()
        affine = transformed_path.get_affine()
        xmm, ymm = affine.transform((xdm, ydm))
        # ---  Insert new point
        self.xd.insert(self.pkLinId+1, xdm)
        self.yd.insert(self.pkLinId+1, ydm)
        self.xm.insert(self.pkLinId+1, xmm)
        self.ym.insert(self.pkLinId+1, ymm)
        self.__drawPoly()
        # ---  Un-select
        self.xs = []
        self.ys = []
        self.__drawSlct()
        self.pkPntId = -1
        self.pkLinId = -1
        self.cbOnChange(self)

    def onMenuPointMid(self, event):
        LOGGER.trace('DAToolPolyline.onMenuPointMid: %s', event)
        assert self.pkPntId != -1
        # ---  Mid point in data
        xdm = 0.5*(self.xd[self.pkPntId-1]+self.xd[self.pkPntId+1])
        ydm = 0.5*(self.yd[self.pkPntId-1]+self.yd[self.pkPntId+1])
        self.__moveSelectedPoint(xdm, ydm)
        # ---  Un-select
        self.xs = []
        self.ys = []
        self.__drawSlct()
        self.pkPntId = -1
        self.pkLinId = -1
        self.cbOnChange(self)

    def onMenuPointVal(self, event):
        LOGGER.trace('DAToolPolyline.onMenuPointVal: %s', event)
        assert self.pkPntId != -1
        dlg = DADlgPosition(None, wx.ID_ANY, "", cbOnApply=self.__moveSelectedPoint)
        dlg.setPosition(self.xd[self.pkPntId], self.yd[self.pkPntId])
        if dlg.ShowModal() == wx.ID_OK:
            xd, yd = dlg.getPosition()
            dlg.Destroy()
            self.__moveSelectedPoint(xd, yd)
            self.cbOnChange(self)

    def onMenuPointDel(self, event):
        LOGGER.trace('DAToolPolyline.onMenuPointDel: %s', event)
        assert self.pkPntId != -1
        # ---  Remove selected point
        del self.xd[self.pkPntId]
        del self.yd[self.pkPntId]
        del self.xm[self.pkPntId]
        del self.ym[self.pkPntId]
        self.__drawPoly()
        # ---  Un-select
        self.xs = []
        self.ys = []
        self.__drawSlct()
        self.pkPntId = -1
        self.pkLinId = -1
        self.cbOnChange(self)

    def onMenuPointXtd(self, event):
        LOGGER.trace('DAToolPolyline.onMenuPointXtd: %s', self.pkPntId)
        assert self.stts is DAToolPolyline.STATUS.edit
        assert self.pkPntId == 0 or self.pkPntId == len(self.xd)-1
        assert self.pkLinId == -1
        # ---  Reverse order if first selected, to enable append
        if self.pkPntId == 0:
            self.__reverse()
        # ---  Unselect point/line
        self.pkPntId = -1
        self.pkLinId = -1
        self.xs = []
        self.ys = []
        self.__drawSlct()
        # ---  Duplicate last point
        self.xd.append(self.xd[-1])
        self.yd.append(self.yd[-1])
        self.xm.append(self.xm[-1])
        self.ym.append(self.ym[-1])
        self.__drawPoly()
        # ---  Set new status
        self.stts = DAToolPolyline.STATUS.append

    def onKeyPress(self, event):
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        pass

    def onKeyRelease(self, event):
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        LOGGER.trace('onKeyRelease: %s', event.key)
        if event.key == 'escape':
            if self.__inAppend():
                # ---  Remove last point
                del self.xd[-1]
                del self.yd[-1]
                del self.xm[-1]
                del self.ym[-1]
                # ---  Step out of append mode
                self.stts = DAToolPolyline.STATUS.none
                self.__drawPoly()
            elif self.__inMove():
                # ---  Un-select
                self.xs = []
                self.ys = []
                self.pkPntId = -1
                self.pkLinId = -1
                # ---  Step out of move mode
                self.stts = DAToolPolyline.STATUS.none
                self.__drawSlct()
            elif self.__inEdit():
                # ---  Un-select
                self.xs = []
                self.ys = []
                self.pkPntId = -1
                self.pkLinId = -1
                # ---  Step out of edit mode
                self.stts = DAToolPolyline.STATUS.none
                self.__drawSlct()
        elif event.key == 'delete':
            if self.pkPntId != -1:
                # ---  Remove selected point
                del self.xd[self.pkPntId]
                del self.yd[self.pkPntId]
                del self.xm[self.pkPntId]
                del self.ym[self.pkPntId]
                self.__drawPoly()
                # ---  Un-select
                self.xs = []
                self.ys = []
                self.__drawSlct()
                self.pkPntId = -1
                self.pkLinId = -1

    def onMouseDownLeft(self, event):
        LOGGER.trace('DAToolPolyline.onMouseDownLeft: (%d, %d)', event.x, event.y)
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.button != MouseButton.LEFT: return    # Move only on left button
        if self.__inAppend(): return
        # ---  Point selected and not yet displayed
        if self.pkPntId != -1 and not self.xs:
            assert not self.xs and not self.ys
            assert self.pkLinId == -1
            id = self.pkPntId
            if id > 0:
                self.xs.append(self.xd[id-1])
                self.ys.append(self.yd[id-1])
            self.xs.append(self.xd[id])
            self.ys.append(self.yd[id])
            if id+1 < len(self.xd):
                self.xs.append(self.xd[id+1])
                self.ys.append(self.yd[id+1])
            self.__drawSlct()
            self.stts = DAToolPolyline.STATUS.move
        # ---  Line selected and not yet displayed
        elif self.pkLinId != -1 and not self.xs:
            assert not self.xs and not self.ys
            assert self.pkPntId == -1
            id = self.pkLinId
            self.xs.append(self.xd[id])
            self.ys.append(self.yd[id])
            self.xs.append(self.xd[id+1])
            self.ys.append(self.yd[id+1])
            self.__drawSlct()
            self.stts = DAToolPolyline.STATUS.edit
        # ---  Un-select
        else:
            self.xs = []
            self.ys = []
            self.pkPntId = -1
            self.pkLinId = -1
            self.__drawSlct()
            self.stts = DAToolPolyline.STATUS.none

    def onMouseDownRight(self, event):
        LOGGER.trace('DAToolPolyline.onMouseDownRight: (%d, %d)', event.x, event.y)
        # ---  Point selected and not yet displayed
        if self.pkPntId != -1 and not self.xs:
            assert not self.xs and not self.ys
            assert self.pkLinId == -1
            self.xs = [ self.xd[self.pkPntId] ]
            self.ys = [ self.yd[self.pkPntId] ]
            self.__drawSlct()
            self.stts = DAToolPolyline.STATUS.edit
            # ---  Menu
            enableMid = not self.pkPntId in [0, len(self.xd)-1]
            self.mnuPointMid.Enable(enableMid)
            self.mnuPointXtd.Enable(not enableMid)
            wx.CallAfter(self.showPopupMenu, self.mnuPoint)
        # ---  Line selected and not yet displayed
        elif self.pkLinId != -1 and not self.xs:
            assert not self.xs and not self.ys
            assert self.pkPntId == -1
            id = self.pkLinId
            self.xs.append(self.xd[id])
            self.ys.append(self.yd[id])
            self.xs.append(self.xd[id+1])
            self.ys.append(self.yd[id+1])
            self.__drawSlct()
            self.stts = DAToolPolyline.STATUS.edit
            # ---  Menu
            wx.CallAfter(self.showPopupMenu, self.mnuLine)
        # ---  Un-select
        else:
            self.xs = []
            self.ys = []
            self.pkPntId = -1
            self.pkLinId = -1
            self.__drawSlct()
            self.stts = DAToolPolyline.STATUS.none
            # ---  Menu
            isOn = len(self.xd) > 0
            for m in self.mnuSave.GetMenuItems():
                m.Enable(isOn)
            wx.CallAfter(self.showPopupMenu, self.mnuSave)

    def onMouseDown(self, event):
        LOGGER.trace('DAToolPolyline.onMouseDown: (%d, %d)', event.x, event.y)
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.button == MouseButton.LEFT:
            self.onMouseDownLeft(event)
        elif event.button == MouseButton.RIGHT:
            self.onMouseDownRight(event)

    def onMouseUpAppend(self, event):
        """
        onMouseUpAppend [summary]

        Append a new point to the polyline. If the point is
        the same as the last point, don't append and exit.

        Args:
            event (mouse event): Mouse event

        Returns:
            [type]: [description]
        """
        LOGGER.trace('DAToolPolyline.onMouseUpAppend: ')
        if len(self.xd) > 1 and np.hypot(event.x-self.xm[-2], event.y-self.ym[-2]) < 5:
            LOGGER.debug('  step out: %s', self.xd)
            # ---  Remove last point
            del self.xd[-1]
            del self.yd[-1]
            del self.xm[-1]
            del self.ym[-1]
            # ---  Reset reversion
            if self.reversed:
                self.__reverse()
            # ---  Step out of edit mode
            self.stts = DAToolPolyline.STATUS.none
            LOGGER.debug('  done: %s', self.xd)
        else:
            LOGGER.debug('  append: %s', self.xd)
            # ---  Append, last point can move
            if len(self.xd) == 0:   # first point, twice
                self.xd.append(event.xdata)
                self.yd.append(event.ydata)
                self.xm.append(event.x)
                self.ym.append(event.y)
            self.xd.append(event.xdata)
            self.yd.append(event.ydata)
            self.xm.append(event.x)
            self.ym.append(event.y)
            LOGGER.debug('  done: %s', self.xd)
        self.__drawPoly()
        self.cbOnChange(self)

    def onMouseUpMove(self, event):
        """
        onMouseUpMove [summary]

        Move the selected point.

        Args:
            event (mouse event): Mouse event

        Returns:
            [type]: [description]
        """
        LOGGER.trace('DAToolPolyline.onMouseUpMove: pkPntId=%d', self.pkPntId)
        xlast = self.xm[self.pkPntId]
        ylast = self.ym[self.pkPntId]
        if np.hypot(event.x-xlast, event.y-ylast) < 5:
            self.xs = [ self.xd[self.pkPntId] ]
            self.ys = [ self.yd[self.pkPntId] ]
            self.__drawSlct()
        else:
            self.xd[self.pkPntId] = event.xdata
            self.yd[self.pkPntId] = event.ydata
            self.xm[self.pkPntId] = event.x
            self.ym[self.pkPntId] = event.y
            self.xs = [ event.xdata ]
            self.ys = [ event.ydata ]
            self.__drawPoly()
            self.__drawSlct()
            self.cbOnChange(self)
        self.stts = DAToolPolyline.STATUS.edit

    def onMouseUp(self, event):
        """
        onMouseUp [summary]

        In edit mode append a new point to the polyline. If the point is
        the same as the last point, don't append and exit.

        Args:
            event (mouse event): Mouse event

        Returns:
            [type]: [description]
        """
        LOGGER.trace('DAToolPolyline.onMouseUp: (%d, %d)', event.x, event.y)
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.button != MouseButton.LEFT: return
        if self.__inAppend():
            self.onMouseUpAppend(event)
            self.pkPntId = -1
            self.pkLinId = -1
        elif self.__inMove():
            self.onMouseUpMove(event)

    def onMouseMove(self, event):
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if len(self.xd) == 0: return
        # ---  Update last point with mouse position
        if self.__inAppend():
            self.xd[-1] = event.xdata
            self.yd[-1] = event.ydata
            self.xm[-1] = event.x
            self.ym[-1] = event.y
            self.__drawPoly()
        elif self.__inMove():
            xlast = self.xm[self.pkPntId]
            ylast = self.ym[self.pkPntId]
            if np.hypot(event.x-xlast, event.y-ylast) < 5:
                pass
            else:
                if self.pkPntId == 0:
                    self.xs[0] = event.xdata
                    self.ys[0] = event.ydata
                else:
                    self.xs[1] = event.xdata
                    self.ys[1] = event.ydata
                self.__drawSlct()
        pub.sendMessage('statusbar.setmessage', message='Length: %.3f' % self.getLength())

    def onPicking(self, event):
        LOGGER.trace('onPicking: ind=%s; len(self.xd)=%d', event.ind, len(self.xd))
        if event.canvas is not self.canvas: return
        if self.__inAppend(): return
        if len(event.ind) != 1:
            raise RuntimeError('Cowardly refusing to select more than 1 item')
        else:
            if event.artist is self.polyPck:
                LOGGER.debug(' picking point: ind=%s', event.ind)
                self.pkPntId = event.ind[0]
                self.pkLinId = -1
            elif event.artist is self.polyDsp:
                if self.pkPntId == -1:
                    LOGGER.debug(' picking line: ind=%s', event.ind)
                    self.pkLinId = event.ind[0]

    def activate(self, state = True):
        pass

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from CTCommon.addLogLevel import addLoggingLevel

    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = '%(asctime)s %(levelname)s %(message)s'
    logHndlr.setFormatter( logging.Formatter(FORMAT) )
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info('Start')

    def main(parent=None):
        fig, current_ax = plt.subplots()                 # make a new plotingrange
        N = 100000                                       # If N is large one can see
        x = np.linspace(0.0, 10.0, N)                    # improvement by use blitting!

        plt.plot(x, +np.sin(.2*np.pi*x), lw=3.5, c='b', alpha=.7)  # plot something
        plt.plot(x, +np.cos(.2*np.pi*x), lw=3.5, c='r', alpha=.5)
        plt.plot(x, -np.sin(.2*np.pi*x), lw=3.5, c='g', alpha=.3)

        r = DAToolPolyline(parent, current_ax)
        r.activate(True)
        plt.show()

    app = wx.App(0)
    frame = wx.Frame(None, -1, '')
    app.SetTopWindow(frame)
    frame.Show()
    main(frame)
    app.MainLoop()
