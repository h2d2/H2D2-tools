# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx.lib.newevent

ID_CNTR= 0xFF0000
def nextID():
    global ID_CNTR
    ID_CNTR += 1
    return ID_CNTR

UpdateDisplayListEvent, DAEVT_UPDATE_DISPLAY_LIST = wx.lib.newevent.NewCommandEvent()
DAEVT_UPDATE_DISPLAY_LIST_ID = nextID()

GridLoadEvent, DAEVT_GRID_LOAD = wx.lib.newevent.NewCommandEvent()
DAEVT_GRID_LOAD_ID = nextID()
BndrLoadEvent, DAEVT_BNDR_LOAD = wx.lib.newevent.NewCommandEvent()
DAEVT_BNDR_LOAD_ID = nextID()
DataLoadEvent, DAEVT_DATA_LOAD = wx.lib.newevent.NewCommandEvent()
DAEVT_DATA_LOAD_ID = nextID()
SRSPLoadEvent, DAEVT_SRSP_LOAD = wx.lib.newevent.NewCommandEvent()
DAEVT_SRSP_LOAD_ID = nextID()
