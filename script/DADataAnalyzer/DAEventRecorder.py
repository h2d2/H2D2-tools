# This class allows to determine the last time the user has worked with
# this application:
# https://wxpython.org/Phoenix/docs/html/wx.EventFilter.html

#https://wxpython.org/Phoenix/docs/html/wx.UIActionSimulator.html

import json
from os import stat_result

import wx

import DAEvents

# https://www.blog.pythonlibrary.org/2011/07/05/wxpython-get-the-event-name-instead-of-an-integer/
LST_EVENTS = {
    DAEvents.DAEVT_GRID_LOAD.typeId             : 'DAEVT_GRID_LOAD',
    DAEvents.DAEVT_BNDR_LOAD.typeId             : 'DAEVT_BNDR_LOAD',
    DAEvents.DAEVT_DATA_LOAD.typeId             : 'DAEVT_DATA_LOAD',
    DAEvents.DAEVT_SRSP_LOAD.typeId             : 'DAEVT_SRSP_LOAD',
    wx.EVT_POWER_SUSPENDING.typeId              : 'EVT_POWER_SUSPENDING',             #10000
    wx.EVT_POWER_SUSPENDED.typeId               : 'EVT_POWER_SUSPENDED',              #10001
    wx.EVT_POWER_SUSPEND_CANCEL.typeId          : 'EVT_POWER_SUSPEND_CANCEL',         #10002
    wx.EVT_POWER_RESUME.typeId                  : 'EVT_POWER_RESUME',                 #10003
    wx.EVT_END_PROCESS.typeId                   : 'EVT_END_PROCESS',                  #10004
    wx.EVT_TIMER.typeId                         : 'EVT_TIMER',                        #10005
    wx.EVT_FSWATCHER.typeId                     : 'EVT_FSWATCHER',                    #10006
    #wx.EVT_IDLE.typeId                          : 'EVT_IDLE',                         #10008
    wx.EVT_THREAD.typeId                        : 'EVT_THREAD',                       #10009
    wx.EVT_BUTTON.typeId                        : 'EVT_BUTTON',                       #10012
    wx.EVT_CHECKBOX.typeId                      : 'EVT_CHECKBOX',                     #10013
    wx.EVT_CHOICE.typeId                        : 'EVT_CHOICE',                       #10014
    wx.EVT_LISTBOX.typeId                       : 'EVT_LISTBOX',                      #10015
    wx.EVT_LISTBOX_DCLICK.typeId                : 'EVT_LISTBOX_DCLICK',               #10016
    wx.EVT_CHECKLISTBOX.typeId                  : 'EVT_CHECKLISTBOX',                 #10017
    wx.EVT_TOOL_RANGE.typeId                    : 'EVT_TOOL_RANGE',                   #10018
    wx.EVT_SLIDER.typeId                        : 'EVT_SLIDER',                       #10019
    wx.EVT_RADIOBOX.typeId                      : 'EVT_RADIOBOX',                     #10020
    wx.EVT_RADIOBUTTON.typeId                   : 'EVT_RADIOBUTTON',                  #10021
    wx.EVT_SCROLLBAR.typeId                     : 'EVT_SCROLLBAR',                    #10022
    wx.EVT_VLBOX.typeId                         : 'EVT_VLBOX',                        #10023
    wx.EVT_COMBOBOX.typeId                      : 'EVT_COMBOBOX',                     #10024
    wx.EVT_TOOL_RCLICKED_RANGE.typeId           : 'EVT_TOOL_RCLICKED_RANGE',          #10025
    #wx.EVT_TOOL_ENTER.typeId                    : 'EVT_TOOL_ENTER',                   #10026
    wx.EVT_TOOL_DROPDOWN.typeId                 : 'EVT_TOOL_DROPDOWN',                #10027
    wx.EVT_COMBOBOX_DROPDOWN.typeId             : 'EVT_COMBOBOX_DROPDOWN',            #10028
    wx.EVT_COMBOBOX_CLOSEUP.typeId              : 'EVT_COMBOBOX_CLOSEUP',             #10029
    #wx.EVT_MOUSE_EVENTS.typeId                  : 'EVT_MOUSE_EVENTS',                 #10030
    wx.EVT_LEFT_UP.typeId                       : 'EVT_LEFT_UP',                      #10031
    wx.EVT_MIDDLE_DOWN.typeId                   : 'EVT_MIDDLE_DOWN',                  #10032
    wx.EVT_MIDDLE_UP.typeId                     : 'EVT_MIDDLE_UP',                    #10033
    wx.EVT_RIGHT_DOWN.typeId                    : 'EVT_RIGHT_DOWN',                   #10034
    wx.EVT_RIGHT_UP.typeId                      : 'EVT_RIGHT_UP',                     #10035
    #wx.EVT_MOTION.typeId                        : 'EVT_MOTION',                       #10036
    #wx.EVT_ENTER_WINDOW.typeId                  : 'EVT_ENTER_WINDOW',                 #10037
    #wx.EVT_LEAVE_WINDOW.typeId                  : 'EVT_LEAVE_WINDOW',                 #10038
    wx.EVT_LEFT_DCLICK.typeId                   : 'EVT_LEFT_DCLICK',                  #10039
    wx.EVT_MIDDLE_DCLICK.typeId                 : 'EVT_MIDDLE_DCLICK',                #10040
    wx.EVT_RIGHT_DCLICK.typeId                  : 'EVT_RIGHT_DCLICK',                 #10041
    wx.EVT_SET_FOCUS.typeId                     : 'EVT_SET_FOCUS',                    #10042
    #wx.EVT_KILL_FOCUS.typeId                    : 'EVT_KILL_FOCUS',                   #10043
    #wx.EVT_CHILD_FOCUS.typeId                   : 'EVT_CHILD_FOCUS',                  #10044
    wx.EVT_MOUSEWHEEL.typeId                    : 'EVT_MOUSEWHEEL',                   #10045
    wx.EVT_MOUSE_AUX1_DOWN.typeId               : 'EVT_MOUSE_AUX1_DOWN',              #10046
    wx.EVT_MOUSE_AUX1_UP.typeId                 : 'EVT_MOUSE_AUX1_UP',                #10047
    wx.EVT_MOUSE_AUX1_DCLICK.typeId             : 'EVT_MOUSE_AUX1_DCLICK',            #10048
    wx.EVT_MOUSE_AUX2_DOWN.typeId               : 'EVT_MOUSE_AUX2_DOWN',              #10049
    wx.EVT_MOUSE_AUX2_UP.typeId                 : 'EVT_MOUSE_AUX2_UP',                #10050
    wx.EVT_MOUSE_AUX2_DCLICK.typeId             : 'EVT_MOUSE_AUX2_DCLICK',            #10051
    wx.EVT_MAGNIFY.typeId                       : 'EVT_MAGNIFY',                      #10052
    #wx.EVT_CHAR.typeId                          : 'EVT_CHAR',                         #10053
    #wx.EVT_CHAR_HOOK.typeId                     : 'EVT_CHAR_HOOK',                    #10055
    wx.EVT_NAVIGATION_KEY.typeId                : 'EVT_NAVIGATION_KEY',               #10056
    #wx.EVT_KEY_DOWN.typeId                      : 'EVT_KEY_DOWN',                     #10057
    #wx.EVT_KEY_UP.typeId                        : 'EVT_KEY_UP',                       #10058
    #wx.EVT_HOTKEY.typeId                        : 'EVT_HOTKEY',                       #10059
    #wx.EVT_SET_CURSOR.typeId                    : 'EVT_SET_CURSOR',                   #10060
    wx.EVT_SCROLL_TOP.typeId                    : 'EVT_SCROLL_TOP',                   #10061
    wx.EVT_SCROLL_BOTTOM.typeId                 : 'EVT_SCROLL_BOTTOM',                #10062
    wx.EVT_SPIN_UP.typeId                       : 'EVT_SPIN_UP',                      #10063
    wx.EVT_SPIN_DOWN.typeId                     : 'EVT_SPIN_DOWN',                    #10064
    wx.EVT_SCROLL_PAGEUP.typeId                 : 'EVT_SCROLL_PAGEUP',                #10065
    wx.EVT_SCROLL_PAGEDOWN.typeId               : 'EVT_SCROLL_PAGEDOWN',              #10066
    wx.EVT_SPIN.typeId                          : 'EVT_SPIN',                         #10067
    wx.EVT_SCROLL_THUMBRELEASE.typeId           : 'EVT_SCROLL_THUMBRELEASE',          #10068
    wx.EVT_SCROLL_ENDSCROLL.typeId              : 'EVT_SCROLL_ENDSCROLL',             #10069
    wx.EVT_SCROLLWIN_TOP.typeId                 : 'EVT_SCROLLWIN_TOP',                #10070
    wx.EVT_SCROLLWIN_BOTTOM.typeId              : 'EVT_SCROLLWIN_BOTTOM',             #10071
    wx.EVT_SCROLLWIN_LINEUP.typeId              : 'EVT_SCROLLWIN_LINEUP',             #10072
    wx.EVT_SCROLLWIN_LINEDOWN.typeId            : 'EVT_SCROLLWIN_LINEDOWN',           #10073
    wx.EVT_SCROLLWIN_PAGEUP.typeId              : 'EVT_SCROLLWIN_PAGEUP',             #10074
    wx.EVT_SCROLLWIN_PAGEDOWN.typeId            : 'EVT_SCROLLWIN_PAGEDOWN',           #10075
    wx.EVT_SCROLLWIN_THUMBTRACK.typeId          : 'EVT_SCROLLWIN_THUMBTRACK',         #10076
    wx.EVT_SCROLLWIN_THUMBRELEASE.typeId        : 'EVT_SCROLLWIN_THUMBRELEASE',       #10077
    wx.EVT_GESTURE_PAN.typeId                   : 'EVT_GESTURE_PAN',                  #10078
    wx.EVT_GESTURE_ZOOM.typeId                  : 'EVT_GESTURE_ZOOM',                 #10079
    wx.EVT_GESTURE_ROTATE.typeId                : 'EVT_GESTURE_ROTATE',               #10080
    wx.EVT_TWO_FINGER_TAP.typeId                : 'EVT_TWO_FINGER_TAP',               #10081
    wx.EVT_LONG_PRESS.typeId                    : 'EVT_LONG_PRESS',                   #10082
    wx.EVT_PRESS_AND_TAP.typeId                 : 'EVT_PRESS_AND_TAP',                #10083
    #wx.EVT_SIZE.typeId                          : 'EVT_SIZE',                         #10084
    wx.EVT_SIZING.typeId                        : 'EVT_SIZING',                       #10085
    #wx.EVT_MOVE.typeId                          : 'EVT_MOVE',                         #10086
    #wx.EVT_MOVING.typeId                        : 'EVT_MOVING',                       #10087
    wx.EVT_MOVE_START.typeId                    : 'EVT_MOVE_START',                   #10088
    wx.EVT_MOVE_END.typeId                      : 'EVT_MOVE_END',                     #10089
    wx.EVT_CLOSE.typeId                         : 'EVT_CLOSE',                        #10090
    wx.EVT_END_SESSION.typeId                   : 'EVT_END_SESSION',                  #10091
    wx.EVT_QUERY_END_SESSION.typeId             : 'EVT_QUERY_END_SESSION',            #10092
    wx.EVT_HIBERNATE.typeId                     : 'EVT_HIBERNATE',                    #10093
    #wx.EVT_ACTIVATE_APP.typeId                  : 'EVT_ACTIVATE_APP',                 #10094
    #wx.EVT_ACTIVATE.typeId                      : 'EVT_ACTIVATE',                     #10095
    #wx.EVT_WINDOW_CREATE.typeId                 : 'EVT_WINDOW_CREATE',                #10096
    #wx.EVT_WINDOW_DESTROY.typeId                : 'EVT_WINDOW_DESTROY',               #10097
    #wx.EVT_SHOW.typeId                          : 'EVT_SHOW',                         #10098
    wx.EVT_ICONIZE.typeId                       : 'EVT_ICONIZE',                      #10099
    wx.EVT_MAXIMIZE.typeId                      : 'EVT_MAXIMIZE',                     #10100
    #wx.EVT_MOUSE_CAPTURE_CHANGED.typeId         : 'EVT_MOUSE_CAPTURE_CHANGED',        #10101
    #wx.EVT_MOUSE_CAPTURE_LOST.typeId            : 'EVT_MOUSE_CAPTURE_LOST',           #10102
    #wx.EVT_PAINT.typeId                         : 'EVT_PAINT',                        #10103
    #wx.EVT_ERASE_BACKGROUND.typeId              : 'EVT_ERASE_BACKGROUND',             #10104
    #wx.EVT_NC_PAINT.typeId                      : 'EVT_NC_PAINT',                     #10105
    #wx.EVT_MENU_OPEN.typeId                     : 'EVT_MENU_OPEN',                    #10106
    #wx.EVT_MENU_CLOSE.typeId                    : 'EVT_MENU_CLOSE',                   #10107
    #wx.EVT_MENU_HIGHLIGHT_ALL.typeId            : 'EVT_MENU_HIGHLIGHT_ALL',           #10108
    wx.EVT_CONTEXT_MENU.typeId                  : 'EVT_CONTEXT_MENU',                 #10109
    wx.EVT_SYS_COLOUR_CHANGED.typeId            : 'EVT_SYS_COLOUR_CHANGED',           #10110
    wx.EVT_DISPLAY_CHANGED.typeId               : 'EVT_DISPLAY_CHANGED',              #10111
    wx.EVT_DPI_CHANGED.typeId                   : 'EVT_DPI_CHANGED',                  #10112
    wx.EVT_QUERY_NEW_PALETTE.typeId             : 'EVT_QUERY_NEW_PALETTE',            #10113
    wx.EVT_PALETTE_CHANGED.typeId               : 'EVT_PALETTE_CHANGED',              #10114
    wx.EVT_JOY_BUTTON_DOWN.typeId               : 'EVT_JOY_BUTTON_DOWN',              #10115
    wx.EVT_JOY_BUTTON_UP.typeId                 : 'EVT_JOY_BUTTON_UP',                #10116
    wx.EVT_JOY_MOVE.typeId                      : 'EVT_JOY_MOVE',                     #10117
    wx.EVT_JOY_ZMOVE.typeId                     : 'EVT_JOY_ZMOVE',                    #10118
    wx.EVT_DROP_FILES.typeId                    : 'EVT_DROP_FILES',                   #10119
    wx.EVT_INIT_DIALOG.typeId                   : 'EVT_INIT_DIALOG',                  #10120
    #wx.EVT_UPDATE_UI_RANGE.typeId               : 'EVT_UPDATE_UI_RANGE',              #10121
    wx.EVT_TEXT_COPY.typeId                     : 'EVT_TEXT_COPY',                    #10122
    wx.EVT_TEXT_CUT.typeId                      : 'EVT_TEXT_CUT',                     #10123
    wx.EVT_TEXT_PASTE.typeId                    : 'EVT_TEXT_PASTE',                   #10124
    wx.EVT_COMMAND_LEFT_CLICK.typeId            : 'EVT_COMMAND_LEFT_CLICK',           #10125
    wx.EVT_COMMAND_LEFT_DCLICK.typeId           : 'EVT_COMMAND_LEFT_DCLICK',          #10126
    wx.EVT_COMMAND_RIGHT_CLICK.typeId           : 'EVT_COMMAND_RIGHT_CLICK',          #10127
    wx.EVT_COMMAND_RIGHT_DCLICK.typeId          : 'EVT_COMMAND_RIGHT_DCLICK',         #10128
    wx.EVT_COMMAND_SET_FOCUS.typeId             : 'EVT_COMMAND_SET_FOCUS',            #10129
    wx.EVT_COMMAND_KILL_FOCUS.typeId            : 'EVT_COMMAND_KILL_FOCUS',           #10130
    wx.EVT_COMMAND_ENTER.typeId                 : 'EVT_COMMAND_ENTER',                #10131
    wx.EVT_HELP_RANGE.typeId                    : 'EVT_HELP_RANGE',                   #10132
    wx.EVT_DETAILED_HELP_RANGE.typeId           : 'EVT_DETAILED_HELP_RANGE',          #10133
    wx.EVT_COLLAPSIBLEPANE_CHANGED.typeId       : 'EVT_COLLAPSIBLEPANE_CHANGED',      #10144
    wx.EVT_TOGGLEBUTTON.typeId                  : 'EVT_TOGGLEBUTTON',                 #10145
    wx.EVT_CLIPBOARD_CHANGED.typeId             : 'EVT_CLIPBOARD_CHANGED',            #10146
    wx.EVT_COLOURPICKER_CHANGED.typeId          : 'EVT_COLOURPICKER_CHANGED',         #10147
    wx.EVT_COLOURPICKER_CURRENT_CHANGED.typeId  : 'EVT_COLOURPICKER_CURRENT_CHANGED', #10148
    wx.EVT_COLOURPICKER_DIALOG_CANCELLED.typeId : 'EVT_COLOURPICKER_DIALOG_CANCELLED',#10149
    wx.EVT_COLOUR_CHANGED.typeId                : 'EVT_COLOUR_CHANGED',               #10150
    wx.EVT_WINDOW_MODAL_DIALOG_CLOSED.typeId    : 'EVT_WINDOW_MODAL_DIALOG_CLOSED',   #10151
    wx.EVT_FIND.typeId                          : 'EVT_FIND',                         #10152
    wx.EVT_FIND_NEXT.typeId                     : 'EVT_FIND_NEXT',                    #10153
    wx.EVT_FIND_REPLACE.typeId                  : 'EVT_FIND_REPLACE',                 #10154
    wx.EVT_FIND_REPLACE_ALL.typeId              : 'EVT_FIND_REPLACE_ALL',             #10155
    wx.EVT_FIND_CLOSE.typeId                    : 'EVT_FIND_CLOSE',                   #10156
    wx.EVT_FILECTRL_SELECTIONCHANGED.typeId     : 'EVT_FILECTRL_SELECTIONCHANGED',    #10157
    wx.EVT_FILECTRL_FILEACTIVATED.typeId        : 'EVT_FILECTRL_FILEACTIVATED',       #10158
    wx.EVT_FILECTRL_FOLDERCHANGED.typeId        : 'EVT_FILECTRL_FOLDERCHANGED',       #10159
    wx.EVT_FILECTRL_FILTERCHANGED.typeId        : 'EVT_FILECTRL_FILTERCHANGED',       #10160
    wx.EVT_FILEPICKER_CHANGED.typeId            : 'EVT_FILEPICKER_CHANGED',           #10161
    wx.EVT_DIRPICKER_CHANGED.typeId             : 'EVT_DIRPICKER_CHANGED',            #10162
    wx.EVT_FONTPICKER_CHANGED.typeId            : 'EVT_FONTPICKER_CHANGED',           #10163
    wx.EVT_HEADER_CLICK.typeId                  : 'EVT_HEADER_CLICK',                 #10164
    wx.EVT_HEADER_RIGHT_CLICK.typeId            : 'EVT_HEADER_RIGHT_CLICK',           #10165
    wx.EVT_HEADER_MIDDLE_CLICK.typeId           : 'EVT_HEADER_MIDDLE_CLICK',          #10166
    wx.EVT_HEADER_DCLICK.typeId                 : 'EVT_HEADER_DCLICK',                #10167
    wx.EVT_HEADER_RIGHT_DCLICK.typeId           : 'EVT_HEADER_RIGHT_DCLICK',          #10168
    wx.EVT_HEADER_MIDDLE_DCLICK.typeId          : 'EVT_HEADER_MIDDLE_DCLICK',         #10169
    wx.EVT_HEADER_SEPARATOR_DCLICK.typeId       : 'EVT_HEADER_SEPARATOR_DCLICK',      #10170
    wx.EVT_HEADER_BEGIN_RESIZE.typeId           : 'EVT_HEADER_BEGIN_RESIZE',          #10171
    #wx.EVT_HEADER_RESIZING.typeId               : 'EVT_HEADER_RESIZING',              #10172
    wx.EVT_HEADER_END_RESIZE.typeId             : 'EVT_HEADER_END_RESIZE',            #10173
    wx.EVT_HEADER_BEGIN_REORDER.typeId          : 'EVT_HEADER_BEGIN_REORDER',         #10174
    wx.EVT_HEADER_END_REORDER.typeId            : 'EVT_HEADER_END_REORDER',           #10175
    wx.EVT_HEADER_DRAGGING_CANCELLED.typeId     : 'EVT_HEADER_DRAGGING_CANCELLED',    #10176
    wx.EVT_LIST_BEGIN_DRAG.typeId               : 'EVT_LIST_BEGIN_DRAG',              #10177
    wx.EVT_LIST_BEGIN_RDRAG.typeId              : 'EVT_LIST_BEGIN_RDRAG',             #10178
    wx.EVT_LIST_BEGIN_LABEL_EDIT.typeId         : 'EVT_LIST_BEGIN_LABEL_EDIT',        #10179
    wx.EVT_LIST_END_LABEL_EDIT.typeId           : 'EVT_LIST_END_LABEL_EDIT',          #10180
    wx.EVT_LIST_DELETE_ITEM.typeId              : 'EVT_LIST_DELETE_ITEM',             #10181
    wx.EVT_LIST_DELETE_ALL_ITEMS.typeId         : 'EVT_LIST_DELETE_ALL_ITEMS',        #10182
    wx.EVT_LIST_ITEM_SELECTED.typeId            : 'EVT_LIST_ITEM_SELECTED',           #10183
    wx.EVT_LIST_ITEM_DESELECTED.typeId          : 'EVT_LIST_ITEM_DESELECTED',         #10184
    wx.EVT_LIST_KEY_DOWN.typeId                 : 'EVT_LIST_KEY_DOWN',                #10185
    wx.EVT_LIST_INSERT_ITEM.typeId              : 'EVT_LIST_INSERT_ITEM',             #10186
    wx.EVT_LIST_COL_CLICK.typeId                : 'EVT_LIST_COL_CLICK',               #10187
    wx.EVT_LIST_COL_RIGHT_CLICK.typeId          : 'EVT_LIST_COL_RIGHT_CLICK',         #10188
    wx.EVT_LIST_COL_BEGIN_DRAG.typeId           : 'EVT_LIST_COL_BEGIN_DRAG',          #10189
    #wx.EVT_LIST_COL_DRAGGING.typeId             : 'EVT_LIST_COL_DRAGGING',            #10190
    wx.EVT_LIST_COL_END_DRAG.typeId             : 'EVT_LIST_COL_END_DRAG',            #10191
    wx.EVT_LIST_ITEM_RIGHT_CLICK.typeId         : 'EVT_LIST_ITEM_RIGHT_CLICK',        #10192
    wx.EVT_LIST_ITEM_MIDDLE_CLICK.typeId        : 'EVT_LIST_ITEM_MIDDLE_CLICK',       #10193
    wx.EVT_LIST_ITEM_ACTIVATED.typeId           : 'EVT_LIST_ITEM_ACTIVATED',          #10194
    wx.EVT_LIST_ITEM_FOCUSED.typeId             : 'EVT_LIST_ITEM_FOCUSED',            #10195
    wx.EVT_LIST_ITEM_CHECKED.typeId             : 'EVT_LIST_ITEM_CHECKED',            #10196
    wx.EVT_LIST_ITEM_UNCHECKED.typeId           : 'EVT_LIST_ITEM_UNCHECKED',          #10197
    wx.EVT_LIST_CACHE_HINT.typeId               : 'EVT_LIST_CACHE_HINT',              #10198
    wx.EVT_NOTEBOOK_PAGE_CHANGED.typeId         : 'EVT_NOTEBOOK_PAGE_CHANGED',        #10199
    #wx.EVT_NOTEBOOK_PAGE_CHANGING.typeId        : 'EVT_NOTEBOOK_PAGE_CHANGING',       #10200
    wx.EVT_SPINCTRL.typeId                      : 'EVT_SPINCTRL',                     #10201
    wx.EVT_SPINCTRLDOUBLE.typeId                : 'EVT_SPINCTRLDOUBLE',               #10202
    wx.EVT_SEARCH_CANCEL.typeId                 : 'EVT_SEARCH_CANCEL',                #10203
    wx.EVT_SEARCHCTRL_SEARCH_BTN.typeId         : 'EVT_SEARCHCTRL_SEARCH_BTN',        #10204
    #wx.EVT_TEXT.typeId                          : 'EVT_TEXT',                         #10205
    wx.EVT_TEXT_ENTER.typeId                    : 'EVT_TEXT_ENTER',                   #10206
    wx.EVT_TEXT_URL.typeId                      : 'EVT_TEXT_URL',                     #10207
    wx.EVT_TEXT_MAXLEN.typeId                   : 'EVT_TEXT_MAXLEN',                  #10208
    wx.EVT_TREE_BEGIN_DRAG.typeId               : 'EVT_TREE_BEGIN_DRAG',              #10209
    wx.EVT_TREE_BEGIN_RDRAG.typeId              : 'EVT_TREE_BEGIN_RDRAG',             #10210
    wx.EVT_TREE_BEGIN_LABEL_EDIT.typeId         : 'EVT_TREE_BEGIN_LABEL_EDIT',        #10211
    wx.EVT_TREE_END_LABEL_EDIT.typeId           : 'EVT_TREE_END_LABEL_EDIT',          #10212
    #wx.EVT_TREE_DELETE_ITEM.typeId              : 'EVT_TREE_DELETE_ITEM',             #10213
    wx.EVT_TREE_GET_INFO.typeId                 : 'EVT_TREE_GET_INFO',                #10214
    wx.EVT_TREE_SET_INFO.typeId                 : 'EVT_TREE_SET_INFO',                #10215
    #wx.EVT_TREE_ITEM_EXPANDED.typeId            : 'EVT_TREE_ITEM_EXPANDED',           #10216
    #wx.EVT_TREE_ITEM_EXPANDING.typeId           : 'EVT_TREE_ITEM_EXPANDING',          #10217
    #wx.EVT_TREE_ITEM_COLLAPSED.typeId           : 'EVT_TREE_ITEM_COLLAPSED',          #10218
    #wx.EVT_TREE_ITEM_COLLAPSING.typeId          : 'EVT_TREE_ITEM_COLLAPSING',         #10219
    #wx.EVT_TREE_SEL_CHANGED.typeId              : 'EVT_TREE_SEL_CHANGED',             #10220
    #wx.EVT_TREE_SEL_CHANGING.typeId             : 'EVT_TREE_SEL_CHANGING',            #10221
    wx.EVT_TREE_KEY_DOWN.typeId                 : 'EVT_TREE_KEY_DOWN',                #10222
    wx.EVT_TREE_ITEM_ACTIVATED.typeId           : 'EVT_TREE_ITEM_ACTIVATED',          #10223
    wx.EVT_TREE_ITEM_RIGHT_CLICK.typeId         : 'EVT_TREE_ITEM_RIGHT_CLICK',        #10224
    wx.EVT_TREE_ITEM_MIDDLE_CLICK.typeId        : 'EVT_TREE_ITEM_MIDDLE_CLICK',       #10225
    wx.EVT_TREE_END_DRAG.typeId                 : 'EVT_TREE_END_DRAG',                #10226
    wx.EVT_TREE_STATE_IMAGE_CLICK.typeId        : 'EVT_TREE_STATE_IMAGE_CLICK',       #10227
    wx.EVT_TREE_ITEM_GETTOOLTIP.typeId          : 'EVT_TREE_ITEM_GETTOOLTIP',         #10228
    wx.EVT_TREE_ITEM_MENU.typeId                : 'EVT_TREE_ITEM_MENU',               #10229
    #wx.EVT_CHOICEBOOK_PAGE_CHANGING.typeId      : 'EVT_CHOICEBOOK_PAGE_CHANGING',     #10230
    wx.EVT_CHOICEBOOK_PAGE_CHANGED.typeId       : 'EVT_CHOICEBOOK_PAGE_CHANGED',      #10231
    wx.EVT_COLLAPSIBLEHEADER_CHANGED.typeId     : 'EVT_COLLAPSIBLEHEADER_CHANGED',    #10232
    wx.EVT_DIRCTRL_SELECTIONCHANGED.typeId      : 'EVT_DIRCTRL_SELECTIONCHANGED',     #10233
    wx.EVT_DIRCTRL_FILEACTIVATED.typeId         : 'EVT_DIRCTRL_FILEACTIVATED',        #10234
    #wx.EVT_LISTBOOK_PAGE_CHANGING.typeId        : 'EVT_LISTBOOK_PAGE_CHANGING',       #10235
    wx.EVT_LISTBOOK_PAGE_CHANGED.typeId         : 'EVT_LISTBOOK_PAGE_CHANGED',        #10236
    wx.EVT_SPLITTER_SASH_POS_CHANGED.typeId     : 'EVT_SPLITTER_SASH_POS_CHANGED',    #10237
    #wx.EVT_SPLITTER_SASH_POS_CHANGING.typeId    : 'EVT_SPLITTER_SASH_POS_CHANGING',   #10238
    wx.EVT_SPLITTER_DOUBLECLICKED.typeId        : 'EVT_SPLITTER_DOUBLECLICKED',       #10239
    wx.EVT_SPLITTER_UNSPLIT.typeId              : 'EVT_SPLITTER_UNSPLIT',             #10240
    #wx.EVT_TOOLBOOK_PAGE_CHANGING.typeId        : 'EVT_TOOLBOOK_PAGE_CHANGING',       #10241
    wx.EVT_TOOLBOOK_PAGE_CHANGED.typeId         : 'EVT_TOOLBOOK_PAGE_CHANGED',        #10242
    #wx.EVT_TREEBOOK_PAGE_CHANGING.typeId        : 'EVT_TREEBOOK_PAGE_CHANGING',       #10243
    wx.EVT_TREEBOOK_PAGE_CHANGED.typeId         : 'EVT_TREEBOOK_PAGE_CHANGED',        #10244
    wx.EVT_TREEBOOK_NODE_COLLAPSED.typeId       : 'EVT_TREEBOOK_NODE_COLLAPSED',      #10245
    wx.EVT_TREEBOOK_NODE_EXPANDED.typeId        : 'EVT_TREEBOOK_NODE_EXPANDED',       #10246
    }

class DAEventPickleWrapper:
    @staticmethod
    def __eventFactory(cname):
        evt = None
        if cname == 'wxCommandEvent':
            evt = wx.CommandEvent()
        elif cname == 'wxPyCommandEvent':
            evt = wx.PyCommandEvent()
        #elif cname == 'wxTreeEvent':
        #    evt = wx.TreeEvent()
        #elif cname == 'xx':
        #    pass
        else:
            raise RuntimeError()
        return evt

    @staticmethod
    def __toDict(evt, skip=()):
        # https://stackoverflow.com/questions/1322068/determine-if-python-variable-is-an-instance-of-a-built-in-type
        def isBuiltinClassInstance(obj):
            return obj.__class__.__module__ in ('__builtin__', '__builtins__', 'builtins')
        vals = {}
        vals['ClassName']     = evt.ClassName
        vals['EventTypeName'] = LST_EVENTS[evt.GetEventType()]
        for key in dir(evt):
            if not key.startswith('Set'): continue
            if key in skip: continue
            if key in ('SetClassInfo'): continue
            if key in ('SetClassObject'): continue
            getter = 'Get%s' % key[3:]
            try:
                print('   %s' % getter)
                f = getattr(evt, getter)
                val = f()
                if isBuiltinClassInstance(val):
                    vals[key] = val
            except:
                pass
        return vals

    @staticmethod
    def toDict(evt):
        if   isinstance(evt, wx._core.TreeEvent):
            return DAEventPickleWrapper.__toDict(evt, skip=('SetClientData', 'SetClientObject', 'SetItem', 'SetOldItem'))
        elif isinstance(evt, wx._core.CommandEvent):
            return DAEventPickleWrapper.__toDict(evt)
        elif isinstance(evt, wx._core.Event):
            return None
        return None

    @staticmethod
    def fromDict(dct):
        cname = dct['ClassName']
        evt = DAEventPickleWrapper.__eventFactory(cname)
        for key, val in dct.items():
            if not key.startswith('Set'): continue
            f = getattr(evt, key)
            _ = f(val)
        return evt

# class DAEventEncoder(json.JSONEncoder):
#     def default(self, obj):
#         if   isinstance(obj, wx._core.CommandEvent):
#             return DAEventPickleWrapper().toJSON(obj)
#         elif isinstance(obj, wx._core.Event):
#             return ''
#         return super().default(obj)

class DAEventDecoder(json.JSONDecoder):
    def default(self, odct):
        try:
            cname = odct['ClassName']
            return DAEventPickleWrapper.fromDict(odct)
        except KeyError:
            return super().default(odct)

class DAEventRecorder(wx.EventFilter):
    def __init__(self, hndlr):
        super().__init__()
        self.hndlr = hndlr
        wx.EvtHandler.AddFilter(self)

        self.recording = False
        self.dumpEvent = False
        self.events = []

    def __del__(self):
        wx.EvtHandler.RemoveFilter(self)

    def FilterEvent(self, event):
        if self.recording and event.GetEventType() in LST_EVENTS:
            evt = event.Clone()
            print((LST_EVENTS[evt.GetEventType()]), type(evt))
            edic = DAEventPickleWrapper.toDict(evt)
            if edic:
                self.events.append(edic)
        return self.Event_Skip

    def startRecording(self):
        self.events = []
        self.recording = True

    def stopRecording(self):
        self.recording = False

    def register(self, file):
        if self.events:
            with open(file, 'w') as ofs:
                json.dump(self.events, ofs, indent=4)

    def playBack(self, file):
        recording = self.recording
        self.recording = False

        with open(file, 'r') as ifs:
            events = json.load(ifs)
        try:
            for edct in events:
                event = DAEventPickleWrapper.fromDict(edct)
                self.hndlr.ProcessEvent(event)
        except Exception as why:
            pass
            raise


        self.recording = recording
        return
