#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import logging
import os
import sys

import webbrowser

if getattr(sys, 'frozen', False):
    DOCDIR = os.path.join(sys._MEIPASS, 'doc')
else:
    DOCDIR = os.path.join(os.path.dirname(__file__), '..', 'doc')
DOCDIR = os.path.normpath(DOCDIR).replace('\\', '/')

LOGGER = logging.getLogger('H2D2.Tools.DataAnalyzer.Help.Help')

def displayHelp():
    LOGGER.trace('DADlgHelpAbout.displayHelp')
    url = 'file://%s/index.html' % DOCDIR
    webbrowser.open(url)

if __name__ == '__main__':
    LOGGER.trace = LOGGER.debug
    displayHelp()
