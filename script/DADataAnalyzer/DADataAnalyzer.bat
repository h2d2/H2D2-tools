@echo off

set BIN_DIR=%~dp0
set BIN_DIR=%BIN_DIR:~0,-1%

if exist "%BIN_DIR%\..\script" (
   set BIN_DIR="%BIN_DIR%\..\script\DADataAnalyzer"
)

pushd "%BIN_DIR%"
if exist DADataAnalyzer.exe (
   DADataAnalyzer.exe %*
) else (
   python DADataAnalyzer.py %*
)
popd

pause