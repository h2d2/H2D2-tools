# -*- coding: utf-8 -*-
# -*- mode: python -*-
import os
import sys
import shutil
sys.setrecursionlimit(5000)

pkg_path = '.'
sup_path = ['..']
try:
    cmdLineArg = os.environ['PYINST_LIBPATH'].strip()
    cmdLineArg = cmdLineArg.encode('unicode_escape')
    cmdLineArg = cmdLineArg.decode('utf-8')
    if cmdLineArg and cmdLineArg[0] not in '"\'':
        cmdLineArg = '"%s"' % cmdLineArg
    cmdLineArg = eval(cmdLineArg, {}, {})
    cmdPathes = []
    for p in cmdLineArg.split():
        if not p.startswith('-L'): raise RuntimeError('Invalid PYINST_LIBPATH component: "%s" does not start with "-L"' % p)
        p = p[2:].strip()
        if not os.path.isdir(p): raise RuntimeError('Invalid PYINST_LIBPATH component: "%s" is not a valid path' % p)
        cmdPathes.append(os.path.normpath(p))
    sup_path = cmdPathes + sup_path
except KeyError as e:
    pass

def getDatasAndBinaries():
    hiddens=[
       'scipy._lib.messagestream',
       'scipy.special.cython_special',
       'scipy.spatial.transform._rotation_groups',
       'scipy.stats',
       #
       'wx.xml',
       'wx.richtext',       # Added for PTProbe

       #
       'CTCommon.xtrn',
       'CTCommon.xtrn.jdutil',
       'CTCommon.QTQuadTree',
       'CTCommon.DTDataBloc',
       'CTCommon.DTPathError',
       'IPImageProcessor.KML',
       ]

    datas = [
        ('../IPImageProcessor/KML/images/*.png',        'bitmaps/IPImageProcessor/KML'),
        ('../DADataAnalyzer/bitmaps/*.png',             'bitmaps/DADataAnalyzer'),
        ('../DADataAnalyzer/bitmaps/cmaps/*.png',       'bitmaps/DADataAnalyzer/cmaps'),
        ('../DADataAnalyzer/bitmaps/16x16-free-application-icons/16x16/*.png', 'bitmaps/DADataAnalyzer/16x16'),
        ('../DADataAnalyzer/doc/*.html',                'doc'),
        ('../DADataAnalyzer/Filters/*.py',              'Filters'),
        ('../DADataAnalyzer/DAHelp/assets/css/*.css',   'assets/DADataAnalyzer/DAHelp/css'),
        ('../DADataAnalyzer/DAHelp/assets/js/*.js',     'assets/DADataAnalyzer/DAHelp/js'),
        ('../DADataAnalyzer/lgpl*.txt',                 '.'),
        ]

    binaries = [
        (shutil.which('gdalsrsinfo'),                   '.'),
        ]

    # ---  Patch for matplotlib
    from pyinstaller_hooks_and_patches.patch_matplotlib import getDatasAndBinaries as gdb
    p_dta, p_bin = gdb()
    datas    += p_dta
    binaries += p_bin

    # ---  Patch for numpy
    from pyinstaller_hooks_and_patches.patch_numpy import getDatasAndBinaries as gdb
    p_dta, p_bin = gdb()
    datas    += p_dta
    binaries += p_bin

    # ---  Patch for wx
    from pyinstaller_hooks_and_patches.patch_wx import getDatasAndBinaries as gdb
    p_dta, p_bin = gdb()
    datas    += p_dta
    binaries += p_bin

    return hiddens, datas, binaries

block_cipher = None

def analyze():
    block_cipher = None
    hiddens, datas, binaries = getDatasAndBinaries()

    a = Analysis(['DADataAnalyzer.py'],
                 pathex=sup_path,
                 binaries=binaries,
                 datas=datas,
                 hiddenimports=hiddens,
                 hookspath=['../pyinstaller_hooks_and_patches'],        # for osgeo
                 runtime_hooks=[],
                 excludes=[],
                 win_no_prefer_redirects=False,
                 win_private_assemblies=False,
                 cipher=block_cipher)
    a.name = 'DADataAnalyzer'        # inject name
    return a

a = analyze()

# pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
# exe = EXE(pyz,
#           a.scripts,
#           exclude_binaries=True,
#           name='DADataAnalyzer',
#           debug=False,
#           strip=False,
#           upx=True,
#           console=True )
# coll = COLLECT(exe,
#                a.binaries,
#                a.zipfiles,
#                a.datas,
#                strip=False,
#                upx=True,
#                name='DADataAnalyzer')
