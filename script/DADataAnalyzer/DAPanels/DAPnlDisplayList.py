# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)
    supPath = os.path.normpath(os.path.join(selfDir, '..', '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import logging
import traceback
import wx
import wx.lib.agw.ultimatelistctrl as ULC
#import wx.lib.agw.customtreectrl   as CT
from CTCommon.xtrn  import customtreectrl as CT
from CTCommon import DTData

from DAPanels.DANodalValueList import DANodalValueListCtrl
from DAPanels.DAPnl2DFigure    import DAPnl2DFigure

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Panel.DisplayList")

class DAPnlDisplayList(wx.Panel):
    def __init__(self, *args, **kwds):
        cb_check = kwds.pop('cb_on_check', None)
        cb_drag  = kwds.pop('cb_on_drag', None)
        wx.Panel.__init__(self, *args, **kwds)
        self.spt_main = wx.SplitterWindow(self, -1, style=wx.SP_3D|wx.SP_BORDER)
        self.pnl_top  = wx.Panel(self.spt_main, -1)
        self.pnl_btm  = wx.Panel(self.spt_main, -1)

        agwStyle  = 0
        agwStyle |= CT.TR_HIDE_ROOT
        agwStyle |= CT.TR_DEFAULT_STYLE
        agwStyle |= CT.TR_HAS_BUTTONS
        agwStyle |= CT.TR_AUTO_CHECK_CHILD
        agwStyle |= CT.TR_AUTO_TOGGLE_CHILD
        agwStyle |= CT.TR_AUTO_CHECK_PARENT
        agwStyle |= CT.TR_ELLIPSIZE_LONG_ITEMS
        agwStyle |= CT.TR_TOOLTIP_ON_LONG_ITEMS
        agwStyle = kwds.pop('agwStyle', agwStyle)
        ulcStyle  = 0
        ulcStyle |= ULC.ULC_REPORT
        ulcStyle |= ULC.ULC_HRULES
        ulcStyle |= ULC.ULC_VRULES
        ulcStyle |= ULC.ULC_SINGLE_SEL
        ulcStyle |= ULC.ULC_SHOW_TOOLTIPS
        ulcStyle |= ULC.ULC_NO_HIGHLIGHT
        self.sbx_lyr = wx.StaticBox        (self.pnl_top, -1, "Display list")
        self.lst_lyr = CT.CustomTreeCtrl   (self.pnl_top, -1, name='Layers', agwStyle=agwStyle)
        self.sbx_sel = wx.StaticBox        (self.pnl_btm, -1, "Layer")
        self.lst_sel = DANodalValueListCtrl(self.pnl_btm, -1, title='Layer',    auto_extend=False, enable_edit=False, agwStyle=ulcStyle | ULC.ULC_NO_HEADER)
        self.sbx_prb = wx.StaticBox        (self.pnl_btm, -1, "Probes")
        self.txt_prb = wx.StaticText       (self.pnl_btm, -1, '---', style=wx.ALIGN_LEFT | wx.ST_ELLIPSIZE_START)
        self.lst_prb = DANodalValueListCtrl(self.pnl_btm, -1, title='Variable', auto_extend=False, enable_edit=False, agwStyle=ulcStyle)

        self.lst_prb.OnItemChecked = self.onProbeCheck

        self.__create_menus()
        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_MENU, self.onMenuLayerOn,      self.mnu_lyr_on)
        self.Bind(wx.EVT_MENU, self.onMenuLayerEdit,    self.mnu_lyr_edit)
        self.Bind(wx.EVT_MENU, self.onMenuLayerNavigate,self.mnu_lyr_nav)
        self.Bind(wx.EVT_MENU, self.onMenuLayerAnimate, self.mnu_lyr_anim)
        self.Bind(wx.EVT_MENU, self.onMenuLayerDelete,  self.mnu_lyr_del)

        self.Bind(CT.EVT_TREE_SEL_CHANGED,      self.onLayerSelect,    self.lst_lyr)
        #self.Bind(CT.EVT_TREE_ITEM_ACTIVATED,   self.onLayerSelect,    self.lst_lyr)
        self.Bind(CT.EVT_TREE_ITEM_CHECKED,     self.onLayerCheck,     self.lst_lyr)
        self.Bind(CT.EVT_TREE_ITEM_RIGHT_CLICK, self.onLayerRclick,    self.lst_lyr)
        self.Bind(CT.EVT_TREE_BEGIN_DRAG,       self.onLayerDragBegin, self.lst_lyr)
        self.Bind(CT.EVT_TREE_END_DRAG,         self.onLayerDragEnd,   self.lst_lyr)

        self.cb_on_check = cb_check
        self.cb_on_drag  = cb_drag
        self.page  = None   # for layers
        self.layer = None   # selected layer
        self.field = None   # for probe
        self.fldsel= None   # for selection probe
        self.probes_activ = []

    def __set_properties(self):
        self.SetMinSize((-1, -1))
        self.spt_main.SetMinSize((-1,-1))
        self.spt_main.SetSashGravity(0.7)
        self.spt_main.SetMinimumPaneSize(50)

        self.txt_prb.SetMinSize(self.lst_prb.GetHeaderHeight())
        self.txt_prb.SetBackgroundColour(self.lst_prb.GetBackgroundColour())
        self.txt_prb.SetBackgroundStyle (self.lst_prb.GetBackgroundStyle())
        self.txt_prb.SetForegroundColour(self.lst_prb.GetForegroundColour())

        self.lst_sel.SetMinSize((-1, 20))
        self.lst_sel.InsertColumn  (1, 'Value')
        self.lst_sel.SetColumnWidth(0, ULC.ULC_AUTOSIZE_USEHEADER)
        self.lst_sel.SetColumnWidth(1, ULC.ULC_AUTOSIZE_FILL)

        self.lst_prb.InsertColumn  (1, 'Value')
        self.lst_prb.SetColumnWidth(0, ULC.ULC_AUTOSIZE_USEHEADER)
        self.lst_prb.SetColumnWidth(1, ULC.ULC_AUTOSIZE_FILL)

    def __do_layout(self):
        szr_top = wx.StaticBoxSizer(self.sbx_lyr,  wx.VERTICAL)
        szr_top.Add(self.lst_lyr, 1, wx.EXPAND, 0)
        self.pnl_top.SetSizer(szr_top)

        szr_btm = wx.BoxSizer(wx.VERTICAL)
        szr_sel = wx.StaticBoxSizer(self.sbx_sel,  wx.VERTICAL)
        szr_sel.Add(self.lst_sel, 0, wx.EXPAND, 0)
        szr_btm.Add(szr_sel, 0, wx.EXPAND, 0)

        szr_prb = wx.StaticBoxSizer(self.sbx_prb,  wx.VERTICAL)
        szr_prb.Add(self.txt_prb, 0, wx.EXPAND, 0)
        szr_prb.Add(self.lst_prb, 1, wx.EXPAND, 0)
        szr_btm.Add(szr_prb, 1, wx.EXPAND, 0)
        self.pnl_btm.SetSizer(szr_btm)

        szr_main = wx.BoxSizer(wx.VERTICAL)
        szr_main.Add(self.spt_main, 1, wx.EXPAND, 0)
        self.spt_main.SplitHorizontally(self.pnl_top, self.pnl_btm, 50)
        self.SetSizer(szr_main)
        self.Layout()

    def __create_menus(self):
        self.mnu_lyr = wx.Menu()
        self.mnu_lyr_on   = wx.MenuItem(self.mnu_lyr, wx.ID_ANY, "Activate","Activate-deactivate the layer", wx.ITEM_CHECK)
        self.mnu_lyr_edit = wx.MenuItem(self.mnu_lyr, wx.ID_ANY, "Edit",    "Edit the selected layer",       wx.ITEM_NORMAL)
        self.mnu_lyr_nav  = wx.MenuItem(self.mnu_lyr, wx.ID_ANY, "Navigate","Navigate the layer",            wx.ITEM_NORMAL)
        self.mnu_lyr_anim = wx.MenuItem(self.mnu_lyr, wx.ID_ANY, "Animate", "Animate the layer",             wx.ITEM_NORMAL)
        self.mnu_lyr_del  = wx.MenuItem(self.mnu_lyr, wx.ID_ANY, "Delete",  "Delete the selected layer",     wx.ITEM_NORMAL)
        self.mnu_lyr.Append(self.mnu_lyr_on)
        self.mnu_lyr.Append(self.mnu_lyr_edit)
        self.mnu_lyr.Append(self.mnu_lyr_nav)
        self.mnu_lyr.Append(self.mnu_lyr_anim)
        self.mnu_lyr.AppendSeparator()
        self.mnu_lyr.Append(self.mnu_lyr_del)

    def __getattr__(self, name):
        attr = getattr(self.lst_lyr, name)
        if hasattr(attr, '__call__'):
            def newfunc(*args, **kwargs):
                result = attr(*args, **kwargs)
                return result
            return newfunc
        else:
            return attr

    def asgCbOnLayerCheck(self, cb):
        self.cb_on_check = cb

    def asgCbOnLayerDrag(self, cb):
        self.cb_on_drag = cb

    def asgData(self, field):
        for row in reversed(list(range(self.lst_prb.GetItemCount()))):
            editable = self.lst_prb.GetItem(row).GetData()
            if not editable:
                self.lst_prb.DeleteItem(row)

        if field and field.getGrid():
            for i in range( field.getNbVal() ):
                text = field.getName(i)
                self.lst_prb.InsertItem(i, text=text, checked=False, editable=False)
            self.lst_prb.Enable(True)

            self.txt_prb.SetLabelText(field.getShortName())
            self.txt_prb.SetToolTip  (field.getLongName())

        else:
            self.lst_prb.Enable(False)

            self.txt_prb.SetLabelText('---')
            self.txt_prb.SetToolTip  ('')

        self.field = field
        if self.field:
            self.onProbeCheck(None)

    def asgPosition(self, xp, yp, xf, yf):
        """
        xp, yp: project coordinates
        xf, yf: field(grid) coordinates
        """
        if self.field and len(self.probes_activ) > 0:
            r = self.field.doProbe((xf,), (yf,))
            for i, v in zip(self.lst_prb.GetChecked(), r[0]):
                self.lst_prb.SetStringItem(i, 1, '%s' % v)
        if self.fldsel:
            r = self.fldsel.doProbe((xf,), (yf,))
            for i, v in zip(self.lst_sel.GetChecked(), r[0]):
                self.lst_sel.SetStringItem(i, 1, '%s' % v)

    def asgPage(self, page):
        # ---  Disable layer from current page
        if self.page and self.page != page:
            for lyr in self.page.get_layers():
                lyr.enable(False)

        # ---  Fill tree
        self.lst_lyr.DeleteAllItems()
        try:
            root = self.lst_lyr.AddRoot("DisplayList Hidden Root Item")
            for lyr in page.get_layers():
                child = self.lst_lyr.AppendItem(root, lyr.title, ct_type=1, data=(lyr,None))
                child.Set3State(True)
                child.Check(lyr.isVisible())
                for itm in lyr:
                    leaf = self.lst_lyr.AppendItem(child, itm.title, ct_type=1, data=(lyr,itm))
                    leaf.Set3State(False)
                    leaf.Check(itm.isVisible())
        except AttributeError as why:
            # Most probably page without get_layers
            pass
        self.lst_lyr.ExpandAll()

        # ---  Set selection
        root = self.lst_lyr.GetRootItem()
        (child, cookie) = self.lst_lyr.GetFirstChild(root)
        while child and child.IsOk():
            lyr, _itm = child.GetData()
            if lyr.isSelected():
                self.lst_lyr.SelectItem(child, True)
            (child, cookie) = self.lst_lyr.GetNextChild(root, cookie)
        # ---  Set selection to last if same page (layer added) or no selection
        if self.page == page or self.lst_lyr.GetSelection() == root:
            selec = self.lst_lyr.GetSelection()
            child = self.lst_lyr.GetLastChild(root)
            if selec != child:
                if selec != root:
                    lyr, _itm = selec.GetData()
                    lyr.select(False)
                    self.lst_lyr.SelectItem(selec, False)
                if child:
                    lyr, _itm = child.GetData()
                    lyr.select(True)
                    self.lst_lyr.SelectItem(child, True)

        # ---  Enable layer from current page
        if self.page and self.page != page:
            for lyr in page.get_layers():
                lyr.enable(True)

        # ---  Swap page
        self.page = page
        self.layer= None

        self.lst_prb.Enable( isinstance(self.page, DAPnl2DFigure) )

    def fillProbeList(self, probes):
        self.lst_prb.DeleteAllItems()
        for item, flag in probes:
            idx = self.lst_prb.AppendItem('  %s' % item, checked=flag, editable=False)
        self.lst_prb.SetColumnWidth(0, ULC.ULC_AUTOSIZE)

    def onLayerSelect(self, event):
        LOGGER.trace('DAPnlDisplayList.onLayerSelected: %s', event.GetItem())
        leaf = event.GetItem()
        if not leaf: return
        if not leaf.GetData(): return

        lyr, _itm = leaf.GetData()
        if lyr == self.layer: return

        if self.layer:
            self.layer.select(False)
            self.layer = None

        field = lyr.getField()
        if field:
            text = field.getShortName()
            self.lst_sel.DeleteAllItems()
            self.lst_sel.AppendItem(text, checked=True, editable=False)
            #self.lst_sel.SetColumnWidth(0, ULC.ULC_AUTOSIZE)
            self.fldsel = field

        self.layer = lyr
        self.layer.select(True)

    def onLayerCheck(self, event):
        LOGGER.trace('DAPnlDisplayList.onLayerCheck: %s', event.GetItem())
        leaf = event.GetItem()
        if leaf and leaf.GetData():
            lyr, itm = leaf.GetData()
            self.page.check_item(leaf.IsChecked(), lyr, itm)
            if self.cb_on_check:
                self.cb_on_check()

    def onLayerRclick(self, event):
        self.layerDeleted = False
        leaf = event.GetItem()
        lyr, itm = leaf.GetData()
        self.lst_lyr.SelectItem(leaf, True)       # set focus to line
        self.mnu_lyr_on.Check( self.lst_lyr.IsItemChecked(leaf) )
        if lyr and lyr.hasNavigator():
            self.mnu_lyr_nav.Enable(True)
        else:
            self.mnu_lyr_nav.Enable(False)
        if lyr and lyr.hasAnimator():
            self.mnu_lyr_anim.Enable(True)
        else:
            self.mnu_lyr_anim.Enable(False)
        self.PopupMenu(self.mnu_lyr)
        if self.layerDeleted:
            event.Veto()
            if self.cb_on_check:
                self.cb_on_check()

    def onLayerDragBegin(self, event):
        print('DAPnlDisplayList.onLayerDragBegin %s' % event.GetItem())
        item = event.GetItem()
        if item and item.GetData():
            lyr, itm = item.GetData()
            if not itm:    # Drag only layers not items
                self.itemDragBegin = item
                event.Allow()

    def onLayerDragEnd(self, event):
        print('DAPnlDisplayList.onLayerDragEnd %s' % event.GetItem())
        itemE = event.GetItem()
        if itemE and itemE.GetData():
            lyr, itm = itemE.GetData()
            if itm:    # Target only layers not items
                event.Veto()
                return

        itemB = self.itemDragBegin
        self.itemDragBegin = None
        try:
            lyrB, _ = itemB.GetData()
            lyrE, _ = itemE.GetData()
            self.page.drag_layer(lyrB, lyrE)
            self.asgPage(self.page)
        except Exception as why:
            LOGGER.error('%s\n%s' % (str(why), traceback.format_exc()))
            event.Veto()

    def onMenuLayerOn(self, event):
        leaf = self.lst_lyr.GetSelection()
        leaf.Check(self.mnu_lyr_on.IsChecked())

    def onMenuLayerEdit(self, event):
        child = self.lst_lyr.GetSelection()
        lyr, itm = child.GetData()
        # ---  Get layer node
        while itm:
            child = child.GetParent()
            lyr, itm = child.GetData()
        # ---  (Re)-draw
        lyr.draw()
        # ---  Update tree
        self.lst_lyr.DeleteChildren(child)
        for itm in lyr:
             leaf = self.lst_lyr.AppendItem(child, itm.title, ct_type=1, data=(lyr,itm))
             leaf.Set3State(False)
             leaf.Check(itm.isVisible())

    def onMenuLayerNavigate(self, event):
        leaf = self.lst_lyr.GetSelection()
        lyr, itm = leaf.GetData()
        lyr.activate(True)

    def onMenuLayerAnimate(self, event):
        leaf = self.lst_lyr.GetSelection()
        lyr, itm = leaf.GetData()
        lyr.activate(True)

    def onMenuLayerDelete(self, event):
        dlg = wx.MessageDialog(self, 'Are you sure? \n', 'Delete item', wx.YES_NO)
        if (dlg.ShowModal() == wx.ID_YES):
            leaf = self.lst_lyr.GetSelection()
            self.lst_lyr.SelectItem(leaf, select=False)
            # just 1 item ? ==> erase layer
            lyr, itm = leaf.GetData()
            if itm:
                if leaf.GetParent().GetChildrenCount(recursively=False) == 1:
                    leaf = leaf.GetParent()
                    lyr, itm = leaf.GetData()
            root = self.lst_lyr.GetRootItem()
            if not itm and self.lst_lyr.GetChildrenCount(root,recursively=False) == 1:
                self.lst_lyr.DeleteAllItems()
            else:
                #jd = max(id, 0)
                self.lst_lyr.Delete(leaf)
                #self.lst_lyr.SelectItem(jd, select=True)
            self.page.del_item(lyr, itm)
            self.layerDeleted = True

    def onProbeCheck(self, event):
        dops = []
        for c in self.lst_prb.BuildOpList():
            op = DTData.DTDataOpCol( c.getCol() )
            op.setLocals(data=self.field)
            dops.append(op)
        if dops:
            nbSteps = self.field.getNbTimeSteps()
            self.field.getDataAtStep(tsteps=[nbSteps-1], dops=dops)
        self.probes_activ = self.lst_prb.GetChecked()


#==============================================================================
#
#==============================================================================
if __name__ == "__main__":
    from DALayers.DALayer import DALayer, DALayerItemCS
    from wx.lib.mixins.inspection import InspectionMixin
    class Page:
        def __init__(self):
            lyr1 = DALayer(title='Layer-1')
            lyr1.items = [ DALayerItemCS(None, None, title='Item-%i-%i' % (1, i)) for i in range(2) ]
            lyr2 = DALayer(title='Layer-2')
            lyr2.items = [ DALayerItemCS(None, None, title='Item-%i-%i' % (2, i)) for i in range(3) ]
            self.layers = [lyr1, lyr2]

        def __get_layer_idx(self, layer):
            for i, l in enumerate(self.layers):
                if l == layer: return i
            raise ValueError

        def check_item(self, fla, lyr, itm):
            pass

        def get_layers(self):
            return self.layers

        def drag_layer(self, lyr_src, lyr_dst):
            idx_old = self.__get_layer_idx(lyr_src)
            idx_new = self.__get_layer_idx(lyr_dst)
            if   idx_new < idx_old:
                self.layers.insert(idx_new,   self.layers.pop(idx_old))
            elif idx_new > idx_old:
                self.layers.insert(idx_new-1, self.layers.pop(idx_old))
            self.isChanged = True


    class MyDialogBox(wx.Dialog):
        def __init__(self, *args, **kwargs):
            kwargs["style"] = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.MAXIMIZE_BOX|wx.SYSTEM_MENU|wx.RESIZE_BORDER # |wx.CLIP_CHILDREN
            wx.Dialog.__init__(self, *args, **kwargs)
            self.pnl = DAPnlDisplayList(self)
            self.btn_cancel = wx.Button(self, wx.ID_CANCEL, "")
            self.btn_ok     = wx.Button(self, wx.ID_OK, "")

            szr_frm = wx.BoxSizer(wx.VERTICAL)
            szr_frm.Add(self.pnl, 1, wx.EXPAND)

            szr_btn = wx.BoxSizer(wx.HORIZONTAL)
            szr_btn.Add(self.btn_cancel,0, wx.EXPAND, 0)
            szr_btn.Add(self.btn_ok,    0, wx.EXPAND, 0)
            szr_frm.Add(szr_btn, 0, wx.EXPAND, 0)

            self.SetSizer(szr_frm)
            self.Layout()
            self.SetSize((300,600))

            self.pnl.asgPage(Page())
            self.pnl.fillProbeList([('ux', True), ('H', False) ])

    class MyApp(wx.App, InspectionMixin):
        def OnInit(self):
            self.Init()
            dlg = MyDialogBox(None)
            #if dlg.ShowModal() == wx.ID_OK:
            dlg.ShowModal()
            print(dlg.pnl.GetChecked())
            return True

    app = MyApp(False)
    app.MainLoop()

