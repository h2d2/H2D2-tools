# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath( os.path.join(selfDir, '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import logging
import traceback

import numpy as np  # Used for validation
from pubsub import pub

import wx
from wx.lib.combotreebox import ComboTreeBox as wx_ComboTreeBox

from . import DANodalValueList
from DAConfig.DAConfig import DAConfig

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Panel.Values")

class Validator:
    class DummyGrids:
        """
        __getattr__ guards against hidden methods
        """
        def __getattr__(self, name):
            raise AttributeError("Expression validation (grids): grids has no attribute '%s'" % name)
        """
        Visible methods
        """
        def __getitem__(self, idxs):
            if not isinstance(idxs, int): raise RuntimeError('Expression validation (grids): Invalid grid index type')
            return Validator.DummyGrid()
    class DummyGrid:
        """
        __getattr__ guards against hidden methods
        """
        def __getattr__(self, name):
            raise AttributeError("Expression validation (grid): fields has no attribute '%s'" % name)
        """
        Visible methods
        """
        def integrate(self, *args, **kwargs):
            if len(args)   != 1: raise RuntimeError('Expression validation (grid.integrate): One and exactly one argument expected')
            if len(kwargs) != 0: raise RuntimeError('Expression validation (grid.integrate): No keyword args expected')
        def ddx(self, *args, **kwargs):
            if len(args)   != 1: raise RuntimeError('Expression validation (grid.ddx): One and exactly one argument expected')
            if len(kwargs) != 0: raise RuntimeError('Expression validation (grid.ddx): No keyword args expected')
        def ddy(self, *args, **kwargs):
            if len(args)   != 1: raise RuntimeError('Expression validation (grid.ddy): One and exactly one argument expected')
            if len(kwargs) != 0: raise RuntimeError('Expression validation (grid.ddy): No keyword args expected')
    class DummyFields:
        """
        __getattr__ guards against hidden methods
        """
        def __getattr__(self, name):
            raise AttributeError("Expression validation (fields): fields has no attribute '%s'" % name)
        """
        Visible methods
        """
        def __getitem__(self, idx):
            if not isinstance(idx, int): raise RuntimeError('Expression validation (fields): Invalid field index type')
            return Validator.DummyField()
    class DummyField:
        """
        __getattr__ guards against hidden methods
        """
        def __getattr__(self, name):
            raise AttributeError("Expression validation (field): field has no attribute '%s'" % name)
        """
        Visible methods
        """
        def __getitem__(self, idx):
            if not isinstance(idx, int): raise RuntimeError('Expression validation (field): Invalid field index type')
            return np.zeros((1,), dtype=float)
        def getGrid(self):
            return self
        def doInterpolate(self, data, X, Y):
            # if not isinstance(idx, int): raise RuntimeError('Expression validation (field): Invalid field index type')
            return np.zeros((1,), dtype=float)
        # def getData(self):
        #     return self

    def __init__(self, field=None):
        self.factu = field

    def __validate_expr(self, expr):
        try:
            # expr.format va générer une exception si le nbr de valeurs
            # n'est pas adéquat. Ce n'est valable que si l'expression
            # n'accède pas d'autres données avec un autre nbr de valeurs.
            n = self.factu.getNbVal()
            _xpr = expr.format(*list(range(n)))
            _lcl = {
                'grids' : Validator.DummyGrids(),
                'grid'  : Validator.DummyGrid(),
                'datas' : Validator.DummyFields(),
                'data'  : Validator.DummyField(),
                }
            return eval(_xpr, None, _lcl)
        except AttributeError as e:
            newMsg = '%s\nEvaluating "%s"' % (str(e), expr)
            LOGGER.error(newMsg)
            raise type(e)(newMsg)
        except Exception as e:
            newMsg = '%s\nEvaluating "%s"' % (str(e), expr)
            LOGGER.error(newMsg)
            raise type(e)(newMsg)

    def setField(self, field):
        self.factu = field

    def validate_expr(self, expr):
        _ret = self.__validate_expr(expr)
        return True

    def validate_value(self, expr):
        ret = self.__validate_expr(expr)
        return isinstance(ret, bool)


class DAPnlNodVal(wx.Panel):
    help = """\
The panel Values is used to manage the active nodal values.
It allows to select the data set and the active variables (columns).
It allows also to filter the variables on their value.
"""
    hxpr = """\
Expression must be a valid Python expression.
To get access to data:
    - "{n}" is the placeholder for the "nth" value of the current "Value";
    - "data[n]" (new syntax)
Pre-defined local variables are:
    - "datas": list of all the "Values"
    - "data":  current "Value"
    - "grids": list of all the "Grids"
    - "grid":  current "Grid"
    - "np" is shortcut for numpy;

Examples:
    To get the min value between col 0 and 1:
        np.minimum({0}, {1})
    To compute the norm:
        np.sqrt({0}*{0} + {1}*{1}) or
        np.hypot({0}, {1})
    To compute the norm of the derivative:
        np.hypot(grid.ddx({0}), grid.ddy({1}))
"""

    hval = """\
Filter on value must be a valid Python expression, yelding an array of truth value.
As in Expression:
    - "{n}" is the placeholder for the nth value of the current "Value";
    - "data[n]"
In addition:
    - cnv: stands for "computed nodal value" or "current nodal value",
      i.e. the result of Expression
Pre-defined local variables are:
    - "datas": list of all the "Values"
    - "data":  current "Value"
    - "grids": list of all the "Grids"
    - "grid":  current "Grid"
    - "np" is shortcut for numpy;

Examples:
    To keep only positive values:
        {0} > 0.0
"""
    def __init__(self, *args, **kwds):
        self.cb_on_data_change = kwds.pop('cb_on_data_change', None)
        super().__init__(*args, **kwds)

        self.sbx_data = wx.StaticBox   (self, wx.ID_ANY, "Value")
        self.cmb_data = wx_ComboTreeBox(self, wx.ID_ANY, style=wx.CB_DROPDOWN | wx.CB_READONLY)

        self.lst_nodvl_fid = DANodalValueList.DANodalValueListCtrl(self, wx.ID_ANY, auto_extend=True, enable_edit=True)
        self.lst_nodvl_fvl = DANodalValueList.DANodalValueListCtrl(self, wx.ID_ANY, title="Filter on values", enable_edit=True)

        self.sbx_user_flt  = wx.StaticBox(self, wx.ID_ANY, "User defined data filter")
        self.cmb_user_flt  = wx.ComboBox (self, wx.ID_ANY, choices=['NIL'], style=wx.CB_DROPDOWN | wx.CB_READONLY)

        self.fields= []
        self.edtbls= {}     # for each field, the editables
        self.gactu = None
        self.factu = None
        self.fprev = None
        self.validator = Validator()

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_COMBOBOX, self.on_cmb_data, self.cmb_data)

    def __set_properties(self):
        self.lst_nodvl_fid.SetValidator(self.validator.validate_expr)
        self.lst_nodvl_fvl.SetValidator(self.validator.validate_value)

        item = self.cmb_data.Append('NIL')
        #self.cmb_data.SetSelection(item)
        self.cmb_data.SetToolTip("Set the active field")

        self.SetToolTip(DAPnlNodVal.help)
        self.lst_nodvl_fid.SetColumnToolTip(0, DAPnlNodVal.hxpr)
        self.lst_nodvl_fvl.SetColumnToolTip(0, DAPnlNodVal.hval)

        self.cmb_user_flt.SetSelection(0)
        self.cmb_user_flt.SetToolTip("Select a user defined filter. A filter is Python class inheriting from DADataFilter")
        #self.lst_nodvl_fvl.Enable(False)

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)

        szr_data = wx.StaticBoxSizer(self.sbx_data, wx.VERTICAL)
        szr_data.Add(self.cmb_data, 0, wx.EXPAND, 0)

        szr_user = wx.StaticBoxSizer(self.sbx_user_flt, wx.VERTICAL)
        szr_user.Add(self.cmb_user_flt, 0, wx.EXPAND, 0)

        szr_main.Add(szr_data,           0, wx.EXPAND, 0)
        szr_main.Add(self.lst_nodvl_fid, 1, wx.EXPAND, 0)
        szr_main.Add(self.lst_nodvl_fvl, 1, wx.EXPAND, 0)
        szr_main.Add(szr_user,           0, wx.EXPAND, 0)

        self.SetSizer(szr_main)
        self.Layout()
        self.configChange()

    def __clear_tree(self):
        """
        ComboTreeBox.Clear do clear the root, even
        with wx.TR_HIDE_ROOT.
        """
        tree = self.cmb_data.GetTree()
        tree.DeleteAllItems()
        tree.AddRoot('')

    def __fill_tree(self):
        """
        Fill the tree.
        Tree items can not be activated individualy. As a
        workaround, mark the arctiv grid with a '*'.
        """
        self.__clear_tree()
        grids = set( [ f.getGrid() for f in self.fields] )
        if grids:
            for g in sorted(grids, key=lambda g: g.getShortName()):
                deco  = '* ' if g is self.gactu else ''
                gname = '%s%s' % (deco, g.getShortName()) if g else 'None - grid not set'
                item = self.cmb_data.Append(gname)
                for i, f in enumerate(self.fields):
                    if f.getGrid() is g:
                        jtem = self.cmb_data.Append('%s' % f.getShortName(), parent=item)
                        self.cmb_data.SetClientData(jtem, i)
        else:
            item = self.cmb_data.Append('NIL')

    def __fill_data(self):
        if self.fprev:
            edits = []
            for row in range(self.lst_nodvl_fid.GetItemCount()):
                isEdit = self.lst_nodvl_fid.GetItemData(row) != 0
                if isEdit:
                    edtTxt = self.lst_nodvl_fid.GetItemText(row)
                    if edtTxt: edits.append(edtTxt)
            self.edtbls[self.fprev] = edits

        self.lst_nodvl_fid.DeleteAllItems()
        if self.factu:
            for i in range( self.factu.getNbVal() ):
                text = '%s' % self.factu.names[i] if i < len(self.factu.names) else '%s' % i
                self.lst_nodvl_fid.AppendItem(text = text, checked = True, editable = False)
            try:
                for text in self.edtbls[self.factu]:
                    self.lst_nodvl_fid.AppendItem(text = text, checked = False, editable = True)
            except KeyError:
                pass
            self.lst_nodvl_fid.AppendItem(checked = False, editable = True)
            self.lst_nodvl_fid.Enable(True)

            self.cmb_data.SetToolTip('%s' % self.factu.getLongName())
        else:
            self.lst_nodvl_fid.Enable(False)
        self.validator.setField(self.factu)

    def on_cmb_data(self, event):
        itm = self.cmb_data.GetSelection()
        try:
            idx = self.cmb_data.GetClientData(itm)
            field = self.fields[idx]
            if field.getGrid() is self.gactu:
                self.fprev = self.factu
                self.factu = self.fields[idx]
                self.__fill_data()
                if self.cb_on_data_change:
                    self.cb_on_data_change(self.factu)
            else:
                errMsg = 'Invalid selection. Grid is not activ.'
                pub.sendMessage('statusbar.flashmessage', message=errMsg)
        except Exception as e:
            errMsg = '%s\n%s' % (str(e), traceback.format_exc())
            print('DAPnlNodVal.on_cmb_data')
            print(errMsg)
            pass

    def asgGrid(self, grid):
        #if self.factu and grid is self.factu.getGrid(): return
        assert not self.factu or self.factu.getGrid() is self.gactu
        if self.gactu and grid is self.gactu: return

        # ---  Search first field of grid
        field = None
        for f in self.fields:
            if f.getGrid() is grid:
                field = f
                break

        # ---  Fill tree
        self.gactu = grid
        self.__fill_tree()

        # ---  Assign
        if field:
            self.fprev = self.factu
            self.factu = field
            item = self.cmb_data.FindString('%s' % self.factu.getShortName())
            self.cmb_data.SetSelection(item)
            self.cmb_data.Enable(True)
            if self.cb_on_data_change:
                self.cb_on_data_change(self.factu)
        else:
            self.fprev = self.factu
            self.factu = None
            self.cmb_data.Enable(False)
            if self.cb_on_data_change:
                self.cb_on_data_change(self.factu)
        self.__fill_data()

    def asgDatas(self, fields):
        assert not self.factu or self.factu.getGrid() is self.gactu

        self.fields = fields
        self.__fill_tree()
        # ---  Select the field
        if not self.fields:
            self.factu = None
        elif self.factu:
            idx = self.fields.index(self.factu)
            item = self.cmb_data.FindClientData(idx)
            self.cmb_data.SetSelection(item)
            self.cmb_data.SetToolTip('%s' % self.factu.getLongName())
        elif self.gactu:
            field = None
            for f in self.fields:
                if f.getGrid() is self.gactu:
                    field = f
            self.fprev = self.factu
            self.factu = field
            if self.factu:
                idx = self.fields.index(self.factu)
                item = self.cmb_data.FindClientData(idx)
                self.cmb_data.SetSelection(item)
                self.cmb_data.SetToolTip('%s' % self.factu.getLongName())
                if self.cb_on_data_change:
                    self.cb_on_data_change(self.factu)
        else:
            raise RuntimeError('DAPnlNodVal, no grid, no field')
        self.cmb_data.GetTree().ExpandAll()
        self.cmb_data.Enable(bool(self.gactu))
        self.__fill_data()

    def getActivValOps(self):
        ops = self.lst_nodvl_fid.BuildOpList()
        usr = self.cmb_user_flt.GetSelection()
        if self.factu:
            grids = list( set( [f.getGrid() for f in self.fields] ) )
            grid  = self.factu.getGrid()
            for op in ops:
                op.setLocals(grids=grids, grid=grid, datas=self.fields, data=self.factu)
            if usr > 0:
                gf = self.cmb_sizes_flt.GetClientData(usr)
                # fltr = loadFilter(gf[1], gf[2])
                # sgrid = fltr.filter(self.grid)
                # if self.data:
                #     field = DAFrameField.as2DFiniteElementSubGridField(sgrid, self.data)
                # else:
                #     field = DAFrameField.as2DFiniteElementSubGridField(sgrid)
        return ops

    def getActivValFltrs(self):
        fts = self.lst_nodvl_fvl.BuildOpList()
        if self.factu:
            grids = list( set( [f.getGrid() for f in self.fields] ) )
            grid  = self.factu.getGrid()
            for ft in fts:
                ft.setLocals(grids=grids, grid=grid, datas=self.fields, data=self.factu)
        return fts

    def getActivNodNames(self):
        return self.lst_nodvl_fid.BuildNameList()

    def configChange(self):
        self.cmb_user_flt.Clear()
        cfg = DAConfig()
        self.cmb_user_flt.Append('NIL', None)
        for gf in cfg.readAllDataFilter():
            self.cmb_user_flt.Append(gf[0], gf)
        self.cmb_user_flt.SetSelection(0)

#==============================================================================
#
#==============================================================================
if __name__ == "__main__":
    class MyDialogBox(wx.Dialog):
        def __init__(self, *args, **kwargs):
            kwargs["style"] = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.MAXIMIZE_BOX|wx.SYSTEM_MENU|wx.RESIZE_BORDER|wx.CLIP_CHILDREN
            super().__init__(*args, **kwargs)
            self.pnl = DAPnlNodVal(self)
            self.btn_cancel = wx.Button(self, wx.ID_CANCEL, "")
            self.btn_ok     = wx.Button(self, wx.ID_OK, "")

            szr_frm = wx.BoxSizer(wx.VERTICAL)
            szr_frm.Add(self.pnl, 1, wx.EXPAND)

            szr_btn = wx.BoxSizer(wx.HORIZONTAL)
            szr_btn.Add(self.btn_cancel,0, wx.EXPAND, 0)
            szr_btn.Add(self.btn_ok,    0, wx.EXPAND, 0)
            szr_frm.Add(szr_btn, 0, wx.EXPAND, 0)

            self.SetSizer(szr_frm)
            self.Layout()

    class MyApp(wx.App):
        def OnInit(self):
            dlg = MyDialogBox(None)
            if dlg.ShowModal() == wx.ID_OK:
                print('OK')
            return True

    app = MyApp(False)
    app.MainLoop()
