# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath( os.path.join(selfDir, '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)
    supPath = os.path.normpath( os.path.join(selfDir, '..', '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import logging
import wx
from wx.lib.combotreebox import ComboTreeBox as wx_ComboTreeBox

from CTCommon import FEMesh
from DAFrameGrid       import DAFrameGrid
from DAFrameField      import DAFrameField
from DARegGrid         import DARegGrid, RgridEvents
from DAConfig.DAConfig import DAConfig
from .DAFilter      import loadFilter
from .DAFilterList  import DAFilterListCtrl
from .DARegGridList import DARegGridList

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Panel.Grids")

class DAPnlGrids(wx.Panel):
    hlp = """\
The panel Grid is used to manage the active grid/nodes.
It allows to select the source grid, to filter nodes
either on node number or on position. It allows also
to create/edit a regular grid.

Expression for node filtering must be a valid Python
logical expression, where:
    {0} is the placeholder for node number
    {1} is the placeholder for x coordinate
    {2} is the placeholder for y coordinate

Examples:
    To get nodes from 1 to 999: {0} < 1000
    To filter on bbox: {1} > 0 and {1} < 1 and {2} > 0 and {2} < 1
"""

    def __init__(self, *args, **kwds):
        self.cb_on_grid_change = kwds.pop('cb_on_grid_change', None)
        self.cb_on_rgrid_event = kwds.pop('cb_on_rgrid_event', None)
        super().__init__(*args, **kwds)

        self.sbx_grid       = wx.StaticBox   (self, wx.ID_ANY, "grid")
        self.cmb_grid       = wx_ComboTreeBox(self, wx.ID_ANY, style=wx.CB_DROPDOWN | wx.CB_READONLY)

        self.sbx_sizes      = wx.StaticBox (self, wx.ID_ANY, "size")
        self.stx_sizes_nnod = wx.StaticText(self, wx.ID_ANY, "nodes:")
        self.txt_sizes_nnod = wx.TextCtrl  (self, wx.ID_ANY, "0", style=wx.TE_READONLY)
        self.stx_sizes_nele = wx.StaticText(self, wx.ID_ANY, "elements:")
        self.txt_sizes_nele = wx.TextCtrl  (self, wx.ID_ANY, "0", style=wx.TE_READONLY)

        self.sbx_bbgr       = wx.StaticBox (self, wx.ID_ANY, "bbox grid")
        self.sbx_bbgr_min   = wx.StaticBox (self, wx.ID_ANY, "min")
        self.sbx_bbgr_max   = wx.StaticBox (self, wx.ID_ANY, "max")
        self.txt_bbgr_xmin  = wx.TextCtrl  (self, wx.ID_ANY, "-1", style=wx.TE_READONLY)
        self.txt_bbgr_ymin  = wx.TextCtrl  (self, wx.ID_ANY, "-1", style=wx.TE_READONLY)
        self.txt_bbgr_xmax  = wx.TextCtrl  (self, wx.ID_ANY, "1",  style=wx.TE_READONLY)
        self.txt_bbgr_ymax  = wx.TextCtrl  (self, wx.ID_ANY, "1",  style=wx.TE_READONLY)

        self.sbx_bbpj       = wx.StaticBox (self, wx.ID_ANY, "bbox project")
        self.sbx_bbpj_min   = wx.StaticBox (self, wx.ID_ANY, "min")
        self.sbx_bbpj_max   = wx.StaticBox (self, wx.ID_ANY, "max")
        self.txt_bbpj_xmin  = wx.TextCtrl  (self, wx.ID_ANY, "-1", style=wx.TE_READONLY)
        self.txt_bbpj_ymin  = wx.TextCtrl  (self, wx.ID_ANY, "-1", style=wx.TE_READONLY)
        self.txt_bbpj_xmax  = wx.TextCtrl  (self, wx.ID_ANY, "1",  style=wx.TE_READONLY)
        self.txt_bbpj_ymax  = wx.TextCtrl  (self, wx.ID_ANY, "1",  style=wx.TE_READONLY)

        self.lst_sizes_fid  = DAFilterListCtrl(self, wx.ID_ANY, title="Filter on nodes",  enable_edit=True, history='H2D2: DataAnalyzer: Filter grid on nodes')
        self.lst_sizes_fbb  = DAFilterListCtrl(self, wx.ID_ANY, title="Filter on coord.", enable_edit=True, history='H2D2: DataAnalyzer: Filter grid on coord')
        self.sbx_sizes_flt  = wx.StaticBox    (self, wx.ID_ANY, "User defined grid filter")
        self.cmb_sizes_flt  = wx.ComboBox     (self, wx.ID_ANY, choices=['NIL'], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.lst_sizes_reg  = DARegGridList   (self, wx.ID_ANY, title="Regular Grid",
                                                                history='H2D2: DataAnalyzer: Regular grid',
                                                                cb_on_rgrid_event=self.onRegGridEvent)

        #self.__create_menus()
        self.__set_properties()
        self.__do_layout()
        self.configChange()

        self.grids= []
        self.grid = None
        self.data = None

        self.Bind(wx.EVT_COMBOBOX,              self.onComboGrid,   self.cmb_grid)
        self.Bind(wx.EVT_TREE_ITEM_RIGHT_CLICK, self.onComboRclick, self.cmb_grid)
        ##self.Bind(wx.EVT_MENU,     self.onMenuGridDelete,   self.mnu_grd_del)
        #self.Bind(wx.EVT_HELP, self.onHelp)

    def __create_menus(self):
        self.mnu_grd = wx.Menu()
        #self.mnu_grd_rld  = wx.MenuItem(self.mnu_grd, wx.ID_ANY, "Reload",  "Reload the selected grid",     wx.ITEM_NORMAL)
        self.mnu_grd_del  = wx.MenuItem(self.mnu_grd, wx.ID_ANY, "Delete",  "Delete the selected grid",     wx.ITEM_NORMAL)
        #self.mnu_grd.Append(self.mnu_lyr_rld)
        self.mnu_grd.Append(self.mnu_lyr_del)

    def __set_properties(self):
        self.SetMinSize((-1, -1))
        self.txt_bbgr_xmin.SetToolTip("Bounding box of the active grid in grid coordinates")
        self.txt_bbgr_xmax.SetToolTip("Bounding box of the active grid in grid coordinates")
        self.txt_bbgr_ymin.SetToolTip("Bounding box of the active grid in grid coordinates")
        self.txt_bbgr_ymax.SetToolTip("Bounding box of the active grid in grid coordinates")
        self.txt_bbpj_xmin.SetToolTip("Bounding box of the active grid in project coordinates")
        self.txt_bbpj_xmax.SetToolTip("Bounding box of the active grid in project coordinates")
        self.txt_bbpj_ymin.SetToolTip("Bounding box of the active grid in project coordinates")
        self.txt_bbpj_ymax.SetToolTip("Bounding box of the active grid in project coordinates")

        item = self.cmb_grid.Append('NIL')
        self.cmb_grid.SetSelection(item)
        self.cmb_grid.SetToolTip("Select the active grid")

        self.lst_sizes_fid.SetValidator(self.__expr_validator_index)
        self.lst_sizes_fbb.SetValidator(self.__expr_validator_coord)
        # self.lst_nodvl_fvl.SetValidator(self.__expr_validator_value)
        self.cmb_sizes_flt.SetSelection(0)
        self.lst_sizes_fid.SetColumnToolTip(0, 'Filter the nodes on their index (0 based)')
        self.lst_sizes_fbb.SetColumnToolTip(0, 'Filter the nodes on their coordinates')
        self.cmb_sizes_flt.SetToolTip('Select a user defined filter. A filter is Python class inheriting from DAGridFilter')

        self.SetToolTip(DAPnlGrids.hlp)

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)

        szr_grid = wx.StaticBoxSizer(self.sbx_grid, wx.VERTICAL)
        szr_grid.Add(self.cmb_grid, 0, wx.EXPAND, 0)

        szr_sizes = wx.StaticBoxSizer(self.sbx_sizes, wx.VERTICAL)
        szr_sizes_top = wx.GridSizer(2, 2, 0, 0)
        szr_sizes_top.Add(self.stx_sizes_nnod, 0, 0, 0)
        szr_sizes_top.Add(self.txt_sizes_nnod, 0, wx.EXPAND, 0)
        szr_sizes_top.Add(self.stx_sizes_nele, 0, 0, 0)
        szr_sizes_top.Add(self.txt_sizes_nele, 0, wx.EXPAND, 0)
        szr_sizes.Add(szr_sizes_top, 0, wx.EXPAND, 0)

        szr_bbgr = wx.StaticBoxSizer(self.sbx_bbgr, wx.VERTICAL)
        szr_bbgr_min = wx.StaticBoxSizer(self.sbx_bbgr_min, wx.HORIZONTAL)
        szr_bbgr_min.Add(self.txt_bbgr_xmin, 1, wx.EXPAND, 0)
        szr_bbgr_min.Add(self.txt_bbgr_ymin, 1, wx.EXPAND, 0)
        szr_bbgr.Add(szr_bbgr_min, 0, wx.EXPAND, 0)
        szr_bbgr_max = wx.StaticBoxSizer(self.sbx_bbgr_max, wx.HORIZONTAL)
        szr_bbgr_max.Add(self.txt_bbgr_xmax, 1, wx.EXPAND, 0)
        szr_bbgr_max.Add(self.txt_bbgr_ymax, 1, wx.EXPAND, 0)
        szr_bbgr.Add(szr_bbgr_max, 0, wx.EXPAND, 0)

        szr_bbpj = wx.StaticBoxSizer(self.sbx_bbpj, wx.VERTICAL)
        szr_bbpj_min = wx.StaticBoxSizer(self.sbx_bbpj_min, wx.HORIZONTAL)
        szr_bbpj_min.Add(self.txt_bbpj_xmin, 1, wx.EXPAND, 0)
        szr_bbpj_min.Add(self.txt_bbpj_ymin, 1, wx.EXPAND, 0)
        szr_bbpj.Add(szr_bbpj_min, 0, wx.EXPAND, 0)
        szr_bbpj_max = wx.StaticBoxSizer(self.sbx_bbpj_max, wx.HORIZONTAL)
        szr_bbpj_max.Add(self.txt_bbpj_xmax, 1, wx.EXPAND, 0)
        szr_bbpj_max.Add(self.txt_bbpj_ymax, 1, wx.EXPAND, 0)
        szr_bbpj.Add(szr_bbpj_max, 0, wx.EXPAND, 0)

        szr_sizes_flt = wx.StaticBoxSizer(self.sbx_sizes_flt, wx.VERTICAL)
        szr_sizes_flt.Add(self.cmb_sizes_flt, 0, wx.EXPAND, 0)

        szr_main.Add(szr_grid,  0, wx.EXPAND, 0)
        szr_main.Add(szr_sizes, 0, wx.EXPAND, 0)
        szr_main.Add(szr_bbgr,  0, wx.EXPAND, 0)
        szr_main.Add(szr_bbpj,  0, wx.EXPAND, 0)
        szr_main.Add(self.lst_sizes_fid, 2, wx.EXPAND, 0)
        szr_main.Add(self.lst_sizes_fbb, 2, wx.EXPAND, 0)
        szr_main.Add(szr_sizes_flt,      0, wx.EXPAND, 0)
        szr_main.AddSpacer(10)
        szr_main.Add(self.lst_sizes_reg, 2, wx.EXPAND, 0)
        self.SetSizer(szr_main)
        self.Layout()

    def __expr_validator_index(self, expr):
        try:
            ret = eval( expr.format(1) )
            return isinstance(ret, bool)
        except:
            return False

    def __expr_validator_coord(self, expr):
        try:
            ret = eval( expr.format(1, 1.0, 1.0) )
            return isinstance(ret, bool)
        except:
            return False

    def __clear_tree(self):
        """
        ComboTreeBox.Clear do clear the root, even
        with wx.TR_HIDE_ROOT.
        """
        tree = self.cmb_grid.GetTree()
        tree.DeleteAllItems()
        tree.AddRoot('')

    def __fill_nodes(self):
        if self.grid:
            nbNod = self.grid.getNbNodes()
            nbEle = self.grid.getNbElements()
            self.txt_sizes_nnod.SetValue('%i' % nbNod)
            self.txt_sizes_nele.SetValue('%i' % nbEle)

            x1, y1, x2, y2 = self.grid.getGrid().getBbox()
            self.txt_bbgr_xmin.SetValue('%s' % x1)
            self.txt_bbgr_ymin.SetValue('%s' % y1)
            self.txt_bbgr_xmax.SetValue('%s' % x2)
            self.txt_bbgr_ymax.SetValue('%s' % y2)
            if self.grid.hasProjection():
                x1, y1, x2, y2 = self.grid.getBbox()
                self.txt_bbpj_xmin.SetValue('%s' % x1)
                self.txt_bbpj_ymin.SetValue('%s' % y1)
                self.txt_bbpj_xmax.SetValue('%s' % x2)
                self.txt_bbpj_ymax.SetValue('%s' % y2)
            else:
                self.txt_bbpj_xmin.SetValue('---')
                self.txt_bbpj_ymin.SetValue('---')
                self.txt_bbpj_xmax.SetValue('---')
                self.txt_bbpj_ymax.SetValue('---')
            self.cmb_grid.SetToolTip('%s' % self.grid.getLongName())
        elif self.data:
            nbNod = self.data.getNbNodes()
            self.txt_sizes_nnod.SetValue('%s' % nbNod)
            self.txt_sizes_nele.SetValue('---')
            self.txt_bbgr_xmin.SetValue('---')
            self.txt_bbgr_ymin.SetValue('---')
            self.txt_bbgr_xmax.SetValue('---')
            self.txt_bbgr_ymax.SetValue('---')
        else:
            self.txt_sizes_nnod.SetValue('---')
            self.txt_sizes_nele.SetValue('---')
            self.txt_bbgr_xmin.SetValue('---')
            self.txt_bbgr_ymin.SetValue('---')
            self.txt_bbgr_xmax.SetValue('---')
            self.txt_bbgr_ymax.SetValue('---')

    def onRegGridEvent(self, cmd: RgridEvents, rgrid: DARegGrid):
        if self.cb_on_rgrid_event:
            self.cb_on_rgrid_event(cmd, rgrid)

    def onComboRclick(self, event):
        self.gridDeleted = False
        item = event.GetItem()
        # name = self.cmb_grid.GetClientData(item)
        self.cmb_grid.SelectItem(item, select=True) # set focus to line
        self.PopupMenu(self.mnu_grd)
        if self.gridDeleted:
            event.Veto()
            if self.cb_on_grid_change:
                self.cb_on_grid_change(self.getParentGrid())

    def onComboGrid(self, event):
        item = self.cmb_grid.GetSelection()
        name = self.cmb_grid.GetClientData(item)
        self.grid = next(g for g in self.grids if g.getLongName() == name)
        # Don't reset data. If needed, changes will be done in asgData()
        # self.data = None
        self.__fill_nodes()
        if self.cb_on_grid_change:
            self.cb_on_grid_change(self.getParentGrid())

    def onMenuGridDelete(self, event):
        dlg = wx.MessageDialog(self, 'Are you sure? \nThis will also delete all data for this grid.', 'Delete grid', wx.YES_NO)
        if (dlg.ShowModal() == wx.ID_YES):
            leaf = self.cmb_grid.GetSelection()
            self.cmb_grid.SelectItem(leaf, select=False)

    def onHelp(self, event):
        #hlp = wx.HelpProvider.Get().GetHelp(self)
        print('DAPnlGrids.on_evt_help', self.hlp)
        # event.Skip()

    def appendSubGrids(self, grid, subGrids):
        _subGrids = [subGrids] if isinstance(subGrids, DAFrameGrid) else subGrids
        gridId = self.cmb_grid.FindClientData(grid.getLongName())
        for subGrid in _subGrids:
            self.cmb_grid.Append(subGrid.getShortName(), clientData=subGrid.getLongName(), parent=gridId)
        self.cmb_grid.GetTree().ExpandAll()
        self.grids.extend(_subGrids)

    def appendGrid(self, grid):
        # ---  Erase NIL if exist
        if self.cmb_grid.GetCount() == 1 and self.cmb_grid.FindString('NIL').IsOk():
            self.__clear_tree()
        # ---  Append to combo and set selection to last
        itemId = self.cmb_grid.Append(grid.getShortName(), clientData=grid.getLongName())
        self.cmb_grid.SetSelection(itemId)
        self.cmb_grid.GetTree().ExpandAll()
        # ---  Append to self
        self.grids.append(grid)
        gridChange = (self.grid != self.grids[-1])
        if gridChange:
            self.grid = self.grids[-1]
            if self.cb_on_grid_change:
                self.cb_on_grid_change(self.grid)
        self.__fill_nodes()

    def asgRgrids(self, rgrids):
        for rg in rgrids:
            self.lst_sizes_reg.appendRgrid(rg)

    def asgGrids(self, grids):
        self.grids = grids
        self.__clear_tree()
        if (self.data and not self.grid) or not self.grids:
            itemId = self.cmb_grid.Append("NIL")
            self.cmb_grid.SetSelection(itemId)

        itemIds = []
        for g in self.grids:
            itemId = self.cmb_grid.Append(g.getShortName(), clientData=g.getLongName())
            itemIds.append(itemId)
        for itemId in itemIds:
            if self.cmb_grid.GetClientData(itemId) == self.grid.getLongName():
                self.cmb_grid.SetSelection(itemId)
                break

        # gridChange = not self.grid                                    # change only on first
        gridChange = not self.grids or (self.grid != self.grids[-1])    # set to last grid
        if gridChange:
            self.grid = self.grids[-1] if self.grids else None
        if gridChange:
            if self.cb_on_grid_change:
                self.cb_on_grid_change(self.grid)

        self.cmb_grid.GetTree().ExpandAll()
        self.__fill_nodes()

    def asgData(self, data):
        if data and self.grid: assert data.grid is self.getParentGrid()
        self.data = data
        self.__fill_nodes()

    def asgPage(self, page, ax):
        self.lst_sizes_reg.asgPage(page, ax)

    def __getActivNodes(self):
        if self.grid:
            nodes = self.grid.getNodes()
            keep1 = self.lst_sizes_fid.FilterListOnValue(nodes)
            keep2 = self.lst_sizes_fbb.FilterListOnValue(nodes)
            #keep3 = self.lst_nodvl_fvl.FilterListOnValue(nodes)
            keep3 = None
        elif self.data:
            xy = (float("inf"), float("inf"))
            nodes = [ xy for i in range(self.data.getNbNodes()) ]
            keep1 = self.lst_sizes_fid.FilterListOnIndex(nodes)
            keep2 = None
            keep3 = None
        else:
            keep1 = None
            keep2 = None
            keep3 = None

        set = None
        if keep1 or keep2 or keep3: set = {}
        if keep1:
            for it in keep1: set[it] = None
        if keep2:
            for it in keep2: set[it] = None
        if keep3:
            for it in keep3: set[it] = None
        return sorted(set.keys()) if set is not None else None

    def getGrid(self):
        return self.grid

    def getParentGrid(self):
        ig = self.grids.index(self.grid)
        while self.grids[ig].getGrid().isSubMesh():
            ig -= 1
        return self.grids[ig]

    def getField(self, includePartialHit=True):
        """
        getField

        Args:
            includePartialHit (bool): If True, partial elements
            are included.
        """
        rgrid, rname = self.lst_sizes_reg.getRegGrid()
        usrfl = self.cmb_sizes_flt.GetSelection()
        if isinstance(rgrid, FEMesh.FE1DRegularGrid):
            fgrid = DAFrameGrid(rgrid, shortName=rname, longName=rname)
            field = DAFrameField.as1DRegularGridField(fgrid, self.data)
        elif isinstance(rgrid, FEMesh.FE2DRegularGrid):
            fgrid = DAFrameGrid(rgrid, shortName=rname, longName=rname)
            field = DAFrameField.as2DRegularGridField(fgrid, self.data)
        elif usrfl > 0:
            gf = self.cmb_sizes_flt.GetClientData(usrfl)
            fltr = loadFilter(gf[1], gf[2])
            sgrid = fltr.filter(self.grid)
            sname = fltr.__module__
            lname = '%s @ %s' % (sname, self.grid.getShortName())
            sgrid = DAFrameGrid(sgrid, parent=self.grid, shortName=sname, longName=lname)
            if self.data:
                field = DAFrameField.as2DFiniteElementSubGridField(sgrid, self.data)
            else:
                field = DAFrameField.as2DFiniteElementSubGridField(sgrid)
        elif not rgrid:
            nodes = self.__getActivNodes()
            # print('DAPnlGrids.getField.nodes', len(nodes) if nodes else nodes)
            if nodes:
                fltr = FEMesh.SubGridFilterOnNodes(keep=nodes)
                sgrid = self.grid.genSubGrid(fltr, includePartialHit)
                sname = 'Filter on nodes'
                lname = '%s @ %s' % (sname, self.grid.getShortName())
                sgrid = DAFrameGrid(sgrid, parent=self.grid, shortName=sname, longName=lname)
                # print('DAPnlGrids.getField.sgrid', type(sgrid))
                if self.data:
                    field = DAFrameField.as2DFiniteElementSubGridField(sgrid, self.data)
                else:
                    field = DAFrameField.as2DFiniteElementSubGridField(sgrid)
            elif self.grid.isSubMesh():
                if self.data:
                    field = DAFrameField.as2DFiniteElementSubGridField(self.grid, self.data)
                else:
                    field = DAFrameField.as2DFiniteElementSubGridField(self.grid)
            else:
                if self.data:
                    field = self.data
                else:
                    field = DAFrameField.as2DFiniteElementField(grid=self.grid)
        else:
            raise ValueError('Not a valid mesh type')
        return field

    def configChange(self):
        self.cmb_sizes_flt.Clear()
        cfg = DAConfig()
        self.cmb_sizes_flt.Append('NIL', None)
        for gf in cfg.readAllGridFilter():
            self.cmb_sizes_flt.Append(gf[0], gf)
        self.cmb_sizes_flt.SetSelection(0)

    def refreshData(self):
        self.__fill_nodes()

#==============================================================================
#
#==============================================================================
if __name__ == "__main__":
    class MyDialogBox(wx.Dialog):
        def __init__(self, *args, **kwargs):
            kwargs["style"] = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.MAXIMIZE_BOX|wx.SYSTEM_MENU|wx.RESIZE_BORDER|wx.CLIP_CHILDREN
            super().__init__(*args, **kwargs)
            self.pnl = DAPnlGrids(self)
            self.btn_cancel = wx.Button(self, wx.ID_CANCEL, "")
            self.btn_ok     = wx.Button(self, wx.ID_OK, "")

            szr_frm = wx.BoxSizer(wx.VERTICAL)
            szr_frm.Add(self.pnl, 1, wx.EXPAND)

            szr_btn = wx.BoxSizer(wx.HORIZONTAL)
            szr_btn.Add(self.btn_cancel,0, wx.EXPAND, 0)
            szr_btn.Add(self.btn_ok,    0, wx.EXPAND, 0)
            szr_frm.Add(szr_btn, 0, wx.EXPAND, 0)

            self.SetSizer(szr_frm)
            self.Layout()

    class MyApp(wx.App):
        def OnInit(self):
            dlg = MyDialogBox(None)
            if dlg.ShowModal() == wx.ID_OK:
                print('OK')
            return True

    app = MyApp(False)
    app.MainLoop()

