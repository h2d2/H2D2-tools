# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from datetime import timedelta
import logging
import wx

from . import DAFilterList
from . import DANodalValueList
try:
    from CTCommon.DTReducOperation    import REDUCTION_OPERATION as Operation
except ImportError:
    from CTCommon.DTReducOperation_pp import REDUCTION_OPERATION as Operation

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Panel.Times")

class DAPnlTimes(wx.Panel):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self.sbx_value= wx.StaticBox (self, wx.ID_ANY, "Value")
        self.txt_value= wx.TextCtrl  (self, wx.ID_ANY, "---", style=wx.TE_READONLY)
        self.sbx_times= wx.StaticBox (self, wx.ID_ANY, "")
        self.stx_tmin = wx.StaticText(self, wx.ID_ANY, "t min:")
        self.txt_tmin = wx.TextCtrl  (self, wx.ID_ANY, "0", style=wx.TE_READONLY)
        self.stx_tmax = wx.StaticText(self, wx.ID_ANY, "t max:")
        self.txt_tmax = wx.TextCtrl  (self, wx.ID_ANY, "-1", style=wx.TE_READONLY)
        self.stx_nsteps = wx.StaticText(self, wx.ID_ANY, "n steps:")
        self.txt_nsteps = wx.TextCtrl  (self, wx.ID_ANY, "0", style=wx.TE_READONLY)
        self.lst_fid = DAFilterList.DAFilterListCtrl(self, wx.ID_ANY, title="Filter on id",   history='H2D2: DataAnalyzer: Time series id filter',   enable_edit=True)
        self.lst_fvl = DAFilterList.DAFilterListCtrl(self, wx.ID_ANY, title="Filter on time", history='H2D2: DataAnalyzer: Time series time filter', enable_edit=True)
        self.lst_xpr = DANodalValueList.DAExpressionListCtrl(self, wx.ID_ANY, history='H2D2: DataAnalyzer: Time series expressions', enable_edit=True)
        self.lst_rdc = wx.ComboBox(self, choices=['%s' % op for op in Operation])

        self.__set_properties()
        self.__do_layout()

        self.fields= { }
        self.field = None

    def __set_properties(self):
        self.SetMinSize((-1, -1))

        self.lst_fid.SetValidator(self.__is_valid_index)
        self.lst_fvl.SetValidator(self.__is_valid_value)
        self.lst_xpr.SetValidator(self.__is_valid_expression)

        self.lst_fid.InsertItem(0, text = '{0} ==  0', checked = False, editable = False)
        self.lst_fid.InsertItem(1, text = '{0} == -1', checked = False, editable = False)

        self.lst_rdc.SetValue('%s' % Operation['op_noop'])

    def __do_layout(self):
        szr_val = wx.StaticBoxSizer(self.sbx_value, wx.VERTICAL)
        szr_val.Add(self.txt_value, 0, wx.EXPAND, 0)
        szr_top  = wx.StaticBoxSizer(self.sbx_times, wx.VERTICAL)
        szr_data = wx.GridSizer(3, 2, 0, 0)
        szr_data.Add(self.stx_tmin, 0, 0, 0)
        szr_data.Add(self.txt_tmin, 0, wx.EXPAND, 0)
        szr_data.Add(self.stx_tmax, 0, 0, 0)
        szr_data.Add(self.txt_tmax, 0, wx.EXPAND, 0)
        szr_data.Add(self.stx_nsteps, 0, 0, 0)
        szr_data.Add(self.txt_nsteps, 0, wx.EXPAND, 0)
        szr_top.Add(szr_data, 1, wx.EXPAND, 0)

        szr_main = wx.BoxSizer(wx.VERTICAL)
        szr_main.Add(szr_val,      0, wx.EXPAND, 0)
        szr_main.Add(szr_top,      0, wx.EXPAND, 0)
        szr_main.Add(self.lst_fid, 1, wx.EXPAND, 0)
        szr_main.Add(self.lst_fvl, 1, wx.EXPAND, 0)
        szr_main.Add(self.lst_xpr, 1, wx.EXPAND, 10)
        szr_main.Add(self.lst_rdc, 0, wx.EXPAND, 0)

        self.SetSizer(szr_main)
        self.Layout()

    def __is_valid_expression(self, expr):
        try:
            ret = eval( expr )
            _it = iter(ret)     # try to iterate
            return True
        except:
            return False

    def __is_valid_index(self, expr):
        try:
            ret = eval( expr.format(1) )
            return isinstance(ret, bool)
        except:
            return False

    def __is_valid_value(self, expr):
        try:
            ret = eval( expr.format( (1.0) ) )
            return isinstance(ret, bool)
        except:
            return False

    def __fills(self):
        if self.field:
            times = self.field.getTimes()
            self.txt_tmin.SetValue('%s' % times[0])
            self.txt_tmax.SetValue('%s' % times[-1])
            self.txt_nsteps.SetValue('%s' % len(times))
            item = self.lst_fid.GetItem(1)
            item.SetText('{0} == %i' % (len(times)-1))
            self.lst_fid.SetItem(item)
            if self.field.epoch:
                self.txt_tmin.SetToolTip('%s' % str(self.field.epoch + timedelta(seconds=float(times[ 0]))))
                self.txt_tmax.SetToolTip('%s' % str(self.field.epoch + timedelta(seconds=float(times[-1]))))
            self.txt_value.SetLabelText(self.field.getShortName())
        else:
            self.txt_tmin.SetValue('0')
            self.txt_tmax.SetValue('-1')
            self.txt_nsteps.SetValue('0')
            item = self.lst_fid.GetItem(1)
            item.SetText('{0} == %i' % (-1))
            self.lst_fid.SetItem(item)
            self.txt_value.SetLabelText('---')

    def asgDatas(self, fields):
        pass

    def asgData(self, field):
        # ---  Get fields attribs from list
        if self.field:
            reduc = self.lst_rdc.GetValue()
            fid_att = self.lst_fid.GetAllItemAttribs()
            fvl_att = self.lst_fvl.GetAllItemAttribs()
            xpr_att = self.lst_xpr.GetAllItemAttribs()
            self.fields[self.field] = (fid_att, fvl_att, xpr_att, reduc)
        # ---  Assign new field
        self.field = field
        self.__fills()
        # ---  Assign times & op to actual field
        if self.field:
            _ = self.getActivTimes()
        # ---  Set field attribs to list
        if field in self.fields:
            fid_att, fvl_att, xpr_att, reduc = self.fields[self.field]
            self.lst_fid.ResetAllItemAttribs(fid_att)
            self.lst_fvl.ResetAllItemAttribs(fvl_att)
            self.lst_xpr.ResetAllItemAttribs(xpr_att)
            self.lst_rdc.SetValue(reduc)

    def getActivTimes(self):
        # ---  Get times
        times = self.field.getTimes()
        keep1 = self.lst_fid.FilterListOnIndex(times)
        keep2 = self.lst_fvl.FilterListOnValue(times)
        keep3 = [ e for xpr in self.lst_xpr.GetExpressions() for e in eval(xpr) ]
        keepSet = None
        if keep1 or keep2 or keep3: keepSet = {}
        if keep1:
            for it in keep1: keepSet[times[it]] = None
        if keep2:
            for it in keep2: keepSet[times[it]] = None
        if keep3:
            for t in keep3: keepSet[t] = None
        try:
            times = list(keepSet.keys())
            times.sort()
        except:
            pass
        return times

    def getReductionOp(self):
        op = self.lst_rdc.GetValue()
        return Operation[ op.split('.')[1] ]


#==============================================================================
#
#==============================================================================
if __name__ == "__main__":
    class DummyData:
        def __init__(self):
            pass
        def getTimes(self):
            return list(range(10))

    class MyDialogBox(wx.Dialog):
        def __init__(self, *args, **kwargs):
            kwargs["style"] = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.MAXIMIZE_BOX|wx.SYSTEM_MENU|wx.RESIZE_BORDER # |wx.CLIP_CHILDREN
            super().__init__(*args, **kwargs)

            self.pnl = DAPnlTimes(self)
            self.btn_cancel = wx.Button(self, wx.ID_CANCEL, "")
            self.btn_ok     = wx.Button(self, wx.ID_OK, "")

            szr_frm = wx.BoxSizer(wx.VERTICAL)
            szr_frm.Add(self.pnl, 1, wx.EXPAND)

            szr_btn = wx.BoxSizer(wx.HORIZONTAL)
            szr_btn.Add(self.btn_cancel,0, wx.EXPAND, 0)
            szr_btn.Add(self.btn_ok,    0, wx.EXPAND, 0)
            szr_frm.Add(szr_btn, 0, wx.EXPAND, 0)

            self.SetSizer(szr_frm)
            self.Layout()

            self.pnl.asgData( DummyData() )

    class MyApp(wx.App):
        def OnInit(self):
            dlg = MyDialogBox(None)
            #if dlg.ShowModal() == wx.ID_OK:
            dlg.ShowModal()
            print(dlg.pnl.getActivTimes())
            return True

    app = MyApp(False)
    app.MainLoop()

