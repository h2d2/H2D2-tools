# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2015-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import wx

from CTCommon.CTWindow import CTWindowMPLFigure

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Panel.2D")

class DAPnl2DFigure(CTWindowMPLFigure):
    help = "The panel 2D is used to display results in 2D as maps"

    def __init__(self, *args, **kwds):
        self.axes = kwds.pop('axes')
        super().__init__(*args, **kwds)

        self.layers = []        # list of DALayer
        self.isDirty = False

    # The tooltip is used by the notebook to display the tab tooltip.
    # On Linux, the tooltip is also displayed on the window which is not
    # the desired effect. On Windows, the tooltip is not displayed on
    # the window.
    # The notebook uses GetToolTip to get access to the tooltip. We provide
    # here a version of GetToolTip returning a tooltip with a very
    # short lifetime, long enough to be displayed on the tab, not long
    # enough to be displayed on the window.
    def GetToolTip(self):
        self.SetToolTip(DAPnl2DFigure.help)
        wx.CallLater(10, self.SetToolTip, None)
        return super().GetToolTip()

    def __get_layer_idx(self, layer):
        for i, l in enumerate(self.layers):
            if l == layer: return i
        raise ValueError

    def setState(self, modified=True):
        self.isDirty = modified

    def get_axes(self):
        return self.axes

    def get_layer(self, idx):
        return self.layers[idx]

    def get_layers(self):
        return self.layers

    def add_layer(self, layer):
        self.layers.append(layer)
        self.isDirty = True

    def del_item(self, layer, item=None):
        if item and len(layer) > 1:
            layer.remove(item)
            self.isDirty = True
        else:
            if item: assert layer[0] is item
            self.del_layer(layer)

    def del_layer(self, layer):
        layer.detach()
        self.layers.remove(layer)
        self.isDirty = True

    def drag_layer(self, lyr_src, lyr_dst):
        idx_old = self.__get_layer_idx(lyr_src)
        idx_new = self.__get_layer_idx(lyr_dst)
        if   idx_new < idx_old:
            self.layers.insert(idx_new,   self.layers.pop(idx_old))
        elif idx_new > idx_old:
            self.layers.insert(idx_new-1, self.layers.pop(idx_old))
        self.isDirty = True

    def is_checked(self, layer, item=None):
        if item: return item.isVisible()
        return layer.isVisible()

    def check_item(self, flag, layer, item=None):
        LOGGER.trace('DAPnl2DFigure.check_item: %s %s', flag, layer)
        if item:
            if item.isVisible() != flag:
                item.setVisible(flag)
                self.isDirty = True
        else:
            if layer.isVisible() != flag:
                layer.setVisible(flag)
                self.isDirty = True

    def __detach_layers(self):
        for lyr in self.layers:
            lyr.detach()

    def __attach_layers(self):
        for lyr in self.layers:
            lyr.attach(self.axes)

    def redraw(self, *args, **kwargs):
        if self.isDirty:
            self.__detach_layers()
            self.__attach_layers()
            self.canvas.draw_idle()
            self.isDirty = False

    def on_draw(self, evt):
        """
        on_draw overload of parent event handler.

        Args:
            evt (_type_): _description_
        """
        for lyr in self.layers:
            self.isDirty |= lyr.update()
        if self.isDirty:
            self.redraw()
