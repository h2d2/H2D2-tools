# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import importlib
import os

## https://lkubuntu.wordpress.com/2012/10/02/writing-a-python-plugin-api/
def loadFilter(fullPath, clsName):
    pth, fic = os.path.split(fullPath)
    mdl, ext = os.path.splitext(fic)
    spec = importlib.util.spec_from_file_location(mdl, fullPath)
    mdul = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mdul)
    return getattr(mdul, clsName)()


class DAValueFilter:
    def __init__(self, expr):
        self.asgExpression(expr)

    def asgExpression(self, expr):
        """
        expr must be a valid Python logical expression.
        {n} can be used as a placeholder for the nth value to filter.
        It is using the implicit conversion to string of many base types.
        """
        self.expr = expr

    def isValid(self, v):
        if isinstance(v, (tuple, list)):
            return eval( self.expr.format(*v) )
        elif hasattr(v, 'asTuple'):
            v_ = v.asTuple()
            return eval( self.expr.format(*v_) )
        else:
            return eval( self.expr.format(v) )

    def __str__(self):
        return self.expr

        
class DAGridFilter:
    """
    Class prototype for a user defined grid filter
    User shall inherit from DAGridFilter and implement the method filter(...)

    As a toy example, it's simplest form could be:
    
    class NoopGridFilter(DAGridFilter):
        def filter(self, sourceGrid):
            return sourceGrid
    """
    
    def filter(self, sourceGrid):
        """
        filter(...) takes as input a source grid (object inheriting from FEMesh), 
        and shall return a valide grid (object inheriting from FEMesh).
        """
        raise NotImplementedError

        
if __name__ == "__main__":
    def main():
        r = loadFilter(r'E:\bld-1810\H2D2-tools\script\DADataAnalyzer\DAConfig.py', 'DAConfig')
        print(r)
    main()