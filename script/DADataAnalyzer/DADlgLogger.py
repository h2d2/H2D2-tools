#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import wx

ZONES = [
'H2D2',
'H2D2.Tools',
#
'H2D2.Tools.Data',
'H2D2.Tools.Data.Data',
'H2D2.Tools.Data.Data.IO',
'H2D2.Tools.Data.Diffusion',
'H2D2.Tools.Data.Field',
'H2D2.Tools.Data.Path',
'H2D2.Tools.Data.StreamFunction',
'H2D2.Tools.Mesh',
'H2D2.Tools.Mesh.IO',
'H2D2.Tools.Mesh.Metrics',
'H2D2.Tools.StreamLines',
'H2D2.Tools.boundary',
#
'H2D2.Tools.ImageProcessor',
'H2D2.Tools.ImageProcessor.GDAL',
'H2D2.Tools.GIS.LandXML',
#
'H2D2.Tools.DataAnalyzer',
'H2D2.Tools.DataAnalyzer.Export',
'H2D2.Tools.DataAnalyzer.Export.KML',
'H2D2.Tools.DataAnalyzer.Export.LandXML',
'H2D2.Tools.DataAnalyzer.Export.SHP',
'H2D2.Tools.DataAnalyzer.Help',
'H2D2.Tools.DataAnalyzer.Help.About',
'H2D2.Tools.DataAnalyzer.Help.Help',
'H2D2.Tools.DataAnalyzer.Layer',
'H2D2.Tools.DataAnalyzer.Layer.AnimatedIso',
'H2D2.Tools.DataAnalyzer.Layer.GridEdit',
'H2D2.Tools.DataAnalyzer.Layer.GridQuality',
'H2D2.Tools.DataAnalyzer.Layer.Iso',
'H2D2.Tools.DataAnalyzer.Layer.OSM',
'H2D2.Tools.DataAnalyzer.Layer.Quiver',
'H2D2.Tools.DataAnalyzer.Layer.Srf3D',
'H2D2.Tools.DataAnalyzer.Layer.StreamLines',
'H2D2.Tools.DataAnalyzer.Panel',
'H2D2.Tools.DataAnalyzer.Panel.1D',
'H2D2.Tools.DataAnalyzer.Panel.2D',
'H2D2.Tools.DataAnalyzer.Panel.3D',
'H2D2.Tools.DataAnalyzer.Panel.DisplayList',
'H2D2.Tools.DataAnalyzer.Panel.Grids',
'H2D2.Tools.DataAnalyzer.Panel.Times',
'H2D2.Tools.DataAnalyzer.Panel.Values',
'H2D2.Tools.DataAnalyzer.Project',
'H2D2.Tools.DataAnalyzer.Tools.ToolPolyline',
]

class DADlgLogZone(wx.Dialog):
    def __init__(self, *args, **kwds):
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        super().__init__(*args, **kwds)

        # --- Cannot be done at module level
        levels = [ logging._levelToName[k] for k in sorted(logging._levelToName, reverse=True) ]

        self.szr_zne_staticbox = wx.StaticBox(self, wx.ID_ANY, "Zone")
        self.szr_lvl_staticbox = wx.StaticBox(self, wx.ID_ANY, "Level")

        self.cbx_zne = wx.ComboBox (self, wx.ID_ANY, choices=ZONES,  style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.cbx_lvl = wx.ComboBox (self, wx.ID_ANY, choices=levels, style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.btn_ok     = wx.Button(self, wx.ID_OK, "")
        self.btn_cancel = wx.Button(self, wx.ID_CANCEL, "")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.on_btn_ok,     self.btn_ok)
        self.Bind(wx.EVT_BUTTON, self.on_btn_cancel, self.btn_cancel)

        self.cb = None

    def __set_properties(self):
        self.SetTitle("Logging zone")
        self.cbx_zne.SetSelection(0)
        self.cbx_lvl.SetSelection(0)
        self.btn_ok.SetDefault()

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)
        szr_btn  = wx.BoxSizer(wx.HORIZONTAL)
        szr_dta  = wx.BoxSizer(wx.VERTICAL)
        szr_zne  = wx.StaticBoxSizer(self.szr_zne_staticbox,  wx.VERTICAL)
        szr_lvl  = wx.StaticBoxSizer(self.szr_lvl_staticbox,  wx.VERTICAL)

        szr_zne.Add(self.cbx_zne, 1, wx.EXPAND, 0)
        szr_lvl.Add(self.cbx_lvl, 1, wx.EXPAND, 0)

        szr_dta.Add(szr_zne, 1, wx.EXPAND, 0)
        szr_dta.Add(szr_lvl, 1, wx.EXPAND, 0)
        szr_main.Add(szr_dta, 0, 0, 0)

        szr_btn.AddStretchSpacer(prop=4)
        szr_btn.Add(self.btn_ok, 0, wx.EXPAND, 0)
        szr_btn.Add(self.btn_cancel, 0, wx.EXPAND, 0)
        szr_main.Add(szr_btn, 1, wx.EXPAND, 0)

        self.SetSizer(szr_main)
        szr_main.Fit(self)
        self.Layout()

    def getValues(self):
        zne = self.cbx_zne.GetValue()
        lvl = self.cbx_lvl.GetValue()
        return zne, getattr(logging, lvl)

    def setValues(self, zone, level):
        pass

    def on_btn_ok(self, event):
        event.Skip()

    def on_btn_cancel(self, event):
        self.Destroy()

if __name__ == "__main__":
    class MyApp(wx.App):
        def OnInit(self):
            dlg = DADlgLogZone(None, wx.ID_ANY, "")
            if dlg.ShowModal() == wx.ID_OK:
                print(dlg.getValues())
            return True

    app = MyApp(0)
    app.MainLoop()
