# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2022
# ---
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os.path
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import enum
import logging
import os.path
from typing import List
import json
import jsons
import dateutil

from DARegGrid    import DARegGrid
from DAFrameGrid  import DAFrameGrid
from DAFrameField import DAFrameField
from CTCommon.FEMesh import FEMesh, FE2DRegularGrid, FE1DRegularGrid
from CTCommon.DTField import DT2DFiniteElementField, DT2DRegularGridField, DT1DRegularGridField

from IPImageProcessor.GDAL.GDLBasemap import GDLSpatialReference

LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.Project")

Actions = enum.Enum   ('Actions', names='load_srs load_grid load_rgrid load_boundary load_data')
Status  = enum.IntEnum('Status',  names='unset ok warning error')

class ActionHandler:
    """
    Call-back prototype to handle actions on project load.

    System event handler make a copy of the events. We rely on a custom id, that is copied,
    to find our event, given a system event.
    """
    EVENT_ID = 0

    @staticmethod
    def nextId():
        ActionHandler.EVENT_ID += 1
        return ActionHandler.EVENT_ID

    def __init__(self):
        self.events = []        # list of events

    def __getEventSetter(self, event):
        def setStatus(status: Status):
            self.setEventStatus(event, status)
        return setStatus

    def addEvent(self, cls, *args):
        """
        addEvent _summary_

        _extended_summary_

        Args:
            cls (class): Event class to create
            args: Args for constructor, typicaly id

        Returns:
            cls: the event
        """
        evt = cls(*args, customId=self.nextId(), status=Status.unset, setStatus=None)
        evt.setStatus = self.__getEventSetter(evt)
        self.events.append(evt)
        return evt

    def getStatus(self) -> Status:
        return max((evt.status for evt in self.events))

    def setEventStatus(self, event, status: Status):
        for i, e in enumerate(self.events):
            if e.customId == event.customId:
                self.events[i].status = status

    def handleAction(self, action, kwargs={}):
        raise NotImplementedError

    def __call__(self, action, kwargs={}):
        return self.handleAction(action, kwargs)

class ProjectItem:
    pass

class ProjectSRS(ProjectItem):
    def __init__(self, srs: GDLSpatialReference):
        self.srs = srs

class ProjectRgrid(ProjectItem):
    def __init__(self, rgrid: DARegGrid):
        self.rgrid = rgrid
    def __eq__(self, other):
        if isinstance(other, DARegGrid):
            return self.rgrid is other
        else:
            return self is other

class ProjectGrid(ProjectItem):
    def __init__(self, grid: DAFrameGrid):
        self.grid = grid
    def __eq__(self, other):
        if isinstance(other, DAFrameGrid):
            return self.grid is other
        else:
            return self is other

class ProjectField:
    def __init__(self, field: DAFrameField):
        self.field = field
    def __eq__(self, other):
        if isinstance(other, DAFrameField):
            return self.field is other
        else:
            return self is other

class DAProject:
    cntr = 0

    def __init__(self, name: str, file=''):
        self.idx  = DAProject.cntr
        DAProject.cntr += 1
        self.file = file # if file else '%s.da.proj' % name
        self.name = name
        self.srs   : ProjectSRS         = None
        self.items : List[ProjectItem] = []
        self.dirty = True # False

    def __str__(self):
        return '#%02x %s' % (self.idx, self.name)

    def __repr__(self):
        info = []
        csname = self.srs.srs.GetName() if self.srs else ''
        info.append('#%02x %s' % (self.idx, self.name))
        if csname:
            info.append('   Coordinate system: %s' % csname)
        info.append('')
        for grid in self.getGrids():
            try:
                info.append('Grid:')
                info.append('    %s' % grid.getLongName())
            except IndexError:
                pass
            try:
                cs = grid.getGridSRS()
                info.append('    Coordinate system: %s' % cs.GetName())
            except:
                pass
            for field in self.getFields():
                if field.getGrid() != grid: continue
                info.append('    Data:')
                info.append('        %s' % field.getLongName())
        return '\n'.join(info)

    def isModified(self) -> bool:
        return self.dirty

    def getFileName(self) -> str:
        return self.file

    def getSRS(self) -> GDLSpatialReference:
        """
        getSRS return the Spatial Reference System of the project.

        """
        return self.srs.srs if self.srs else None

    def setSRS(self, srs):
        """
        setSRS sets the Spatial Reference System for the project

        Args:
            srs (GDLSpatialReference): Spatial Reference System
        """
        self.srs = ProjectSRS(srs)
        # for grid in self.grids:
        #     grid.setProjectSRS()

    def hasSRS(self):
        return bool(self.srs)

    def addRgrid(self, rgrid: DARegGrid):
        self.dirty = True
        self.items.append( ProjectRgrid(rgrid) )

    def addGrid(self, grid: DAFrameGrid):
        self.dirty = True
        self.items.append( ProjectGrid(grid) )

    def addField(self, field: DAFrameField):
        self.dirty = True
        self.items.append( ProjectField(field) )

    def modifyRgrid(self, rgrid: DARegGrid):
        for i, r in enumerate(self.items):
            if not isinstance(r, ProjectRgrid): continue
            if not r.rgrid.uuid == rgrid.uuid: continue
            self.items[i] = ProjectRgrid(rgrid)
            break
        self.dirty = True

    def removeRgrid(self, rgrid: DARegGrid):
        self.items.remove(rgrid)
        del rgrid
        self.dirty = True

    def removeGrid(self, grid: DAFrameGrid):
        self.items.remove(grid)
        del grid
        self.dirty = True

    def removeField(self, field: DAFrameField):
        self.items.remove(field)
        del field
        self.dirty = True

    def getField(self, idx) -> DAFrameField:
        return self.getFields()[idx]

    def getFields(self) -> List[DAFrameField]:
        return [ i.field for i in self.items if isinstance(i, ProjectField) ]

    def hasFields(self) -> bool:
        return len(self.getFields()) > 0

    def getRgrid(self, idx) -> DARegGrid:
        return self.getRgrids()[idx].rgrid

    def getRgrids(self) -> List[DARegGrid]:
        return [ i.rgrid for i in self.items if isinstance(i, ProjectRgrid) ]

    def hasRgrids(self) -> bool:
        return len(self.getRgrids()) > 0

    def getGrid(self, idx) -> DAFrameGrid:
        return self.getGrids()[idx]

    def getGrids(self) -> List[DAFrameGrid]:
        return [ i.grid for i in self.items if isinstance(i, ProjectGrid) ]

    def hasGrids(self) -> bool:
        return len(self.getGrids()) > 0

    def getGridIndex(self, grid) -> int:
        if isinstance(grid, ProjectGrid):
            return self.items.index(grid)
        if isinstance(grid, DAFrameGrid):
            return [ g.grid if isinstance(g, ProjectGrid) else None for g in self.items ].index(grid)
        if isinstance(grid, FEMesh):
            return [ g.grid.getGrid() if isinstance(g, ProjectGrid) else None for g in self.items ].index(grid)
        raise ValueError('Invalid grid type: %s' % type(grid))

    def getName(self):
        return self.name

    def setName(self, name):
        if name != self.name:
            self.name = name
            self.dirty = True

    def close(self):
        for i in self.items: del i
        self = DAProject('Dummy name for empty project')
        DAFrameField.resetCounters()
        DAFrameGrid.resetCounters()

    def save(self, filename=''):
        """
        save _summary_

        Args:
            filename (str, optional): If supplied, will replace the current file name.
        """
        if not self.dirty: return
        if filename:
            self.file = filename

        with open(self.file, 'w', encoding='utf-8') as ofs:
            jsdmp = jsons.dump(self)
            json.dump(jsdmp, ofs, ensure_ascii=False, indent=4)
        self.dirty = False

    def saveAs(self, file, name=''):
        with open(file, 'w', encoding='utf-8') as ofs:
            jsdmp = jsons.dump(self)
            if name:
                jsdmp['name'] = name
            json.dump(jsdmp, ofs, ensure_ascii=False, indent=4)

def loadProjectFromFile(file: str, handler: ActionHandler = None) -> DAProject:
    with open(file, 'r', encoding='utf-8') as ifs:
        jsdmp = json.load(ifs)
        obj = jsons.load(jsdmp, DAProject, eventHandler=handler)
    obj.file = file
    obj.dirty = False
    return obj


#---------------------------------------------------------------
#---------------------------------------------------------------

def toJSONS_ProjectSRS(obj: ProjectSRS, **kwargs) -> dict:
    LOGGER.debug('In toJSONS_ProjectSRS')
    return {'EPSG': obj.srs.GetEPSGCode() } if obj else { }

def fromJSONS_ProjectSRS(vals: dict, cls: type = ProjectSRS, **kwargs) -> ProjectSRS:
    assert cls == ProjectSRS
    epsg = vals.get('EPSG', '')
    return ProjectSRS( GDLSpatialReference().ImportFromEPSG(epsg) ) if epsg else None

jsons.set_serializer  (toJSONS_ProjectSRS,   (ProjectSRS, None))
jsons.set_deserializer(fromJSONS_ProjectSRS, (ProjectSRS, None))
#---------------------------------------------------------------

def toJSONS_ProjectRgrid(obj: ProjectRgrid, proj: DAProject = None, **kwargs) -> dict:
    LOGGER.debug('In toJSONS_ProjectRgrid')
    rgrid = obj.rgrid
    vals = {}
    vals['rgrid_name'] = rgrid.getName()
    vals['rgrid_kind'] = str(rgrid.kind)
    vals['rgrid_size'] = rgrid.size
    vals['rgrid_uuid'] = str(rgrid.uuid)
    vals['rgrid_json'] = rgrid.toStr()
    LOGGER.debug('In toJSONS_ProjectRgrid done')
    return vals

def fromJSONS_ProjectRgrid(vals: dict, cls: type = ProjectRgrid, proj: DAProject = None, eventHandler: ActionHandler = None, **kwargs) -> ProjectGrid:
    LOGGER.debug('In fromJSONS_ProjectRgrid')
    assert cls == ProjectRgrid
    #name = vals['rgrid_name']
    #kind = eval(vals['rgrid_kind'])
    #size = vals['rgrid_size']
    #uuid = vals['rgrid_uuid']
    jstr = vals['rgrid_json']
    eventHandler(Actions.load_rgrid, jstr)
    LOGGER.debug('In fromJSONS_ProjectRgrid done')
    return

jsons.set_serializer  (toJSONS_ProjectRgrid,   ProjectRgrid)
jsons.set_deserializer(fromJSONS_ProjectRgrid, ProjectRgrid)
#---------------------------------------------------------------

def toJSONS_ProjectGrid(obj: ProjectGrid, proj: DAProject = None, **kwargs) -> dict:
    LOGGER.debug('In toJSONS_ProjectGrid')
    grid = obj.grid
    g2p  = grid.getProjectionFromGridToProj()
    gp = grid.getParent()
    vals = {}
    vals['idx'] = grid.idx
    vals['parent_index'] = proj.getGridIndex(gp) if gp else -1
    vals['grid_type']= str(type(grid.grid))
    if gp:
        pass
    else:
        if   isinstance(grid.grid, FEMesh):
            vals['files_name'] = grid.getFiles()
            vals['files_date'] = [ os.path.getmtime(f) for f in grid.getFiles()]
            vals['files_size'] = [ os.path.getsize(f)  for f in grid.getFiles()]
        elif isinstance(grid.grid, FE2DRegularGrid):
            raise NotImplementedError('FE2DRegularGrid not yet valid')
            vals['grid_name'] = grid.getName()
            vals['grid_size'] = grid.getGridSize()
        elif isinstance(grid.grid, FE1DRegularGrid):
            raise NotImplementedError('FE1DRegularGrid not yet valid')
            vals['grid_name'] = grid.getName()
            vals['grid_size'] = grid.getGridSize()
            vals['grid_vertexes'] = grid.vertexes()
    vals['bbox']  = grid.getBbox()
    vals['shape'] = [grid.getNbNodes(), grid.getNbElements()]
    vals['srs']   = jsons.dumps(ProjectSRS(g2p.getSourceCS())) if g2p else None
    LOGGER.debug('In toJSONS_ProjectGrid done')
    return vals

def fromJSONS_ProjectGrid(vals: dict, cls: type = ProjectGrid, proj: DAProject = None, eventHandler: ActionHandler = None, **kwargs):
    LOGGER.debug('In fromJSONS_ProjectGrid')
    assert cls == ProjectGrid
    srsProj = proj.getSRS()
    idx = vals['idx']
    prntIdx  = vals['parent_index']
    gridType = vals['grid_type']
    if   gridType == str(FEMesh) and prntIdx == -1:
        fnames = vals['files_name']
        fdates = vals['files_date']
        fsizes = vals['files_size']
        for fn, fd, fs in zip(fnames, fdates, fsizes):
            if os.path.getmtime(fn) != fd: raise RuntimeError('File "%s" was modified' % fn)
            if os.path.getsize (fn) != fs: raise RuntimeError('File size for "%s" does not match %d' % (fn, fs))
        kwargs = {'files': fnames }
        eventHandler(Actions.load_grid, kwargs)
    elif gridType == str(FEMesh):
        raise NotImplementedError('FEMesh as subgrid not yet valid')
    elif gridType == str(FE2DRegularGrid):
        raise NotImplementedError('FE2DRegularGrid not yet valid')
        gridName = vals['grid_name']
        gridSize = vals['grid_size']
        gridBbox = vals['bbox']
        #eventHandler(Actions.load_rgrid, kwargs)
    elif gridType == str(FE1DRegularGrid):
        raise NotImplementedError('FE1DRegularGrid not yet valid')
        gridName = vals['grid_name']
        gridSize = vals['grid_size']
        gridVtxs = vals['grid_vertexes']
        #eventHandler(Actions.load_rgrid, kwargs)
    else:
        raise ValueError('Invalid grid type: %s' % gridType)
    LOGGER.debug('In fromJSONS_ProjectGrid done')
    return

jsons.set_serializer  (toJSONS_ProjectGrid,   ProjectGrid)
jsons.set_deserializer(fromJSONS_ProjectGrid, ProjectGrid)
#---------------------------------------------------------------

def toJSONS_ProjectField(obj: ProjectField, proj: DAProject = None, **kwargs) -> dict:
    LOGGER.debug('In toJSONS_ProjectField')
    daFld: DAFrameField = obj.field
    daGrd: DAFrameGrid = daFld.getGrid()
    vals = {}
    vals['idx'] = daFld.idx
    vals['field_type']= str(type(daFld.field))
    if   isinstance(daFld.field, DT2DFiniteElementField):
        vals['files_name'] = daFld.getFiles()
        vals['files_date'] = [ os.path.getmtime(f) for f, _ in daFld.getFiles()]
        vals['files_size'] = [ os.path.getsize(f)  for f, _ in daFld.getFiles()]
    elif isinstance(daFld.field, DT2DRegularGridField):
        raise NotImplementedError('DT2DRegularGridField not implemented')
    elif isinstance(daFld.field, DT1DRegularGridField):
        raise NotImplementedError('DT1DRegularGridField not implemented')
    vals['shape']      = [daFld.getNbVal(), daFld.getNbNodes()]
    vals['names']      = daFld.getNames()
    vals['epoch']      = daFld.getEpoch().isoformat() if daFld.getEpoch() else None
    vals['grid_type']  = str(type(daGrd.grid))
    vals['grid_index'] = proj.getGridIndex(daGrd)
    LOGGER.debug('In toJSONS_ProjectField done')
    return vals

def fromJSONS_ProjectField(vals: dict, cls: type = ProjectField, proj: DAProject = None, eventHandler: ActionHandler = None, **kwargs):
    LOGGER.debug('In fromJSONS_ProjectField')
    assert cls == ProjectField
    field = None
    idx = vals['idx']
    dataType = vals['field_type']
    if   dataType == str(DT2DFiniteElementField):
        fnames = vals['files_name']
        fdates = vals['files_date']
        fsizes = vals['files_size']
        for (fn, fc), fd, fs in zip(fnames, fdates, fsizes):
            if os.path.getmtime(fn) != fd: raise RuntimeError('File "%s" was modified' % fn)
            if os.path.getsize (fn) != fs: raise RuntimeError('File size for "%s" does not match %d' % (fn, fs))
        names = vals['names']
        epoch = dateutil.parser.isoparse(vals['epoch']) if vals['epoch'] else None
        kwargs = {'files': fnames, 'names': names, 'epoch': epoch}
        eventHandler(Actions.load_data, kwargs)
    elif isinstance(field, DT2DRegularGridField):
        raise NotImplementedError('DT2DRegularGridField not implemented')
    elif isinstance(field, DT1DRegularGridField):
        raise NotImplementedError('DT1DRegularGridField not implemented')
    LOGGER.debug('In fromJSONS_ProjectField done')
    return

jsons.set_serializer  (toJSONS_ProjectField,   ProjectField)
jsons.set_deserializer(fromJSONS_ProjectField, ProjectField)
#---------------------------------------------------------------

def toJSONS_Project(obj: DAProject, **kwargs) -> dict:
    LOGGER.debug('In toJSONS_Project')
    vals = {}
    vals['idx']  = obj.idx
    vals['file'] = obj.file
    vals['name'] = obj.name
    vals['srs']  = jsons.dumps(obj.srs, proj=obj)
    vals['items'] = [ (type(i).__name__, jsons.dumps(i, proj=obj)) for i in obj.items ]
    LOGGER.debug('In toJSONS_Project done')
    return vals

def fromJSONS_Project(vals: dict, cls: type = DAProject, **kwargs) -> DAProject:
    LOGGER.debug('In fromJSONS_Project')
    hndlr = kwargs.pop('eventHandler', None)
    idx  = vals['idx']
    file = vals['file']
    name = vals['name']
    prj = DAProject(name = name, file = file)
    prj.cntr = max(idx, prj.cntr-1)
    prj.idx  = idx
    prj.srs  = jsons.loads(vals['srs'], ProjectSRS)
    clss = {cls.__name__ : cls for cls in (ProjectRgrid, ProjectGrid, ProjectField) }
    for iname, ivals in vals['items']:
        cls = clss[iname]
        jsons.loads(ivals, cls, proj=prj, eventHandler=hndlr)
    LOGGER.debug('In fromJSONS_Project done')
    return prj

jsons.set_serializer  (toJSONS_Project,   DAProject)
jsons.set_deserializer(fromJSONS_Project, DAProject)



if __name__ == "__main__":
    from DAFrameField import DAFrameField
    from IPImageProcessor.GDAL.GDLBasemap import GDLSpatialReference

    def main():
        p = DAProject('__DummyProject__')

        srsProj = GDLSpatialReference()
        srsProj.ImportFromEPSG(4326)
        srsGrid = srsProj

        p.setSRS(srsProj)

        fs = ['../CTCommon/_a.cor', '../CTCommon/_a.ele']
        g = FEMesh(files=fs)
        fg = DAFrameGrid(g, srsGrid=srsGrid, srsProj=srsProj)
        p.addGrid(fg)
        f2 = DARegGrid()
        p.addRgrid(f2)

        fs = '../CTCommon/_a.cor'
        ff = DAFrameField.as2DFiniteElementField(fg, fs)
        p.addField(ff)

        p.save('../CTCommon/_a.da.proj')
        p2 = loadProjectFromFile('../CTCommon/_a.da.proj')
        print(len(p2.items))

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)

    main()