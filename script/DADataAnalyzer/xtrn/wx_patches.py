# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2022
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
LOGGER = logging.getLogger("H2D2.Tools.DataAnalyzer.wx_patches")

import wx.lib.agw.flatnotebook as wx_fnb
import wx.lib.combotreebox     as wx_ctb

#--- Patch pour wx.lib.agw.flatnotebook.PageContainer.ShowTabTooltip
#    qui ne fait pas de reset du tooltip lorsqu'il n'y en a pas
LOGGER.warning('Patch: wx.lib.agw.flatnotebook.PageContainer.ShowTabTooltip: no reset of tooltip when tooltip is empty')
def wx_fnb_PageContainer_ShowTabTooltip(self, tabIdx):
    """
    Shows a tab tooltip.

    :param `tabIdx`: an integer specifying the page index.
    """

    pWindow = self._pParent.GetPage(tabIdx)

    if pWindow:
        pToolTip = pWindow.GetToolTip()
        if pToolTip and pToolTip.GetWindow() == pWindow:
            return self.SetToolTip(pToolTip.GetTip())
    return self.SetToolTip('')

wx_fnb.PageContainer.ShowTabTooltip = wx_fnb_PageContainer_ShowTabTooltip


#--- Patch pour wx.lib.combotreebox.BaseComboTreeBox
#    qui fait des appels à Get/SetPyItemData qui est 'deprecated'
LOGGER.warning('Patch: wx.lib.combotreebox.BaseComboTreeBox.GetClientData: deprecated call')
def wx_ctb_BaseComboTreeBox_GetClientData(self, item):
    return self._tree.GetItemData(item)

def wx_ctb_BaseComboTreeBox_SetClientData(self, item, clientData):
    self._tree.SetItemData(item, clientData)

wx_ctb.BaseComboTreeBox.GetClientData = wx_ctb_BaseComboTreeBox_GetClientData
wx_ctb.BaseComboTreeBox.SetClientData = wx_ctb_BaseComboTreeBox_SetClientData


#--- Patch pour le scroll qui a des coord aberrantes
# https://github.com/wxWidgets/Phoenix/issues/1707
import wx
if wx.__version__ <= '4.1.1':
    LOGGER.warning('Patch: matplotlib.backends.backend_wxagg.FigureCanvasWxAgg._onMouseWheel: bug on scroll')
    import matplotlib.backends.backend_wxagg as wxagg
    from matplotlib.backend_bases import FigureCanvasBase

    def wxagg_FigureCanvasWxAgg_onMouseWheel(self, event):
            """Translate mouse wheel events into matplotlib events"""
            # Determine mouse location
            x = event.GetX()
            y = self.figure.bbox.height - event.GetY()
            # Convert delta/rotation/rate into a floating point step size
            step = event.LinesPerAction * event.WheelRotation / event.WheelDelta
            # Done handling event
            event.Skip()
            # Mac gives two events for every wheel event; skip every second one.
            if wx.Platform == '__WXMAC__':
                if not hasattr(self, '_skipwheelevent'):
                    self._skipwheelevent = True
                elif self._skipwheelevent:
                    self._skipwheelevent = False
                    return  # Return without processing event
                else:
                    self._skipwheelevent = True
            FigureCanvasBase.scroll_event(self, x, y, step, guiEvent=None) #event)

    def wxagg_FigureCanvasWxAgg_onMotion(self, event):
        """Start measuring on an axis."""
        x = event.GetX()
        y = self.figure.bbox.height - event.GetY()
        event.Skip()
        FigureCanvasBase.motion_notify_event(self, x, y, guiEvent=None) #event)

    assert '_on_mouse_wheel' in dir(wxagg.FigureCanvasWxAgg)
    assert '_on_motion'      in dir(wxagg.FigureCanvasWxAgg)
    wxagg.FigureCanvasWxAgg._on_mouse_wheel = wxagg_FigureCanvasWxAgg_onMouseWheel
    wxagg.FigureCanvasWxAgg._on_motion      = wxagg_FigureCanvasWxAgg_onMotion
