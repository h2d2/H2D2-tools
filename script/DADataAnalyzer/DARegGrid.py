# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2021
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import copy
import enum
import json
import numpy as np
import uuid

from CTCommon.FEMesh import FE2DRegularGrid, FE1DRegularGrid

RgridKind   = enum.Enum('RgridKind',   'Grid0D Grid1D Grid2D')
RgridEvents = enum.Enum('RgridEvents', 'add modify delete select unselect')

class DARegGrid:
    def __init__(self, name='New item', kind=RgridKind.Grid2D, xy=[ (0.0,0.0), (1.0,1.0)], size=(1,1) ):
        self.rgrid = None
        self.dirty = True

        self.name = name
        self.kind = kind
        if xy:
            ndxy = np.asarray(xy)
            self.bbox = ( np.min(ndxy[0,:]), np.max(ndxy[0,:]),
                          np.min(ndxy[1,:]), np.max(ndxy[1,:]) )
            self.xy   = ndxy.tolist()
        else:
            self.xy = []
        self.size = size
        self.uuid = uuid.uuid4()

    def __deepcopy__(self, memo):
        """
        Deep copy, but skipping rgrid
        """
        newone = type(self)()
        memo[id(self)] = newone
        for k, v in self.__dict__.items():
            if k in ['rgrid']:
                v = None
            setattr(newone, k, copy.deepcopy(v, memo))
        return newone

    def __str__(self):
        s = '%s %s %s %s' % (self.kind.name, self.size, self.bbox[:2], self.bbox[2:])
        return s

    def toStr(self):
        s = {'kind' : self.kind.name, 'name' : self.name, 'size' : self.size, 'xy' : self.xy, 'uuid': str(self.uuid)}
        return json.dumps(s)

    def fromStr(self, s):
        s_ = json.loads(s)
        self.uuid = uuid.UUID(s_['uuid'])
        self.kind = RgridKind[s_['kind']]
        self.setName(s_['name'])
        self.setSize(s_['size'])
        self.setXY(s_['xy'])

    def isDefault(self):
        """
        isDefault return True if self is equivalent to
        a constructor call without arguments.

        Returns:
            bool: True if untouched
        """
        gdef = DARegGrid()
        if self.name != gdef.name: return False
        if self.kind != gdef.kind: return False
        if self.xy   != gdef.xy:   return False
        if self.size != gdef.size: return False
        return True

    def getName(self):
        return self.name

    def getKind(self):
        return self.kind

    def getXmin(self):
        return self.bbox[:2]

    def getXmax(self):
        return self.bbox[2:]

    def getXY(self):
        """
        Returns the points
        """
        return self.xy

    def getBbox(self, stretchfactor=0, margins=(0, 0)):
        """
        Returns the bounding box as (xmin, ymin, xmax, ymax)
        """
        return self.rgrid.getBbox(stretchfactor=stretchfactor, margins=margins) if (self.rgrid and not self.dirty) else self.bbox

    def getSize(self):
        """
        Returns the size as number of elements in each direction
        """
        return self.rgrid.getGridSize() if (self.rgrid and not self.dirty) else self.size

    def getRegGrid(self):
        if self.dirty:
            self.rgrid = self.__genRegGrid()
            self.dirty = False
        return self.rgrid

    def __genRegGrid(self):
        rgrid = None
        if   self.getKind() == RgridKind.Grid0D:
            raise NotImplementedError
        elif self.getKind() == RgridKind.Grid1D:
            xy = self.getXY()
            ns, *_ = self.getSize()
            rgrid = FE1DRegularGrid(vertexes=xy, gridSize=ns+1)
        elif self.getKind() == RgridKind.Grid2D:
            bbx = self.getBbox()
            nx, ny = self.getSize()
            rgrid = FE2DRegularGrid(bbox=bbx, gridSize=(nx+1, ny+1))
        return rgrid

    def setName(self, name):
        self.name = name

    def setKind(self, kind):
        self.kind = kind
        self.dirty = True

    def setXY(self, xy):
        """
        Set the points
        """
        if xy:
            ndxy = np.asarray(xy)
            self.bbox = ( np.min(ndxy[0,:]), np.max(ndxy[0,:]),
                          np.min(ndxy[1,:]), np.max(ndxy[1,:]) )
            self.xy   = ndxy.tolist()
        else:
            self.xy = []
        self.dirty = True

    def setSize(self, size):
        """
        Set the size as number of elements in each direction
        """
        self.size = size
        self.dirty = True

##    def setRegGrid(self, r):
##        if isinstance(r, FE1DRegularGrid):
##            self.name = '__Grid name shall be in FE__'
##            self.xy   = r.getVertexes()
##            self.size = r.getGridSize()
##            self.bbox = r.getBbox()
##            self.kind = RgridKind.Grid1D
##        elif isinstance(r, FE2DRegularGrid):
##            pass
##        else:
##            raise RuntimeError('Invalid grid type, expected one of ["FE1DRegularGrid", "FE2DRegularGrid"], got ""' % type(r))

    def validate(self):
        if   self.kind == RgridKind.Grid0D:
            pass
        elif self.kind == RgridKind.Grid1D:
            if (self.size[0] < 1): return "Invalid step size: %i" % self.size[0]
        elif self.kind == RgridKind.Grid2D:
            if (self.bbox[0] >= self.bbox[2]): return "Invalid xmin/xmax: xmax <= xmin"
            if (self.bbox[1] >= self.bbox[3]): return "Invalid ymin/ymax: ymax <= ymin"
            if (self.size[0] < 1): return "Invalid x size: %i" % self.size[0]
            if (self.size[1] < 1): return "Invalid y size: %i" % self.size[1]
        return None

    def __getattr__(self, name):
        """
        Transfert all unknown calls to the regular grid
        """
        return getattr(self.rgrid, name)

