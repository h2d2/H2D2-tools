#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2021
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx

HISTORIES = [
    'H2D2: DataAnalyzer: Filter grid on nodes',
    'H2D2: DataAnalyzer: Filter grid on coord',
    'H2D2: DataAnalyzer: Regular grid',
    #
    'H2D2: DataAnalyzer: Time serie id filter',
    'H2D2: DataAnalyzer: Time serie time filter',
    'H2D2: DataAnalyzer: Time serie expressions',
    #
    'H2D2: DataAnalyzer: Nodal value expressions',
    ]

class DADlgHistory(wx.Dialog):
    """
    DADlgHistory, dialog box to clean history lists
    """

    help = """\
History list cleaner allows to delete entries in history list.
"""

    def __init__(self, *args, **kwds):
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        super().__init__(*args, **kwds)

        self.sbx_hst   = wx.StaticBox(self, wx.ID_ANY, "History")
        self.sbx_itm   = wx.StaticBox(self, wx.ID_ANY, "Entries")
        self.cbx_hst   = wx.ComboBox (self, wx.ID_ANY, choices=HISTORIES,  style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.lst_itm   = wx.ListCtrl (self, wx.ID_ANY,  style=wx.LC_REPORT | wx.LC_NO_HEADER | wx.LC_SINGLE_SEL)
        self.btn_clear = wx.Button(self, wx.ID_CLEAR, "")
        self.btn_close = wx.Button(self, wx.ID_CLOSE, "")

        self.paths = []

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_COMBOBOX, self.on_hst_select, self.cbx_hst)
        self.Bind(wx.EVT_BUTTON,   self.on_btn_clear,  self.btn_clear)
        self.Bind(wx.EVT_BUTTON,   self.on_btn_close,  self.btn_close)


    def __set_properties(self):
        self.SetTitle("History")
        self.SetToolTip (DADlgHistory.help)
        self.btn_clear.SetDefault()
        self.btn_clear.Enable(False)
        self.lst_itm.InsertColumn(0, 'Item', width=400) 
        self.cbx_hst.SetSelection(0)
        self.on_hst_select(None)

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)
        szr_btn  = wx.BoxSizer(wx.HORIZONTAL)
        szr_dta  = wx.BoxSizer(wx.VERTICAL)
        szr_hst  = wx.StaticBoxSizer(self.sbx_hst,  wx.VERTICAL)
        szr_itm  = wx.StaticBoxSizer(self.sbx_itm,  wx.VERTICAL)

        szr_hst.Add(self.cbx_hst, 1, wx.EXPAND, 0)
        szr_itm.Add(self.lst_itm, 1, wx.EXPAND, 0)

        szr_dta.Add(szr_hst, 0, wx.EXPAND, 0)
        szr_dta.Add(szr_itm, 1, wx.EXPAND, 0)
        szr_main.Add(szr_dta, 0, 0, 0)

        szr_btn.AddStretchSpacer(prop=4)
        szr_btn.Add(self.btn_clear, 0, wx.EXPAND, 0)
        szr_btn.Add(self.btn_close, 0, wx.EXPAND, 0)
        szr_main.Add(szr_btn, 1, wx.EXPAND, 0)

        self.SetSizer(szr_main)
        szr_main.Fit(self)
        self.Layout()

    def on_hst_select(self, event):
        hst = self.cbx_hst.GetStringSelection()
        cnf = wx.Config(hst, style=wx.CONFIG_USE_LOCAL_FILE)
        #
        items = {}
        moreItm, valueItm, indexItm = cnf.GetFirstEntry()
        while moreItm:
            p = valueItm
            d = cnf.Read(p)
            if d:
                k = int(p[6:]) if p.startswith('string') else p
                items[k] = (d, p)
            moreItm, valueItm, indexItm = cnf.GetNextEntry(indexItm)
        #
        moreGrp, valueGrp, indexGrp = cnf.GetFirstGroup()
        while moreGrp:
            moreItm, valueItm, indexItm = cnf.GetFirstEntry()
            while moreItm:
                p = '/'.join(valueGrp, valueItm)
                d = cnf.Read(p)
                if d:
                    k = int(p[6:]) if p.startswith('string') else p
                    items[k] = (d, p)
                moreItm, valueItm, indexItm = cnf.GetNextEntry(indexItm)
            moreGrp, valueGrp, indexGrp = cnf.GetNextGroup(indexGrp)
        #
        self.paths = []
        self.lst_itm.DeleteAllItems()
        for i, k in enumerate(sorted(items.keys())):
            itm = self.lst_itm.Append( (items[k][0], ) )
            self.lst_itm.SetItemData(itm, i)
            self.paths.append(items[k][1])
        self.lst_itm.Select(0, on=1)
        self.btn_clear.Enable(True)

    def on_btn_clear(self, event):
        idx = self.lst_itm.GetFirstSelected()
        pth = self.paths[idx]
        #
        hst = self.cbx_hst.GetStringSelection()
        cnf = wx.Config(hst, style=wx.CONFIG_USE_LOCAL_FILE)
        cnf.Write(pth, '')
        cnf.Flush()
        #
        cnt = self.lst_itm.GetItemCount()
        if cnt > 0:
            self.lst_itm.DeleteItem(idx)
            cnt = self.lst_itm.GetItemCount()
        if cnt > 0:
            self.lst_itm.Select( min(cnt-1, idx) )
        else:
            self.btn_clear.Enable(False)

    def on_btn_close(self, event):
        self.Destroy()

if __name__ == "__main__":
    class MyApp(wx.App):
        def OnInit(self):
            dlg = DADlgHistory(None, wx.ID_ANY, "")
            if dlg.ShowModal() == wx.ID_OK:
                pass
            return True

    app = MyApp(0)
    app.MainLoop()
