#!/usr/bin/python

"""
# http://zetcode.com/wxpython/tips/

Isabelle

When an error occurs in an application, an error dialog usually appears.
This might get annoying. I have noticed a better solution in a SAP system.
When a user enters an invalid command, statusbar turns red and an error message
is displayed on statusbar. The red colour catches the eye and the user can
easily read the error message. The following code mimics this situation.
"""

import wx

class DABell:
    """
    The Bell class
    """
    def __init__(self, frame, statusbar):
        self.statusbar = statusbar

        self.timer = wx.Timer()
        self.blick = 0
        self.oldColor = None

        self.timer.Bind(wx.EVT_TIMER, self.OnTimer)

    def displayMessage(self, message):
        self.oldColor = self.statusbar.GetBackgroundColour()

        self.statusbar.SetBackgroundColour('RED')
        self.statusbar.PushStatusText(message) #, 0)
        self.statusbar.Refresh()
        self.timer.Start(50)

    def OnTimer(self, event):
        self.blick = self.blick + 1
        if self.blick == 25:
            self.statusbar.SetBackgroundColour(self.oldColor)
            self.statusbar.PopStatusText() #0)
            self.statusbar.Refresh()
            self.timer.Stop()
            self.blick = 0

if __name__ == '__main__':
    class Example(wx.Frame):
        def __init__(self, *args, **kwargs):
            super(Example, self).__init__(*args, **kwargs)
            self.statusbar = self.CreateStatusBar()
            self.statusbar.SetStatusText('Welcome to Isabelle')

            self.Bind(wx.EVT_LEFT_DOWN, self.onClick)

            self.bell = DABell(self, self.statusbar)
            self.Show()

        def onClick(self, evt):
            self.bell.displayMessage('Hello world!')


    def main():
        ex = wx.App()
        Example(None, -1, 'Test')
        ex.MainLoop()

    main()
