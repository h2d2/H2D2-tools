# -*- coding: utf-8 -*-
# -*- mode: python -*-
import os

from PyInstaller import compat

def getDatasAndBinaries():
    datas = []
    binaries = []

    if compat.is_linux:
        XPTH = os.path.join(os.environ['HOME'], 'opt/extras/usr/lib/x86_64-linux-gnu')
        if os.path.isdir(XPTH):
            binaries = [ ('%s/*' % XPTH, '.') ]

    return datas, binaries
