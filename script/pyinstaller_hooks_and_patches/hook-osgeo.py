# -*- coding: utf-8 -*-
# -*- mode: python -*-
import os
import sys

# The hook generate a warning with "_pyinstaller_hooks_contrib\hooks\rthooks\pyi_rth_osgeo.py"
# pyinstaller 4.5.1
# gdal 3.3.2


from PyInstaller.utils.hooks import collect_data_files
from PyInstaller.compat      import is_win, is_linux, base_prefix

# With stock osgeo hook, proj files are not copied and
# pyproj is not a hidden import.
root_path = base_prefix
if is_linux:
    share_path = 'share'
elif is_win:
    share_path = os.path.join('Library', 'share')
else:
    raise RuntimeError('Unsupported OS: "%s"' % sys.platform)
tgt_gdal_data = os.path.join(share_path, 'proj')
src_gdal_data = os.path.join(root_path, share_path, 'proj')
datas = [ (src_gdal_data, tgt_gdal_data) ]

# With stock osgeo hook, "*.pyd" are placed in the top directory.
# They are not found on import. Move them to package directory
binaries = [ (p,l) for p, l in collect_data_files('osgeo', include_py_files=True) if p.endswith('.pyd') ]

