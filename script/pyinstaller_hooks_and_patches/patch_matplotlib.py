# -*- coding: utf-8 -*-
# -*- mode: python -*-
import os
import sys

from PyInstaller.utils.hooks import exec_statement

def getDatasAndBinaries():
    datas = []
    binaries = []
    
    # ---  Patch for matplotlib > 3.3.0 with pyinstaller 3.6
    # https://github.com/matplotlib/matplotlib/issues/17962
    # https://stackoverflow.com/questions/63163027/how-to-use-pyinstaller-with-matplotlib-in-use
    pyi_version = exec_statement(
        "import PyInstaller; print(PyInstaller.__version__)")
    if pyi_version.split('.')[:2] == ['3', '6']:
        mpl_version = exec_statement(
            "import matplotlib; print(matplotlib.__version__)")
        if mpl_version.split('.') > ['3', '3', '0']:
            mpl_data_dir = exec_statement(
                "import matplotlib; print(matplotlib._get_data_path())")
            mpl_data = (mpl_data_dir, 'matplotlib/mpl-data')
            datas.append(mpl_data)

    return datas, binaries
