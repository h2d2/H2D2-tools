# -*- coding: utf-8 -*-
# -*- mode: python -*-
import os
import sys
import re

from PyInstaller import compat

def getDatasAndBinaries():
    datas = []
    binaries = []
    
    # ---  Patch for numpy/mkl
    # mkl libraries are now if format "mkl_name[.digit].dll"
    # The code is adapted from "hooks-numpy.core.py"
    if compat.is_win:
        lib_dir = os.path.join(compat.base_prefix, "Library", "bin")
    else:
        lib_dir = os.path.join(compat.base_prefix, "lib")
    if os.path.isdir(lib_dir):
        re_mkllib = re.compile(r'^(lib)?(mkl\w+)(\.\w+)+\.(dll|so|dylib)', re.IGNORECASE)
        dlls_mkl = [f for f in os.listdir(lib_dir) if re_mkllib.match(f)]
        if dlls_mkl:
            binaries += [(os.path.join(lib_dir, f), '.') for f in dlls_mkl]

    return datas, binaries
