## !/usr/bin/env python
# -*- coding: utf-8 -*-
## cython: profile=True
# cython: linetrace=True
#************************************************************************
# --- Copyright (c) Yves Secretan 2022
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import enum
import logging
import os
import sys
import numpy as np

if not getattr(sys, 'frozen', False):
    supPath = os.path.join(os.environ['INRS_DEV'], 'H2D2-tools', 'script')
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

from .FEMesh import FEElementL2, FEElementT3, FEElementT6L, FEMesh
from .FEMatrixCRS import FEMatrixCRS, SFEdge, kimp2klocn, ASSEMBLY

LOGGER = logging.getLogger("H2D2.Tools.Data.StreamFunction")

"""
Fonction courant sur un maillage T6L ou T3

Note:
    La partie résolution ne fonctionne pas bien ou pas du tout.
    En LS (Least-Square) la convergence est super lente, même avec
    les pré-conditionnement.
    En résolution de la fonction courant, la matrice est à
    diagonale nulle. Pas de CG et GMRes ne converge pas sur la
    bonne solution.
"""

FORMULATION    = enum.Enum('FORMULATION',    names='unset least_square pde')
PRECONDITIONER = enum.Enum('PRECONDITIONER', names='unset unity diag sumabs')
SOLVER         = enum.Enum('SOLVER',         names='unset fixpoint CG GMRes')

class StreamFunction:
    def __init__(self):
        self.matrix = None
        self.kindAssembly = None
        self.kindPrecond  = PRECONDITIONER.unset
        self.kindSolver   = SOLVER.unset
        self.mesh         = None
        self.meshL2       = None

    def getMeshForInit(self, mesh):
        if not isinstance(mesh.getElement(0), FEElementT3):
            return mesh
        if not self.meshL2 or self.mesh != mesh:
            self.meshL2 = mesh.genSideGrid()
            self.mesh   = mesh
        return self.meshL2

    def getMeshForDimMatrix(self, mesh):
        return mesh

    def getMeshForAsmK(self, mesh):
        return mesh

    def getMeshForAsmF(self, mesh):
        return mesh

    def asmF_L2(self, mesh, data, klocn):
        raise NotImplementedError

    def asmF_T3(self, mesh, data, klocn):
        raise NotImplementedError

    def asmF_T6L(self, mesh, data, klocn):
        raise NotImplementedError

    def asmF(self, mesh, data, klocn):
        # ---  Controls
        if data.ndim         != 2: raise ValueError('data shall be 2d')
        if data.shape[1]     != 2: raise ValueError('data shall be of shape (:,2)')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if mesh.getNbNodes() != data.shape[0]  : raise ValueError('size mismatch between mesh and data')
        #
        e0 = mesh.getElement(0)
        if isinstance(e0, FEElementL2):
            return self.asmF_L2(mesh, data, klocn)
        if isinstance(e0, FEElementT3):
            return self.asmF_T3(mesh, data, klocn)
        elif isinstance(e0, FEElementT6L):
            return self.asmF_T6L(mesh, data, klocn)
        else:
            raise ValueError('only L2, T3 and T6L elements are supported')

    def asmK_L2(self, mesh, klocn):
        raise NotImplementedError

    def asmK_T3(self, mesh, klocn):
        raise NotImplementedError

    def asmK_T6L(self, mesh, klocn):
        raise NotImplementedError

    def asmK(self, mesh, klocn):
        """
        asmK [summary]

        Laplacian for the potential

        Args:
            mesh  (FEMesh): the mesh
            klocn (iterable): table of dof indexes
        """
        # ---  Controls
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0]: raise ValueError('size mismatch between mesh and klocn')
        #
        e0 = mesh.getElement(0)
        if isinstance(e0, FEElementL2):
            return self.asmK_L2(mesh, klocn)
        elif isinstance(e0, FEElementT3):
            return self.asmK_T3(mesh, klocn)
        elif isinstance(e0, FEElementT6L):
            return self.asmK_T6L(mesh, klocn)
        else:
            raise ValueError('only L2, T3 and T6L elements are supported')

    def __init_L2(self, mesh, data, klocn):
        """
        init solution for L2 elements

        First guess, integrating specific discharge or velocity;
        B.C. are homogeneous Dirichlet.

        Args:
            mesh  (FEMesh):
            data  (DTData):     Specific discharge or velocity
            klocn (2d array):   Table de localisation, les noeuds imposés sont négatif
        """

        # ---  Controls
        if data.ndim         != 2: raise ValueError('data shall be 2d')
        if data.shape[1]     != 2: raise ValueError('data shall be of shape (:,2)')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0]: raise ValueError('size mismatch between mesh and klocn')
        # For submesh, like skin or boundary, data > mesh. integrate works on global indexes
        # if mesh.getNbNodes() != data.shape[0]:  raise ValueError('size mismatch between mesh and data')

        vdlg  = np.zeros(mesh.getNbNodes(), dtype=float)
        kimp  = (klocn < 0).nonzero()[0]    # noeuds imposés
        todos = set(kimp)                   # noeuds à traiter
        dones = set()                       # noeuds traités
        while todos:
            no = todos.pop()
            for e in mesh.getElementsOfNode(no):
                no0, no1 = e.getConnectivities()
                # ---  Node 0
                if no0 == no and no1 not in dones and no1 not in todos:
                    vdlg[no1] = vdlg[no0] + e.integrateVector(data)
                    todos.add(no1)
                # ---  Node 1
                elif no1 == no and no0 not in dones and no0 not in todos:
                    vdlg[no0] = vdlg[no1] - e.integrateVector(data)
                    todos.add(no0)
            dones.add(no)
        # ---  Homogeneous BC
        vdlg[kimp] = 0
        return vdlg

    @staticmethod
    def __init_T3__addOneSide(dummy, vdlg, data, no0, no1, kne, nds):
        """
        cython (0.29.32) has a bug. The type of the first argument is the type
        of the class.
        """
        l2 = FEElementL2(-1, -1, nds[no0], nds[no1])
        qs = l2.integrateVector(data)
        vdlg[kne[no1]] = vdlg[kne[no0]] + qs

    def __init_T3(self, mesh, data, klocn):
        """
        init solution for T3 elements

        First guess, integrating specific discharge or velocity;
        B.C. are homogeneous Dirichlet.

        Args:
            mesh  (FEMesh):
            data  (DTData):     Specific discharge or velocity
            klocn (2d array):   Table de localisation, les noeuds imposés sont négatif
        """
        # ---  Controls
        if data.ndim         != 2: raise ValueError('data shall be 2d')
        if data.shape[1]     != 2: raise ValueError('data shall be of shape (:,2)')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0]: raise ValueError('size mismatch between mesh and klocn')
        if mesh.getNbNodes() != data.shape[0]:  raise ValueError('size mismatch between mesh and data')

        vdlg  = np.zeros(mesh.getNbNodes(), dtype=float)
        kimp  = (klocn < 0).nonzero()[0]    # noeuds imposés
        todos = set(kimp)                   # noeuds à traiter
        dones = set()                       # noeuds traités
        while todos:
            no = todos.pop()
            for e in mesh.getElementsOfNode(no):
                nds = e.getNodes()
                kne = e.getConnectivities()
                # ---  Node 0 - side 0-1 and side 0-2
                if kne[0] == no:
                    if kne[1] not in dones:
                        StreamFunction.__init_T3__addOneSide(None, vdlg, data, 0, 1, kne, nds)
                        todos.add(kne[1])
                    if kne[2] not in dones:
                        StreamFunction.__init_T3__addOneSide(None, vdlg, data, 0, 2, kne, nds)
                        todos.add(kne[2])
                # ---  Node 1 - side 1-2 and side 1-0
                elif kne[1] == no:
                    if kne[2] not in dones:
                        StreamFunction.__init_T3__addOneSide(None, vdlg, data, 1, 2, kne, nds)
                        todos.add(kne[2])
                    if kne[0] not in dones:
                        StreamFunction.__init_T3__addOneSide(None, vdlg, data, 1, 0, kne, nds)
                        todos.add(kne[0])
                # ---  Node 2 - side 2-0 and side 2-1
                elif kne[2] == no:
                    if kne[0] not in dones:
                        StreamFunction.__init_T3__addOneSide(None, vdlg, data, 2, 0, kne, nds)
                        todos.add(kne[0])
                    if kne[1] not in dones:
                        StreamFunction.__init_T3__addOneSide(None, vdlg, data, 2, 1, kne, nds)
                        todos.add(kne[1])
            dones.add(no)
        # ---  Homogeneous BC
        vdlg[kimp] = 0
        return vdlg

    @staticmethod
    def __init_T6L__addOneSide(dummy, vdlg, data, no0, no1, no2, kne, nds):
        """
        cython (0.29.32) has a bug. The type of the first argument is the type
        of the class.
        """
        l2 = FEElementL2(-1, -1, nds[no0], nds[no1])
        qs = l2.integrateVector(data)
        vdlg[kne[no1]] = vdlg[kne[no0]] + qs
        l2 = FEElementL2(-1, -1, nds[no1], nds[no2])
        qs = l2.integrateVector(data)
        vdlg[kne[no2]] = vdlg[kne[no1]] + qs

    def __init_T6L(self, mesh, data, klocn):
        """
        init solution for T6L elements

        First guess, integrating specific discharge or velocity;
        B.C. are homogeneous Dirichlet.

        Args:
            mesh  (FEMesh):
            data  (DTData):     Specific discharge or velocity
            klocn (2d array):   Table de localisation, les noeuds imposés sont négatif
        """
        # ---  Controls
        if data.ndim         != 2: raise ValueError('data shall be 2d')
        if data.shape[1]     != 2: raise ValueError('data shall be of shape (:,2)')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0]: raise ValueError('size mismatch between mesh and klocn')
        if mesh.getNbNodes() != data.shape[0]:  raise ValueError('size mismatch between mesh and data')

        vdlg  = np.zeros(mesh.getNbNodes(), dtype=float)
        kimp  = (klocn < 0).nonzero()[0]    # noeuds imposés
        todos = set(kimp)                   # noeuds à traiter
        dones = set()                       # noeuds traités
        while todos:
            no = todos.pop()
            for e in mesh.getElementsOfNode(no):
                nds = e.getNodes()
                kne = e.getConnectivities()
                # ---  Node 0 - side 0-1-2 and side 0-5-4
                if kne[0] == no:
                    if kne[2] not in dones:
                        StreamFunction.__init_T6L__addOneSide(None, vdlg, data, 0, 1, 2, kne, nds)
                        todos.add(kne[2])
                    if kne[4] not in dones:
                        StreamFunction.__init_T6L__addOneSide(None, vdlg, data, 0, 5, 4, kne, nds)
                        todos.add(kne[4])
                # ---  Node 2 - side 2-3-4 and side 2-1-0
                elif kne[2] == no:
                    if kne[4] not in dones:
                        StreamFunction.__init_T6L__addOneSide(None, vdlg, data, 2, 3, 4, kne, nds)
                        todos.add(kne[4])
                    if kne[0] not in dones:
                        StreamFunction.__init_T6L__addOneSide(None, vdlg, data, 2, 1, 0, kne, nds)
                        todos.add(kne[0])
                # ---  Node 4 - side 4-5-0 and side 4-3-2
                elif kne[4] == no:
                    if kne[0] not in dones:
                        StreamFunction.__init_T6L__addOneSide(None, vdlg, data, 4, 5, 0, kne, nds)
                        todos.add(kne[0])
                    if kne[2] not in dones:
                        StreamFunction.__init_T6L__addOneSide(None, vdlg, data, 4, 3, 2, kne, nds)
                        todos.add(kne[2])
            dones.add(no)
        # ---  Homogeneous BC
        vdlg[kimp] = 0
        return vdlg

    def init(self, mesh, data, klocn):
        """
        init solution

        First guess, integrating specific discharge or velocity;
        B.C. are homogeneous Dirichlet.

        Args:
            mesh  (FEMesh):
            data  (DTData):     Specific discharge or velocity
            klocn (2d array):   Table de localisation, les noeuds imposés sont négatif
        """
        e0 = mesh.getElement(0)
        if isinstance(e0, FEElementL2):
            return self.__init_L2(mesh, data, klocn)
        elif isinstance(e0, FEElementT3):
            return self.__init_T3(mesh, data, klocn)
        elif isinstance(e0, FEElementT6L):
            return self.__init_T6L(mesh, data, klocn)
        else:
            raise ValueError('Invalid element type. Only L2, T3 and T6L are supported')

    def computeStreamFunction(self, mesh, data, kimp=None, nitermax=0, tol_rel=1.0e-2, tol_abs=1.0e-6):
        """
        computeStreamFunction

        Only T3 meshes are supported for iterative refinement.

        Args:
            mesh  (FEMesh):
            data  (DTData):   Specific discharge or velocity
            nitermax (int):   Number of refinement iterations
            solver (enum):    Solver type
            precond (enum):   Solver type
        """
        # ---  Controls
        if (nitermax > 0 and
            self.kindAssembly == ASSEMBLY.byElements and
            not isinstance(mesh.getElement(0), FEElementT3)):
            raise ValueError('Only T3 element are supported for iterative refinement')

        # ---  Impose node 0 from element 0 at potential 0.0
        if not kimp:
            ele0 = mesh.getElement(0)
            nod0 = ele0.getNode(0)
            ino0 = nod0.getGlobalIndex()
            kimp = (ino0,)
        klocn = kimp2klocn(kimp, mesh.getNbNodes())

        # ---  Initial solution
        LOGGER.debug('initial solution')
        vdlg = self.init(self.getMeshForInit(mesh), data, klocn)
        LOGGER.debug('init done')
        if nitermax == 0:
            return vdlg

        # ---  Iterative refinement
        LOGGER.debug('Refining solution')
        self.matrix = FEMatrixCRS(self.kindAssembly)
        LOGGER.debug('dimMatrix')
        self.matrix.dimMatrix(self.getMeshForDimMatrix(mesh), klocn)
        LOGGER.debug('dimMatrix done')
        LOGGER.debug('asmK')
        self.asmK(self.getMeshForAsmK(mesh), klocn)
        LOGGER.debug('asmK done')

        LOGGER.debug('asmF')
        vrhs = self.asmF(self.getMeshForAsmF(mesh), data, klocn)
        LOGGER.debug('asmF done')

        LOGGER.debug('precond')
        if self.kindPrecond == PRECONDITIONER.unity:
            self.matrix.genPrecondIdent()
        elif self.kindPrecond == PRECONDITIONER.diag:
            self.matrix.genPrecondKDiag()
        elif self.kindPrecond == PRECONDITIONER.sumabs:
            self.matrix.genPrecondKtDiag()
        else:
            raise RuntimeError('Invalid preconditionner: %s' % self.kindPrecond)
        LOGGER.debug('precond done')

        LOGGER.debug('solve')
        if self.kindSolver == SOLVER.fixpoint:
            vsol = self.matrix.solveFixPointDiag(vrhs, vdlg, tol_rel=tol_rel, tol_abs=tol_abs, nitermax=nitermax)
        elif self.kindSolver == SOLVER.CG:
            vsol = self.matrix.solveCG(vrhs, vdlg, tol_rel=tol_rel, tol_abs=tol_abs, nitermax=nitermax)
        elif self.kindSolver == SOLVER.GMRes:
            vsol = self.matrix.solveScipyGMRes(vrhs, vdlg, tol_rel=tol_rel, tol_abs=tol_abs, nitermax=nitermax)
        else:
            raise RuntimeError('Invalid solver: %s' % self.kindSolver)
        LOGGER.debug('solve done')

        return vsol

class StreamFunctionLS(StreamFunction):
    """
    StreamFunctionLS _summary_

    init:   T3-> L2 in parent
    dim:    T3-> L2
    K:      neutral to element type
    F:      T3-> L2

    Args:
        StreamFunction (_type_): _description_
    """
    def __init__(self):
        super().__init__()
        self.kindAssembly = ASSEMBLY.byElementEdges
        self.kindPrecond  = PRECONDITIONER.sumabs
        self.kindSolver   = SOLVER.CG

    def getMeshForDimMatrix(self, mesh):
        if not isinstance(mesh.getElement(0), FEElementT3):
            return mesh
        if not self.meshL2 or self.mesh != mesh:
            self.meshL2 = mesh.genSideGrid()
            self.mesh   = mesh
        return self.meshL2

    def getMeshForAsmF(self, mesh):
        if not isinstance(mesh.getElement(0), FEElementT3):
            return mesh
        if not self.meshL2 or self.mesh != mesh:
            self.meshL2 = mesh.genSideGrid()
            self.mesh   = mesh
        return self.meshL2

    def asmF_L2(self, mesh, data, klocn):
        # ---  Controls
        if data.ndim         != 2: raise ValueError('data shall be 2d')
        if data.shape[1]     != 2: raise ValueError('data shall be of shape (:,2)')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if mesh.getNbNodes() != data.shape[0]  : raise ValueError('size mismatch between mesh and data')

        ndlt = klocn.size
        vfg = np.zeros(ndlt, dtype=float)
        for e in mesh.iterElements():
            no0, no1 = e.getConnectivities()
            qs = e.integrateVector(data)
            #if (id := klocn[no0,0]) >= 0: vfg[id] -= qs
            #if (id := klocn[no1,0]) >= 0: vfg[id] += qs
            if klocn[no0,0] >= 0: vfg[klocn[no0,0]] -= qs
            if klocn[no1,0] >= 0: vfg[klocn[no1,0]] += qs
        # ---  Imposed dof have value 0.0
        kimp = (np.ravel(klocn) < 0).nonzero()
        assert np.all(vfg[kimp] == 0)
        return vfg

    def asmF_T3(self, mesh, data, klocn):
        # ---  Controls
        if data.ndim         != 2: raise ValueError('data shall be 2d')
        if data.shape[1]     != 2: raise ValueError('data shall be of shape (:,2)')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if mesh.getNbNodes() != data.shape[0]  : raise ValueError('size mismatch between mesh and data')

        ndlt = klocn.size
        vfg = np.zeros(ndlt, dtype=float)
        sides = set()
        for e in mesh.iterElements():
            nds = e.getNodes()
            kne = e.getConnectivities()
            # ---  Side 0-1
            s = SFEdge(kne[0], kne[1])
            if s in sides:
                sides.discard(s)
            else:
                l2 = FEElementL2(-1, -1, nds[0], nds[1])
                qs = l2.integrateVector(data)
                #if (id := klocn[kne[0],0]) >= 0: vfg[id] -= qs
                #if (id := klocn[kne[1],0]) >= 0: vfg[id] += qs
                if klocn[kne[0],0] >= 0: vfg[klocn[kne[0],0]] -= qs
                if klocn[kne[1],0] >= 0: vfg[klocn[kne[1],0]] += qs
                sides.add(s)
            # ---  Side 1-2
            s = SFEdge(kne[1], kne[2])
            if s in sides:
                sides.discard(s)
            else:
                l2 = FEElementL2(-1, -1, nds[1], nds[2])
                qs = l2.integrateVector(data)
                #if (id := klocn[kne[1],0]) >= 0: vfg[id] -= qs
                #if (id := klocn[kne[2],0]) >= 0: vfg[id] += qs
                if klocn[kne[1],0] >= 0: vfg[klocn[kne[1],0]] -= qs
                if klocn[kne[2],0] >= 0: vfg[klocn[kne[2],0]] += qs
                sides.add(s)
            # ---  Side 2-0
            s = SFEdge(kne[2], kne[0])
            if s in sides:
                sides.discard(s)
            else:
                l2 = FEElementL2(-1, -1, nds[2], nds[0])
                qs = l2.integrateVector(data)
                #if (id := klocn[kne[2],0]) >= 0: vfg[id] -= qs
                #if (id := klocn[kne[0],0]) >= 0: vfg[id] += qs
                if klocn[kne[2],0] >= 0: vfg[klocn[kne[2],0]] -= qs
                if klocn[kne[0],0] >= 0: vfg[klocn[kne[0],0]] += qs
                sides.add(s)
        # ---  Imposed dof have value 0.0
        kimp = (np.ravel(klocn) < 0).nonzero()
        assert np.all(vfg[kimp] == 0)
        return vfg

    def asmF_T6L(self, mesh, data, klocn):
        # ---  Controls
        if data.ndim         != 2: raise ValueError('data shall be 2d')
        if data.shape[1]     != 2: raise ValueError('data shall be of shape (:,2)')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if mesh.getNbNodes() != data.shape[0]  : raise ValueError('size mismatch between mesh and data')

        ndlt = klocn.size
        vfg = np.zeros(ndlt, dtype=float)
        sides = set()
        for e in mesh.iterElements():
            nds = e.getNodes()
            kne = e.getConnectivities()
            # ---  Side 0-1-2
            s = SFEdge(kne[0], kne[2])
            if s in sides:
                sides.discard(s)
            else:
                l2 = FEElementL2(-1, -1, nds[0], nds[1])
                qs = l2.integrateVector(data)
                #if (id := klocn[kne[0],0]) >= 0: vfg[id] -= qs
                #if (id := klocn[kne[1],0]) >= 0: vfg[id] += qs
                if klocn[kne[0],0] >= 0: vfg[klocn[kne[0],0]] -= qs
                if klocn[kne[1],0] >= 0: vfg[klocn[kne[1],0]] += qs
                l2 = FEElementL2(-1, -1, nds[1], nds[2])
                qs = l2.integrateVector(data)
                #if (id := klocn[kne[1],0]) >= 0: vfg[id] -= qs
                #if (id := klocn[kne[2],0]) >= 0: vfg[id] += qs
                if klocn[kne[1],0] >= 0: vfg[klocn[kne[1],0]] -= qs
                if klocn[kne[2],0] >= 0: vfg[klocn[kne[2],0]] += qs
                sides.add(s)
            # ---  Side 2-3-4
            s = SFEdge(kne[2], kne[4])
            if s in sides:
                sides.discard(s)
            else:
                l2 = FEElementL2(-1, -1, nds[2], nds[3])
                qs = l2.integrateVector(data)
                #if (id := klocn[kne[2],0]) >= 0: vfg[id] -= qs
                #if (id := klocn[kne[3],0]) >= 0: vfg[id] += qs
                if klocn[kne[2],0] >= 0: vfg[klocn[kne[2],0]] -= qs
                if klocn[kne[3],0] >= 0: vfg[klocn[kne[3],0]] += qs
                l2 = FEElementL2(-1, -1, nds[3], nds[4])
                qs = l2.integrateVector(data)
                #if (id := klocn[kne[3],0]) >= 0: vfg[id] -= qs
                #if (id := klocn[kne[4],0]) >= 0: vfg[id] += qs
                if klocn[kne[3],0] >= 0: vfg[klocn[kne[3],0]] -= qs
                if klocn[kne[4],0] >= 0: vfg[klocn[kne[4],0]] += qs
                sides.add(s)
            # ---  Side 4-5-0
            s = SFEdge(kne[4], kne[0])
            if s in sides:
                sides.discard(s)
            else:
                l2 = FEElementL2(-1, -1, nds[4], nds[5])
                qs = l2.integrateVector(data)
                #if (id := klocn[kne[4],0]) >= 0: vfg[id] -= qs
                #if (id := klocn[kne[5],0]) >= 0: vfg[id] += qs
                if klocn[kne[4],0] >= 0: vfg[klocn[kne[4],0]] -= qs
                if klocn[kne[5],0] >= 0: vfg[klocn[kne[5],0]] += qs
                l2 = FEElementL2(-1, -1, nds[5], nds[0])
                qs = l2.integrateVector(data)
                #if (id := klocn[kne[5],0]) >= 0: vfg[id] -= qs
                #if (id := klocn[kne[0],0]) >= 0: vfg[id] += qs
                if klocn[kne[5],0] >= 0: vfg[klocn[kne[5],0]] -= qs
                if klocn[kne[0],0] >= 0: vfg[klocn[kne[0],0]] += qs
                sides.add(s)
        # ---  Imposed dof have value 0.0
        kimp = (np.ravel(klocn) < 0).nonzero()
        assert np.all(vfg[kimp] == 0)
        return vfg

    def asmK_slow(self, mesh, klocn):
        """
        __asmK_slow [summary]

        Least-Square version

        Args:
            mesh  (FEMesh): the mesh
            klocn (iterable): table of dof indexes
        """
        assert self.matrix
        mtx = self.matrix   # short cut to matrix

        vke = np.array([ [1, -1], [-1, 1]])
        sides = set()
        for e in mesh.iterElements():
            kne = e.getConnectivities()
            #
            s = SFEdge(kne[0], kne[2])
            if s in sides:
                sides.discard(s)
            else:
                mtx.asmKe(vke, (kne[0], kne[1]))
                mtx.asmKe(vke, (kne[1], kne[2]))
                sides.add(s)
            #
            s = SFEdge(kne[2], kne[4])
            if s in sides:
                sides.discard(s)
            else:
                mtx.asmKe(vke, (kne[2], kne[3]))
                mtx.asmKe(vke, (kne[3], kne[4]))
                sides.add(s)
            #
            s = SFEdge(kne[4], kne[0])
            if s in sides:
                sides.discard(s)
            else:
                mtx.asmKe(vke, (kne[4], kne[5]))
                mtx.asmKe(vke, (kne[5], kne[0]))
                sides.add(s)
        #=======================================================
        # Begin DEBUG code
        ndlt = len(mtx.ia) - 1
        for i in range(1,ndlt):
            j0 = mtx.ia[i]
            j1 = mtx.ia[i+1]
            jj = mtx.getJAIdx(i, i)
            assert mtx.va[jj] == (j1-j0-1)
            assert np.all(mtx.va[j0  :jj] == -1)
            assert np.all(mtx.va[jj+1:j1] == -1)
        # End DEBUG code
        #=======================================================
        # ---  Imposed dof have value 1.0
        kimp = (np.ravel(klocn) < 0).nonzero()
        for i in kimp:
            for j in i:
                jj = mtx.getJAIdx(j, j)
                mtx.va[jj] = 1

    def asmK_fast(self, mesh, klocn):
        """
        __asmK_fast [summary]

        Least-Square version

        Args:
            mesh  (FEMesh): the mesh
            klocn (iterable): table of dof indexes
        """
        assert self.matrix
        mtx = self.matrix   # short cut to matrix

        # ---  Initialize to -1
        mtx.va = np.full(mtx.ia[-1], -1.0, dtype=float)
        # ---  Set diagonal entries
        nnt = len(mtx.ia) - 1
        for i in range(nnt):
            j0 = mtx.ia[i]
            j1 = mtx.ia[i+1]
            jj = mtx.getJAIdx(i, i)
            mtx.va[jj] = (j1-j0-1)
        # ---  Imposed dof have value 1.0
        kimp = (np.ravel(klocn) < 0).nonzero()
        for i in kimp:
            for j in i:
                jj = mtx.getJAIdx(j, j)
                mtx.va[jj] = 1

    # cython n'accepte pas la syntaxe simple;
    # remplace par des fonctions.
    # asmK_T3  = asmK_fast
    # asmK_T6L = asmK_fast
    def asmK_T3(self, mesh, klocn):
        self.asmK_fast(mesh, klocn)

    def asmK_T6L(self, mesh, klocn):
        self.asmK_fast(mesh, klocn)

class StreamFunctionPDE(StreamFunction):
    """
    StreamFunctionPDE _summary_

    init:   T3-> L2 in parent
    dim:    T3-> L2
    K:      must stay with 2D element
    F:      must stay with 2D element

    Args:
        StreamFunction (_type_): _description_
    """
    def __init__(self):
        super().__init__()
        self.kindAssembly = ASSEMBLY.byElements
        self.kindPrecond  = PRECONDITIONER.sumabs
        self.kindSolver   = SOLVER.GMRes

    def getMeshForDimMatrix(self, mesh):
        if not isinstance(mesh.getElement(0), FEElementT3):
            return mesh
        if not self.meshL2 or self.mesh != mesh:
            self.meshL2 = mesh.genSideGrid()
            self.mesh   = mesh
        return self.meshL2

    def asmF_T3(self, mesh, data, klocn):
        assert isinstance(mesh.getElement(0), FEElementT3)
        assert data.ndim         == 2
        assert data.shape[1]     == 2
        assert klocn.ndim        == 2
        assert klocn.shape[1]    == 1
        assert mesh.getNbNodes() == klocn.shape[0]
        assert mesh.getNbNodes() == data.shape[0]

        ndlt = klocn.size
        vfg = np.zeros(ndlt, dtype=float)
        # ---  Filter skin
        sides = set()
        for e in mesh.iterElements():
            nds = e.getNodes()
            kne = e.getConnectivities()
            # ---  Side 0-1
            s = SFEdge(kne[0], kne[1], (e, 0))
            if s in sides:
                sides.discard(s)
            else:
                sides.add(s)
            # ---  Side 1-2
            s = SFEdge(kne[1], kne[2], (e, 1))
            if s in sides:
                sides.discard(s)
            else:
                sides.add(s)
            # ---  Side 2-0
            s = SFEdge(kne[2], kne[0], (e, 2))
            if s in sides:
                sides.discard(s)
            else:
                sides.add(s)
        # ---  Loop on skin elements
        for side in sides:
            e, s = side.info
            nds = e.getNodes()
            kne = e.getConnectivities()
            # ---  Side 0-1
            if s == 0:
                nds = nds[0:2]
                kne = kne[0:2]
            elif s == 1:
                nds = nds[1:3]
                kne = kne[1:3]
            else:
                nds = (nds[2], nds[0])
                kne = (kne[2], kne[0])
            l2 = FEElementL2(-1, -1, nds[0], nds[1])
            qs = 0.5 * l2.integrateVector(data)
            #if (id := klocn[kne[0],0]) >= 0: vfg[id] += qs
            #if (id := klocn[kne[1],0]) >= 0: vfg[id] += qs
            if klocn[kne[0],0] >= 0: vfg[klocn[kne[0],0]] += qs
            if klocn[kne[1],0] >= 0: vfg[klocn[kne[1],0]] += qs
        # ---  Imposed dof have value 0.0
        kimp = (np.ravel(klocn) < 0).nonzero()
        assert np.all(vfg[kimp] == 0)
        return vfg

    def asmF_T6L(self, mesh, data, klocn):
        assert isinstance(mesh.getElement(0), FEElementT6L)
        assert data.ndim         == 2
        assert data.shape[1]     == 2
        assert klocn.ndim        == 2
        assert klocn.shape[1]    == 1
        assert mesh.getNbNodes() == klocn.shape[0]
        assert mesh.getNbNodes() == data.shape[0]

        ndlt = klocn.size
        vfg = np.zeros(ndlt, dtype=float)
        # ---  Filter skin
        sides = set()
        for e in mesh.iterElements():
            nds = e.getNodes()
            kne = e.getConnectivities()
            # ---  Side 0-1-2
            s = SFEdge(kne[0], kne[2], (e, 0))
            if s in sides:
                sides.discard(s)
            else:
                sides.add(s)
            # ---  Side 2-3-4
            s = SFEdge(kne[2], kne[4], (e, 1))
            if s in sides:
                sides.discard(s)
            else:
                sides.add(s)
            # ---  Side 4-5-0
            s = SFEdge(kne[4], kne[0], (e, 3))
            if s in sides:
                sides.discard(s)
            else:
                sides.add(s)
        # ---  Loop on skin elements
        for side in sides:
            e, s = side.info
            nds = e.getNodes()
            kne = e.getConnectivities()
            # ---  Side 0-1-2
            if s == 0:
                nds = nds[0:3]
                kne = kne[0:3]
            elif s == 1:
                nds = nds[2:5]
                kne = kne[2:5]
            else:
                nds = (nds[4], nds[5], nds[0])
                kne = (kne[4], kne[5], kne[0])
            l2 = FEElementL2(-1, -1, nds[0], nds[1])
            qs = 0.5 * l2.integrateVector(data)
            #if (id := klocn[kne[0],0]) >= 0: vfg[id] += qs
            #if (id := klocn[kne[1],0]) >= 0: vfg[id] += qs
            if klocn[kne[0],0] >= 0: vfg[klocn[kne[0],0]] += qs
            if klocn[kne[1],0] >= 0: vfg[klocn[kne[1],0]] += qs
            l2 = FEElementL2(-1, -1, nds[1], nds[2])
            qs = 0.5 * l2.integrateVector(data)
            #if (id := klocn[kne[1],0]) >= 0: vfg[id] += qs
            #if (id := klocn[kne[2],0]) >= 0: vfg[id] += qs
            if klocn[kne[1],0] >= 0: vfg[klocn[kne[1],0]] += qs
            if klocn[kne[2],0] >= 0: vfg[klocn[kne[2],0]] += qs
        # ---  Imposed dof have value 0.0
        kimp = (np.ravel(klocn) < 0).nonzero()
        assert np.all(vfg[kimp] == 0)
        return vfg

    def asmK_T3_slow(self, mesh, klocn):
        """
        __asmK_slow [summary]

        Laplacian for the stream function for T3 element.
        Galerkin formulation with part integration.
        Assemble -K.

        Args:
            mesh  (FEMesh): the mesh
            klocn (iterable): table of dof indexes
        """
        assert isinstance(mesh.getElement(0), FEElementT3)
        assert klocn.ndim        == 2
        assert klocn.shape[1]    == 1
        assert mesh.getNbNodes() == klocn.shape[0]
        assert self.matrix
        mtx = self.matrix   # short cut to matrix

        mtx.va[:] = 0
        for e in mesh.iterElements():
            kne  = e.getConnectivities()
            (vkx, vex), (vky, vey) = e.jacobian()
            c   = 0.5 * e.detJ()
            ddx = np.array([(-vkx-vex, vkx, vex)])          # / 2
            ddy = np.array([(-vky-vey, vky, vey)])          # / 2
            vke = c * (ddx.T*ddy - ddy.T*ddx)               # 4 / (2*2)
            vke[(0,1,2), (0,1,2)] += c * 1.0e-16
            mtx.asmKe(vke, kne)
        # ---  Imposed dof have value 1.0
        kimp = (np.ravel(klocn) < 0).nonzero()
        for i in kimp:
            for j in i:
                jj = mtx.getJAIdx(j, j)
                mtx.va[jj] = 1

    def asmK_T6L_slow(self, mesh, klocn):
        """
        asmK_slow [summary]

        Laplacian for the potential for a T6L element.
        Galerkin formulation with part integration.
        Assemble -K.

        Args:
            mesh  (FEMesh): the mesh
            klocn (iterable): table of dof indexes
        """
        assert isinstance(mesh.getElement(0), FEElementT6L)
        assert klocn.ndim        == 2
        assert klocn.shape[1]    == 1
        assert mesh.getNbNodes() == klocn.shape[0]
        assert self.matrix
        mtx = self.matrix   # short cut to matrix

        mtx.va[:] = 0
        diag = (0, 1, 2, 3, 4, 5)
        for e in mesh.iterElements():
            kne  = e.getConnectivities()
            (vkx, vex), (vky, vey) = e.jacobian()
            c   = e.detJ() / 2
            ddx = np.array([(-vkx-vex, vkx, vex)])          # / 2
            ddy = np.array([(-vky-vey, vky, vey)])          # / 2
            vke = (ddx.T*ddy - ddy.T*ddx) * c               # 4 / (2*2)
            vke[diag, diag] += 1.0e-16
            mtx.asmKe(vke, (kne[0], kne[1], kne[5]))
            mtx.asmKe(vke, (kne[1], kne[2], kne[3]))
            mtx.asmKe(vke, (kne[5], kne[3], kne[4]))
            mtx.asmKe(vke, (kne[3], kne[5], kne[1]))   # (-*-)=+ for internal T3
        # ---  Imposed dof have value 1.0
        kimp = (np.ravel(klocn) < 0).nonzero()
        for i in kimp:
            for j in i:
                jj = mtx.getJAIdx(j, j)
                mtx.va[jj] = 1

    # cython n'accepte pas la syntaxe simple;
    # remplace par des fonctions.
    # asmK_T3  = asmK_T3_slow
    # asmK_T6L = asmK_T6L_slow
    def asmK_T3(self, mesh, klocn):
        self.asmK_T3_slow(mesh, klocn)

    def asmK_T6L(self, mesh, klocn):
        self.asmK_T6L_slow(mesh, klocn)

def computeStreamFunction(mesh, qxqy, kimp=None,
                          nitermax=0,
                          tol_rel=1.0e-2,
                          tol_abs=1.0e-6,
                          formulation=FORMULATION.least_square):
    if formulation == FORMULATION.least_square:
        sf = StreamFunctionLS()
    elif formulation == FORMULATION.pde:
        sf = StreamFunctionPDE()
    else:
        raise RuntimeError('Invalid formulation: %s' % formulation)
    return sf.computeStreamFunction(mesh, qxqy, kimp, nitermax=nitermax, tol_rel=tol_rel, tol_abs=tol_abs)

if __name__ == "__main__":
    from . import DTData

    def main(f_out, f_cor, f_ele, f_vno, cols):
        assert not cols or len(cols) == 2
        LOGGER.info('Stream function: Start')

        LOGGER.debug('read grid')
        meshT6L = FEMesh([f_cor, f_ele])
        meshT6L.centerEdges()
        meshT3 = meshT6L.genT3Grid()
        LOGGER.debug('read grid done')

        LOGGER.debug('read data')
        data = DTData.DTData(file=f_vno, cols=cols)
        qxqy = data.getDataAtStep(0)
        LOGGER.debug('read data done')
        vsol = computeStreamFunction(meshT3, qxqy, nitermax=5)

        fc = DTData.DTData(data=[(0.0, vsol)])
        fc.writeAsAscii(f_out)

    import cProfile
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    # d_grd = 'E:/Projets/ECCC/MamawiLake/simu/grid'
    # d_vno = 'E:/Projets/ECCC/MamawiLake/simu/simu-5cm'
    # f_cor = os.path.join(d_grd, 'Mamawifine.cor')
    # f_ele = os.path.join(d_grd, 'Mamawifine.ele')
    # f_vno = os.path.join(d_vno, 'Mamawifinefin2.5cm.fin')
    # f_out = os.path.join(d_vno, 'Mamawifinefin2.5cm.pst.sf')

    # d_grd = 'E:/Projets/ECCC/OP/Débit Sortant/20x5'
    # d_vno = 'E:/Projets/ECCC/OP/Débit Sortant/20x5'
    # f_cor = os.path.join(d_grd, 'test.cor')
    # f_ele = os.path.join(d_grd, 'test.ele')
    # f_vno = os.path.join(d_vno, 'test.fin.sf')
    # f_out = os.path.join(d_vno, 'test.pst.sf')

    d_grd = 'E:/Projets/Carillon/MiseEau_H2D2/ef/200_grid'
    d_vno = 'E:/Projets/Carillon/MiseEau_H2D2/ef/03'
    f_cor = os.path.join(d_grd, 'car_amont.cor')
    f_ele = os.path.join(d_grd, 'car_amont.ele')
    f_vno = os.path.join(d_vno, 'f_ddl.fin')
    f_out = os.path.join(d_vno, 'test.pst.sf')

    #cProfile.run('main(f_out, f_cor, f_ele, f_vno, cols=(0,1))', sort='tottime')
    main(f_out, f_cor, f_ele, f_vno, cols=(0,1))
