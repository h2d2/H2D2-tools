//************************************************************************
// --- Copyright (c) Yves Secretan 2021
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Classe:
//    QTBytestream
//
// Description:
//
// Attributs:
//
// Notes:
//************************************************************************
#ifndef QTBytestream_c_H
#define QTBytestream_c_H

#include <cstdint>      // uint8_t
#include <cstddef>      // size_t

// ---  Forward declaration
class QTTree;
class QTNode;
class QTItem;

class QTBytestream
{
private:
   static const size_t GUID_LEN = 36;
   typedef char guid_t[GUID_LEN+2];
   static const guid_t GUID;

private:
   uint8_t* m_base;
   uint8_t* m_data;
   uint8_t* m_top;
   size_t   m_size;

   void    grow         ();

   template <typename TTType>
   QTBytestream& operator<< (const TTType&);
   QTBytestream& operator<< (const QTNode&);
   QTBytestream& operator<< (const QTItem&);
   QTBytestream& operator<< (const guid_t&);

   template <typename TTType>
   QTBytestream& operator>> (TTType&);
   QTBytestream& operator>> (QTNode&);
   QTBytestream& operator>> (QTItem&);
   QTBytestream& operator>> (guid_t&);

public:
   QTBytestream   ();
   QTBytestream   (uint8_t* buffer, size_t sz);
   QTBytestream   (const QTBytestream& other);
   ~QTBytestream  ();
   QTBytestream& operator = (const QTBytestream& other);

   size_t   size() const;
   uint8_t* data() const;

   QTBytestream& operator<< (const QTTree&);
   QTBytestream& operator>> (QTTree&);
};

#include "QTBytestream_c.hpp"

#endif // QTBytestream_c_H
