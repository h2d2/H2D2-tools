# distutils: language = c++
## # distutils: sources = QTQuadTree_c.cpp

from cpython         cimport array

from cython.operator cimport dereference as cy_dereference

cimport numpy as np
import  numpy as npp

## cdef extern from "QTQuadTree_c.h":
##     cdef cppclass QTTree:
##         QTTree()
##         QTTree(double[4])
##         QTTree(double[4], long, long)
##         
##         void         insert     (long, double[4])
##         void         remove     (long, double[4])
##         vector[long] intersect  (double[4])
## 
##         long         size       ()
##         long         maxItemsEff()
##         long         maxDepthEff()
##         long         maxItemsLmt()
##         long         maxDepthLmt()

# Create a Cython extension type which holds a C++ instance
# as an attribute and create a bunch of forwarding methods
# Python extension type.
cdef class QTQuadTree:
    # Attributes are declared in the .pxd file
    # cdef QTTree *head

    def __cinit__(QTQuadTree self, *args, **kwargs):
        # print('In QTQuadTree.__cinit__')
        self.head = new QTTree()
        
    def __init__(QTQuadTree self, bbox=None, long max_items = -1, long max_depth=-1):
        # print('In QTQuadTree.__init__')
        cdef array.array bb4
        if bbox:
            bb4 = array.array('d', bbox)
            if max_items == -1 or max_depth == -1:
                self.head.init(bb4.data.as_doubles)
            else:
                self.head.init(bb4.data.as_doubles, max_items, max_depth)

    def __getstate__(QTQuadTree self):
        # print('In QTQuadTree.__getstate__')
        cdef QTBytestream obs
        obs << cy_dereference(self.head)
        cdef np.uintp_t  bufLen = obs.size()
        cdef np.uint8_t* bufPtr = obs.data()
        cdef np.uint8_t[::1] bufView = <np.uint8_t[:bufLen]> bufPtr
        # Copy data to transfer ownership
        # as obs will be destroyed on function exit
        cdef np.ndarray bufNDar = npp.copy(bufView)
        # print(bufLen)
        # print(bufNDar)
        # print('Done QTQuadTree.__getstate__')
        return (bufLen, bufNDar)

    def __setstate__(QTQuadTree self, state):
        # print('In QTQuadTree.__setstate__')
        cdef np.uintp_t bufLen
        cdef np.ndarray bufNDar
        bufLen, bufNDar = state
        cdef np.uint8_t* bufPtr = <np.uint8_t*> bufNDar.data
        cdef QTBytestream ibs = QTBytestream(bufPtr, bufLen)
        ibs >> cy_dereference(self.head)
        # print('Done QTQuadTree.__setstate__')

    def __dealloc__(QTQuadTree self):
        del self.head
        self.head = NULL

    def insert(QTQuadTree self, long item, bbox):
        cdef array.array bb4 = array.array('d', bbox)
        self.head.insert(item, bb4.data.as_doubles)

    def remove(QTQuadTree self, long item, bbox):
        cdef array.array bb4 = array.array('d', bbox)
        self.head.remove(item, bb4.data.as_doubles)

    def intersect(QTQuadTree self, bbox):
        cdef array.array bb4 = array.array('d', bbox)
        return self.head.intersect(bb4.data.as_doubles)

    def bbox(QTQuadTree self):
        cdef const double* bb = self.head.bbox()
        return (bb[0], bb[1], bb[2], bb[3])
        
    def size(QTQuadTree self):
        return self.head.size()

    def maxItemsEff(QTQuadTree self):
        return self.head.maxItemsEff()

    def maxDepthEff(QTQuadTree self):
        return self.head.maxDepthEff()

    def maxItemsLmt(QTQuadTree self):
        return self.head.maxItemsLmt()

    def maxDepthLmt(QTQuadTree self):
        return self.head.maxDepthLmt()

cpdef object rebuildQTQuadTree(object state):
    o = QTQuadTree()
    o.setState(state)
    return o
