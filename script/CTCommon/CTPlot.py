#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import sys
if 'matplotlib' not in sys.modules:
    import matplotlib
import matplotlib.figure
import matplotlib.ticker
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg

import enum
import wx

from . import CTCurve
from . import CTUtil

# The class Plot encapsulte the common part to plot a collection
# of curves. Children shall implement the data assignation.

AxisFormatter = enum.Enum('AxisFormatter', 'none seconds_to_iso epoch_to_isoutc')

class Plot:
    def __init__(self, parent, *args, **kwargs):
        self.fig    = kwargs.pop('fig',  None)
        self.ax     = kwargs.pop('axes', None)
        self.xlabel = kwargs.pop('xlabel', 'Simulation Time')
        self.ylabel = kwargs.pop('ylabel', 'h')
        self.xfmttr = kwargs.pop('xformatter', AxisFormatter.seconds_to_iso)
        self.yfmttr = kwargs.pop('yformatter', AxisFormatter.none)

        if not self.fig:
            sp_param = matplotlib.figure.SubplotParams(left=0.10, bottom=0.15, right=0.98, top=0.96)
            self.fig = matplotlib.figure.Figure(subplotpars=sp_param)
            self.canvas = FigureCanvasWxAgg(parent, -1, self.fig)
        else:
            self.canvas = self.fig.canvas
        if not self.ax:
            self.ax  = self.fig.add_subplot(111)

        self.sourcex = 'stime'
        self.reset()

    def reset(self):
        self.curves = []
        CTCurve.Curves.reset_counter()

        self.vpx1 = 0.0
        self.vpx2 = 3600*12
        self.vpy1 = -10.0
        self.vpy2 =  10.0

        self.xmin = self.vpx1
        self.xmax = self.vpx2
        self.ymin = self.vpy1
        self.ymax = self.vpy2

        self.__init_figure()

    def __get_ticks_size(self):
        nDays = (self.vpx2-self.vpx1) / (24*3600)
        nDays = int(nDays + 0.9)
        if (nDays <= 1):
            tksH = (1.0, 0.25)
        elif (nDays == 2):
            tksH = (2.0, 0.5)
        elif (nDays <= 6):
            tksH = (6.0, 1.0)
        elif (nDays <= 12):
            tksH = (12.0, 3.0)
        else:
            n = int(nDays/6.0 + 0.9)
            tksH = (n*4, n)
        return tksH[0]*3600, tksH[1]*3600

    def __init_figure(self):
        self.ax.clear()

        self.ax.set_xlim  (self.vpx1, self.vpx2)
        if self.xfmttr in (AxisFormatter.seconds_to_iso, AxisFormatter.epoch_to_isoutc):
            if self.xfmttr is AxisFormatter.seconds_to_iso:
                fnc = CTUtil.seconds_to_iso
            elif self.xfmttr is AxisFormatter.epoch_to_isoutc:
                fnc = CTUtil.epoch_to_isoutc
            tmaj, tmin = self.__get_ticks_size()
            self.ax.xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(fnc))
            self.ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(tmaj))
            self.ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(tmin))
            self.fig.autofmt_xdate()
        else:
            pass
        self.ax.set_xlabel(self.xlabel)

        self.ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
        self.ax.axhline(0.0, linestyle='-', color='darkgrey', alpha=0.7)
        self.ax.set_axisbelow(True)
        self.ax.set_ylim  (self.vpy1, self.vpy2)
        self.ax.set_ylabel(self.ylabel)

    def load_data_from_file(self, *args):
        raise NotImplementedError

    def load_data_from_table(self, *args):
        raise NotImplementedError

    def draw_plot(self):
        CSS = self.fill_plot()
        self.redraw()
        return CSS

    def fill_plot(self):
        self.__init_figure()
        return [ c.fill_plot(self.ax) for c in self.curves ]

    def get_canvas(self):
        return self.canvas

    def get_colors (self):
        clr = []
        for c in self.curves:
            clr.append( c.get_colors() )
        return clr

    def get_background_color(self):
        return self.ax.get_facecolor()

    def set_background_color(self, clr):
        self.fig.set_facecolor(clr)
        self.ax.set_facecolor(clr)

    def get_data_spnx(self):
        """Data span in x"""
        return (self.xmin, self.xmax)

    def get_data_spny(self):
        """Data span in y"""
        return (self.ymin, self.ymax)

    def get_entry(self, p, x):
        for c in self.curves:
            itm = c.get_entry(p)
            if (itm): return itm
        return None

    def get_stats (self):
        txt = []
        for c in self.curves:
            txt.append( c.get_stats() )
        return txt

    def get_view_ctrx(self):
        return (self.vpx1 + self.vpx2) / 2

    def get_view_ctry(self):
        return (self.vpy1 + self.vpy2) / 2

    def get_view_rngx(self):
        return (self.vpx2 - self.vpx1)

    def get_view_rngy(self):
        return (self.vpy2 - self.vpy1)

    def get_view_vptx(self):
        return (self.vpx1, self.vpx2)

    def get_view_vpty(self):
        return (self.vpy1, self.vpy2)

    def get_visibility(self):
        r = [ ]
        for c in self.curves:
            r.append( c.get_visibility() )
        return r

    def redraw(self):
        if self.xfmttr in (AxisFormatter.seconds_to_iso, AxisFormatter.epoch_to_isoutc):
            tmaj, tmin = self.__get_ticks_size()
            self.ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(tmaj))
            self.ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(tmin))
            self.fig.autofmt_xdate()
        self.ax.set_xlim(self.vpx1, self.vpx2)
        self.ax.set_ylim(self.vpy1, self.vpy2)
        self.canvas.draw()

    def reset_view(self):
        self.reset_viewx()
        self.reset_viewy()

    def reset_viewx(self):
        xmin, xmax = self.get_data_spnx()
        self.set_view_vptx(xmin, xmax)

    def reset_viewy(self):
        ymin, ymax = self.get_data_spny()
        self.set_view_vpty(ymin, ymax)

    def set_formatterx(self, frmtr):
        assert(frmtr in AxisFormatter)
        self.xfmttr = frmtr
        
    def set_formattery(self, frmtr):
        assert(frmtr in AxisFormatter)
        self.yfmttr = frmtr
        
    def set_sourcex(self, attrib):
        pass

    def set_view_x(self, viewCtr, viewRng):
        try:
            dataSpan = self.get_data_spnx()
            self.vpx1 = max(viewCtr-0.50*viewRng, dataSpan[0])
        except:
            self.vpx1 = viewCtr-0.50*viewRng
        self.vpx2 = self.vpx1 + viewRng
        #self.vpx2 = max(self.vpx1+viewRng, viewRng)

    def set_view_y(self, viewCtr, viewRng):
        #try:
        #    dataSpan = self.get_data_spny()
        #    self.vpy1 = max(viewCtr-0.50*viewRng, dataSpan[0])
        #except:
        #    self.vpy1 = viewCtr-0.50*viewRng
        self.vpy1 = viewCtr-0.50*viewRng
        self.vpy2 = self.vpy1 + viewRng

    def set_view_ctrx(self, viewCtr):
        viewRng = self.get_view_rngx()
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_ctry(self, viewCtr):
        viewRng = self.get_view_rngy()
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_view_rngx(self, viewRng):
        viewCtr = self.get_view_ctrx()
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_rngy(self, viewRng):
        viewCtr = self.get_view_ctry()
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_view_vptx(self, xmin, xmax):
        viewCtr = 0.5*(xmax + xmin)
        viewRng = xmax - xmin
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_vpty(self, ymin, ymax):
        viewCtr = 0.5*(ymax + ymin)
        viewRng = ymax - ymin
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_visibility(self, visi):
        for c, v in zip(self.curves, visi):
            c.set_visibility(v)

    def on_update(self):
        modified = False
        for c in self.curves:
            mdf = c.on_update()
            if (mdf):
                rx = c.get_data_rangex()
                ry = c.get_data_rangey()
                self.xmin = min(self.xmin, rx[0])
                self.xmax = max(self.xmax, rx[1])
                self.ymin = min(self.ymin, ry[0])
                self.ymax = max(self.ymax, ry[1])
                modified = True
        if (modified):
            viewCtr = self.get_view_ctrx()
            viewRng = self.get_view_rngx()
            self.set_view_x(viewCtr, viewRng)
            self.draw_plot()
        return modified

if __name__ == '__main__':
    app = wx.App()

    frame = wx.Frame(None, -1, "")
    plot = Plot(frame)
    plot.load_data_from_file('p1.prb')
    plot.draw_plot()

    frame.Show()
    app.MainLoop()
