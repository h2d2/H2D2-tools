# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import numpy

from . import CTUtil

# The class Curves encapsulate the common part to manage a collection
# of Curve(s). Children shall implement the data assignation.

class Curves:
    @staticmethod
    def reset_counter():
        Curve.reset_counter()

    def __init__(self):
        self.__reset()

    def __reset(self):
        self.file  = None
        self.curves = []
        self.xmin =  1.0e99
        self.xmax = -1.0e99
        self.ymin =  1.0e99
        self.ymax = -1.0e99

        self.vpx1 = -1.0e99
        self.vpx2 =  1.0e99
        self.vpy1 = -1.0e99
        self.vpy2 =  1.0e99

    def read_file_to_eof(self, fname):
        raise NotImplementedError

    def read_table_to_eod(self, data):
        raise NotImplementedError

    def load_data_from_file(self, fname):

        if (self.file): self.file.close()
        self.__reset()

        self.file = open(fname, 'r')
        self.read_file_to_eof()

        self.vpx1 = self.xmin
        self.vpx2 = self.xmax
        self.vpy1 = self.ymin
        self.vpy2 = self.ymax

    def load_data_from_table(self, data):
        if (self.file): self.file.close()
        self.__reset()

        self.read_table_to_eod(data)

        self.vpx1 = self.xmin
        self.vpx2 = self.xmax
        self.vpy1 = self.ymin
        self.vpy2 = self.ymax

    def fill_plot(self, ax):
        return [ c.fill_plot(ax) for c in self.curves ]

    def get_entry(self, p):
        for c in self.curves:
            itm = c.get_entry(p)
            if (itm): return itm
        return None

    def get_colors(self):
        clr = []
        for c in self.curves:
            clr.append( c.get_color() )
        return clr

    def get_data_rangex(self):
        return self.xmin, self.xmax

    def get_data_rangey(self):
        return self.ymin, self.ymax

    def get_stats(self):
        txt = []
        for c in self.curves:
            txt.append( '   ' + c.get_stats() )
        return txt

    def set_data_rangex(self, r):
        self.vpx1 = r[0]
        self.vpx2 = r[1]

    def on_update(self):
        modified = False
        if (not self.file): return modified
        where = self.file.tell()
        try:
            self.read_file_to_eof()
        except:
            self.file.seek(where)
        modified = where != self.file.tell()
        return modified

    def get_visibility(self):
        r = [ ]
        for c in self.curves:
            r.append( c.get_visibility() )
        return r

    def set_visibility(self, visi):
        for c, v in zip(self.curves, visi):
            c.set_visibility(v)

    def __iter__(self):
        return self.curves.__iter__()

    def __getitem__(self, i):
        return self.curves[i]

    def __len__(self):
        return len(self.curves)

class Curve:
    counter = 0

    @staticmethod
    def reset_counter():
        Curve.counter = 0

    def __init__(self, idx):
        self.__reset()
        self.clr = CTUtil.get_clr(Curve.counter)
        self.uid = Curve.counter
        self.idx = idx
        Curve.counter += 1

    def __reset(self):
        self.dsp  = True
        self.idx  = None
        self.data = []
        self.xmin =  1.0e99
        self.xmax = -1.0e99
        self.ymin =  1.0e99
        self.ymax = -1.0e99

        self.vpx1 = -1.0e99
        self.vpx2 =  1.0e99
        self.vpy1 = -1.0e99
        self.vpy2 =  1.0e99

    def add_entry(self, time, val):
        self.data.append( [time, val] )
        self.ymin = min(self.ymin, val)
        self.ymax = max(self.ymax, val)
        self.xmin = min(self.xmin, time)
        self.xmax = max(self.xmax, time)

    def fill_plot(self, ax):
        if (not self.dsp): 
            return None
        def is_ok(e) : return ((e[0] >= self.vpx1 and e[0] <= self.vpx2) and \
                               (e[1] >= self.vpy1 and e[1] <= self.vpy2))
        e_flt = list(filter(is_ok, self.data))
        x = numpy.array( [e[0] for e in e_flt] )
        y = numpy.array( [e[1] for e in e_flt] )
        #mkt, mkc = entry.get_marker()
        self.line = ax.plot(x, y, color = self.clr, picker=True, pickradius=2) #, linestyle=stl, color=clr, marker=mkt, markerfacecolor=mkc, pickradius=5)
        return self.line

    def get_entry(self, p):
        if (p in self.line):
            return self
        else:
            return None

    def get_color(self):
        return self.clr

    def get_data_rangex(self):
        return self.xmin, self.xmax

    def get_data_rangey(self):
        return self.ymin, self.ymax

    def get_stats(self):
        return 'vmin= %f; vmax = %f' % (self.ymin, self.ymax)

    def set_data_rangex(self, r):
        self.vpx1 = r[0]
        self.vpx2 = r[1]

    def get_visibility(self):
        return self.dsp

    def set_visibility(self, visibility = True):
        self.dsp = visibility

if __name__ == '__main__':
    plot = Curve()
    plot.load_data_from_file('p1.prb')
    plot.fill_plot(None)

