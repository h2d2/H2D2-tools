#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools   import Extension, setup
from Cython.Build import cythonize

import numpy

# ---  As we are in a module, set path to parent
import os
import sys
os.chdir('..')
for iarg, arg in enumerate(sys.argv):
    if not arg.startswith("--build-lib="): continue
    pth = arg.split("=")[1]
    if pth[0] in ['"', "'"]: pth = eval(pth, None, None)
    pth = os.path.join(pth, '..')
    pth = os.path.normpath(pth)
    sys.argv[iarg] = '--build-lib=%s' % pth

# ---  Module definition
pkg_args = {'language' : 'c++'}
pkg_name = 'CTCommon'
mdl_file = 'DTPath'
ext_modules=[
    Extension('%s.%s'     % (pkg_name, mdl_file),
              ['%s/%s.py' % (pkg_name, mdl_file)],
              include_dirs=[numpy.get_include()],
              **pkg_args,
             ),
]

# ---  Setup
setup(
  name = pkg_name,
  include_dirs=['CTCommon'],
  ext_modules = cythonize(ext_modules,
                          annotate      = True,
                          language_level=3),
)
