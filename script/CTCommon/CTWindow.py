# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import sys
if 'matplotlib' not in sys.modules:
    import matplotlib
    matplotlib.use('WXAgg') # do this before importing pyplot

import matplotlib.backends.backend_wxagg
from matplotlib.backends.backend_wxagg import NavigationToolbar2WxAgg
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg

import threading
import time
import wx
import logging

LOGGER = logging.getLogger("H2D2.Tools.Common")

lock = threading.Lock()

class CTWindow(wx.Window):
    """
    The class CTWindow is a kind of proxy for CTPlot. It is necessary to
    handle the event wx.EVT_IDLE which is not correctly reported by CTPlot.
    """

    def __init__(self,
                 proxied_class    = None,
                 do_update        = True,
                 cb_on_data_change= None,
                 cb_on_data_pick  = None,
                 cb_on_mouse_move = None,
                 parent = None,
                 *args,
                 **kwargs):
        super().__init__(parent)
        self.in_pause = False

        self.do_update         = do_update
        self.cb_on_data_change = cb_on_data_change
        self.cb_on_data_pick   = cb_on_data_pick
        self.cb_on_mouse_move  = cb_on_mouse_move

        self.fig = proxied_class(self, *args, **kwargs)

        self.canvas  = self.fig.get_canvas()
        self.toolbar = NavigationToolbar2WxAgg(self.canvas)
        #self.toolbar = MyNavigationToolbar(self.canvas)
        #self.toolbar = MyNavigationToolbar(self.canvas)
        tw, th = self.toolbar.GetSize()
        fw, fh = self.canvas.GetSize()
        self.toolbar.SetSize(wx.Size(fw, th))
        self.toolbar.update()

        self.Bind(wx.EVT_SIZE,   self.on_resize)
        self.mplCIDs = []
        if self.cb_on_mouse_move:
            self.mplCIDs.append( self.canvas.mpl_connect('motion_notify_event', self.on_mouse_move) )
        if self.cb_on_data_pick:
            self.mplCIDs.append( self.canvas.mpl_connect('pick_event', self.on_pick) )
        self.lastUpdate = None

    def __del__(self):
        try:
            for cid in self.mplCIDs:
                self.canvas.mpl_disconnect(cid)
        except:
            pass
        finally:
            self.mplCIDs = []

    def __getattr__(self, name):
        attr = getattr(self.fig, name)
        if hasattr(attr, '__call__'):
            def newfunc(*args, **kwargs):
                wx.BeginBusyCursor()
                result = attr(*args, **kwargs)
                wx.EndBusyCursor()
                return result
            return newfunc
        else:
            return attr

    def get_figure(self):
        return self.fig

    def get_toolbar(self):
        return self.toolbar

    def load_data_from_file(self, data):
        wx.BeginBusyCursor()
        self.fig.load_data_from_file(data)
        self.fig.draw_plot()
        wx.EndBusyCursor()
        if (self.cb_on_data_change):
            self.cb_on_data_change()
        if (self.do_update):
            self.lastUpdate = time.perf_counter()
            self.Bind(wx.EVT_IDLE, self.on_update)

    def on_mouse_move(self, evt):
        if (evt.inaxes is None): return
        if (evt.xdata is None): return
        try:
            self.cb_on_mouse_move(evt.xdata, evt.ydata)
        except:
            s = evt.inaxes.format_coord(evt.xdata, evt.ydata)
            self.cb_on_mouse_move(s)

    def on_update(self, *args):
        modif = False
        if (self.in_pause): return modif
        if (time.perf_counter() - self.lastUpdate) < 5.0: return modif
        if lock.acquire(False):
            try:
                wx.BeginBusyCursor()
                modif = self.fig.on_update()
                wx.EndBusyCursor()
                if (modif and self.cb_on_data_change):
                    self.cb_on_data_change()
            finally:
                lock.release()
            self.lastUpdate = time.perf_counter()
        return modif

    def on_pick(self, event):
        line = event.artist
        if not line: return True
        N = len(event.ind)
        if not N: return True
        if (event.ind[0]):
            xdata = line.get_xdata()
            ind = event.ind[0]
            itm = self.fig.get_entry(line, xdata[ind])
        else:
            itm = self.fig.get_entry(line, None)
        if (itm): self.cb_on_data_pick(itm)
        return True

    def on_resize(self, *args, **kwargs):
        self.canvas.SetSize(self.GetSize())

    def pause(self, stat):
        self.in_pause = stat

    def is_pausing(self):
        return self.in_pause

class CTWindowMPLNavigationToolbar(NavigationToolbar2WxAgg):
    """
    Specialize NavigationToolbar2WxAgg to implement
    state save and restore.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.skip_move = False
        self.state = ''

    def saveState(self):
        #print('CTWindowMPLNavigationToolbar.saveState')
        self.state = self.mode.name

    def restoreState(self):
        """
        restoreState

        Returns:
            bool: True if mode changed
        """
        assert self.state
        oldMode = self.mode.name
        self.resetState()
        #print('CTWindowMPLNavigationToolbar.restoreState')
        if self.state == 'PAN':  self.pan()
        if self.state == 'ZOOM': self.zoom()
        self.state = ''
        return oldMode != self.mode.name

    def resetState(self):
        """
        resetState _summary_

        Returns:
            bool: True if mode changed
        """
        #print('CTWindowMPLNavigationToolbar.resetState')
        oldMode = self.mode.name
        if self.mode.name == 'PAN':  self.pan()
        if self.mode.name == 'ZOOM': self.zoom()
        assert self.mode.name not in ['PAN', 'ZOOM']
        return oldMode != self.mode.name


class CTWindowMPLFigure(wx.Window):
    """
    Encapsulate a matplotlib Figure and its toolbar
    """
    BASE_SCALE_FACTOR = 1.5

    def __init__(self,
                 fig = None,
                 cb_on_mouse_move    = None,
                 cb_on_mouse_click   = None,
                 cb_on_mouse_release = None,
                 cb_on_mouse_scroll  = None,
                 cb_on_key_press     = None,
                 cb_on_key_release   = None,
                 cb_on_pick_event    = None,
                 parent = None):
        super().__init__(parent)

        self.fig = fig
        self.cb_on_mouse_move    = cb_on_mouse_move
        self.cb_on_mouse_click   = cb_on_mouse_click
        self.cb_on_mouse_release = cb_on_mouse_release
        self.cb_on_mouse_scroll  = cb_on_mouse_scroll
        self.cb_on_key_press     = cb_on_key_press
        self.cb_on_key_release   = cb_on_key_release
        self.cb_on_pick_event    = cb_on_pick_event

        # ---  Switch to WxAgg canvas, preserving already registred callbacks
        cbs = fig.canvas.callbacks.callbacks
        self.canvas  = FigureCanvasWxAgg(self, -1, self.fig)
        self.canvas.callbacks.callbacks.update(cbs)

        # ---  Toolbar
        self.toolbar = CTWindowMPLNavigationToolbar(self.canvas)
        self.toolbar.Hide()

        self.__set_size()

        self.Bind(wx.EVT_SIZE, self.on_resize)
        #self.Bind(wx.EVT_PAINT, self.on_redraw)
        self.mplCIDs = []
        if self.cb_on_mouse_move:
            self.mplCIDs.append( self.canvas.mpl_connect('motion_notify_event', self.on_mouse_move) )
        if self.cb_on_mouse_click:
            self.mplCIDs.append( self.canvas.mpl_connect('button_press_event', self.on_mouse_click) )
        if True: # self.cb_on_mouse_scroll:
            self.mplCIDs.append( fig.canvas.mpl_connect('scroll_event', self.on_mouse_scroll) )
        if self.cb_on_key_press:
            self.mplCIDs.append( self.canvas.mpl_connect('key_press_event', self.on_key_press) )
        if self.cb_on_key_release:
            self.mplCIDs.append( self.canvas.mpl_connect('key_release_event', self.on_key_release) )
        if self.cb_on_pick_event:
            self.mplCIDs.append( self.canvas.mpl_connect('button_press_event', self.on_pick_event) )
        self.mplCIDs.append( self.canvas.mpl_connect('draw_event', self.on_draw) )

    def __del__(self):
        try:
            for cid in self.mplCIDs:
                self.canvas.mpl_disconnect(cid)
        except:
            pass
        finally:
            self.mplCIDs = []

    def __set_size(self):
        self.canvas.SetSize(self.GetSize())

    def __getattr__(self, name):
        attr = getattr(self.fig, name)
        if hasattr(attr, '__call__'):
            def newfunc(*args, **kwargs):
                wx.BeginBusyCursor()
                result = attr(*args, **kwargs)
                wx.EndBusyCursor()
                return result
            return newfunc
        else:
            return attr

    def get_figure(self):
        return self.fig

    def get_toolbar(self):
        return self.toolbar

    def doCenter(self, xdata, ydata):
        ax = self.canvas.figure.axes[0]

        # ---  Current x and y limits
        cur_xlim = ax.get_xlim()
        cur_ylim = ax.get_ylim()
        # ---  Half-size
        dx = (cur_xlim[1] - cur_xlim[0]) * 0.5
        dy = (cur_ylim[1] - cur_ylim[0]) * 0.5
        # ---  Push the current view limits and position onto the stack
        self.toolbar.push_current()
        # ---  New limits
        ax.set_xlim( (xdata - dx, xdata + dx) )
        ax.set_ylim( (ydata - dy, ydata + dy) )
        # ---  Redraw
        ax.figure.canvas.draw()

    def getBBox(self):
        """
        getBBox return the current viewlimits bbox in
        data coordinates.
        """
        ax = self.canvas.figure.axes[0]
        x0, x1 = ax.get_xlim()
        y0, y1 = ax.get_ylim()
        return x0, y0, x1, y1

    def setBBox(self, bbox):
        """
        setBBox set the current viewlimits bbox in
        data coordinates.
        """
        self.toolbar.push_current()
        ax = self.canvas.figure.axes[0]
        ax.set_xlim((bbox[0], bbox[2]))
        ax.set_ylim((bbox[1], bbox[3]))
        self.canvas.draw_idle()

    def redraw(self):
        self.canvas.draw()

    def show(self, b):
        pass

    def on_mouse_click(self, evt):
        if evt.inaxes is None: return
        if evt.xdata  is None: return
        self.cb_on_mouse_click(evt.xdata, evt.ydata)

    def on_mouse_move(self, evt):
        if evt.inaxes is None: return
        if evt.xdata  is None: return
        self.cb_on_mouse_move(evt.xdata, evt.ydata)

    def on_mouse_scroll(self, evt):
        """
        Adapted from
        https://stackoverflow.com/questions/11551049/matplotlib-plot-zooming-with-scroll-wheel
        """
        if evt.inaxes is None: return
        if evt.xdata  is None: return

        ax = self.canvas.figure.axes[0]

        # ---  Current x and y limits
        cur_xlim = ax.get_xlim()
        cur_ylim = ax.get_ylim()
        # ---  Event location
        xdata, ydata = evt.xdata, evt.ydata
        # ---  Distance from the cursor to the edge
        dx_lft = xdata - cur_xlim[0]
        dx_rgh = cur_xlim[1] - xdata
        dy_top = ydata - cur_ylim[0]
        dy_bot = cur_ylim[1] - ydata
        # ---  Scale factor
        if evt.button == 'up':        # Zoom in
            scale_factor = 1.0 / CTWindowMPLFigure.BASE_SCALE_FACTOR
        elif evt.button == 'down':    # Zoom out
            scale_factor = CTWindowMPLFigure.BASE_SCALE_FACTOR
        else:                         # Error
            scale_factor = 1
        # ---  Push the current view limits and position onto the stack
        self.toolbar.push_current()
        # ---  New limits
        ax.set_xlim([xdata - dx_lft*scale_factor, xdata + dx_rgh*scale_factor])
        ax.set_ylim([ydata - dy_top*scale_factor, ydata + dy_bot*scale_factor])
        # ---  Redraw
        ax.figure.canvas.draw()

    def on_key_press(self, evt):
        LOGGER.debug('on_key_press: %s', evt.key)
        self.cb_on_key_press()

    def on_key_release(self, evt):
        LOGGER.debug('on_key_release: %s', evt.key)
        self.cb_on_key_release(evt.key)

    def on_pick_event(self, evt):
        if (evt.inaxes is None): return
        if (evt.xdata  is None): return
        #thisline = event.artist
        #xdata, ydata = thisline.get_data()
        #ind = event.ind
        #print('on pick line:', zip(xdata[ind], ydata[ind]))
        #self.cb_on_pick_event(evt.xdata, evt.ydata)

    def on_btn_reset(self, evt):
        self.toolbar.home()

    def on_btn_forward(self, evt):
        self.toolbar.forward()

    def on_btn_backward(self, evt):
        self.toolbar.back()

    def on_btn_pan(self, evt):
        self.toolbar.pan()

    def on_btn_zoom_to_rectangle(self, evt):
        self.toolbar.zoom()

    def on_resize(self, *args, **kwargs):
        self.__set_size()

    def on_draw(self, evt):
        pass