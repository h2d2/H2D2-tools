#! /bin/bash

function delete_one_ext
{
   for p in `ls *.setup.py 2>/dev/null`
   do
      rm -f ${p/setup.py/$1}
   done
}


pushd xtrn >/dev/null
rm -rf build
delete_one_ext so
delete_one_ext h
delete_one_ext hpp
delete_one_ext cpp
delete_one_ext c
delete_one_ext html
popd >/dev/null

rm -rf build
delete_one_ext so
delete_one_ext h
delete_one_ext hpp
delete_one_ext cpp
delete_one_ext c
delete_one_ext html
