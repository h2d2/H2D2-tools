#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Mesh metric kind
"""

import enum

ELEMENT_QUALITY_METRICS = enum.Enum('ELEMENT_QUALITY_METRICS', 
                                    ('area', 
                                     'aspectRatio', 
                                     'aspectFrobenius', 
                                     'condition', 
                                     'edgeRatio', 
                                     'maxAngle', 
                                     'minAngle', 
                                     'scaledJacobian', 
                                     'radiusRatio', 
                                     'angularSkewness', 
                                     'skewness', 
                                    ))
