#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2012-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
IO functionalities for H2D2 data files.
"""

import array
import datetime
import logging
import os
import sys
import struct
import numpy as np

if __name__ == "__main__":
    import CTException
    import DTDataBloc as DTBloc
    from xtrn import jdutil
else:
    from . import CTException
    from . import DTDataBloc as DTBloc
    from .xtrn import jdutil

LOGGER = logging.getLogger("H2D2.Tools.Data.Data.IO")

try:
    SEEK_SET = os.SEEK_SET
    SEEK_CUR = os.SEEK_CUR
except AttributeError:
    SEEK_SET = 0
    SEEK_CUR = 1

DBL_SIZE = array.array('d').itemsize
RBUF_DEF = -1
RBUF_BIG = 1 << 20

def isBinary(fname):
    """
    Check for binary file.
    Return True if the file is in a binary format.
    """
    fi = open(fname, 'rb')
    line = fi.read(1024)
    fi.close()
    is_bin = False
    for c in line:
        if c not in b' \"\'+-,.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz\t\r\n':
            is_bin = True
            break
    return is_bin

def isLineEndingPlatformCompatible(fname):
    """
    Return True is the line ending in the file is compatible with the platform,
    i.e. CR-LF for Windows and LF for *nix
    """
    fi = open(fname, 'rb')
    line = fi.read(1024)
    fi.close()
    if b'\r\n' in line or b'\n\r' in line:
        return sys.platform.startswith('win')
    elif b'\n' in line:
        return sys.platform.startswith('linux')
    #elif (b'\r' in line): return
    #    return sys.platform.startswith('darwin')
    else:
        raise CTException.CTException('Unsupported platform: %s' % sys.platform)


class DTDataIO:
    """IO class, reader-writer to a file"""
    def __init__(self, fnam, cols=None):
        self.fnam = fnam
        self.fpos = -1
        if isinstance(cols, int):
            self.cols = (cols,)
        else:
            self.cols = cols
        self.nrow = 0
        self.ncol = 0
        self.last_fi  = None
        self.last_dim = None

    def __checkHeader(self, fi, nrow, ncol, time):
        if fi == self.last_fi:
            if self.last_dim:
                msg = []
                if nrow != self.last_dim[0]:
                    msg.append('Inconsistent headers in nrow: %d != %d' % (nrow, self.last_dim[0]))
                if ncol != self.last_dim[1]:
                    msg.append('Inconsistent headers in ncol: %d != %d' % (ncol, self.last_dim[1]))
                if time <= self.last_dim[2]:
                    msg.append('Invalid time series: %s <= %s' % (str(time), str(self.last_dim[2])))
                if msg:
                    raise CTException.CTException('\n'.join(msg))
            else:
                self.last_dim = (nrow, ncol, time)
        else:
            self.last_fi  = fi
            self.last_dim = (nrow, ncol, time)

    def getFile(self):
        return self.fnam

    def openForRead(self, fnam, bsize=RBUF_DEF):
        raise NotImplementedError

    def openForWrite(self, fnam, doAppend=True):
        raise NotImplementedError

    def readMeta(self, fi):
        raise NotImplementedError

    def readHeader(self, fi):
        raise NotImplementedError

    def readRecord(self, fi, ncol):
        raise NotImplementedError

    def skipRecords(self, fi, ncol, n=1):
        raise NotImplementedError

    def readDim(self):
        fi = self.openForRead(self.fnam, bsize=RBUF_DEF)
        self.readMeta(fi)
        dims = self.readHeader(fi)
        fi.close()
        self.nrow, self.ncol = dims[0:2]
        if not self.cols: self.cols = list(range(self.ncol))
        return dims

    def readStruct(self):
        """
        Read the structure of the file. The structure is
        returned as a list of DTDataBloc.
        """
        LOGGER.debug('DTDataIO: read structure: %s', self.fnam)
        blocs = []

        self.readDim()
        self.last_fi = None
        fi = self.openForRead(self.fnam, bsize=RBUF_BIG)
        try:
            self.readMeta(fi)
            while True:
                vmin = [( 1.0e99, -1) for j in self.cols]
                vmax = [(-1.0e99, -1) for j in self.cols]

                pos = self.fpos
                self.nrow, self.ncol, time = self.readHeader(fi)
                self.__checkHeader(fi, self.nrow, self.ncol, time)
                for irow in range(self.nrow):
                    vals = self.readRecord(fi, self.ncol)
                    for ic, iv in enumerate(self.cols):
                        if vals[iv] < vmin[ic][0]: vmin[ic] = (vals[iv], irow)
                        if vals[iv] > vmax[ic][0]: vmax[ic] = (vals[iv], irow)
                bloc = DTBloc.DTDataBloc(self,
                                         self.fnam,
                                         pos,
                                         None,  # data
                                         self.nrow,
                                         len(self.cols),
                                         time,
                                         vmin,
                                         vmax)
                blocs.append(bloc)
        except (EOFError, StopIteration):
            pass
        except Exception as e:
            # https://stackoverflow.com/questions/6062576/adding-information-to-an-exception
            msg = '\n'.join((
                '%s' % str(e),
                'Bloc number: %d' % (len(blocs)+1),
                'File offset: %d' % self.fpos,
                ))
            raise type(e)(msg) from e
        fi.close()
        self.last_fi = None
        return blocs

    def readStructNoStats(self):
        """
        Read the structure of the file. The structure is
        returned as a list of DTDataBloc.
        """
        LOGGER.debug('DTDataIO: read structure without stats: %s', self.fnam)
        blocs = []

        self.readDim()
        self.last_fi = None
        fi = self.openForRead(self.fnam, bsize=RBUF_DEF)
        try:
            vmin = [(-1.0e99, -1) for j in self.cols]
            vmax = [( 1.0e99, -1) for j in self.cols]

            self.readMeta(fi)
            while True:
                pos = self.fpos
                self.nrow, self.ncol, time = self.readHeader(fi)
                self.__checkHeader(fi, self.nrow, self.ncol, time)
                self.skipRecords(fi, self.ncol, self.nrow)
                bloc = DTBloc.DTDataBloc(self,
                                         self.fnam,
                                         pos,
                                         None,  # data
                                         self.nrow,
                                         len(self.cols),
                                         time,
                                         vmin,
                                         vmax)
                blocs.append(bloc)
        except (EOFError, StopIteration):
            pass
        except Exception as e:
            # https://stackoverflow.com/questions/6062576/adding-information-to-an-exception
            msg = '\n'.join((
                '%s' % str(e),
                'Bloc number: %d' % (len(blocs)+1),
                'File offset: %d' % self.fpos,
                ))
            raise type(e)(msg) from e
        fi.close()
        self.last_fi = None
        #self.checkStruct(blocs)
        return blocs

    def checkStruct(self, blocs):
        """
        Check the structure by accessing each bloc and comparing the header information.
        """
        LOGGER.debug('DTDataIO: check structure: %s', self.fnam)

        fi = self.openForRead(self.fnam, bsize=RBUF_DEF)
        self.readMeta(fi)
        for b in blocs:
            fi.seek(b.pos)
            nrow, ncol, time = self.readHeader(fi)
            assert nrow == b.nrow
            assert ncol == b.ncol
            assert time == b.time
        fi.close()

    def readDataAtStep(self, bloc, buf):
        LOGGER.debug('DTDataIO: load data at time %f', bloc.time)
        fi = self.openForRead(self.fnam, bsize=RBUF_BIG)
        fi.seek(bloc.pos)
        nrow, ncol, time = self.readHeader(fi)
        assert nrow == self.nrow
        assert ncol == self.ncol
        assert time == bloc.time
        nval = len(self.cols)
        if nval == 1:
            for inod in range(self.nrow):
                vals = self.readRecord(fi, self.ncol)
                buf[inod] = vals[self.cols[0]]
        elif nval == 2:
            for inod in range(self.nrow):
                vals = self.readRecord(fi, self.ncol)
                buf[inod, 0] = vals[self.cols[0]]
                buf[inod, 1] = vals[self.cols[1]]
        elif nval == self.ncol:
            for inod in range(self.nrow):
                vals = self.readRecord(fi, self.ncol)
                buf[inod,:] = vals[:]
        else:
            for inod in range(self.nrow):
                vals = self.readRecord(fi, self.ncol)
                for ic in self.cols: buf[inod, ic] = vals[ic]
        return buf

    def readRowsAtStep(self, rows, bloc, buf):
        LOGGER.debug('DTDataIO: load rows at time %f', bloc.time)
        skips = rows[0:1] + [(a1-a0-1) for a0, a1 in zip(rows[:-1], rows[1:])]
        assert len(skips) == len(rows)
        assert len(rows)  <= self.nrow
        fi = self.openForRead(self.fnam, bsize=RBUF_DEF)
        fi.seek(bloc.pos)
        nrow, ncol, time = self.readHeader(fi)
        assert nrow == self.nrow
        assert ncol == self.ncol
        assert time == bloc.time
        nval = len(self.cols)
        if nval == 1:
            for n, ir in zip(skips, rows):
                self.skipRecords(fi, self.ncol, n)
                vals = self.readRecord(fi, self.ncol)
                buf[ir] = vals[self.cols[0]]
        elif nval == 2:
            for n, ir in zip(skips, rows):
                self.skipRecords(fi, self.ncol, n)
                vals = self.readRecord(fi, self.ncol)
                buf[ir, 0] = vals[self.cols[0]]
                buf[ir, 1] = vals[self.cols[1]]
        else:
            for n, ir in zip(skips, rows):
                self.skipRecords(fi, self.ncol, n)
                vals = self.readRecord(fi, self.ncol)
                for ic in self.cols: buf[ir, ic] = vals[ic]
        return buf

    def writeMeta(self, fo, nrow, ncol, time, vmin, vmax):
        raise NotImplementedError

    def writeHeader(self, fo, nrow, ncol, time, vmin, vmax):
        raise NotImplementedError

    def writeRecord(self, fo, vals):
        raise NotImplementedError

    def writeDataBloc(self, bloc, buf):
        if os.path.isfile(self.fnam) and self.fpos == -1:
            raise CTException.CTException('Trying to append data to an already existing file')
        addMeta = not os.path.isfile(self.fnam)
        fo = self.openForWrite(self.fnam)
        if addMeta:
            self.writeMeta(fo, bloc.nrow, bloc.ncol, bloc.time, bloc.vmin, bloc.vmax)
        else:
            fo.seek(self.fpos)
        self.writeHeader(fo, bloc.nrow, bloc.ncol, bloc.time, bloc.vmin, bloc.vmax)
        if bloc.ncol > 1:
            for inod in range(bloc.nrow):
                self.writeRecord(fo, buf[inod, :])
        else:
            for inod in range(bloc.nrow):
                self.writeRecord(fo, (buf[inod],))
        self.fpos = fo.tell()
        self.writeEnd(fo)
        fo.close()

    def writeData(self, time, data):
        fi = self.openForWrite(self.fnam)
        try:
            nrow, ncol = np.shape(data)
            vmin = list( np.min(data, axis=1) )
            vmax = list( np.max(data, axis=1) )
        except:
            nrow, ncol = (np.shape(data)[0], 1)
            vmin = list( np.array( (np.min(data),) ) )
            vmax = list( np.array( (np.max(data),) ) )
        self.writeHeader(fi, nrow, ncol, time, vmin, vmax)
        if ncol > 1:
            for inod in range(nrow):
                self.writeRecord(fi, data[inod])
        else:
            for inod in range(nrow):
                self.writeRecord(fi, (data[inod],))
        fi.close()

    def writeEnd(self, fo):
        raise NotImplementedError

class DTDataIOAsciiBase(DTDataIO):
    def __init__(self, fnam, cols=None):
        DTDataIO.__init__(self, fnam, cols)

    def openForRead(self, fnam, bsize=RBUF_DEF):
        self.fpos = 0
        return open(fnam, 'rb', bsize)

    def openForWrite(self, fnam, doAppend=True):
        if doAppend:
            return open(fnam, 'at', 1 << 20)
        else:
            return open(fnam, 'wt', 1 << 20)

    def skipRecords(self, fi, ncol, n=1):
        i = n
        while i > 0:
            line = next(fi)
            self.fpos += len(line)
            i -= 1

    def readMeta(self, fi):
        # Defaut NoOp
        pass

    def writeMeta(self, fo, nrow, ncol, time, vmin, vmax):
        # Defaut NoOp
        pass

    def writeEnd(self, fo):
        # Defaut NoOp
        pass

class DTDataIOAscii(DTDataIOAsciiBase):
    def __init__(self, fnam, cols=None):
        DTDataIOAsciiBase.__init__(self, fnam, cols)

    def isValid(self):
        if isBinary(self.fnam): return False
        fi = self.openForRead(self.fnam, bsize=RBUF_DEF)
        try:
            _nrow, ncol, _time = self.readHeader(fi)
            self.readRecord(fi, ncol)
        except (EOFError, ValueError):
            return False
        except IndexError:
            raise CTException.CTException('Inconsistent number of columns and line size')
        fi.close()
        return True

    def readHeader(self, fi):
        line = next(fi)
        self.fpos += len(line)
        nrow, ncol, time = line.split()
        return int(nrow), int(ncol), float(time)

    def readRecord(self, fi, ncol):
        line = next(fi)
        self.fpos += len(line)
        return np.fromstring(line, dtype=np.float64, count=ncol, sep=' ')

    def writeHeader(self, fo, nrow, ncol, time, vmin, vmax):
        fo.write(' %i %i %17.9e\n' % (nrow, ncol, time))

    def writeRecord(self, fo, vals):
        for v in vals: fo.write('%17.9e' % v)
        fo.write('\n')

class DTDataIOAsciiReducedPrecision(DTDataIOAsciiBase):
    def __init__(self, res, fnam, cols=None):
        DTDataIOAsciiBase.__init__(self, fnam, cols)
        self.res = res
        self.coef = []

    def isValid(self):
        if isBinary(self.fnam): return False
        fi = self.openForRead(self.fnam, bsize=RBUF_DEF)
        try:
            _nrow, ncol, _time = self.readHeader(fi)
            if len(self.coef) != ncol: return False
            for i in range(ncol):
                if len(self.coef[i]) != 3: return False
                vmin, vmax, vdel = self.coef[i]
                if vdel < 1.0e-16: return False
                res = (vmax-vmin)/vdel
                if int(res+0.5) != self.res: return False
            self.readRecord(fi, ncol)
        except (EOFError, ValueError):
            return False
        except IndexError:
            raise CTException.CTException('Inconsistent number of values and line size')
        fi.close()
        return True

    def readHeader(self, fi):
        self.coef = []
        line = next(fi)
        self.fpos += len(line)
        nrow, ncol, time = line.split()
        for _i in range(int(ncol)):
            line = next(fi)
            toks = line.split()
            self.coef.append([float(t) for t in toks])
        return int(nrow), int(ncol), float(time)

    def readRecord(self, fi, ncol):
        def expandVal(self, val, ic):
            return val*self.coef[ic][2] + self.coef[ic][0]
        line = next(fi)
        self.fpos += len(line)
        toks = line.split()
        return [expandVal(self, float(toks[j]), j) for j in range(ncol)]

    def writeHeader(self, fo, nrow, ncol, time, vmin, vmax):
        self.coef = []
        fo.write(' %i %i %g\n' % (nrow, ncol, time))
        for v1, v2 in zip(vmin, vmax):
            vd = (vmax-vmin) / self.res
            self.coef.append([v1, v2, vd])
            fo.write(' %g %g %g' % (v1, v2, vd))

    def writeRecord(self, fo, vals):
        def reduceVal(self, val, ic):
            r = (val-self.coef[ic][0]) / self.coef[ic][2]
            return int(r+0.5)
        for ic, v in enumerate(vals): fo.write(' %g' % reduceVal(self, v, ic))
        fo.write('\n')

class DTDataIOAsciiI08(DTDataIOAsciiReducedPrecision):
    def __init__(self, fnam, cols=None):
        DTDataIOAsciiReducedPrecision.__init__(self, 2 << (8-1), fnam, cols)

class DTDataIOAsciiI16(DTDataIOAsciiReducedPrecision):
    def __init__(self, fnam, cols=None):
        DTDataIOAsciiReducedPrecision.__init__(self, 2 << (16-1), fnam, cols)

class DTDataIOAsciiSMS(DTDataIOAsciiBase):
    def __init__(self, fnam, cols=None):
        super().__init__(fnam, cols)
        self.epoch = datetime.datetime.fromtimestamp(0, tz=datetime.timezone.utc)
        self.tmult = 1.0        # time multiplier to get seconds

    def __nextLine(self, fi):
        """
        __nextLine [summary]

        Advance one line in file fi, adjusting the file position

        Args:
            fi (file): input file
        """
        line = next(fi)
        self.fpos += len(line)
        return line.strip()

    def isValid(self):
        if isBinary(self.fnam): return False
        fi = self.openForRead(self.fnam, bsize=RBUF_DEF)
        try:
            self.readMeta(fi)
            _nrow, ncol, _time = self.readHeader(fi)
            self.readRecord(fi, ncol)
        except (EOFError, ValueError):
            return False
        except IndexError:
            raise CTException.CTException('Inconsistent number of columns and line size')
        finally:
            fi.close()
        return True

    def readMeta(self, fi):
        line = self.__nextLine(fi).lower()
        if line != b'dataset':
            raise CTException.CTException('Not a valid SMS data file')
        line = self.__nextLine(fi).lower()
        if line.startswith(b'objtype'):
            objtype = line.split(maxsplit=1)[1]
            if objtype != b'"mesh2d"':
                raise CTException.CTException('Invalid OBJTYPE "%s". Only type "mesh2D" is supported' % objtype)
        else:
            raise CTException.CTException('Not a valid SMS data file, OBJTYPE card expected')

        line = self.__nextLine(fi).lower()
        if line == b'begscl':
            nbCol = 1
            line = self.__nextLine(fi).lower()
        elif line == b'begvec':
            nbCol = 2
            line = self.__nextLine(fi).lower()
            if line.startswith(b'vectype'):
                vectype = int(line.split(maxsplit=1)[1])
                if vectype != 0:
                    raise CTException.CTException('Invalid VECTYPE "%s". Only type "0" is supported' % vectype)
                line = self.__nextLine(fi).lower()
            elif line.startswith(b'nd'):
                pass
            else:
                raise CTException.CTException('Not a valid SMS data file, VECTYPE card expected')
        else:
            raise CTException.CTException('Invalid data type "%s"' % line)

        if line.startswith(b'nd'):
            nbNod = int( line.split()[1] )
        else:
            raise CTException.CTException('Not a valid SMS data file, ND card expected')
        line = self.__nextLine(fi).lower()
        if line.startswith(b'nc'):
            nbCel = int( line.split()[1] )
            # if nbCel != 0:
            #     raise CTException.CTException('Invalide NC count of %d. Only nodal values are supported' % nbCel)
        else:
            raise CTException.CTException('Not a valid SMS data file, NC card expected')

        line = self.__nextLine(fi)      # NAME
        line = self.__nextLine(fi).lower()
        if line.startswith(b'rt_julian'):
            ep = float( line.split(maxsplit=1)[1] )
            self.epoch = jdutil.jd_to_datetime(ep, tzinfo=datetime.timezone.utc)
            line = self.__nextLine(fi).lower()
        if line.startswith(b'timeunits'):
            units = line.split(maxsplit=1)[1]
            if   units in (b'hours', b'0'):
                self.tmult = 1.0 / 3600.0
            elif units in (b'minutes', b'1'):
                self.tmult = 1.0 / 60.0
            elif units in (b'seconds', b'2'):
                self.tmult = 1.0
            else:
                raise CTException.CTException('Invalid TIMEUNIT "%s"' % units)

        self.ncol = nbCol
        self.nrow = nbNod

    def readHeader(self, fi):
        line = self.__nextLine(fi).lower()
        if line == b'endds':
            raise StopIteration()
        elif line.startswith(b'ts '):
            tks = line.split()
            hasStts, delta = int(tks[1]) != 0, float(tks[2])
            time = self.epoch + datetime.timedelta(seconds=delta*self.tmult)
            if hasStts:
                # raise CTWarning('Activity status are ignored')
                self.skipRecords(fi, self.ncol, self.nrow)
        else:
            raise CTException.CTException('Not a valid SMS data file, TS card expected')

        return (self.nrow, self.ncol, time.timestamp())

    def readRecord(self, fi, ncol):
        line = next(fi)
        self.fpos += len(line)
        return np.fromstring(line, dtype=np.float64, count=ncol, sep=' ')

    def writeMeta(self, fo, nrow, ncol, time, vmin, vmax):
        fo.write('DATASET\n')
        fo.write('OBJTYPE %s\n' % '"mesh2d"')
        if ncol == 1:
            fo.write('BEGSCL\n')
        elif ncol == 2:
            fo.write('BEGVEC\n')
            fo.write('VECTYPE 0\n')     # Values at nodes
        else:
            raise CTException.CTException('Unsupported number of columns: %d not in [1, 2]' % ncol)
        fo.write('ND %d\n' % nrow)
        fo.write('NC %d\n' % 0)
        fo.write('RT_JULIAN %f\n' % jdutil.datetime_to_jd(self.epoch))
        fo.write('TIMEUNITS %s\n' % 'seconds')

    def writeHeader(self, fo, nrow, ncol, time, vmin, vmax):
        fo.write('TS %d %f\n' % (0, time))

    def writeRecord(self, fo, vals):
        for v in vals: fo.write('%17.9e' % v)
        fo.write('\n')

    def writeEnd(self, fo):
        fo.write('ENDDS\n')


class DTDataIOBinaryBase(DTDataIO):
    def __init__(self, fnam, cols=None):
        DTDataIO.__init__(self, fnam, cols)

    def openForRead(self, fnam, bsize=RBUF_DEF):
        self.fpos = 0
        return open(fnam, 'rb', bsize)

    def openForWrite(self, fnam, doAppend=True):
        if doAppend:
            return open(fnam, 'ab', 1 << 20)
        else:
            return open(fnam, 'wb', 1 << 20)

    def readMeta(self, fi):
        # Defaut NoOp
        pass

    def writeMeta(self, fo, nrow, ncol, time, vmin, vmax):
        # Defaut NoOp
        pass

    def writeEnd(self, fo):
        # Defaut NoOp
        pass

class DTDataIOBinary(DTDataIOBinaryBase):
    """
    Note: file read are buffered. fpos reflects the actual file position
    and not the position inside the buffer.
    """
    def __init__(self, fnam, cols=None):
        DTDataIOBinaryBase.__init__(self, fnam, cols)
        self.buffer= None
        self.rsize = 0      # record size (in items)
        self.dsize = 0      # data size   (in items)
        self.bsize = 0      # buffer size (in items)
        self.bbtm  = 0      # buffer bottom: always 0
        self.btop  = 0      # buffer top
        self.bptr  = 0      # buffer pointer: force reading on next access
        self.dptr  = 0      # data pointer

    def isValid(self):
        if not isBinary(self.fnam): return False
        fi = self.openForRead(self.fnam, bsize=RBUF_DEF)
        try:
            _nrow, ncol, _time = self.readHeader(fi)
            self.readRecord(fi, ncol)
        except (EOFError, ValueError):
            return False
        except IndexError:
            raise CTException.CTException('Inconsistent number of values and line size')
        fi.close()
        return True

    def readHeader(self, fi):
        a = array.array('d')
        a.fromfile(fi, 3)
        self.fpos = fi.tell()
        try:
            drow, dcol, time = a
            nrow = int(drow)
            ncol = int(dcol)
            self.rsize = ncol                       # record size (in items)
            self.dsize = self.rsize * nrow          # data size   (in items)
            self.bsize = int(8192 / ncol) * ncol    # buffer size (in items)
            self.bbtm  = 0                          # buffer bottom: always 0
            self.btop  = 0                          # buffer top
            self.bptr  = 0                          # buffer pointer: force reading on next access
            self.dptr  = 0                          # data pointer
        except ValueError:
            raise EOFError
        return nrow, ncol, time

    def readRecord(self, fi, ncol):
        """
        Read the full current record (i.e. all columns)
        ncol is the
        The position is moved forward by one record.
        """
        if self.bptr >= self.btop:
            isize = np.dtype(np.float64).itemsize
            bsize = min(self.bsize, self.dsize-self.dptr)
            self.buffer = np.frombuffer(fi.read(bsize*isize), dtype=np.float64, count=bsize)
            self.fpos = fi.tell()
            self.bbtm = 0
            self.btop = bsize
            self.bptr = self.bbtm
        p1 = self.bptr
        self.bptr += self.rsize
        self.dptr += self.rsize
        return self.buffer[p1:p1+ncol]

    def skipRecords(self, fi, ncol, n=1):
        """
        Skip n records from the current read position
        """
        assert ncol == self.rsize
        nitm = n * self.rsize
        if (self.bptr+nitm) < self.btop:
            self.bptr += nitm
        else:
            ioff = nitm - (self.btop-self.bptr)
            fi.seek(DBL_SIZE * ioff, SEEK_CUR)
            self.fpos = fi.tell()
            self.bbtm = 0       # Flush buffer
            self.btop = 0       #   force read on next access
            self.bptr = 0
        self.dptr += nitm

    def writeHeader(self, fo, nrow, ncol, time, vmin, vmax):
        v = array.array('d', [float(nrow), float(ncol), time])
        v.tofile(fo)

    def writeRecord(self, fo, vals):
        v = array.array('d', vals)
        v.tofile(fo)

class DTDataIOBinaryReducedPrecision(DTDataIOBinaryBase):
    def __init__(self, res, fnam, cols=None):
        DTDataIOBinaryBase.__init__(self, fnam, cols)
        self.res = res
        self.coef = []

    def isValid(self):
        if not isBinary(self.fnam): return False
        fi = self.openForRead(self.fnam, bsize=RBUF_DEF)
        try:
            _nrow, ncol, _time = self.readHeader(fi)
            if len(self.coef) != ncol: return False
            for i in range(ncol):
                if len(self.coef[i]) != 3: return False
                vmin, vmax, vdel = self.coef[i]
                res = (vmax-vmin)/vdel
                if int(res+0.5) != self.res: return False
            self.readRecord(fi, ncol)
        except (EOFError, ValueError):
            return False
        except ZeroDivisionError:
            return False
        except IndexError:
            raise CTException.CTException('Inconsistent number of values and line size')
        fi.close()
        return True

    def readHeader(self, fi):
        self.coef = []
        a = array.array('d')
        a.fromfile(fi, 3)
        drow, dcol, time = a
        for _i in range(int(dcol)):
            a.fromfile(fi, 3)
            self.coef.append([v for v in a])
        self.fpos = fi.tell()
        return int(drow), int(dcol), time

    def readRecord(self, fi, ncol):
        def expandVal(self, val, ic):
            return val*self.coef[ic][2] + self.coef[ic][0]
        a = array.array('d', (0,)*ncol)
        a.fromfile(fi, ncol)
        self.fpos = fi.tell()
        return [expandVal(self, a[j], j) for j in range(ncol)]

    def writeHeader(self, fo, nrow, ncol, time, vmin, vmax):
        self.coef = []
        data = struct.pack('ddd', float(nrow), float(ncol), time)
        fo.write(data)
        for v1, v2 in zip(vmin, vmax):
            vd = (vmax-vmin) / self.res
            self.coef.append([v1, v2, vd])
            data = struct.pack('ddd', v1, v2, vd)
            fo.write(data)

    def writeRecord(self, fo, vals):
        raise NotImplementedError('TODO: adjust precision')
        #def reduceVal(self, val, ic):
        #    r = (val-self.coef[ic][0]) / self.coef[ic][2]
        #    return int(r+0.5)
        #v = array.array('i', list(map(reduceVal, vals)))
        #v.tofile(fo)

class DTDataIOBinaryI08(DTDataIOBinaryReducedPrecision):
    def __init__(self, fnam, cols=None):
        DTDataIOBinaryReducedPrecision.__init__(self, 2 << (8-1), fnam, cols)

class DTDataIOBinaryI16(DTDataIOBinaryReducedPrecision):
    def __init__(self, fnam, cols=None):
        DTDataIOBinaryReducedPrecision.__init__(self, 2 << (16-1), fnam, cols)


def constructReaderFromFile_isValid(r, m):
    if r.isValid():
        LOGGER.debug(m)
        return r
    return None

def constructReaderFromFile(fnam, cols=None):
    LOGGER.debug('Construct reader for : %s', fnam)
    if not os.path.exists(fnam):
        raise CTException.CTException('File not found: %s' % repr(fnam))

    if isBinary(fnam):
        LOGGER.debug('   detected as binary file')
        readers = [(DTDataIOBinaryI08, '   detected as H2D2 reduced precision 8bits'),
                   (DTDataIOBinaryI16, '   detected as H2D2 reduced precision 16bits'),
                   (DTDataIOBinary,    '   detected as H2D2 full precision')]
        for c, m in readers:
            r = constructReaderFromFile_isValid(c(fnam, cols), m)
            if r: return r
    else:
        LOGGER.debug('   detected as ascii file')
        readers = [(DTDataIOAsciiI08, '   detected as H2D2 reduced precision 8bits'),
                   (DTDataIOAsciiI16, '   detected as H2D2 reduced precision 16bits'),
                   (DTDataIOAscii,    '   detected as H2D2 full precision'),
                   (DTDataIOAsciiSMS, '   detected as SMS')]
        for c, m in readers:
            try:
                r = constructReaderFromFile_isValid(c(fnam, cols), m)
                if r: return r
            except Exception as e:
                LOGGER.debug('While constructing : %s', c.__name__)
                LOGGER.debug('   exception: %s', str(e))
    raise CTException.CTException('No valid reader found for file: %s' % fnam)


if __name__ == "__main__":
    def main():
        #p = 'E:/Projets_simulation/EQ/Dry-Wet/Simulations/GLOBAL_01/Simulation/global01_0036'
        #f = os.path.join(p, 'simul000.pst.sim')
        p = r'E:/Projets/Carillon'
        #f = os.path.join(p, 'Niveau 350 ans.txt')
        f = os.path.join(p, 'Vitesses 350 ans.dat')
        reader = constructReaderFromFile(f) #, cols=(2,4))
        blocs = reader.readStructNoStats()
        #blocs = reader.readStruct()
        for b in blocs:
            print('%s' % b.path)
            print('   data: %s' % b.data)
            print('   pos : %s' % b.pos)
            print('   nrow: %s' % b.nrow)
            print('   ncol: %s' % b.ncol)
            print('   time: %s' % b.time)
            print('   vmin: %s' % b.vmin)
            print('   vmax: %s' % b.vmax)

        nr, nc = blocs[0].nrow, blocs[0].ncol
        if nc == 1:
            buf = np.zeros([nr])
        else:
            buf = np.zeros([nr, nc])
        fo = os.path.join(p, 'Vitesses 350 ans-o.dat')
        writer = DTDataIOAsciiSMS(fo)
        for b in blocs:
            d = b.reader.readDataAtStep(b, buf)
            writer.writeDataBloc(b, d)

    streamHandler = logging.StreamHandler()
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    #import timeit
    #t = timeit.timeit('main()', setup="from __main__ import main", number=1)
    #print t
    #import cProfile
    #cProfile.run('main()')

    main()
