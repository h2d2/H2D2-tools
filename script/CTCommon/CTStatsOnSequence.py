#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2010-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from . import CTEntry

class CTStats:
    """Statistics on sequences

    Classifies the sequences according to the number of iterations
    and prints statics.
    Parameters: step
    """

    @staticmethod
    def menu_entry():
        return 'Statistics on sequences'

    @staticmethod
    def menu_bubble():
        bbl =  """Classifies the sequences according to the number of iterations and prints statics."""
        return bbl.strip()

    def __init__(self):
        pass

    def __call__(self, convhist, step = 5):
        self.classes = { }
        self.iters   = { }
        self.step = step

        def fltr_cb(e):
            c = e.stts.nitr / self.step
            self.classes.setdefault(c, 0)
            self.iters.setdefault  (c, 0)
            self.classes[c] += 1
            self.iters[c] += e.stts.nitr

        fltr = CTEntry.Filter('typename == "AlgoSequence"')
        convhist.filter_on_container(fltr_cb, fltr.filter)

        keys = list(self.classes.keys())
        keys.sort()
        cml1 = 0
        cml2 = 0
        for k in keys:
            cml1 += self.classes[k]
            cml2 += self.iters  [k]
        cml1 = float(cml1)
        cml2 = float(cml2)

        txt = [ ]
        c1 = 0
        c2 = 0
        txt.append("          Classes                  Cumulated")
        txt.append("  class   nseq  niter  |       nseq         iter     it/sq")
        txt.append("==========================================================")
        for k in range(int(keys[-1])+1):
            k1 = k*self.step
            k2 = k1 + self.step
            try:
                c = self.classes[k]
                i = self.iters[k]
                c1 += c
                c2 += i
                txt.append("%3i-%3i %6i %6i  | %5i (%3i%%)  %5i (%3i%%) %6.2f" % (k1, k2, c, i, c1, int(100.0*c1/cml1+0.5), c2, int(100.0*c2/cml2+0.5), float(c2)/float(c1)))
            except:
                txt.append("%3i-%3i %6i %6i  |" % (k1, k2, 0, 0))
        return txt

def main(argv):
    usage = \
"""
Usage:
    python CTStatsOnSequence.py filename [classes size]
"""

    if (len(argv) < 1 or len(argv) > 2):
        print(usage)
        return

    fname = argv[0]
    try:
        step = int(argv[1])
    except:
        step = None

    file = open(fname, 'r')

    convhist = CTEntry.AlgoConvergence()

    entry = CTEntry.Entry()
    while (entry.read(file)):
        convhist.append(entry)
    file.close()

    convhist.filter_bbox()
    convhist.count()

    stat = CTStats()
    if (step):
        stat(convhist, step)
    else:
        stat(convhist)

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
