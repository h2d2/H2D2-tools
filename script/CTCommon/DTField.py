# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import enum
import threading

from . import FEMesh
from . import DTData
try:
    from .DTReducOperation    import REDUCTION_OPERATION as Operation
except ImportError:
    from .DTReducOperation_pp import REDUCTION_OPERATION as Operation

import numpy as np

import logging
LOGGER = logging.getLogger("H2D2.Tools.Data.Field")
LOCK   = threading.Lock()

class DTField:
    CONTEXT = enum.Enum('CONTEXT', names=('unset', 'values', 'statistics'))

    def __init__(self):
        self.ctxs  = {}                     # Operation context (Op, Timesteps, CONTEXT)
        self.av_dt = None                   # Activ data: data
        self.av_cx = None                   # Activ data: context
        self.av_op = None                   # Activ data: data ops

    def __copy__(self):
        """
        Shallow copy, while reseting the activ data
        """
        from inspect import signature
        sig  = signature(type(self).__init__)
        narg = len(sig.parameters) - 1
        args = narg*(None,)
        newone = type(self)(*args)
        return self.shallowCopy(newone)

    def shallowCopy(self, newone):
        newone.ctxs  = self.ctxs
        newone.av_cx = None # self.av_cx   # None
        newone.av_op = None # self.av_op   # None
        newone.av_dt = None
        return newone

    def setContext(self, reducOp, tsteps, context):
        """
        DTField method might be called back by an operation. To fullfill the call,
        we will need the actual (reducOp, tsteps).
        As data loading can happen in a thread, keep a dictionnary with
        the thread id as key.
        """
        self.ctxs[threading.get_ident()] = (reducOp, tsteps, context)

    def resetContext(self):
        """
        """
        self.ctxs[threading.get_ident()] = (Operation.op_noop, None, DTField.CONTEXT.unset)

    def getContext(self):
        return self.ctxs[threading.get_ident()]

    def __xtrValues(self, dops, dfls):
        # The sequence diagram DTOp_eval display the interaction
        # betwen all participants
        # To be used @ https://sequencediagram.org/
        LOGGER.debug('DTField: Get data with dops %s', dops)
        ctx = self.getContext()
        if ctx != self.av_cx or dops != self.av_op:
            c = np.array(())    # array of size 0, for cython
            for iop, op in enumerate(dops):
                r = op()
                if dfls:
                    m = np.ones(np.shape(r)[0], dtype=np.bool8)
                    for ft in dfls:
                        m |= ft(cnv=r)
                    r = np.putmask(r, m, np.NAN)
                if c.size == 0:
                    try:
                        c = np.empty( (np.shape(r)[0], len(dops)) )
                    except IndexError:
                        c = np.empty( (1, len(dops)) )
                c[:,iop] = r[:,0]
            with LOCK:
                self.av_cx = ctx
                self.av_op = dops
                self.av_dt = c
        return self.av_dt

    def getDataStat(self, reducOp = Operation.op_noop, tsteps = None, dops=[], dfls=[]):
        LOGGER.debug('DTField: Get data stats with dops %s', dops)
        self.setContext(reducOp, tsteps, DTField.CONTEXT.statistics)
        self.__xtrValues(dops, dfls)
        self.resetContext()

    def getDataValues(self, reducOp = Operation.op_noop, tsteps = None, dops=[], dfls=[]):
        # The sequence diagram DTOp_eval display the interaction
        # betwen all participants
        # To be used @ https://sequencediagram.org/
        LOGGER.debug('DTField: Get data values with dops %s', dops)
        self.setContext(reducOp, tsteps, DTField.CONTEXT.values)
        self.__xtrValues(dops, dfls)
        self.resetContext()
        return self.av_dt

    def getDataAtTime(self, reducOp = Operation.op_noop, tsteps = None, dops = []):
        raise NotImplementedError

    def getDataAtStep(self, reducOp = Operation.op_noop, tsteps = None, dops = []):
        raise NotImplementedError

    def getDataActual(self):
        with LOCK:
            return self.av_dt

    def getData(self):
        raise NotImplementedError

    def doInterpolate(self, data, X, Y):
        raise NotImplementedError

    def doProbe(self, X, Y):
        raise NotImplementedError

    def getGrid(self):
        raise NotImplementedError

    def getGridAsRunLength(self):
        raise NotImplementedError

    def getDataMin(self):
        raise NotImplementedError

    def getDataMax(self):
        raise NotImplementedError

class DT2DFiniteElementField(DTField):
    def __init__(self, mesh, data=None, source=None):
        super().__init__()
        self.mesh = mesh    # FEMesh or subgrid
        self.data = data    # DTData
        self.srce = source  # DTField in case of subgrid
        self.itrp = FEMesh.FEInterpolatedPoints(self.mesh) if self.mesh else None
        self.prbe = FEMesh.FEProbeOnPoints(self.mesh) if self.mesh else None
        self.ndgb = self.mesh.getNodeNumbersGlobal() if self.mesh and self.mesh.isSubMesh() else None

    def __str__(self):
        if self.av_cx:
            o, t, _ = self.av_cx
            if o == Operation.op_noop:
                s = ''
            else:
                s = '%s(' % o.name
            if len(t) == 1:
                s += '%f' % t[0]
            else:
                s += '[%f,%f]' % (t[0], t[-1])
            if o != Operation.op_noop:
                s += ')'
        else:
            s = ''
        return s

    def __getitem__(self, col):
        reducOp, tsteps, _ = self.getContext()
        op = DTData.DTDataReduc()
        r = op.compute(self.data, reducOp=reducOp, tsteps=tsteps, cols=(col,))
        return r

    def shallowCopy(self, newone):
        """
        Shallow copy, while reseting the activ data
        """
        newone = super().shallowCopy(newone)
        newone.mesh = self.mesh
        newone.ndgb = self.ndgb
        newone.itrp = self.itrp
        newone.prbe = self.prbe
        newone.data = self.data
        newone.srce = self.srce
        return newone

    def getDataAtTime(self, reducOp=Operation.op_noop, tsteps=None, dops=[]): # , dfls=[]):
        LOGGER.debug('DT2DFiniteElementField: Get data with op %s at times (%s)', reducOp, tsteps)
        if not self.srce:
            r = self.getDataValues(reducOp=reducOp, tsteps=tsteps, dops=dops)
        else:
            r = self.srce.getDataAtTime(reducOp=reducOp, tsteps=tsteps, dops=dops)
            self.av_dt = r[self.ndgb]
        return self.av_dt

    def getDataAtStep(self, reducOp=Operation.op_noop, tsteps=None, dops=[]):
        LOGGER.debug('DT2DFiniteElementField: Get data with op %s at steps (%s)', reducOp, tsteps)
        if not self.srce:
            self.getDataValues(reducOp=reducOp, tsteps=tsteps, dops=dops)
        else:
            r = self.srce.getDataAtStep(reducOp=reducOp, tsteps=tsteps, dops=dops)
            self.av_dt = r[self.ndgb]
        return self.av_dt

    def getData(self):
        return self.data

    def doInterpolate(self, data, X, Y):
        # if data. != self.mesh.getNbNodes()
        self.itrp.setCoord(X, Y)
        i = self.itrp.interpolate(data)
        return i

    def doProbe(self, X, Y):
        return self.prbe.interpolate(X, Y, self.av_dt)

    def getGrid(self):
        return self.mesh

    def getGridAsRunLength(self):
        if not self.mesh:
            raise RuntimeError('No grid')
        if not isinstance(self.mesh.getElement(0), FEMesh.FEElementL2):
            raise RuntimeError('Invalid element type: only L2 elements are supported')
        s = [0.0]
        np = -1
        for e in self.mesh.iterElements():
            n0, n1 = e.getConnectivities()
            if np != -1 and n0 != np:
                raise RuntimeError('Multiple chains. Only first returned.')
            np = n1
            l = e.sideLength()
            s.append( s[-1]+l )
        return s

    def getDataMin(self):
        if self.av_dt.ndim == 1:
            return np.min(self.av_dt)
        elif self.av_dt.ndim == 2 and self.av_dt.shape[-1] == 1:
            return np.min(self.av_dt)
        else:
            return np.min(np.linalg.norm(self.av_dt, axis=self.av_dt.ndim-1))

    def getDataMax(self):
        if self.av_dt.ndim == 1:
            return np.max(self.av_dt)
        elif self.av_dt.ndim == 2 and self.av_dt.shape[-1] == 1:
            return np.max(self.av_dt)
        else:
            return np.max(np.linalg.norm(self.av_dt, axis=self.av_dt.ndim-1))

class DT2DRegularGridField(DTField):
    def __init__(self, rgrid, source):
        super().__init__()
        self.rgrid  = rgrid
        self.source = source    # DTField

    def __getitem__(self, col):
        reducOp, tsteps, _ = self.getContext()
        op = DTData.DTDataReduc()
        r = op.compute(self.data, reducOp=reducOp, tsteps=tsteps, cols=(col,))
        r = self.source.doInterpolate(r, *self.rgrid.getCoordinates())
        return r

    def shallowCopy(self, newone):
        """
        Shallow copy, while reseting the activ data
        """
        newone = super().shallowCopy(newone)
        newone.rgrid  = self.rgrid
        newone.source = self.source
        return newone

    def getContext(self):
        return self.source.getContext()

    def getDataAtTime(self, reducOp = Operation.op_noop, tsteps = None, dops = []):
        LOGGER.debug('DT2DRegularGridField: Get data with op %s at times (%s)', reducOp, tsteps)
        r = self.source.getDataAtTime(reducOp, tsteps, dops=dops)
        self.av_dt = self.source.doInterpolate(r, *self.rgrid.getCoordinates())
        return self.av_dt

    def getDataAtStep(self, reducOp = Operation.op_noop, tsteps = None, dops = []):
        LOGGER.debug('DT2DRegularGridField: Get data with op %s at steps (%s)', reducOp, tsteps)
        r = self.source.getDataAtStep(reducOp, tsteps, dops=dops)
        self.av_dt = self.source.doInterpolate(r, *self.rgrid.getCoordinates())
        return self.av_dt

    def getData(self):
        return self.source.getData() if self.source else None

    def doInterpolate(self, data, X, Y):
        raise NotImplementedError

    def doProbe(self, X, Y):
        raise NotImplementedError

    def getGrid(self):
        return self.rgrid

    def getDataMin(self):
        """
        return min ignoring nan
        """
        if self.av_dt.ndim == 1:
            return np.nanmin(self.av_dt)
        else:
            return np.nanmin(np.linalg.norm(self.av_dt, axis=self.av_dt.ndim-1))

    def getDataMax(self):
        """
        return max ignoring nan
        """
        if self.av_dt.ndim == 1:
            return np.nanmax(self.av_dt)
        else:
            return np.nanmax(np.linalg.norm(self.av_dt, axis=self.av_dt.ndim-1))

class DT1DRegularGridField(DTField):
    def __init__(self, rgrid, source):
        super().__init__()
        self.rgrid  = rgrid
        self.source = source    # DTField

    def __getitem__(self, col):
        reducOp, tsteps, _ = self.getContext()
        op = DTData.DTDataReduc()
        r = op.compute(self.data, reducOp=reducOp, tsteps=tsteps, cols=(col,))
        r = self.source.doInterpolate(r, *self.rgrid.getCoordinates())
        return r

    def shallowCopy(self, newone):
        """
        Shallow copy, while reseting the activ data
        """
        newone = super().shallowCopy(newone)
        newone.rgrid  = self.rgrid
        newone.source = self.source
        return newone

    def getContext(self):
        return self.source.getContext()

    def getDataAtTime(self, reducOp = Operation.op_noop, tsteps = None, dops = []):
        LOGGER.debug('DT1DRegularGridField: Get data with op %s at times (%s)', reducOp, tsteps)
        r = self.source.getDataAtTime(reducOp, tsteps, dops=dops)
        self.av_dt = self.source.doInterpolate(r, *self.rgrid.getCoordinates())
        return self.av_dt

    def getDataAtStep(self, reducOp = Operation.op_noop, tsteps = None, dops = []):
        LOGGER.debug('DT1DRegularGridField: Get data with op %s at steps (%s)', reducOp, tsteps)
        r = self.source.getDataAtStep(reducOp, tsteps, dops=dops)
        self.av_dt = self.source.doInterpolate(r, *self.rgrid.getCoordinates())
        return self.av_dt

    def getData(self):
        return self.source.getData() if self.source else None

    def doInterpolate(self, data, X, Y):
        raise NotImplementedError

    def doProbe(self, X, Y):
        raise NotImplementedError

    def getGrid(self):
        return self.rgrid

    def getGridAsRunLength(self):
        return self.rgrid.asRunLength()

    def getDataMin(self):
        """
        return min ignoring nan
        """
        return np.nanmin(self.av_dt)

    def getDataMax(self):
        """
        return max ignoring nan
        """
        return np.nanmax(self.av_dt)


if __name__ == "__main__":
    # pylint: disable=all
    def main():
        import os
        p = 'E:/Projets_simulation/EQ/Dry-Wet/Simulations/GLOBAL_01/Simulation/global01_0036'
        f = os.path.join(p, 'simul000.pst.sim')
        reader = DTData.constructReaderFromFile(f) #, cols = (3,))
        blocs = reader.readStructNoStats()

    streamHandler = logging.StreamHandler()
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    main()

