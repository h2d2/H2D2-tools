
#include "QTQuadTree_c.h"

QTPoolAllocator QTNode::allocator;

void QTNode::resizeAllocator(size_t n)
{
   QTNode::allocator.resize(n);
}

QTTree::QTTree()
   : m_headP(nullptr)
{
}

QTTree::~QTTree()
{
   delete m_headP;
   m_headP = nullptr;
}

void QTTree::init(double bbox[4])
{
   return init(bbox, MAX_ITEMS, MAX_DEPTH);
}

void QTTree::init(double bbox[4], long max_items, long max_depth)
{
   m_bbox = { bbox[0], bbox[1], bbox[2], bbox[3] };

   QTNode::resizeAllocator(128);

   double width  = bbox[2] - bbox[0];
   double height = bbox[3] - bbox[1];
   double midx = (bbox[2] + bbox[0]) / 2.0;
   double midy = (bbox[3] + bbox[1]) / 2.0;
   long depth = 0;
   m_headP = new QTNode(midx, midy, width, height, max_items, max_depth, depth);
}

bool QTTree::operator == (const QTTree& other) const
{
   if (this->m_bbox != other.m_bbox)     return false;
   if ( this->m_headP && !other.m_headP) return false;
   if (!this->m_headP &&  other.m_headP) return false;
   if ( this->m_headP &&  other.m_headP && *this->m_headP != *other.m_headP) return false;
   return true;
}

bool QTTree::operator != (const QTTree& other) const
{
   return !(*this == other);
}

void QTTree::insert(TTInfo item, double bbox[4])
{
   TTBBox bb = { bbox[0], bbox[1], bbox[2], bbox[3] };
   this->m_headP->insert( QTItem(item, bb) );
}

void QTTree::remove(TTInfo item, double bbox[4])
{
   TTBBox bb = { bbox[0], bbox[1], bbox[2], bbox[3] };
   this->m_headP->insert(QTItem(item, bb));
}

TTResults QTTree::intersect(double bbox[4]) const
{
   TTBBox bb = { bbox[0], bbox[1], bbox[2], bbox[3] };
   TTResults results;
   TTUniquer uniq;
   this->m_headP->intersect(bb, results, uniq);
   return results;
}

const double* QTTree::bbox() const
{
   return this->m_bbox.data();
}

size_t QTTree::size() const
{
   return this->m_headP ? this->m_headP->size() : 0;
}

size_t QTTree::maxItemsEff() const
{
   return this->m_headP ? this->m_headP->maxItemsEff() : 0;
}

size_t QTTree::maxDepthEff() const
{
   return this->m_headP ? this->m_headP->maxDepthEff() : 0;
}

size_t QTTree::maxItemsLmt() const
{
   return this->m_headP ? this->m_headP->maxItemsLmt() : 0;
}

size_t QTTree::maxDepthLmt() const
{
   return this->m_headP ? this->m_headP->maxDepthLmt() : 0;
}

size_t QTTree::countNodes() const
{
   return this->m_headP ? (this->m_headP->countNodes() + 1) : 0;
}

size_t QTTree::countItems() const
{
   return this->m_headP ? this->m_headP->countItems() : 0;
}
