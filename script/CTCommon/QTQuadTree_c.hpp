//************************************************************************
// --- Copyright (c) Yves Secretan 2019
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************
#ifndef QTQuadTree_c_HPP
#define QTQuadTree_c_HPP

#include <algorithm>

/*
* ========================================================================
* QTItem
* ========================================================================
*/
inline
QTItem::QTItem()
   : m_bbox({ 0.0, 0.0, 0.0, 0.0 })
   , m_item(-1)
{
}

inline
QTItem::QTItem(const TTInfo& item, const TTBBox& bbox)
   : m_bbox(bbox)
   , m_item(item)
{
}

inline
bool QTItem::operator == (const QTItem& other) const
{
   return (this->m_item == other.m_item && this->m_bbox == other.m_bbox);
}

inline
bool QTItem::operator != (const QTItem& other) const
{
   return !(*this == other);
}

/*
* ========================================================================
* QTNode
* ========================================================================
*/
inline
QTNode::QTNode()
   : m_childs({ nullptr, nullptr, nullptr, nullptr })
   , m_center({ 0.0, 0.0 })
   , m_span({ -1.0, -1.0 })
   , max_items(-1)
   , max_depth(-1)
   , depth(-1)
{
}

inline
QTNode::QTNode(double x, double y, double width, double height, long max_items, long max_depth, long depth)
   : m_childs({ nullptr, nullptr, nullptr, nullptr })
   , m_center({ x, y })
   , m_span({ width, height })
   , max_items(max_items)
   , max_depth(max_depth)
   , depth(depth)
{
   this->m_items.reserve(max_items + 1);
}

inline
QTNode::~QTNode()
{
   delete m_childs[0];
   delete m_childs[1];
   delete m_childs[2];
   delete m_childs[3];
   m_childs = { nullptr, nullptr, nullptr, nullptr };
}

inline
void* QTNode::operator new(size_t size)
{
   return allocator.allocate(size);
}

inline
void QTNode::operator delete(void* ptr, size_t size)
{
   return allocator.deallocate(ptr, size);
}

inline
bool QTNode::operator == (const QTNode& other) const
{
   if (this->depth != other.depth) return false;
   if (this->max_depth != other.max_depth) return false;
   if (this->max_items != other.max_items) return false;
   if (this->m_span != other.m_span)    return false;
   if (this->m_center != other.m_center)  return false;

   if (this->m_childs[0] && !other.m_childs[0]) return false;
   if (!this->m_childs[0] && other.m_childs[0]) return false;
   if (this->m_childs[0] && other.m_childs[0] && *this->m_childs[0] != *other.m_childs[0])  return false;

   if (this->m_childs[1] && !other.m_childs[1]) return false;
   if (!this->m_childs[1] && other.m_childs[1]) return false;
   if (this->m_childs[1] && other.m_childs[1] && *this->m_childs[1] != *other.m_childs[1])  return false;

   if (this->m_childs[2] && !other.m_childs[2]) return false;
   if (!this->m_childs[2] && other.m_childs[2]) return false;
   if (this->m_childs[2] && other.m_childs[2] && *this->m_childs[1] != *other.m_childs[1])  return false;

   if (this->m_childs[3] && !other.m_childs[3]) return false;
   if (!this->m_childs[3] && other.m_childs[3]) return false;
   if (this->m_childs[3] && other.m_childs[3] && *this->m_childs[1] != *other.m_childs[1])  return false;

   return true;
}

inline
bool QTNode::operator != (const QTNode& other) const
{
   return !(*this == other);
}

inline
void QTNode::insert_into_children(const QTItem& item)
{
   if (item.m_bbox[0] <= this->m_center[0])
   {
      if (item.m_bbox[1] <= this->m_center[1])
         this->m_childs[0]->insert(item);
      if (item.m_bbox[3] >= this->m_center[1])
         this->m_childs[1]->insert(item);
   }
   if (item.m_bbox[2] > this->m_center[0])
   {
      if (item.m_bbox[1] <= this->m_center[1])
         this->m_childs[2]->insert(item);
      if (item.m_bbox[3] >= this->m_center[1])
         this->m_childs[3]->insert(item);
   }
}

inline 
void QTNode::remove_from_children(const QTItem& item)
{
   // try to remove from children
   if (item.m_bbox[0] <= this->m_center[0])
   {
      if (item.m_bbox[1] <= this->m_center[1])
         this->m_childs[0]->remove(item);
      if (item.m_bbox[3] >= this->m_center[1])
         this->m_childs[1]->remove(item);
   }
   if (item.m_bbox[2] > this->m_center[0])
   {
      if (item.m_bbox[1] <= this->m_center[1])
         this->m_childs[2]->remove(item);
      if (item.m_bbox[3] >= this->m_center[1])
         this->m_childs[3]->remove(item);
   }
}

inline
void QTNode::split()
{
   double quartwidth = this->m_span[0] / 4.0;
   double quartheight= this->m_span[1] / 4.0;
   double halfwidth  = this->m_span[0] / 2.0;
   double halfheight = this->m_span[1] / 2.0;
   double x1 = this->m_center[0] - quartwidth;
   double x2 = this->m_center[0] + quartwidth;
   double y1 = this->m_center[1] - quartheight;
   double y2 = this->m_center[1] + quartheight;
   long   new_depth = this->depth + 1;

   this->m_childs[0] = new QTNode(x1, y1, halfwidth, halfheight,
                                    this->max_items, this->max_depth, new_depth);
   this->m_childs[1] = new QTNode(x1, y2, halfwidth, halfheight,
                                    this->max_items, this->max_depth, new_depth);
   this->m_childs[2] = new QTNode(x2, y1, halfwidth, halfheight,
                                    this->max_items, this->max_depth, new_depth);
   this->m_childs[3] = new QTNode(x2, y2, halfwidth, halfheight,
                                    this->max_items, this->max_depth, new_depth);

   for (TTItems::const_iterator nI = this->m_items.begin();
      nI != this->m_items.end();
      ++nI)
   {
      this->insert_into_children(*nI);
   }
   this->m_items.clear();
}

inline
void QTNode::insert(const QTItem& item)
{
   // Insert a node into the treee, eventualy spliting.
   if (this->m_childs[0] == nullptr)
   {
      this->m_items.push_back(item);

      if (this->m_items.size() > this->max_items &&
         this->depth < this->max_depth)
      {
         this->split();
      }
   }
   else
   {
      this->insert_into_children(item);
   }
}

inline
void QTNode::remove(const QTItem& item)
{
   if (this->m_childs[0] == nullptr)
   {
      TTItems::const_iterator posI = std::find(this->m_items.begin(), this->m_items.end(), item);
      if (posI != this->m_items.end()) // == myVector.end() means the element was not found
         this->m_items.erase(posI);
   }
   else
      this->remove_from_children(item);
}

inline
void QTNode::intersect(const TTBBox& rect, TTResults& results, TTUniquer& uniq) const
{
   // ---  Search children
   if (this->m_childs[0] != nullptr)
   {
      if (rect[0] <= this->m_center[0])
      {
         if (rect[1] <= this->m_center[1])
            this->m_childs[0]->intersect(rect, results, uniq);
         if (rect[3] >= this->m_center[1])
            this->m_childs[1]->intersect(rect, results, uniq);
      }
      if (rect[2] >= this->m_center[0])
      {
         if (rect[1] <= this->m_center[1])
            this->m_childs[2]->intersect(rect, results, uniq);
         if (rect[3] >= this->m_center[1])
            this->m_childs[3]->intersect(rect, results, uniq);
      }
   }
   // ---  Search node at this level
   for (TTItems::const_iterator iI = this->m_items.begin();
      iI != this->m_items.end();
      ++iI)
   {
      const QTItem& item(*iI);
      if (uniq.find(item.m_item) != uniq.end()) continue;
      if (item.m_bbox[2] < rect[0]) continue;
      if (item.m_bbox[0] > rect[2]) continue;
      if (item.m_bbox[3] < rect[1]) continue;
      if (item.m_bbox[1] > rect[3]) continue;
      results.push_back(item.m_item);
      uniq.insert(item.m_item);
   }
}

inline
size_t QTNode::size() const
{
   /*
   Returns a count of the total number of items
   inserted into the quadtree.
   */
   size_t size = 0;
   if (this->m_childs[0])
   {
      size += this->m_childs[0]->size();
      size += this->m_childs[1]->size();
      size += this->m_childs[2]->size();
      size += this->m_childs[3]->size();
   }
   size += this->m_items.size();
   return size;
}

inline
size_t QTNode::maxItemsEff() const
{
   size_t n = this->m_items.size();
   if (this->m_childs[0])
   {
      n = std::max(n, this->m_childs[0]->maxItemsEff());
      n = std::max(n, this->m_childs[1]->maxItemsEff());
      n = std::max(n, this->m_childs[2]->maxItemsEff());
      n = std::max(n, this->m_childs[3]->maxItemsEff());
   }
   return n;
}

inline
size_t QTNode::maxDepthEff() const
{
   size_t n = this->depth;
   if (this->m_childs[0])
   {
      n = std::max(n, this->m_childs[0]->maxDepthEff());
      n = std::max(n, this->m_childs[1]->maxDepthEff());
      n = std::max(n, this->m_childs[2]->maxDepthEff());
      n = std::max(n, this->m_childs[3]->maxDepthEff());
   }
   return n;
}

inline
size_t QTNode::maxItemsLmt() const
{
   return this->max_items;
}

inline
size_t QTNode::maxDepthLmt() const
{
   return this->max_depth;
}

inline
size_t QTNode::countNodes() const
{
   size_t cnt = 0;
   cnt += this->m_childs[0] ? (this->m_childs[0]->countNodes() + 1) : 0;
   cnt += this->m_childs[1] ? (this->m_childs[1]->countNodes() + 1) : 0;
   cnt += this->m_childs[2] ? (this->m_childs[2]->countNodes() + 1) : 0;
   cnt += this->m_childs[3] ? (this->m_childs[3]->countNodes() + 1) : 0;
   return cnt;
}

inline
size_t QTNode::countItems() const
{
   size_t cnt = this->m_items.size();
   cnt += this->m_childs[0] ? this->m_childs[0]->countItems() : 0;
   cnt += this->m_childs[1] ? this->m_childs[1]->countItems() : 0;
   cnt += this->m_childs[2] ? this->m_childs[2]->countItems() : 0;
   cnt += this->m_childs[3] ? this->m_childs[3]->countItems() : 0;
   return cnt;
}

#endif // QTQuadTree_c_HPP
