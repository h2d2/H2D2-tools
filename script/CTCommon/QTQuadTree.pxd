# -*- coding: utf-8 -*-
# distutils: language = c++
## cython: profile=True
## cython: linetrace=True
# cython: boundscheck(False)
# cython: wraparound(False)
# cython: initializedcheck(False)
# cython: nonecheck(False)
# cython: language_level(3)

from libcpp.vector cimport vector
from numpy         cimport uint8_t

cdef extern from "QTQuadTree_c.h":
    cdef cppclass QTTree:
        QTTree()
        void init(double[4])
        void init(double[4], long, long)
        
        void            insert     (long, double[4])
        void            remove     (long, double[4])
        vector[long]    intersect  (double[4])

        const double*   bbox       ()
        long            size       ()
        long            maxItemsEff()
        long            maxDepthEff()
        long            maxItemsLmt()
        long            maxDepthLmt()

cdef extern from "QTBytestream_c.h":
    cdef cppclass QTBytestream:
        QTBytestream()
        QTBytestream(uint8_t*, size_t)
        QTBytestream(const QTBytestream&)
        QTBytestream& operator= (const QTBytestream&)

        size_t   size()
        uint8_t* data()

        QTBytestream& operator<< (const QTTree&) except +
        QTBytestream& operator>> (const QTTree&) except +


cdef class QTQuadTree:
    cdef QTTree *head
