//************************************************************************
// --- Copyright (c) Yves Secretan 2021
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Classe:
//    QTAllocator
//
// Notes:
//    http://dmitrysoshnikov.com/compilers/writing-a-pool-allocator/
//************************************************************************

#include "QTAllocator_c.h"

#include <iostream>

void QTPoolAllocator::resize(size_t chunksPerBlock) {

   size_t freeSize = 0;
   Chunk* freeChunk = mAlloc;
   while (freeChunk != nullptr) {
      freeChunk = freeChunk->next;
      ++freeSize;
   }

   mChunksPerBlock = chunksPerBlock;
   mChunksOnResize = freeSize % chunksPerBlock;
}

/**
 * Allocates a new block from OS.
 *
 * Returns a Chunk pointer set to the beginning of the block.
 */
Chunk* QTPoolAllocator::allocateBlock(size_t chunkSize) {
   size_t chunksEff = mChunksPerBlock - mChunksOnResize;
   size_t blockSize = chunksEff * chunkSize;
   mChunksOnResize = 0;

   // The first chunk of the new block.
   Chunk* blockBegin = reinterpret_cast<Chunk*>(malloc(blockSize));

   // Once the block is allocated, we need to chain all
   // the chunks in this block:
   // The loop in in [0, chunksEff-1[, but to use a size_t index,
   // shift by +1
   Chunk* chunk = blockBegin;
   for (size_t i = 1; i < chunksEff; ++i)
   {
      chunk->next = reinterpret_cast<Chunk*>(reinterpret_cast<char*>(chunk) + chunkSize);
      chunk = chunk->next;
   }
   chunk->next = nullptr;

   return blockBegin;
}

/**
 * Returns the first free chunk in the block.
 *
 * If there are no chunks left in the block,
 * allocates a new block.
 */
void* QTPoolAllocator::allocate(size_t size) {

   // No chunks left in the current block, or no any block
   // exists yet. Allocate a new one, passing the chunk size:
   if (mAlloc == nullptr) {
      mAlloc = allocateBlock(size);
   }

   // The return value is the current position of
   // the allocation pointer:
   Chunk* freeChunk = mAlloc;

   // Advance (bump) the allocation pointer to the next chunk.
   //
   // When no chunks left, the `mAlloc` will be set to `nullptr`, and
   // this will cause allocation of a new block on the next request:
   mAlloc = mAlloc->next;

   return freeChunk;
}

/**
 * Puts the chunk into the front of the chunks list.
 */
void QTPoolAllocator::deallocate(void* chunk, size_t size) {

   // The freed chunk's next pointer points to the
   // current allocation pointer:
   reinterpret_cast<Chunk*>(chunk)->next = mAlloc;

   // And the allocation pointer is now set
   // to the returned (free) chunk:
   mAlloc = reinterpret_cast<Chunk*>(chunk);
}
