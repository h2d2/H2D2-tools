# -*- coding: utf-8 -*-

# This is an automatically generated file.
# Manual changes will be merged and conflicts marked!
#
# Generated by py2pxd_ version 0.0.3 on 2022-12-16 12:52:30

import cython
from numpy cimport ndarray
from pubsub import pub
from . cimport CTException
from . cimport DTDataBloc
from . cimport DTDataIO
from .DTReducOperation cimport REDUCTION_OPERATION as OperationPXD

cdef class DTDataOpBase:
    cdef public dict         locals
    #
    # cdef function cannot have starstar argument
    # cpdef              setLocals       (DTDataOp self, dict kwargs)

cdef class DTDataOp(DTDataOpBase):
    cdef public object       ast
    cdef public str          code
    cdef public str          src
    #
    @cython.locals (ast = object, ptrn = str, repl = str, src = str)
    cdef tuple         __getAst        (DTDataOp self)
    #@cython.locals (lcls = dict)
    #cpdef object       __call__        (DTDataOp self, object cnv=*)

cdef class DTDataOpCol(DTDataOpBase):
    cdef public long         col
    #
    cpdef long         getCol          (DTDataOpCol self)
    #cpdef object       __call__        (DTDataOpCol self)

cdef class DTData:
    cdef public list         blocs
    cdef public ndarray      buf
    cdef public list         cache
    cdef public list         files
    cdef public long         ncol
    cdef public long         nrow
    #
    cpdef              reset           (DTData self)
    @cython.locals (b = DTDataBloc.DTDataBloc, blocs = list, d = ndarray, errMsg = list, ncol = long, nrow = long, shp = tuple, t = double, vmax = list, vmin = list)
    cpdef              appendData      (DTData self, list data)
    @cython.locals (blocs = list, errMsg = list, ncol = long, nrow = long, readr = DTDataIO.DTDataIO)
    cpdef              appendFile      (DTData self, str file, tuple cols=*)
    @cython.locals (b0 = ndarray, bloc = DTDataBloc.DTDataBloc)
    cpdef ndarray      __loadDataAtStep(DTData self, long i)
    @cython.locals (alfa = double, d1 = ndarray, d2 = ndarray, i = long, i1 = long, i2 = long, t1 = double, t2 = double)
    cpdef ndarray      __loadDataAtTime(DTData self, double time)
    @cython.locals (_i = object, bloc = DTDataBloc.DTDataBloc, d_ = object, i_ = long, r = object)
    cpdef object       __loadNodesAtStep(DTData self, long i, object nodes, bint compact=*)
    @cython.locals (alfa = object, d1 = object, d2 = object, i = object, i1 = long, i2 = object, t1 = object, t2 = object)
    cpdef object       __loadNodesAtTime(DTData self, double time, object nodes, bint compact=*)
    cpdef object       getFiles        (DTData self)
    cpdef long         getNbNodes      (DTData self)
    cpdef long         getNbVal        (DTData self)
    cpdef long         getNbTimeSteps  (DTData self)
    cpdef double       getTimeMin      (DTData self)
    cpdef double       getTimeMax      (DTData self)
    @cython.locals (times = list)
    cpdef list         getTimes        (DTData self)
    @cython.locals (cols_ = object)
    cpdef ndarray      __filterCols    (DTData self, ndarray d, list cols)
    @cython.locals (md5 = object)
    cpdef object       getDataMD5      (DTData self, ndarray d)
    @cython.locals (d = object)
    cpdef ndarray      getDataAtTime   (DTData self, double time, list cols=*)
    @cython.locals (d = object)
    cpdef ndarray      getDataAtStep   (DTData self, long i, list cols=*)
    @cython.locals (d = object)
    cpdef ndarray      getNodesAtTime  (DTData self, double time, list nodes=*, list cols=*, bint compact=*)
    @cython.locals (d = object)
    cpdef ndarray      getNodesAtStep  (DTData self, long i, list nodes=*, list cols=*, bint compact=*)
    @cython.locals (d = object)
    cpdef object       getPointsAtTime (DTData self, double time, object points, list cols=*)
    @cython.locals (d = object)
    cpdef object       getPointsAtStep (DTData self, long i, object points, list cols=*)
    @cython.locals (d = object, ib = object, r = list, t = object)
    cpdef object       getTimeSerieAtNodes(DTData self, list nodes=*, object tmin=*, object tmax=*, list cols=*)
    @cython.locals (d = object, ib = object, r = list, t = object)
    cpdef object       getTimeSerieAtPoints(DTData self, object points, object tmin=*, object tmax=*, list cols=*)
    @cython.locals (b = DTDataBloc.DTDataBloc, d = ndarray, i = long)
    cpdef              write           (DTData self, DTDataIO.DTDataIO writer, object maskCB=*)
    @cython.locals (w = DTDataIO.DTDataIO)
    cpdef              writeAsAscii    (DTData self, str f, object maskCB=*)
    @cython.locals (w = DTDataIO.DTDataIO)
    cpdef              writeAsBinary   (DTData self, str f, object maskCB=*)
    @cython.locals (t = double, v = object)
    cpdef              writeTimeSerie  (DTData self, DTDataIO.DTDataIO writer, object data)
    @cython.locals (w = DTDataIO.DTDataIO)
    cpdef              writeTimeSerieAsAscii(DTData self, str f, object data)
    @cython.locals (w = DTDataIO.DTDataIO)
    cpdef              writeTimeSerieAsBinary(DTData self, str f, object data)
    @cython.locals (b = DTDataBloc.DTDataBloc, d = object, f = str, i = long, w = DTDataIO.DTDataIO)
    cpdef              splitAsAscii    (DTData self, str root)
    @cython.locals (b = DTDataBloc.DTDataBloc, d = object, f = str, i = long, w = DTDataIO.DTDataIO)
    cpdef              splitAsBinary   (DTData self, str root)

cdef class DTDataStats:
    cdef public ndarray      max
    cdef public ndarray      min
    cdef public ndarray      s0
    cdef public ndarray      s1
    cdef public ndarray      s2
    #
    @cython.locals (d = object, it = long, l = long, n = ndarray, nVal = long, t = double, times = object, ttmp = object)
    cpdef              compute         (DTDataStats self, object data, object nodes=*, object tsteps=*, list cols=*)
    cpdef ndarray      getMax          (DTDataStats self)
    cpdef ndarray      getMin          (DTDataStats self)
    cpdef ndarray      getMean         (DTDataStats self)
    @cython.locals (m = ndarray, s2 = ndarray)
    cpdef ndarray      getStd          (DTDataStats self)
    cpdef ndarray      getCount        (DTDataStats self)

cdef class DTDataReduc:
    cdef public object       val
    #
    @cython.locals (acc = object, d = object, doProgress = object, getDataFnc = object, it = object, t = object, times = object)
    cpdef object       compute         (DTDataReduc self, object data, OperationPXD reducOp=*, object tsteps=*, list cols=*)
    cpdef object       getVal          (DTDataReduc self)

@cython.locals (d = object, dd = object, dr = object, f = object, it = list, m = object, m1 = object, m2 = object, md5 = list, p = str, t = object, times = object)
cpdef object       main            ()

