#!/usr/bin/env python
# -*- coding: utf-8 -*-
## cython: profile=True
# cython: linetrace=True
#************************************************************************
# --- Copyright (c) Yves Secretan 2022
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Cache path utilities
"""

import glob
import logging
import os
import platform

LOGGER = logging.getLogger("H2D2.Tools.Cache")

# ---  Get the cache directory
def getCachePath(subPath=''):
    cachePath = ''
    if platform.system() == 'Windows':
        if not cachePath:
            try:
                p = os.path.join(os.environ['LOCALAPPDATA'], 'H2D2')
                if os.path.isdir(p): cachePath = p
            except KeyError:
                pass
        if not cachePath:
            try:
                p = os.path.join(os.environ['APPDATA'], 'H2D2')
                if os.path.isdir(p): cachePath = p
            except KeyError:
                pass
        if not cachePath:
            cachePath = os.path.join(os.environ['LOCALAPPDATA'], 'H2D2')
    elif platform.system() == 'Linux':
        try:
            p = os.path.join(os.environ['HOME'], '.H2D2')
            if os.path.isdir(p): cachePath = p
        except KeyError:
            pass
        if not cachePath:
            cachePath = os.path.join(os.environ['HOME'], '.H2D2')
    else:
        raise RuntimeError('Unsupported platform: %s' % platform.system())

    if cachePath:
        cachePath = os.path.join(cachePath, 'cache', subPath)
        if not os.path.isdir(cachePath):
            try:
                os.makedirs(cachePath)
            except:
                raise RuntimeError('Could not locate/create H2D2 cache directory: %s' % cachePath)
    else:
        raise RuntimeError('Could not locate/create H2D2 cache directory')

    return cachePath

def clearCache(subPath='', patterns=('*.*',)):
    cachePath = getCachePath(subPath)
    if cachePath:
        for spt in patterns:
            pth = os.path.join(cachePath, spt)
            for p in glob.glob(pth):
                os.remove(p)

