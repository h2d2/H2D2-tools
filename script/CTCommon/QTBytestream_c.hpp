//************************************************************************
// --- Copyright (c) Yves Secretan 2021
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Classe:
//    QTBytestream
//
// Description:
// 
// Attributs:
//
// Notes:
//************************************************************************
#ifndef QTBytestream_c_HPP
#define QTBytestream_c_HPP

#include "QTQuadTree_c.h"

#include <assert.h>

inline
size_t QTBytestream::size() const
{
   return (m_data-m_base);
}

/*
* Only relevant for output stream;
*/
inline 
uint8_t* QTBytestream::data() const
{
   return m_base;
}

template <typename TTType>
QTBytestream& QTBytestream::operator<< (const TTType& value)
{
   if (m_data+sizeof(TTType) >= m_top)
      grow();

   *reinterpret_cast<TTType*>(m_data) = value;
   m_data += sizeof(TTType);
   return *this;
}

template <typename TTType>
QTBytestream& QTBytestream::operator>>(TTType& value)
{
   assert(m_data + sizeof(TTType) <= m_top);
   value = *reinterpret_cast<TTType*>(m_data);
   m_data += sizeof(TTType);
   return *this;
}

#endif // QTBytestream_c_HPP
