#!/usr/bin/env python
# -*- coding: utf-8 -*-
## cython: profile=True
# cython: linetrace=True
#************************************************************************
# --- Copyright (c) Yves Secretan 2022
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import enum
import logging
import os
import sys
import numpy as np
from scipy.sparse import linalg

if __name__ == "__main__":
    supPath = os.path.join(os.environ['INRS_DEV'], 'H2D2-tools', 'script')
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

#from . import FEMesh
from CTCommon import FEMesh

LOGGER = logging.getLogger("H2D2.Tools.Mesh.Matrix")

"""
Sparse CRS matrix
"""

ASSEMBLY = enum.Enum('ASSEMBLY', names='byElements byElementEdges')

def kimp2klocn(kimp, nnt, ndln=1):
    """
    kimp2klocn translate imposed nodes to dof indexes.

    Translate kimp, array of imposed nodes, to klocn,
    table de dof localisation.

    Args:
        kimp (array):   array of imposed nodes
        nnt  (int):     total number of nodes
        ndln (int):     number of dof per node

    Returns:
        np.array(nnt, ndln): table of dof indexes
    """
    ndlt = nnt * ndln
    klocn = np.linspace(0, ndlt, ndlt, endpoint=False, dtype=int).reshape(-1,ndln)
    klocn[kimp, 0:ndln] = -1
    return klocn

class SFEdge:
    __slots__ = 'no1', 'no2', 'info'
    def __init__(self, no1: int, no2: int, info = None):
        if no1 < no2:
            self.no1 = no1
            self.no2 = no2
        else:
            self.no1 = no2
            self.no2 = no1
        self.info = info

    def __eq__(self, other):
        if self.no1 != other.no1: return False
        if self.no2 != other.no2: return False
        return True

    def __hash__(self):
        return hash( (self.no1, self.no2) )

class FEMatrixCRS:
    JA_TAG = -123456

    def __init__(self, kind=ASSEMBLY.byElements):
        self.asmKind = kind
        self.ia = np.empty(0, dtype=int)
        self.ja = np.empty(0, dtype=int)
        self.va = np.empty(0, dtype=float)
        self.vd = 1.0       # diagonal precond

    def __addEdgeToIA(self, no0, no1, klocn):
        for idi in klocn[no0,:]:
            if idi < 0: continue
            for idj in klocn[no1,:]:
                if idj < 0: continue
                self.ia[idi] += 1
        for idi in klocn[no1,:]:
            if idi < 0: continue
            for idj in klocn[no0,:]:
                if idj < 0: continue
                self.ia[idi] += 1

    def __addEdgeToJA(self, no0, no1, klocn):
        # 0-1
        for idi in klocn[no0,:]:
            if idi < 0: continue
            jn = self.ia[idi+1]-1         # Use last slot as negative counter
            for idj in klocn[no1,:]:
                if idj < 0: continue
                ij = self.ia[idi] - self.ja[jn]
                #=======================================================
                # Begin DEBUG code
                assert self.ja[ij] == FEMatrixCRS.JA_TAG
                # End DEBUG code
                #=======================================================
                self.ja[ij] = idj
                self.ja[jn]-= 1
        # 1-0
        for idi in klocn[no1,:]:
            if idi < 0: continue
            jn = self.ia[idi+1]-1         # Use last slot as negative counter
            for idj in klocn[no0,:]:
                if idj < 0: continue
                ij = self.ia[idi] - self.ja[jn]
                #=======================================================
                # Begin DEBUG code
                assert self.ja[ij] == FEMatrixCRS.JA_TAG
                # End DEBUG code
                #=======================================================
                self.ja[ij] = idj
                self.ja[jn]-= 1

    def __fillIAFromEdgesT6L(self, mesh, klocn):
        if not isinstance(mesh.getElement(0), FEMesh.FEElementT6L): raise ValueError('only T6L element are supported')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if not np.any(klocn < 0): raise ValueError('klocn has no imposed dof')

        ndlt = klocn.size
        self.ia = np.ones(ndlt+1, dtype=int)    # (ones) for diag
        self.ia[-1] = 0
        # ---  Count
        sides = set()   # uniquer
        for e in mesh.iterElements():
            kne = e.getConnectivities()
            #
            s = SFEdge(kne[0], kne[2])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToIA(kne[0], kne[1], klocn)
                self.__addEdgeToIA(kne[1], kne[2], klocn)
                sides.add(s)
            #
            s = SFEdge(kne[2], kne[4])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToIA(kne[2], kne[3], klocn)
                self.__addEdgeToIA(kne[3], kne[4], klocn)
                sides.add(s)
            #
            s = SFEdge(kne[4], kne[0])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToIA(kne[4], kne[5], klocn)
                self.__addEdgeToIA(kne[5], kne[0], klocn)
                sides.add(s)
        # ---  Imposed dof have only diagonal entry
        kimp = (np.ravel(klocn) < 0).nonzero()
        self.ia[kimp] = 1
        # ---  Cumulate table
        self.ia[1:] = np.cumsum(self.ia[:-1])
        self.ia[0] = 0

    def __fillJAFromEdgesT6L(self, mesh, klocn):
        if not isinstance(mesh.getElement(0), FEMesh.FEElementT6L): raise ValueError('only T6L elements are supported')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if not np.any(klocn < 0): raise ValueError('klocn has no imposed dof')

        self.ja = np.zeros(self.ia[-1], dtype=int)
        #=======================================================
        # Begin DEBUG code
        self.ja[:] = FEMatrixCRS.JA_TAG
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            jn = self.ia[i+1]-1
            self.ja[jn] = 0
        # End DEBUG code
        #=======================================================
        # ---  Fill ja
        sides = set()
        for e in mesh.iterElements():
            kne = e.getConnectivities()
            #
            s = SFEdge(kne[0], kne[2])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToJA(kne[0], kne[1], klocn)
                self.__addEdgeToJA(kne[1], kne[2], klocn)
                sides.add(s)
            #
            s = SFEdge(kne[2], kne[4])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToJA(kne[2], kne[3], klocn)
                self.__addEdgeToJA(kne[3], kne[4], klocn)
                sides.add(s)
            #
            s = SFEdge(kne[4], kne[0])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToJA(kne[4], kne[5], klocn)
                self.__addEdgeToJA(kne[5], kne[0], klocn)
                sides.add(s)
        #=======================================================
        # Begin DEBUG code
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            j0 = self.ia[i]
            j1 = self.ia[i+1]
            assert np.all(self.ja[j0:j1] != FEMatrixCRS.JA_TAG)
            jn = j1-1
            assert(self.ja[jn] < 0 or
                   self.ja[jn] == 0 and j0 == jn)
        # End DEBUG code
        #=======================================================
        # ---  Add diagonal
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            jn = self.ia[i+1]-1
            self.ja[jn] = i
        # --- Sort for faster search
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            j0 = self.ia[i]
            j1 = self.ia[i+1]
            self.ja[j0:j1] = sorted(self.ja[j0:j1])

    def __fillIAFromElementsT3(self, mesh, klocn):
        if not isinstance(mesh.getElement(0), FEMesh.FEElementT3): raise ValueError('only T3 elements are supported')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if not np.any(klocn < 0): raise ValueError('klocn has no imposed dof')

        self.ia = np.ones(klocn.size+1, dtype=int)    # 1 for diag
        self.ia[-1] = 0
        # ---  Count
        sides = set()
        for e in mesh.iterElements():
            kne = e.getConnectivities()
            #
            s = SFEdge(kne[0], kne[1])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToIA(kne[0], kne[1], klocn)
                sides.add(s)
            #
            s = SFEdge(kne[1], kne[2])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToIA(kne[1], kne[2], klocn)
                sides.add(s)
            #
            s = SFEdge(kne[2], kne[0])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToIA(kne[2], kne[0], klocn)
                sides.add(s)
        # ---  Cumulate table
        self.ia[1:] = np.cumsum(self.ia[:-1])
        self.ia[0] = 0

    def __fillJAFromElementsT3(self, mesh, klocn):
        if not isinstance(mesh.getElement(0), FEMesh.FEElementT3): raise ValueError('only T3 elements are supported')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if not np.any(klocn < 0): raise ValueError('klocn has no imposed dof')

        self.ja = np.zeros(self.ia[-1], dtype=int)
        #=======================================================
        # Begin DEBUG code
        self.ja[:] = FEMatrixCRS.JA_TAG
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            jn = self.ia[i+1]-1
            self.ja[jn] = 0
        # End DEBUG code
        #=======================================================
        # ---  Fill off diagonal
        sides = set()
        for e in mesh.iterElements():
            kne = e.getConnectivities()
            #
            s = SFEdge(kne[0], kne[1])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToJA(kne[0], kne[1], klocn)
                sides.add(s)
            #
            s = SFEdge(kne[1], kne[2])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToJA(kne[1], kne[2], klocn)
                sides.add(s)
            #
            s = SFEdge(kne[2], kne[0])
            if s in sides:
                sides.discard(s)
            else:
                self.__addEdgeToJA(kne[2], kne[0], klocn)
                sides.add(s)
        #=======================================================
        # Begin DEBUG code
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            j0 = self.ia[i]
            j1 = self.ia[i+1]
            assert np.all(self.ja[j0:j1] != FEMatrixCRS.JA_TAG)
            jn = j1-1
            assert(self.ja[jn] < 0 or
                   self.ja[jn] == 0 and j0 == jn)
        # End DEBUG code
        #=======================================================
        # ---  Add diagonal
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            jn = self.ia[i+1]-1
            self.ja[jn] = i
        # --- Sort for faster search
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            j0 = self.ia[i]
            j1 = self.ia[i+1]
            self.ja[j0:j1] = sorted(self.ja[j0:j1])

    def __fillIAFromElementsL2(self, mesh, klocn):
        if not isinstance(mesh.getElement(0), FEMesh.FEElementL2): raise ValueError('only L2 elements are supported')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if not np.any(klocn < 0): raise ValueError('klocn has no imposed dof')

        self.ia = np.ones(klocn.size+1, dtype=int)    # 1 for diag
        self.ia[-1] = 0
        # ---  Count
        for e in mesh.iterElements():
            no0, no1 = e.getConnectivities()
            self.__addEdgeToIA(no0, no1, klocn)
        # ---  Cumulate table
        self.ia[1:] = np.cumsum(self.ia[:-1])
        self.ia[0] = 0

    def __fillJAFromElementsL2(self, mesh, klocn):
        if not isinstance(mesh.getElement(0), FEMesh.FEElementL2): raise ValueError('only L2 elements are supported')
        if klocn.ndim        != 2: raise ValueError('klocn shall be 2d')
        if klocn.shape[1]    != 1: raise ValueError('klocn shall be of shape (:,1), 1 dof per node')
        if mesh.getNbNodes() != klocn.shape[0] : raise ValueError('size mismatch between mesh and klocn')
        if not np.any(klocn < 0): raise ValueError('klocn has no imposed dof')

        self.ja = np.zeros(self.ia[-1], dtype=int)
        #=======================================================
        # Begin DEBUG code
        self.ja[:] = FEMatrixCRS.JA_TAG
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            jn = self.ia[i+1]-1
            self.ja[jn] = 0
        # End DEBUG code
        #=======================================================
        # ---  Fill off diagonal
        for e in mesh.iterElements():
            no0, no1 = e.getConnectivities()
            self.__addEdgeToJA(no0, no1, klocn)
        #=======================================================
        # Begin DEBUG code
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            j0 = self.ia[i]
            j1 = self.ia[i+1]
            assert np.all(self.ja[j0:j1] != FEMatrixCRS.JA_TAG)
            jn = j1-1
            assert(self.ja[jn] < 0 or
                   self.ja[jn] == 0 and j0 == jn)
        # End DEBUG code
        #=======================================================
        # ---  Add diagonal
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            jn = self.ia[i+1]-1
            self.ja[jn] = i
        # --- Sort for faster search
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            j0 = self.ia[i]
            j1 = self.ia[i+1]
            self.ja[j0:j1] = sorted(self.ja[j0:j1])

    def __dimMatrixFromEdges(self, mesh, klocn):
        if isinstance(mesh.getElement(0), FEMesh.FEElementL2):
            self.__fillIAFromElementsL2(mesh, klocn)
            self.__fillJAFromElementsL2(mesh, klocn)
        elif isinstance(mesh.getElement(0), FEMesh.FEElementT3):
            self.__fillIAFromElementsT3(mesh, klocn)
            self.__fillJAFromElementsT3(mesh, klocn)
        elif isinstance(mesh.getElement(0), FEMesh.FEElementT6L):
            self.__fillIAFromEdgesT6L(mesh, klocn)
            self.__fillJAFromEdgesT6L(mesh, klocn)
        else:
            raise ValueError('only L2, T3 and T6L elements are supported')

    def __dimMatrixFromElements(self, mesh, klocn):
        if isinstance(mesh.getElement(0), FEMesh.FEElementL2):
            self.__fillIAFromElementsL2(mesh, klocn)
            self.__fillJAFromElementsL2(mesh, klocn)
        elif isinstance(mesh.getElement(0), FEMesh.FEElementT3):
            self.__fillIAFromElementsT3(mesh, klocn)
            self.__fillJAFromElementsT3(mesh, klocn)
        else:
            raise ValueError('only L2 and T3 elements are supported')

    def getJAIdx(self, no0, no1):
        """
        getJAIdx retourne l'indice dans ja de (no0, no1)
        retourne -1 s'il n'existe pas
        """
        j0 = self.ia[no0]
        j1 = self.ia[no0+1]
        for ij in range(j0,j1):
            if self.ja[ij] == no1: return ij
        return -1

    def hasJAIdx(self, no0, no1):
        """
        hasJAIdx retourne True si (no0, no1) existe dans ja.
        """
        j0 = self.ia[no0]
        j1 = self.ia[no0+1]
        for i in range(j0,j1):
            if self.ja[i] == no1: True
        return False

    def dumpCol(self, ic):
        col = []
        ndlt = self.ia.size - 1
        for i in range(ndlt):
            ij = self.getJAIdx(i, ic)
            if ij < 0: continue
            col.append(self.va[ij])
        return np.array(col)

    def asmKe(self, vke, kloce):
        # cython error:
        #   Cannot convert 'npy_intp *' to Python object
        # assert np.all(np.array(vke.shape) == kloce.size)

        ndle = len(kloce)
        for i in range(ndle):
            idi = kloce[i]
            if idi < 0: continue
            for j in range(ndle):
                idj = kloce[j]
                if idj < 0: continue
                ij = self.getJAIdx(idi, idj)
                if ij < 0: continue
                self.va[ij] += vke[i,j]

    def dimMatrix(self, mesh, klocn):
        if self.asmKind == ASSEMBLY.byElements:
            self.__dimMatrixFromElements(mesh, klocn)
        else:
            self.__dimMatrixFromEdges(mesh, klocn)
        self.va = np.zeros(self.ia[-1], dtype=np.float64)

    def matvec(self, vrhs):
        vmul = np.zeros_like(vrhs)
        ndlt = len(self.ia) - 1
        for i in range(ndlt):
            j0 = self.ia[i]
            j1 = self.ia[i+1]
            vmul[i] = np.dot(self.va[j0:j1], vrhs[self.ja[j0:j1]])
        return vmul

    def genPrecondIdent(self):
        self.vd = 1.0

    def genPrecondKDiag(self):
        ndlt = len(self.ia) - 1
        self.vd = np.zeros(ndlt, dtype=np.float64)
        for i in range(ndlt):
            jj = self.getJAIdx(i, i)
            self.vd[i] = self.va[jj]
        self.vd = 1.0 / self.vd

    def genPrecondKtDiag(self):
        ndlt = len(self.ia) - 1
        self.vd = np.zeros(ndlt, dtype=np.float64)
        for i in range(ndlt):
            j0 = self.ia[i]
            j1 = self.ia[i+1]
            self.vd[i] = np.sum( np.fabs(self.va[j0:j1]) )
        self.vd = 2.0 / self.vd

    def precond(self, vrhs):
        return self.vd * vrhs

    def solveFixPointDiag(self, b, x, tol_rel=1.0e-2, tol_abs=1.0e-6, nitermax=-1):
        LOGGER.info('Iterative refinement with fixed point (tol=%e, niter=%d)' % (tol_rel, nitermax))
        if nitermax < 0: nitermax = len(b)
        d = np.zeros_like(x)        # deltas
        r = b - self.matvec(x)      # résidu initial
        tolrel = r * tol_rel
        for it in range(nitermax):
            r -= self.matvec(d)     # contrib delta
            d = self.precond(r)     # solve
            x -= d                  # update
            nrm = np.amax(d)
            if nrm < tolrel or nrm < tol_abs:
                break
            LOGGER.info("%3d: %.3e %.3e" % (it, nrm, nrm/tolrel))
        return x

    def solveCG(self, b, x, tol_rel=1.0e-2, tol_abs=1.0e-6, nitermax=-1):
        """
        Adapted from:
        https://gist.github.com/glederrey/7fe6e03bbc85c81ed60f3585eea2e073

        A function to solve [A]{x} = {b} linear equation system with the
        conjugate gradient method.
        More at: http://en.wikipedia.org/wiki/Conjugate_gradient_method
        ========== Parameters ==========
        A : matrix
            A real symmetric positive definite matrix.
        b : vector
            The right hand side (RHS) vector of the system.
        x : vector
            The starting guess for the solution.
        """
        LOGGER.info('Iterative refinement with CG (tol=%e, niter=%d)' % (tol_rel, nitermax))
        if nitermax < 0: nitermax = len(b)
        r = b - self.matvec(x)
        z = self.vd * r
        p = z
        rsold  = np.dot(np.transpose(r), z)
        rssqr  = np.sqrt(rsold)
        tolrel = tol_rel * rssqr
        tolabs = tol_abs
        LOGGER.info("%3d: %.3e %.3e" % (-1, rssqr, rssqr/tolrel*tol_rel))
        if rssqr < tolrel or rssqr < tolabs:
            return x

        for it in range(nitermax):
            Ap = self.matvec(p)
            alpha = rsold / np.dot(np.transpose(p), Ap)
            x += alpha * p
            r -= alpha * Ap
            z = self.vd * r
            rsnew = np.dot(np.transpose(r), z)
            rssqr = np.sqrt(rsnew)
            LOGGER.info("%3d: %.3e %.3e" % (it, rssqr, rssqr/tolrel*tol_rel))
            if rssqr < tolrel or rssqr < tolabs:
                break
            p = z + (rsnew/rsold)*p
            rsold = rsnew
        return x

    def solveScipyGMRes(self, b, x, tol_rel=1.0e-2, tol_abs=1.0e-6, nitermax=-1):
        """
        solveScipyGMRes _summary_

        Use Generalized Minimal RESidual iteration to solve Ax = b
        _extended_summary_

        Args:
            b (np.array): Right hand side vector
            x (np.array): Initial solution
            tolerance (float, optional): _description_. Defaults to 1.0e-2.
            nitermax (int, optional): _description_. Defaults to -1.
        """
        class gmres_cb:
            def __init__(self):
                self.nrm0 = 1.0e99
                self.iter = 0
            def __call__(self, nrm):
                if self.iter == 0:
                    self.nrm0 = nrm
                self.iter += 1
                LOGGER.info('%3d: %.3e (%.3e)' % (self.iter, nrm, nrm/self.nrm0))
        #
        LOGGER.info('Iterative refinement with GMRes (tol=%e, niter=%d)' % (tol_rel, nitermax))
        #
        nnt = len(b)
        cb  = gmres_cb()
        A = linalg.LinearOperator((nnt, nnt), matvec=self.matvec)
        P = linalg.LinearOperator((nnt, nnt), matvec=self.precond)
        x, exitCode = linalg.gmres(A, b, x0=x, M=P,
                                   tol=tol_rel*cb.nrm0,
                                   atol=tol_abs,
                                   maxiter=nitermax if nitermax > 0 else None,
                                   callback=cb, callback_type='pr_norm')
        LOGGER.debug('Exit code: %s', exitCode)
        return x

if __name__ == "__main__":
    from CTCommon import FEMesh

    def main(f_cor, f_ele):
        LOGGER.info('Stream function: Start')

        LOGGER.debug('read grid')
        meshT6 = FEMesh.FEMesh([f_cor, f_ele])
        meshT3 = meshT6.genT3Grid()
        LOGGER.debug('read grid done')

        kimp = [0]
        klocn = kimp2klocn(kimp, meshT3.getNbNodes())
        mx = FEMatrixCRS(kind=ASSEMBLY.byElements)
        mx.dimMatrix(meshT3, klocn)

    import cProfile
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    d_grd = 'E:/Projets/ECCC/OP/Débit Sortant/20x5'
    f_cor = os.path.join(d_grd, 'test.cor')
    f_ele = os.path.join(d_grd, 'test.ele')
    #cProfile.run('main(f_cor, f_ele)', sort='tottime')
    main(f_cor, f_ele)
