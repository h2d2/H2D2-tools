//************************************************************************
// --- Copyright (c) Yves Secretan 2021
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Classe:
//    QTAllocator
//
// Description:
//
// Attributs:
//
// Notes:
// http://dmitrysoshnikov.com/compilers/writing-a-pool-allocator/
//************************************************************************
#ifndef QTAllocator_c_H
#define QTAllocator_c_H

#include <cstddef>      // size_t

struct Chunk {
   /**
    * When a chunk is free, the `next` contains the
    * address of the next chunk in a list.
    *
    * When it's allocated, this space is used by
    * the user.
    */
   Chunk* next;
};

/**
 * The allocator class.
 *
 * Features:
 *
 *   - Parametrized by number of chunks per block
 *   - Keeps track of the allocation pointer
 *   - Bump-allocates chunks
 *   - Requests a new larger block when needed
 *
 */
class QTPoolAllocator {
public:
   QTPoolAllocator()
      : mChunksPerBlock(0), mChunksOnResize(0) {}
   QTPoolAllocator(size_t chunksPerBlock)
      : mChunksPerBlock(chunksPerBlock), mChunksOnResize(0) {}

   void  resize      (size_t);      // Set number of chunks per block

   void* allocate    (size_t size);
   void  deallocate  (void* ptr, size_t size);

private:
   size_t mChunksPerBlock;          // Number of chunks per block.
   size_t mChunksOnResize;          // Number of chunks left when resizing.
   Chunk* mAlloc = nullptr;         // Allocation pointer.

   Chunk* allocateBlock(size_t);    // Allocates a block(pool) for chunks
};

#endif // QTAllocator_c_H
