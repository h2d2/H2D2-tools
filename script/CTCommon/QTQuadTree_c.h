//************************************************************************
// --- Copyright (c) Yves Secretan 2019
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Classe:
//    QTItem
//    QTNode
//    QTTree
//
// Description:
// 
// Attributs:
//
// Notes:
//************************************************************************
#ifndef QTQuadTree_c_H
#define QTQuadTree_c_H

#include <array>
#include <set>
#include <vector>

#include "QTAllocator_c.h"

// ---  Forward declaration
class QTByteStream;

// ---  typedef's used
typedef long TTInfo;
typedef std::array<double, 4> TTBBox;
typedef std::set<TTInfo>      TTUniquer;
typedef std::vector<TTInfo>   TTResults;

//************************************************************************
// Classe: QTItem
//
// Description:
//    User information item with AABB bbox.
//
// Attributs:
//    m_bbox      Item bbox
//    m_item      Item ID
//
// Notes:
//************************************************************************
class QTItem
{
public:
   TTBBox m_bbox;
   TTInfo m_item;

   QTItem();
   QTItem(const TTInfo& item, const TTBBox& bbox);
   bool operator == (const QTItem& other) const;
   bool operator != (const QTItem& other) const;
};

//************************************************************************
// Classe: QTNode
//
// Description:
//    User information item with AABB bbox.
//
// Attributs:
//    m_bbox      Item bbox
//    m_item      Item ID
//
// Notes:
   /*
   Internal backend version of the index.
   The index being used behind the scenes. Has all the same methods as the user
   index, but requires more technical arguments when initiating it than the
   user-friendly version.
   */
   //************************************************************************
class QTNode
{
private:
   typedef std::array<QTNode*, 4> TTChilds;
   typedef std::array<double, 2>  TTArray;
   typedef std::vector<QTItem>    TTItems;

   static QTPoolAllocator allocator;

   TTItems  m_items;
   TTChilds m_childs;
   TTArray  m_center;
   TTArray  m_span;
   long max_items;
   long max_depth;
   long depth;

   void insert_into_children(const QTItem& item);
   void remove_from_children(const QTItem& item);
   void split();

public:
   QTNode();
   QTNode(double x, double y, double width, double height, long max_items, long max_depth, long depth);
   ~QTNode();

   static void  resizeAllocator  (size_t);
   static void* operator new     (size_t);
   static void  operator delete  (void*, size_t);

   bool operator == (const QTNode& other) const;
   bool operator != (const QTNode& other) const;

   void    insert      (const QTItem& item);
   void    remove      (const QTItem& item);
   void    intersect   (const TTBBox& rect, TTResults& results, TTUniquer& uniq) const;

   size_t  size        () const;
   size_t  maxItemsEff () const;
   size_t  maxDepthEff () const;
   size_t  maxItemsLmt () const;
   size_t  maxDepthLmt () const;
   size_t  countNodes  () const;
   size_t  countItems  () const;

   friend class QTBytestream;
};

class QTTree
{
private:
   TTBBox  m_bbox;
   QTNode *m_headP;

public:
   static const long MAX_ITEMS = 10;
   static const long MAX_DEPTH = 20;

   QTTree();
   ~QTTree();

   void init(double bbox[4]);
   void init(double bbox[4], long max_items, long max_depth);

   bool operator == (const QTTree& other) const;
   bool operator != (const QTTree& other) const;

   void      insert     (TTInfo i, double bbox[4]);
   void      remove     (TTInfo i, double bbox[4]);
   TTResults intersect  (double bbox[4]) const;

   const double*   bbox () const;
   size_t    size       () const;
   size_t    maxItemsEff() const;
   size_t    maxDepthEff() const;
   size_t    maxItemsLmt() const;
   size_t    maxDepthLmt() const;
   size_t    countNodes() const;
   size_t    countItems() const;

   friend class QTBytestream;
};

#include "QTQuadTree_c.hpp"

#endif // QTQuadTree_c_H
