=======================================
DEFINITION
=======================================

enum.py
==============
VIDE

enum.pxd
===============
import cython
cpdef enum ELEMENT_QUALITY_METRICS:
   area = 1

enum_pp.py  (_pp for pure python)
==========
import enum
ELEMENT_QUALITY_METRICS = enum.Enum('ELEMENT_QUALITY_METRICS', ('area'))

enum.pxd
========
PAS UTILISÉ

La partie définition fonctionne correctement. Dans Python, on peut faire un
   from enum_cython import ELEMENT_QUALITY_METRICS
et on obtient bien une enum Python.


=======================================
UTILISATION 
=======================================

Dans le code py
===============
Il faut ABSOLUMENT faire un import AS sinon on a une erreur de compil

try:
    from .FEMeshMetricKind import ELEMENT_QUALITY_METRICS as EQM
except ImportError:
    from .FEMeshMetricKind_pp import ELEMENT_QUALITY_METRICS as EQM

ou plus simplement (si l'enum est toujours dispo sous forme compilée)
    from .FEMeshMetricKind import ELEMENT_QUALITY_METRICS as EQM
    
    
Dans le code pxd
================
from .FEMeshMetricKind import ELEMENT_QUALITY_METRICS as EQM_PXD



ATTENTION
   # La comparaison directe des item de l'enum ne retourne pas toujours
   # le bon résultat. La comparaison sur valeur est stable. Sauf que ...
   # Cython passe des int!
