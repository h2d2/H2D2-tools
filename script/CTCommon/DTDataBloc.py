#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import numbers

class DTDataBloc:
    """
    H2D2 data file bloc information.
    """

    def __init__(self, reader, path, pos, data, nrow, ncol, time, vmin, vmax):
        self.reader = reader
        self.path = path
        self.data = data
        self.pos  = pos
        self.nrow = nrow
        self.ncol = ncol
        self.time = time
        self.vmin = vmin
        self.vmax = vmax

    def __lt__(self, other):
        if isinstance(other, numbers.Number):
            return self.time < other
        return self.time < other.time

    def __eq__(self, other):
        if isinstance(other, numbers.Number):
            return self.time == other
        return self.time == other.time
