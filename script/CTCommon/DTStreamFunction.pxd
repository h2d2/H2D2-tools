# -*- coding: utf-8 -*-

# This is an automatically generated file.
# Manual changes will be merged and conflicts marked!
#
# Generated by py2pxd_ version 0.0.3 on 2022-12-21 08:54:31

import cython
cimport numpy as np

from .FEMesh cimport FENode, FEElementL2, FEElementT3, FEElementT6L, FEMesh
from .FEMatrixCRS cimport SFEdge

cdef class StreamFunction:
    cdef public object      kindAssembly
    cdef public object      kindPrecond
    cdef public object      kindSolver
    cdef public object      matrix
    cdef public object      mesh
    cdef public object      meshL2
    #
    cpdef FEMesh       getMeshForInit  (StreamFunction self, FEMesh mesh)
    cpdef FEMesh       getMeshForDimMatrix(StreamFunction self, FEMesh mesh)
    cpdef FEMesh       getMeshForAsmK  (StreamFunction self, FEMesh mesh)
    cpdef FEMesh       getMeshForAsmF  (StreamFunction self, FEMesh mesh)
    @cython.locals (id = long)
    cpdef np.ndarray   asmF_L2         (StreamFunction self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    cpdef np.ndarray   asmF_T3         (StreamFunction self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    cpdef np.ndarray   asmF_T6L        (StreamFunction self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @cython.locals (e0 = object)
    cpdef np.ndarray   asmF            (StreamFunction self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    cpdef              asmK_L2         (StreamFunction self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    cpdef              asmK_T3         (StreamFunction self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    cpdef              asmK_T6L        (StreamFunction self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    @cython.locals (e0 = object)
    cpdef object       asmK            (StreamFunction self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    @cython.locals (dones = set, e = FEElementL2, kimp = np.ndarray, no = long, no0 = long, no1 = long, todos = set, vdlg = np.ndarray)
    cdef np.ndarray    __init_L2       (StreamFunction self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @staticmethod
    @cython.locals (l2 = FEElementL2, qs = double)
    cdef               __init_T3__addOneSide(StreamFunction dummy, np.ndarray[double, ndim=1] vdlg, np.ndarray[double, ndim=2] data, long no0, long no1, tuple kne, tuple nds)
    @cython.locals (dones = set, e = FEElementT3, kimp = np.ndarray, kne = tuple, nds = tuple, no = long, todos = set, vdlg = np.ndarray)
    cdef np.ndarray    __init_T3       (StreamFunction self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @staticmethod
    @cython.locals (l2 = FEElementL2, qs = double)
    cdef               __init_T6L__addOneSide(StreamFunction dummy, np.ndarray[double, ndim=1] vdlg, np.ndarray[double, ndim=2] data, long no0, long no1, long no2, tuple kne, tuple nds)
    @cython.locals (dones = set, e = FEElementT6L, kimp = np.ndarray, kne = tuple, nds = tuple, no = long, todos = set, vdlg = np.ndarray)
    cdef np.ndarray    __init_T6L      (StreamFunction self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @cython.locals (e0 = object)
    cpdef np.ndarray   init            (StreamFunction self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @cython.locals (ele0 = object, ino0 = long, klocn = np.ndarray, nod0 = FENode, vdlg = np.ndarray, vrhs = np.ndarray, vsol = np.ndarray)
    cpdef np.ndarray   computeStreamFunction(StreamFunction self, FEMesh mesh, np.ndarray[double, ndim=2] data, object kimp=*, long nitermax=*, double tol_rel=*, double tol_abs=*)

cdef class StreamFunctionLS(StreamFunction):
    #
    cpdef FEMesh       getMeshForDimMatrix(StreamFunctionLS self, FEMesh mesh)
    cpdef FEMesh       getMeshForAsmF  (StreamFunctionLS self, FEMesh mesh)
    @cython.locals (e = object, kimp = np.ndarray, ndlt = long, no0 = long, no1 = long, qs = double, vfg = np.ndarray)
    cpdef np.ndarray   asmF_L2         (StreamFunctionLS self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @cython.locals (e = object, kimp = np.ndarray, kne = tuple, l2 = FEElementL2, ndlt = long, nds = tuple, qs = double, s = SFEdge, sides = set, vfg = np.ndarray)
    cpdef np.ndarray   asmF_T3         (StreamFunctionLS self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @cython.locals (e = object, kimp = np.ndarray, kne = tuple, l2 = FEElementL2, ndlt = long, nds = tuple, qs = double, s = SFEdge, sides = set, vfg = np.ndarray)
    cpdef np.ndarray   asmF_T6L        (StreamFunctionLS self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @cython.locals (e = object, i = object, j = object, j0 = object, j1 = object, jj = object, kimp = object, kne = object, mtx = object, ndlt = object, s = object, sides = set, vke = object)
    cpdef              asmK_slow       (StreamFunctionLS self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    @cython.locals (i = object, j = object, j0 = object, j1 = object, jj = object, kimp = object, mtx = object, nnt = object)
    cpdef              asmK_fast       (StreamFunctionLS self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    cpdef              asmK_T3         (StreamFunctionLS self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    cpdef              asmK_T6L        (StreamFunctionLS self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)

cdef class StreamFunctionPDE(StreamFunction):
    #
    cpdef FEMesh       getMeshForDimMatrix(StreamFunctionPDE self, FEMesh mesh)
    @cython.locals (e = object, kimp = np.ndarray, kne = tuple, l2 = FEElementL2, ndlt = object, nds = tuple, qs = object, s = object, side = object, sides = set, vfg = np.ndarray)
    cpdef np.ndarray   asmF_T3         (StreamFunctionPDE self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @cython.locals (e = object, kimp = np.ndarray, kne = tuple, l2 = FEElementL2, ndlt = object, nds = tuple, qs = object, s = object, side = object, sides = set, vfg = np.ndarray)
    cpdef np.ndarray   asmF_T6L        (StreamFunctionPDE self, FEMesh mesh, np.ndarray[double, ndim=2] data, np.ndarray[long, ndim=2] klocn)
    @cython.locals (c = object, ddx = object, ddy = object, e = object, i = object, j = object, jj = object, kimp = object, kne = object, mtx = object, vke = object)
    cpdef              asmK_T3_slow    (StreamFunctionPDE self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    @cython.locals (c = object, ddx = object, ddy = object, diag = tuple, e = object, i = object, j = object, jj = object, kimp = object, kne = object, mtx = object, vke = object)
    cpdef              asmK_T6L_slow   (StreamFunctionPDE self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    cpdef              asmK_T3         (StreamFunctionPDE self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)
    cpdef              asmK_T6L        (StreamFunctionPDE self, FEMesh mesh, np.ndarray[long, ndim=2] klocn)

@cython.locals (sf = object)
cpdef object       computeStreamFunction(FEMesh mesh, object qxqy, object kimp=*, long nitermax=*, double tol_rel=*, double tol_abs=*, object formulation=*)

@cython.locals (data = object, fc = object, meshT3 = object, meshT6L = object, qxqy = object, vsol = object)
cpdef              main            (object f_out, object f_cor, object f_ele, object f_vno, object cols)

