#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Data path error codes
"""

import enum

ERR_CODE = enum.Enum('ERR_CODE',
    ('ERR_OK',
     'ERR_OUT_OF_ITER',
     'ERR_OUT_OF_TIME',
     'ERR_ALL_ITER_OUTSIDE',
     'ERR_POINT_OUTSIDE',
     'ERR_END_OF_PATH',
     'ERR_BROKEN_NEIGHBOUR_LIST',
    ) )
