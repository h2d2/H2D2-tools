#!/usr/bin/env python
# -*- coding: utf-8 -*-
## cython: profile=True
# cython: linetrace=True
#************************************************************************
# --- Copyright (c) INRS 2012-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Unstructured triangular finite element mesh.
"""

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)
    from CTCommon import FEMeshIO

import logging
import sys
import numpy as np

from CTCommon.FEMesh import FEMesh, FEElementT3, FEElementT6L

LOGGER = logging.getLogger("H2D2.Tools.boundary")

class FEBoundary:
    def __init__(self, name='', parentMesh=None, parentSkin=None, parentMeshConnec=None, parentSkinConnec=None):
        self.name  = name
        self.pmesh = parentMesh
        self.pskin = parentSkin
        self.gr_con= parentMeshConnec
        self.sk_con= parentSkinConnec
        self.mesh  = None
        self.sngls = []
        self.sgmts = []    # Tuple of element index (start, end) of segment

    #@profile
    def __filterElements(self, nodes):
        LOGGER.debug('Filter elements for %s', self.name)
        # ---  Extract side connectivities
        lm0 = self.pmesh.getElement(0)
        if isinstance(lm0, FEElementT3):
            knSides = [(0, 1), (1, 2), (2, 0)]
        elif isinstance(lm0, FEElementT6L):
            knSides = [(0, 1, 2), None, (2, 3, 4), None, (4, 5, 0)]
        else:
            raise RuntimeError('Unsupported type of element: %s', type(lm0))
        # ---  Filter
        eles = []
        snds = set()    # segment nodes
        segs = []
        ichn = -1
        for ie, kne in enumerate(self.sk_con):
            if kne[ 0] not in nodes: continue
            if kne[-1] not in nodes: continue

            eles.append(self.sk_con[ie])

            if kne[0] != ichn:
                segs.append(len(eles)-1)
            ichn = kne[1]
            # ---  Get nodes of skin element
            #      Go back to parent mesh as skin is always L2
            ele0 = np.where(self.gr_con == kne[ 0])[0]   # Elements with first node
            ele1 = np.where(self.gr_con == kne[-1])[0]   # Elements with last node
            #assert np.intersect1d(ele0, ele1).shape == (1,)
            for ie0 in np.intersect1d(ele0, ele1):
                ik = np.where(self.gr_con[ie0] == kne[0])[0][0]
                tt = [ self.gr_con[ie0][i] for i in knSides[ik] ]    # no tuple comprehension for cython
                snds.update(tt)
        if segs: segs.append(len(eles))

        self.sgmts = [ seg for seg in zip(segs[0:], segs[1:]) ] if segs else []
        self.sngls = list( set(nodes).difference(snds) )

        # ---  Controls
        outs = [ n for n in self.sngls if n not in self.sk_con ]
        if outs:
            raise RuntimeWarning('Boundary "%s" has nodes not on skin: %s' % (self.name, list(outs)))

        return eles

    def genMesh(self, nodes):
        # ---  Controls
        dups = set()
        dups = [n for n in nodes if n in dups or dups.add(n)]
        if dups:
            raise RuntimeError('Boundary "%s" has duplicated nodes: %s' % (self.name, list(dups)))
        # ---  Process
        connec = []
        try:
            connec = self.__filterElements(nodes)
            self.mesh = self.pmesh.genCompactGrid(nodes=nodes, connec=connec)
        except RuntimeWarning:
            self.mesh = self.pmesh.genCompactGrid(nodes=nodes, connec=connec)
            raise

    def getName(self):
        return self.name

    def getMesh(self):
        return self.mesh

    def getSegment(self, iseg):
        return self.sgmts[iseg]

    def getNbrSegments(self):
        return len(self.sgmts)

    def getNbrSingularNodes(self):
        return len(self.sngls)

    def getSingularNodes(self):
        return self.sngls

    def __repr__(self):
        msg = ['Boundary:',
               '   name: %s' % self.getName(),
               '   nbrNodes:        %6d' % self.mesh.getNbNodes(),
               '   nbrElements:     %6d' % self.mesh.getNbElements(),
               '   nbrSegments:     %6d' % len(self.sgmts),
               '   nbrSingularNodes:%6d' % len(self.sngls),
        ]
        return '\n'.join(msg)

class FEBoundaries:
    def __init__(self, mesh):
        self.gr_con = None
        self.sk_con = None
        self.mesh = mesh
        self.skin = mesh.genSkin(doChain=True)
        self.file = ''
        self.bnds = []

    def __xtrConnec(self):
        gr_ele = self.mesh.getElements()
        gr_con = np.array( [[n.getGlobalIndex() for n in e.getNodes()] for e in gr_ele] )
        sk_ele = self.skin.getElements()
        sk_con = np.array( [[n.getGlobalIndex() for n in e.getNodes()] for e in sk_ele] )
        self.gr_con = gr_con
        self.sk_con = sk_con

    def __readBnds(self, file):
        bndNodes = { }
        with open(file, 'rt') as ifs:
            nlines = int( next(ifs).strip() )
            for il in range(nlines):
                l = next(ifs).strip()
                tks = l.split()
                name = tks[0]
                nods = [ (int(tk)-1) for tk in tks[1:] ]
                if name not in bndNodes:
                    bndNodes[name] = []
                bndNodes[name].extend(nods)
        bnds = {}
        nodMax = self.mesh.getNbNodes() if self.mesh else sys.maxsize
        errMsg = []
        wrnMsg = []
        for name, nods in bndNodes.items():
            inError = False
            if not nods:
                errMsg.append('Boundary "%s" has zero nodes' % name)
                inError = True
            if min(nods) < 0:
                ns = [ n for n in nods if n < 0 ]
                if len(ns) < 10:
                    nstr = ', '.join(['%d' % (n+1) for n in ns])
                else:
                    nstr = ', '.join(['%d' % (n+1) for n in ns[:10]]) + ' ...'
                errMsg.append('boundary "%s" has invalid nodes: %s' % (name, nstr))
                inError = True
            if max(nods) >= nodMax:
                ns = [ n for n in nods if n >= nodMax ]
                if len(ns) < 10:
                    nstr = ', '.join(['%d' % (n+1) for n in ns])
                else:
                    nstr = ', '.join(['%d' % (n+1) for n in ns[:10]]) + ' ...'
                errMsg.append('Boundary "%s" has invalid nodes. %d is out of parent mesh.' % (name, max(nods)+1))
                inError = True
            if not inError:
                bnd = None
                try:
                    bnd = FEBoundary(name, parentMesh=self.mesh, parentSkin=self.skin, parentMeshConnec=self.gr_con, parentSkinConnec=self.sk_con)
                    bnd.genMesh(nods)
                    bnds[name] = bnd
                except RuntimeWarning as e:
                    if bnd: bnds[name] = bnd
                    if len(wrnMsg) < 10:
                        wrnMsg.append(str(e))
                    elif len(wrnMsg) == 10:
                        wrnMsg.append('...')
                except RuntimeError as e:
                    if len(errMsg) < 10:
                        wrnMsg.append(str(e))
                    elif len(errMsg) == 10:
                        errMsg.append('...')
                    else:
                        break

        self.bnds = list(bnds.values())
        if errMsg:
            raise RuntimeError('\n'.join(errMsg) + '\n'.join(wrnMsg))
        if wrnMsg:
            raise RuntimeWarning('\n'.join(wrnMsg))

    def readBoundaries(self, file):
        self.file = ''
        self.__xtrConnec()
        self.__readBnds(file)

    def getFile(self):
        return self.file

    def getBaseMesh(self):
        return self.mesh

    def getBoundary(self, ib):
        return self.bnds[ib]

    def getNbBoundaries(self):
        return len(self.bnds)

    def iterBoundaries(self):
        for item in self.bnds:
            yield item

if __name__ == "__main__":
    from CTCommon.FEMesh   import FEMesh, FEElementT3
    from CTCommon import FEMeshIO

    def t_main():
        # d = 'E:/Projets/Carillon/MiseEau_H2D2/100_production_V02_ML/200_grid/'
        d = 'E:/Projets/Carillon/MiseEau_H2D2/100_production/200_grid/'
        fcor = 'car_amont_V01.cor'
        fele = 'car_amont_V01.ele'
        reader = FEMeshIO.FEMeshH2D2T6LIO( [d+fcor, d+fele],)
        mT6L = FEMesh()
        reader.readMesh(mT6L)

        #fbnd = 'car_amont_V01.bnd'
        fbnd = 'car_amont.bnd'
        bnds = FEBoundaries(mesh=mT6L)
        bnds.readBoundaries(file=d+fbnd)

        for bnd in bnds.iterBoundaries():
             print(str(bnd))


    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)
    t_main()
