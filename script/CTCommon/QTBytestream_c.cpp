//************************************************************************
// --- Copyright (c) Yves Secretan 2021
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Classe:
//
// Description:
// 
// Attributs:
//
// Notes:
//************************************************************************

#include "QTBytestream_c.h"
#include "QTQuadTree_c.h"

#include <cstring>
#include <stdexcept>
#include <iostream>

#ifdef QTBYTESTREAM_DEBUG
#  include <chrono>
#  include <thread>
#  include <iostream>
#endif

const QTBytestream::guid_t QTBytestream::GUID = "3314b683-3421-4b7d-99ef-e6c19703b36e";

void debugWait()
{
#ifdef QTBYTESTREAM_DEBUG
   std::cout << "Waiting" << std::endl;
   bool done = true;
   do
   {
      std::this_thread::sleep_for(std::chrono::seconds(15));
   } while (!done);
   std::cout << "End waiting" << std::endl;
#endif
}

/*
* Default constructor, for output 
*/
QTBytestream::QTBytestream()
   : m_base(nullptr)
   , m_data(nullptr)
   , m_top (nullptr)
   , m_size(0)
{}

/*
* Constructor with buffer, for input
*/
QTBytestream::QTBytestream(uint8_t* buffer, size_t sz)
   : m_base(nullptr)
   , m_data(buffer)
   , m_top (buffer + sz)
   , m_size(sz)
{}

/*
* Copy constructor and operator= are required to manage m_base
*/
QTBytestream::QTBytestream(const QTBytestream& other)
   : m_base(nullptr)
   , m_data(other.m_data)
   , m_top (other.m_top)
   , m_size(other.m_size)
{
}

/*
* Destructor
*/
QTBytestream::~QTBytestream()
{
   delete m_base;
   m_base = nullptr;
}

QTBytestream& QTBytestream::operator = (const QTBytestream& other)
{
   // Guard self assignment
   if (this == &other)
      return *this;

   delete m_base;
   m_base = nullptr;
   m_data = other.m_data;
   m_top = other.m_top;
   m_size = other.m_size;
   return *this;
}

void QTBytestream::grow()
{
   size_t old_ct = (m_data - m_base);
   size_t new_sz = std::max(m_size*2, size_t(1*1024*1024));
   
   uint8_t* m_new = new uint8_t[new_sz];
   std::memcpy(m_new, m_base, old_ct);
   delete m_base;

   m_base = m_new;
   m_data = m_base + old_ct;
   m_top  = m_base + new_sz;
   m_size = new_sz;
}

QTBytestream& QTBytestream::operator<< (const QTBytestream::guid_t& item)
{
   QTBytestream& os = *this;
   for (size_t i = 0; i < GUID_LEN; ++i)
      os << item[i];
   return os;
}

QTBytestream& QTBytestream::operator>> (QTBytestream::guid_t& item)
{
   QTBytestream& is = *this;
   for (size_t i = 0; i < GUID_LEN; ++i)
      is >> item[i];
   return is;
}

QTBytestream& QTBytestream::operator<< (const QTItem& item)
{
   QTBytestream& os = *this;
   os << item.m_bbox[0] << item.m_bbox[1] << item.m_bbox[2] << item.m_bbox[3];
   os << item.m_item;
   return os;
}

QTBytestream& QTBytestream::operator>> (QTItem& item)
{
   QTBytestream& is = *this;
   is >> item.m_bbox[0] >> item.m_bbox[1] >> item.m_bbox[2] >> item.m_bbox[3];
   is >> item.m_item;
   return is;
}

QTBytestream& QTBytestream::operator<< (const QTNode& node)
{
   QTBytestream& os = *this;
   os << node.max_depth << node.max_items << node.depth;
   os << node.m_span[0] << node.m_span[1];
   os << node.m_center[0] << node.m_center[1];

   os << node.m_childs[0]; if (node.m_childs[0]) os << *node.m_childs[0];
   os << node.m_childs[1]; if (node.m_childs[1]) os << *node.m_childs[1];
   os << node.m_childs[2]; if (node.m_childs[2]) os << *node.m_childs[2];
   os << node.m_childs[3]; if (node.m_childs[3]) os << *node.m_childs[3];

   os << node.m_items.size();
   for (auto iI = node.m_items.begin(); iI != node.m_items.end(); ++iI)
      os << *iI;

   return os;
}

QTBytestream& QTBytestream::operator>> (QTNode& node)
{
   QTBytestream& is = *this;
   is >> node.max_depth >> node.max_items >> node.depth;
   is >> node.m_span[0] >> node.m_span[1];
   is >> node.m_center[0] >> node.m_center[1];

   {
      QTNode *nP;
      is >> nP; if (nP) { nP = new QTNode(); is >> *nP;} node.m_childs[0] = nP;
      is >> nP; if (nP) { nP = new QTNode(); is >> *nP;} node.m_childs[1] = nP;
      is >> nP; if (nP) { nP = new QTNode(); is >> *nP;} node.m_childs[2] = nP;
      is >> nP; if (nP) { nP = new QTNode(); is >> *nP;} node.m_childs[3] = nP;
   }

   {
      QTNode::TTItems::size_type  sz;
      QTNode::TTItems::value_type it;
      is >> sz; node.m_items.reserve(sz);
      for (QTNode::TTItems::size_type i = 0 ; i < sz; ++i)
      {
         is >> it;
         node.m_items.push_back(it);
      }
   }

   return is;
}

QTBytestream& QTBytestream::operator<< (const QTTree& tree)
{
   QTBytestream& os = *this;
   
   os << QTBytestream::GUID;
   os << tree.countNodes() << tree.countItems();
   os << tree.m_bbox[0] << tree.m_bbox[1] << tree.m_bbox[2] << tree.m_bbox[3];
   os << tree.m_headP; if (tree.m_headP) os << *tree.m_headP;

   return os;
}

QTBytestream& QTBytestream::operator>> (QTTree& tree)
{
   QTBytestream& is = *this;

   guid_t guid = {};
   is >> guid;
   if (std::strncmp(guid, QTBytestream::GUID, GUID_LEN) != 0)
      throw std::runtime_error( "Invalid QTQuadTree GUID; possible corrupt bytestream." );

   size_t nbNodes, nbItems;
   is >> nbNodes >> nbItems;
   if (nbNodes)
      QTNode::resizeAllocator(nbNodes);
   is >> tree.m_bbox[0] >> tree.m_bbox[1] >> tree.m_bbox[2] >> tree.m_bbox[3];

   QTNode* nP = nullptr;
   is >> nP; if (nP) { nP = new QTNode(); is >> *nP; } tree.m_headP = nP;

   return is;
}
