#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == '__main__':
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '../..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

from CTCommon.XMLWriter   import XMLWriter
from IPImageProcessor.KML import KMLMeta
from IPImageProcessor.MPL import MPLUtil

import collections.abc
import datetime
import enum
import shapefile
import struct

copyright = \
'''\
<!-- ************************************************************************ -->
<!--  GMLWriter 21.04 -->
<!--  Copyright (c) INRS 2018 -->
<!--  Institut National de la Recherche Scientifique (INRS) -->
<!--  Copyright (c) Yves Secretan 2021 -->
<!--  -->
<!--  Distributed under the GNU Lesser General Public License, Version 3.0. -->
<!--  See accompanying file LICENSE.txt. -->
<!-- ************************************************************************ -->

<!-- File generated automatically, changes will be lost on next update -->
'''

class GMLWriter(XMLWriter):
    zOffset = 2.0
    validDataFields = ['name', 'kind', 'variable', 'value', 'color', 'position']

    def __init__(self, meta, fname = None):
        self.cntrs = {}
        self.pointColorUniquer = set()
        self.lineColorUniquer = set()
        self.polyColorUniquer = set()

        self.meta = meta
        self.is_hd = meta['quality'] == KMLMeta.Quality.high
        
        XMLWriter.__init__(self, fname)

    def close(self):
        XMLWriter.close(self)
        
    def __incCounter(self, name):
        try:
            self.cntrs[name] += 1
        except KeyError:
            self.cntrs[name] = 1
        return self.cntrs[name]

    def __getCounter(self, name):
        try:
            return self.cntrs[name]
        except KeyError:
            return 0

    def __getPolygonsFromPath(self, path, forceCCW = False):
        if (self.is_hd):
            return MPLUtil.getHDPolygonsFromPath(path, forceCCW)
        else:
            return MPLUtil.getLDPolygonsFromPath(path, forceCCW)

    def __writeVertex(self, x, y):
        self.writeLine('%f, %f' % (x, y))

    def __writePoints(self, vertices):
        for vertex in vertices:
            self.openElem('gml:Point')
            self.openElem('gml:coordinates')
            self.__writeVertex(vertex[0], vertex[1])
            self.closeElem()              # coordinates
            self.closeElem()              # LineString

    def __writeLineString(self, vertices):
        self.openElem('gml:LineString') # name  gml:srsName
        self.openElem('gml:coordinates')
        for vertex in vertices:
            self.__writeVertex(vertex[0], vertex[1])
        self.closeElem()                  # coordinates
        self.closeElem()                  # LineString

    def __writeLinearRing(self, vertices, tag='gml:outerBoundaryIs'):
        self.openElem(tag)
        self.openElem('gml:LinearRing')
        self.openElem('gml:coordinates')
        for vertex in vertices:
            self.__writeVertex(vertex[0], vertex[1])
        self.closeElem()                  # coordinates
        self.closeElem()                  # LinearRing
        self.closeElem()                  # tag

    def __writeTimePrimitive(self, time=None):
        timCntr = self.__incCounter('TimePrimitive')
        try:
            t = [ "{0}Z".format(t_.isoformat()) for t_ in time[0:2] ]
            self.openElem('TimeSpan', 'id="TimePrimitive_{0:04d}"'.format(timCntr))
            self.writeTag('begin', t[0])
            self.writeTag('end',   t[1])
            self.closeElem()        #TimeSpan
        except:
            try:
                t = "{0}Z".format(time.isoformat())
                self.openElem('TimeStamp', 'id="TimePrimitive_{0:04d}"'.format(timCntr))
                self.writeTag('when', t)
                self.closeElem()    #TimeStamp
            except:
                pass

    def __openPlacemark(self, time=None, **kwargs):
        name  = kwargs.get('name',  '')
        value = kwargs.get('value', None)

        title = ' '.join( (name, 'value=%s' % value) ) if value else name
        title = title.strip()
        descr = ''

        pmkCntr = self.__incCounter('Placemark')
        self.openElem('Placemark', 'id="Placemark_%06d"' % pmkCntr)
        if title: self.writeTag('name', title)
        if descr: self.writeTag('description', descr)
        self.writeSchemaData(**kwargs)
        if time: self.__writeTimePrimitive(time)

    def writeSchemaData(self, *args, **kwargs):
        if not kwargs: return

        self.openElem('ExtendedData')
        self.openElem('SchemaData', 'schemaUrl="#SHOP_Schema"')
        for k, v in kwargs.items():
            if k not in GMLWriter.validDataFields:
                raise ValueError('Invalid key: %s not in %s' % (k, GMLWriter.validDataFields))
            self.writeTag('SimpleData name="%s"' % k, '%s' % str(v))
        self.closeElem()                   # SchemaData
        self.closeElem()                   # ExtendedData

    def openLayer(self, title):
        lyrCntr = self.__incCounter('Layer')
        self.openElem('sld:UserLayer')
        self.writeTag('sld:Name', '%d' % lyrCntr)
        # self.writeTag('sld:Description', '%d' % self.layerCntr)

    def closeLayer(self):
        self.closeElem()    # UserStyle
        
    def writeDocumentRegion(self, bbox):
        self.openElem ('gml:boundedBy')
        self.openElem ('gml:Envelope')
        self.writeTag ('gml:upperCorner', '%.14f %.14f' % (bbox[3], bbox[0]))
        self.writeTag ('gml:lowerCorner', '%.14f %.14f' % (bbox[2], bbox[1]))
        self.closeElem()    # gml:Envelope
        self.closeElem()    # Region

    def writeDocumentHead(self, time = None):
        docCntr = self.__incCounter('Document')

        m = self.meta
        name = m['authority']
        if m['title']:    name = ' - '.join((name, m['title']))
        if m['time']:     name = ' - '.join((name, '%sZ' % m['time'].isoformat()))

        descr  = ''
        if m['authority']: descr += 'Authority %s\n' % m['authority']
        if m['title']:     descr += 'Title     %s\n' % m['title']
        if m['time']:      descr += 'Time      %sZ\n'% m['time'].isoformat()

        #self.openElem('Document', 'id="Document_%06d"' % docCntr)
        #self.writeTag('name', name)
        #self.writeTag('description', descr)

        #self.__writeTimePrimitive(time)

    def writeHeader(self, time=None):
        XMLWriter.writeHeader(self, copyright)
        atts_sld =  {
            'version'   : '1.0.0',
            'xmlns'     : 'http://www.opengis.net/ogc',
            'xmlns:sld' : 'http://www.opengis.net/sld',
            'xmlns:ogc' : 'http://www.opengis.net/ogc',
            'xmlns:gml' : 'http://www.opengis.net/gml',
            'xmlns:xsi' : 'http://www.w3.org/2001/XMLSchema-instance',
            'xsi:schemaLocation' : 'http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd',
        }
        self.openElem ('sld:StyledLayerDescriptor', atts_sld)
        self.writeDocumentHead  (time)
        self.writeDocumentRegion(self.meta['bbox'])

    def writeGeometryAsLineCollection(self, collections, levels, colors, time=None, title='Geometry'):
        """
        Add a LineCollection to an openend KML file
        """
        assert len(collections) == len(colors)

        for polygons, color in zip(collections, colors):
            if not polygons: continue

            self.__openPlacemark(name=title, color=color, time=time)
            self.writeTag('styleUrl', '#SHOP_LineColor_%s' % color)
            if (len(polygons) > 1): self.openElem('MultiGeometry')

            for polygon in polygons:
                self.__writeLineString(polygon)

            if (len(polygons) > 1): self.closeElem()    # MultiGeometry
            self.closeElem()                            # Placemark

    def writeNodesAsPointCollection(self, collections, levels, colors, time=None, title='Nodes'):
        """
        Add a Folder of points to an openend KML file
        """
        assert len(collections) == len(colors)

        for (polygons, color) in zip(collections, colors):

            self.openElem('Folder')
            self.writeTag('name', title)
            self.writeTag('visibility', 1)
            self.writeTag('open', 0)
            self.writeTag('styleUrl', '#SHOP_NodeStyle')
            self.writeSchemaData(name=title, kind='Nodes')

            for polygon in polygons:
                for point in polygon:
                    self.__openPlacemark()
                    self.writeTag('styleUrl', '#SHOP_PointColor_%s' % color)
                    self.writeSchemaData(kind='Node', position='%.7f, %.7f'% (point[0], point[1]))
                    self.__writePoints( (point,) )
                    self.closeElem()                    # Placemark

            self.closeElem()                            # Folder

    def writeIsoLineAsLineCollection(self, collections, levels, colors, time=None, title='Isoline'):
        """
        Add a LineCollection to an openend KML file
        """
        assert len(collections) == len(colors)
        assert len(levels) == len(colors)

        self.openElem('gml:FeatureCollection')
        for polygons, level, color in zip(collections, levels, colors):
            self.openElem('gml:Feature')
            if (len(polygons) > 1): 
                self.openElem('gml:MultiGeometry')
            for polygon in polygons:
                self.__writeLineString(polygon)
            if (len(polygons) > 1): 
                self.closeElem()    # MultiGeometry
            self.closeElem()    # 'sld:FeatureMember')
        self.closeElem()        # 'sld:InlineFeature'

        #self.openElem('sld:UserStyle')
        #self.writeTag('sld:Name', title)
        #self.writeTag('sld:Title', title)
        #self.openElem('sld:FeatureTypeStyle')
        #for polygons, level, color in zip(collections, levels, colors):
        #    if not polygons: continue
        #
        #    try:
        #        color = color[0]
        #    except:
        #        pass
        #    value = '%.6e' % level # eng_string(level, sig_figs=3, si=False)
        #
        #    #self.__openPlacemark(name='Iso-line', value=value, color=color, time=time)
        #    self.writeTag('styleUrl', '#SHOP_LineColor_%s' % color)
        #    if (len(polygons) > 1): self.openElem('MultiGeometry')
        #
        #    for polygon in polygons:
        #        self.__writeLineString(polygon)
        #
        #    if (len(polygons) > 1): self.closeElem()    # MultiGeometry
        #    self.closeElem()                            # Placemark
        #self.closeElem()
        #self.closeElem()


    def writeIsovalueAsPolyCollection(self, collections, levels_, colors, time=None, title='Isovalue'):
        """
        Add a PolyCollection to an openend KML file
        """
        assert (len(collections) == len(colors))
        assert (len(levels_) == len(colors)-1 or len(levels_) == len(colors)+1)
        if (len(levels_) == len(colors)-1):
            levels = [ -1.0e99 ]
            levels.extend(levels_)
            levels.append(1.0e99)
        else:
            levels = levels_

        for polygons, lmin, lmax, color in zip(collections, levels, levels[1:], colors):
            if not polygons: continue

            value = '(%.6e, %.6e)' % (lmin, lmax)

            self.__openPlacemark(name='Iso-value', value=value, color=color, time=time)
            self.writeTag('styleUrl', '#SHOP_PolyColor_%s' % color)
            if (len(polygons) > 1): self.openElem('MultiGeometry')

            for polygon in polygons:
                self.openElem('Polygon')
                self.writeTag('altitudeMode', 'clampToGround')

                ring = polygon[0]
                self.__writeLinearRing(ring, 'outerBoundaryIs')
                for ring in polygon[1:]:
                    self.__writeLinearRing(ring, 'innerBoundaryIs')

                self.closeElem()                    # Polygon

            if (len(polygons) > 1): self.closeElem()    # MultiGeometry
            self.closeElem()                            # Placemark

    def writePathAsLineCollection(self, collections, levels, colors, time=None, title='Particle path'):
        """
        Add a LineCollection to an openend KML file
        """
        assert len(collections) == len(colors)

        for polygons, polycolors in zip(collections, colors):
            if not polygons: continue

            self.openElem('Folder')
            self.writeTag('name', title)
            self.writeTag('visibility', 1)
            self.writeTag('open', 0)
            self.writeTag('styleUrl', '#SHOP_PathStyle')
            self.writeSchemaData(name=title, kind='Particle path')

            for polygon, color in zip(polygons, polycolors):
                self.__openPlacemark()       # (title='', color=color, time=time)
                self.writeTag('styleUrl', '#SHOP_LineColor_%s' % color)
                self.__writeLineString(polygon)
                self.closeElem()                        # Placemark

            self.closeElem()                            # Folder

    def writeQuiverAsPolyCollection(self, collections, levels, colors, time=None, title='Quiver'):
        """
        Add a quiver to an openend KML file
        """
        assert len(collections) == len(colors)

        self.openElem('Folder')
        self.writeTag('name', title)
        self.writeTag('visibility', 1)
        self.writeTag('open', 0)
        self.writeTag('styleUrl', '#SHOP_QuiverStyle')
        self.writeSchemaData(name=title, kind='Quiver')

        for polyline, color in zip(collections, colors):
            self.__openPlacemark()       # (title='', color=color, time=time)
            self.writeTag('styleUrl', '#SHOP_PolyColor_%s' % color)
            self.openElem('Polygon')
            self.__writeLinearRing(polyline)
            self.closeElem()                        # Polygon
            self.closeElem()                        # Placemark

        self.closeElem()                            # Folder

    def writeEllipsesAsPolyCollection(self, collections, levels, colors, time=None, title='Diffusion ellipse'):
        """
        Add ellipses to an openend KML file
        """
        assert len(collections) == len(colors)

        self.openElem('Folder')
        self.writeTag('name', title)
        self.writeTag('visibility', 1)
        self.writeTag('open', 0)
        self.writeTag('styleUrl', '#SHOP_EllipsesStyle')
        self.writeSchemaData(name=title, kind='Ellipses')

        for polyline, color in zip(collections, colors):
            self.__openPlacemark(name=title, color=color, time=time)
            self.writeTag('styleUrl', '#SHOP_PolyColor_%s' % color)
            self.openElem('Polygon')
            self.__writeLinearRing(polyline)
            self.closeElem()                        # Polygon
            self.closeElem()                        # Placemark

        self.closeElem()                            # Folder

    def writeTextAsPointCollection(self, collections, levels, colors, time=None, title='Annotation'):
        """
        Add annotations to an openend KML file
        """
        assert len(collections) == len(colors)

        self.openElem('Folder')
        self.writeTag('name', title)
        self.writeTag('visibility', 1)
        self.writeTag('open', 0)
        self.writeTag('styleUrl', '#SHOP_AnnotationStyle')
        self.writeSchemaData(name=title, kind='Text')

        for (x, y, t), color in zip(collections, colors):
            self.__openPlacemark(name=t, color=color, time=time, position='%.7f, %.7f'% (x, y))
            self.writeTag('styleUrl', '#SHOP_PointColor_%s' % color)
            self.__writePoints( ((x,y),) )
            #self.writeTag('Legend', t)
            self.closeElem()                    # Placemark

        self.closeElem()                        # Folder

def main():
    """
    Illustrate simple contour plotting, contours on an image with
    a colorbar for the contours, and labelled contours.
    From contour_demo.py.
    """
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt
    from IPImageProcessor.KML.KMLWriter import bivariate_normal

    matplotlib.rcParams['xtick.direction'] = 'out'
    matplotlib.rcParams['ytick.direction'] = 'out'

    delta = 1.0 # 0.025
    x = np.arange(-3.0, 3.0, delta)
    y = np.arange(-2.0, 2.0, delta)
    X, Y = np.meshgrid(x, y)
    Z1 = bivariate_normal(X, Y, 1.0, 1.0,-1.0,-1.0)
    Z2 = bivariate_normal(X, Y, 1.0, 1.0, 1.0, 1.0)
    # difference of Gaussians
    Z = 10.0 * (Z2 + Z1)

    plt.figure()
    CS = plt.contour(X, Y, Z)
    #CS = plt.contourf(X, Y, Z)
    plt.title('Simplest default no labels')

    meta = KMLMeta.KMLMetaData()
    meta['authority']= 'GMLWriter'
    meta['title']    = 'Test'
    meta['bbox']     = [ 2.0, -2.0, 3.0, -3.0 ]   # n, s, e, w
    meta['quality']  = KMLMeta.Quality.high

    poly = MPLUtil.MPLUtil.getPolylinesFromContourSet(CS)
    #poly = MPLUtil.MPLUtil.getPolygonsFromContourSet(CS)
    lvls = MPLUtil.MPLUtil.getLevelsFromContourSet(CS)
    clrs = MPLUtil.MPLUtil.getColorsFromContourSet(CS)

    writer = GMLWriter(meta, "test_gml.gml")
    writer.writeHeader()
    writer.openLayer('poly')
    writer.writeIsoLineAsLineCollection(poly, lvls, clrs)
    #writer.writeIsoValueAsPolyCollection(poly, lvls, clrs)
    writer.closeLayer()
    writer.writeFooter()


    plt.show()

if __name__ == '__main__':
    main()