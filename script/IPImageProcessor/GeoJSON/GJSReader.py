#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2021
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == '__main__':
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '../..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import collections.abc
import json
import numpy as np
import six

from descartes import PolygonPatch

def isIterable(arg):
    """
    Return True if arg is iterable but not a dict or a string
    """
    return (isinstance(arg, collections.abc.Iterable) and
            not isinstance(arg, dict) and
            not isinstance(arg, six.string_types))

def getPatchesFromData(data, filter=[], bbox=[], **kwargs):
    """
    getPatchesFromData [summary]

    Return the geometry as a list of Matplotlib patches

    Args:
        data (dict): GeoJSON data dictionary
        filter (list, optional): Filter on GeoJSON type name. Defaults to [].
        bbox (list, optional):   Filter on bbox (xmin, ymin, xmax, ymax). Defaults to [].
        **kwargs: Optional args. The `kwargs` are those supported by the matplotlib.patches.Polygon class
                  constructor.
    """
    patches = []
    items = data if isIterable(data) else [data]
    for item in items:
        for ftr in item['features']:
            geo = ftr['geometry']
            if filter and geo['type'] not in filter: continue
            if bbox:
                xy = np.array(geo['coordinates'])
                if xy.ndim == 2:
                    (gxmin, gymin), (gxmax, gymax) = np.amin(xy, 0), np.amax(xy, 0)
                else:
                    gmin, gmax = np.amin(xy, -2), np.amax(xy, -2)
                    (gxmin, gymin), (gxmax, gymax) = np.amin(gmin, 0), np.amax(gmax, 0)
                bxmin, bymin, bxmax, bymax = bbox
                if gxmin > bxmax: continue
                if gxmax < bxmin: continue
                if gymin > bymax: continue
                if gymax < bymin: continue
            patches.append( PolygonPatch(geo, **kwargs) )
    return patches

def getPatchesFromFile(fname, filter=[], bbox=[], **kwargs):
    """
    addFileToAxes [summary]

    [extended_summary]

    Args:
        ax (Axes): Matplotlib axes
        fname (str): File name
        filter (list, optional): Filter on GeoJSON type name. Defaults to [].
        bbox (list, optional):   Filter on bbox (xmin, ymin, xmax, ymax). Defaults to [].
        **kwargs: Optional args. The `kwargs` are those supported by the matplotlib.patches.Polygon class
                  constructor.
    """
    with open(fname, 'r') as f:
        data = json.load(f)
    return getPatchesFromData(data, filter, bbox, **kwargs)

def addDataToAxes(ax, data, filter=[], bbox=[], **kwargs):
    """
    addGeometryToAxes [summary]

    Add the geometry to a Matplotlib axes

    Args:
        ax (Axes): Matplotlib axes
        data (dict): GeoJSON data dictionary
        filter (list, optional): Filter on GeoJSON type name. Defaults to [].
        bbox (list, optional):   Filter on bbox (xmin, ymin, xmax, ymax). Defaults to [].
        **kwargs: Optional args. The `kwargs` are those supported by the matplotlib.patches.Polygon class
                  constructor.
    """
    patches = getPatchesFromData(data, filter, bbox, **kwargs)
    for p in patches:
        ax.add_patch(p)
    return patches

def addFileToAxes(ax, fname, filter=[], bbox=[], **kwargs):
    """
    addFileToAxes [summary]

    [extended_summary]

    Args:
        ax (Axes): Matplotlib axes
        fname (str): File name
        filter (list, optional): Filter on GeoJSON type name. Defaults to [].
        bbox (list, optional):   Filter on bbox (xmin, ymin, xmax, ymax). Defaults to [].
        **kwargs: Optional args. The `kwargs` are those supported by the matplotlib.patches.Polygon class
                  constructor.
    """
    with open(fname, 'r') as f:
        data = json.load(f)
        patches = addDataToAxes(data, ax, filter, bbox, **kwargs)
    return patches

def main():
    import matplotlib.pyplot as plt
    import shapefile

    BLUE = '#6699cc'

    fig = plt.figure()
    ax = fig.gca()

    fname = r'E:\Projets\ECCC\Parna\shp\Mamawi_contour_hr.shp'
    reader = shapefile.Reader(fname)
    gjs = reader.__geo_interface__
    addDataToAxes(ax, gjs, bbox=[0,0,1e7,1e7], fc=BLUE, ec='k')
    ax.axis('scaled')
    plt.show()

if __name__ == '__main__':
    main()