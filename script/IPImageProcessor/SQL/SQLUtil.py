#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2014
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import struct

# ---  Byte Order
BO_BIG_ENDIAN           = 0 # xdr,power-pc
BO_LITTLE_ENDIAN        = 1 # ndr, intel, amd

# ---  Geometry
GO_POINT                = 1
GO_LINESTRING           = 2
GO_POLYGON              = 3
GO_MULTIPOINT           = 4
GO_MULTILINESTRING      = 5
GO_MULTIPOLYGON         = 6
GO_GEOMETRYCOLLECTION   = 7
GO_RING                 = 10001

isBigEndian = struct.pack('>i', 1) == struct.pack('=i', 1)
BYTE_ORDER  = BO_BIG_ENDIAN if isBigEndian else BO_LITTLE_ENDIAN
VRTX_LEN    = 16

#--------------------------------------------------------------------------
def getHeader(geo):
    buf = bytearray(5)
    struct.pack_into('b', buf, 0, BYTE_ORDER)
    struct.pack_into('I', buf, 1, geo)
    return buf

def getVertexAsWKB(vertex):
    buf = bytearray(VRTX_LEN)
    struct.pack_into('2d', buf, 0, *vertex)
    return buf

def getRingAsWKB(ring):
    fmt = '%iB' % VRTX_LEN
    buf = bytearray( 4 + len(ring) * VRTX_LEN )

    struct.pack_into('I', buf, 0, len(ring))
    off = 4
    for v in ring:
        struct.pack_into(fmt, buf, off, *getVertexAsWKB(v))
        off += VRTX_LEN
    return buf

def getPointAsWKB(vertex):
    buf = getHeader(GO_POINT)
    buf.extend( getVertexAsWKB(vertex) )
    return buf

def getLineStringAsWKB(rings):
    buf = getHeader(GO_LINESTRING)
    buf.extend( struct.pack('I',len(rings)) )
    for r in rings:
        buf.extend( getRingAsWKB(r) )
    return buf

def getPolygonAsWKB(rings):
    buf = getHeader(GO_POLYGON)
    buf.extend( struct.pack('I',len(rings)) )
    for r in rings:
        buf.extend( getRingAsWKB(r) )
    return buf

def getMultiPolygonAsWKB(polys):
    buf = getHeader(GO_MULTIPOLYGON)
    buf.extend( struct.pack('I',len(polys)) )
    for p in polys:
        buf.extend( getPolygonAsWKB(p) )
    return buf

#--------------------------------------------------------------------------
def getVertexAsWKT(vertex):
    return '%s %s' % (vertex[0], vertex[1])

def getLineStringAsWKT(ring):
    return 'LINESTRING', '(%s)' % ', '.join( [ getVertexAsWKT(v) for v in ring ] )

def getMultiLinestringAsWKT(polys):
    return 'MULTILINESTRING', '(%s)' % ', '.join( [ getLineStringAsWKT(p)[1] for p in polys ] )

def getRingAsWKT(ring):
    return 'RING', '(%s)' % ', '.join( [ getVertexAsWKT(v) for v in ring ] )

def getPolygonAsWKT(rings):
    return 'POLYGON', '(%s)' % ', '.join( [ getRingAsWKT(r)[1] for r in rings ] )

def getMultiPolygonAsWKT(polys):
    return 'MULTIPOLYGON', '(%s)' % ', '.join( [ getPolygonAsWKT(p)[1] for p in polys ] )


#--------------------------------------------------------------------------
def main():
    def unpackRing(ring):
        print('npnt', struct.unpack_from('I', ring, 0))
        print('pnts:', struct.unpack_from('10d', ring, 4))
        return 84

    def unpackPolygon(poly):
        print('BO', struct.unpack_from('b', poly, 0))
        print('GO', struct.unpack_from('I', poly, 1))
        print('nring', struct.unpack_from('I', poly, 5))
        off = 9
        n, = struct.unpack_from('I', poly, 5)
        for i in range(n):
            off += unpackRing(poly[off:])
        return off

    def unpackMultiPolygon(polys):
        print('BO', struct.unpack_from('b', polys, 0))
        print('GO', struct.unpack_from('I', polys, 1))
        print('npoly', struct.unpack_from('I', polys, 5))
        off = 9
        n, = struct.unpack_from('I', polys, 5)
        for i in range(n):
            off += unpackPolygon(polys[off:])
        return off

    v = (1, 1)
    r = ((1, 1), (2, 1), (2, 2), (1, 2), (1, 1))
    p = (((1, 1), (2, 1), (2, 2), (1, 2), (1, 1)),)
    mp = (p, p)

    print('--- Vertex')
    vWKB = getVertexAsWKB(v)
    print(struct.unpack('2d', vWKB))
    print('--- done')

    print('--- Ring')
    rWKB = getRingAsWKB(r)
    unpackRing(rWKB)
    print('--- done')

    print('--- Polygon')
    pWKB = getPolygonAsWKB(p)
    unpackPolygon(pWKB)
    print('--- done')

    print('--- MultiPolygon')
    mWKB = getMultiPolygonAsWKB(mp)
    unpackMultiPolygon(mWKB)
    print('--- done')

if __name__ == '__main__':
    main()
