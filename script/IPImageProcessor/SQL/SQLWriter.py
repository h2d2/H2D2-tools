#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from . import SQLUtil
import ..KML.KMLMeta

import codecs

class SQLWriter:
    '''
    Abstract base class to write SQL statements.
    Children shall implement the method write.
    '''
    def __init__(self, meta):
        self.name = ''
        self.values = [
            ['simultime', 'TIMESTAMP',        None],  # name, sql_type, value
            ['vmin',      'DOUBLE PRECISION', None],
            ['vmax',      'DOUBLE PRECISION', None],
            ['style',     'VARCHAR(8)',       None],
            ['geom',      'GEOMETRY',         None],
            ]

        self.meta = meta

    def write(self, l):
        raise NotImplementedError

    def writeHeader(self):
        pass

    def writeFooter(self):
        pass

    def beginTransaction(self):
        self.write('BEGIN;')

    def endTransaction(self):
        self.write('COMMIT;')

    def createTable(self, kind):
        m = self.meta
        t = '%sZ' % m['time_start'].isoformat().replace(':','_')
        self.name = '_'.join( (m['authority'], t, m['variable']) )
        self.srid = 4326
        ndim = 2

        cols = [ '%s %s' % (v[0],v[1]) for v in self.values[:-1] ]
        geom = self.values[-1][0]
        addg = "AddGeometryColumn('%s', '%s', %d, '%s', %d)" % (self.name, geom, self.srid, kind, ndim)
        test = "WHERE NOT EXISTS (SELECT column_name FROM information_schema.columns WHERE table_name='%s' and column_name='%s')" % (self.name, geom)
        self.write('CREATE TABLE IF NOT EXISTS "%s" (gid SERIAL NOT NULL PRIMARY KEY, %s);' % (self.name, ', '.join(cols)))
        self.write("SELECT %s %s;" % (addg, test))
        """
        CREATE INDEX "sidx_SHOP - 2009-06-16T07:30:00Z - Conservative contaminant_geo"
          ON "SHOP - 2009-06-16T07:30:00Z - Conservative contaminant"
          USING gist
          (geom);
        """

    def writeLineCollection(self, collections, levels, colors):
        assert (len(collections) == len(colors))
        assert (len(levels) == len(colors))

        self.beginTransaction()
        self.createTable('MULTILINESTRING')
        time = self.meta['time_step'].isoformat()
        for polygons, level, color in zip(collections, levels, colors):
            geom = ''.join( SQLUtil.getMultiLinestringAsWKT(polygons) )
            self.values[0][-1] = "'%s'" % time
            self.values[1][-1] = level
            self.values[2][-1] = level
            self.values[3][-1] = "'%s'" % color
            self.values[4][-1] = "'SRID=%d;%s'" % (self.srid, geom)

            cols = [ v[0] for v in self.values ]
            vals = [ '%s' % v[-1] for v in self.values ]
            self.write('INSERT INTO "%s" (%s) VALUES (%s);' % (self.name, ', '.join(cols), ', '.join(vals)) )

        self.endTransaction()

    def writePolyCollection(self, collections, levels_, colors):
        assert (len(collections) == len(colors))
        assert (len(levels_) == len(colors)-1 or len(levels_) == len(colors)+1)
        if (len(levels_) == len(colors)-1):
            levels = [ -1.0e9 ]
            levels.extend(levels_)
            levels.append(1.0e9)
        else:
            levels = levels_

        self.beginTransaction()
        self.createTable('MULTIPOLYGON')
        time = self.meta['time_step'].isoformat()
        for polygons, lmin, lmax, color in zip(collections, levels, levels[1:], colors):
            geom = ''.join( SQLUtil.getMultiPolygonAsWKT(polygons) )
            self.values[0][-1] = "'%s'" % time
            self.values[1][-1] = lmin
            self.values[2][-1] = lmax
            self.values[3][-1] = "'%s'" % color
            self.values[4][-1] = "'SRID=%d;%s'" % (self.srid, geom)

            cols = [ v[0] for v in self.values ]
            vals = [ '%s' % v[-1] for v in self.values ]
            self.write('INSERT INTO "%s" (%s) VALUES (%s);' % (self.name, ', '.join(cols), ', '.join(vals)) )

        self.endTransaction()

class SQLFileWriter(SQLWriter):
    '''
    Writer to file
    '''
    def __init__(self, meta, fname = None):
        SQLWriter.__init__(self, meta)

        if (fname):
            if isinstance(fname, str):
                self.fout = codecs.open(fname, 'w', 'utf-8')
            else:
                self.fout = fname
        else:
            self.fout = sys.stdout

    def write(self, l):
        if isinstance(l, str):
            self.fout.write( '%s\n' % (l) )
        else:
            for i in l: self.write(i)

class SQLDBWriter(SQLWriter):
    '''
    Generic data-base writer. Write to a cursor
    '''
    def __init__(self, meta, cursor):
        SQLWriter.__init__(self, meta)
        self.cursor = cursor

    def write(self, sql):
        if isinstance(sql, str):
            print(sql[:min(80,len(sql))])
            self.cursor.execute(sql)
        else:
            for s in sql: self.write(s)

class SQLiteWriter(SQLDBWriter):
    '''
    SpatiaLite writer.
    '''
    def __init__(self, meta, fname = ':memory:'):
        import sqlite3
        import os
        print(sqlite3.sqlite_version)
        print(sqlite3.sqlite_version_info)
        dllDir  = r'E:\dev\H2D2\tools\script\IPImageProcessor\SQL\spatialite-4.1.1-DLL-win-x86'
        os.environ['PATH'] = dllDir + ";" + os.environ['PATH']
        dllPath = 'libspatialite-4.dll'

        conn = sqlite3.connect(fname)
        conn.enable_load_extension(True)
        conn.execute("SELECT load_extension('%s')" % dllPath)
#        print 'done loaded'
#        conn.execute("SELECT InitSpatialMetaData()")
#        print 'done InitSpatialMetaData'

        curs = conn.cursor()
        SQLDBWriter.__init__(self, meta, curs)

class PGWriter(SQLDBWriter):
    '''
    PostGIS writer.
    '''
    def __init__(self, meta, database, user, password, host, port = 5432):
        import psycopg2
        conn = psycopg2.connect(database=database, user=user, password=password, host=host, port=port)
        curs = conn.cursor()
        SQLDBWriter.__init__(self, meta, curs)
        self.write('SET CLIENT_ENCODING TO UTF8;')
        self.write('SET STANDARD_CONFORMING_STRINGS TO ON;')

def main():
    '''
    Illustrate simple contour plotting, contours on an image with
    a colorbar for the contours, and labelled contours.
    From contour_demo.py.
    '''
    import numpy as np
    import matplotlib
    import matplotlib.mlab   as mlab
    import matplotlib.pyplot as plt
    from MPLUtil import MPLUtil
    import KMLMeta

    matplotlib.rcParams['xtick.direction'] = 'out'
    matplotlib.rcParams['ytick.direction'] = 'out'

    delta = 0.025
    x = np.arange(-3.0, 3.0+delta, delta)
    y = np.arange(-2.0, 2.0+delta, delta)
    X, Y = np.meshgrid(x, y)
    Z1 = mlab.bivariate_normal(X, Y, 1.0, 1.0,-1.0,-1.0)
    Z2 = mlab.bivariate_normal(X, Y, 1.0, 1.0, 1.0, 1.0)
    # difference of Gaussians
    Z = 10.0 * (Z2 + Z1)

    plt.figure()
    CS = plt.contourf(X, Y, Z)
    plt.title('Simplest default no labels')

    meta = KMLMeta.KMLMetaData()
    meta['authority']= 'KMLWriter'
    meta['title']    = 'Test'
    meta['variable'] = 'Temperature'
    meta['bbox']     = [ 2.0, -2.0, 3.0, -3.0 ]   # n, s, e, w
    meta['quality']  = KMLMeta.Quality.low

    poly = MPLUtil.getPolygonsFromContourSet(CS)
    lvls = MPLUtil.getLevelsFromContourSet(CS)
    clrs = MPLUtil.getColorsFromContourSet(CS)
##    """
#    print len(clrs), clrs
#    print len(lvls), lvls
#    print len(poly)
#    for polygons,color in zip(poly, clrs):
#        print ' '*3, len(polygons), color
#        for polygon in polygons:
#            print ' '*6, len(polygon)
#            for ring in polygon:
#                print ' '*12, len(ring), type(ring)
##    """

#    writer = SQLFileWriter(meta, "a.sql")
#    writer = SQLiteWriter(meta, "b.sqlite")
    writer = PGWriter(meta, "geonode", host="192.168.8.200", user="postgres", password="QwpoAs;l")
    writer.writePolyCollection(poly, lvls, clrs)

    plt.show()

if __name__ == '__main__':
    main()