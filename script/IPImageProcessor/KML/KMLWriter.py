#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == '__main__':
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '../..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import collections.abc
import os
import sys
import zipfile

import numpy as np

from CTCommon.XMLWriter   import XMLWriter
from IPImageProcessor.MPL import MPLUtil
from IPImageProcessor.KML import KMLMeta

if getattr(sys, 'frozen', False):
    ICON_ROOT = os.path.join(sys._MEIPASS, 'bitmaps', 'IPImageProcessor', 'KML')
else:
    ICON_ROOT = os.path.dirname(__file__)

ICON_FILES = [
    os.path.join(ICON_ROOT, 'images', 'map-location.png'),
    os.path.join(ICON_ROOT, 'images', 'right-arrow.png'),
    os.path.join(ICON_ROOT, 'images', 'ellipse.png'),
    os.path.join(ICON_ROOT, 'images', 'font.png'),
    ]
    
COPYRIGHT = \
'''\
<!-- ************************************************************************ -->
<!--  KMLWriter 18.04 -->
<!--  Copyright (c) INRS 2014-2018 -->
<!--  Institut National de la Recherche Scientifique (INRS) -->
<!--  -->
<!--  Distributed under the GNU Lesser General Public License, Version 3.0. -->
<!--  See accompanying file LICENSE.txt. -->
<!-- ************************************************************************ -->

<!-- File generated automatically, changes will be lost on next update -->
'''

#https://stackoverflow.com/questions/17973278/python-decimal-engineering-notation-for-mili-10e-3-and-micro-10e-6
import math
def eng_string( x, sig_figs=3, si=True):
    """
    Returns float/int value <x> formatted in a simplified engineering format -
    using an exponent that is a multiple of 3.

    sig_figs: number of significant figures

    si: if true, use SI suffix for exponent, e.g. k instead of e3, n instead of
    e-9 etc.
    """
    x = float(x)
    sign = ''
    if x < 0:
        x = -x
        sign = '-'
    if x == 0:
        exp = 0
        exp3 = 0
        x3 = 0
    else:
        exp = int(math.floor(math.log10( x )))
        exp3 = exp - ( exp % 3)
        x3 = x / ( 10 ** exp3)
        x3 = round( x3, -int( math.floor(math.log10( x3 )) - (sig_figs-1)) )
        if x3 == int(x3): # prevent from displaying .0
            x3 = int(x3)

    if si and exp3 >= -24 and exp3 <= 24 and exp3 != 0:
        exp3_text = 'yzafpnum kMGTPEZY'[ exp3 // 3 + 8]
    elif exp3 == 0:
        exp3_text = ''
    else:
        exp3_text = 'e%s' % exp3

    return ( '%s%s%s') % ( sign, x3, exp3_text)

    
class KMLWriter(XMLWriter):
    zOffset = 2.0
    validDataFields = ['name', 'kind', 'variable', 'value', 'color', 'position']

    def __init__(self, meta, fname = None):
        self.cntrs = {}
        self.pointColorUniquer = set()
        self.lineColorUniquer = set()
        self.polyColorUniquer = set()

        self.meta = meta
        self.is_hd = meta['quality'] == KMLMeta.Quality.high
        
        self.fname_kml = None
        self.fname_kmz = None
        if fname:
            xtn = os.path.splitext(fname)[1]
            if xtn.lower() in ['.kmz']:
                self.fname_kml = '%s.%s' % (fname, 'kml')
                self.fname_kmz = fname
            else:
                self.fname_kml = fname
                self.fname_kmz = None

        XMLWriter.__init__(self, self.fname_kml)

    def close(self):
        XMLWriter.close(self)
        if self.fname_kmz: 
            self.__compressToKMZ(self.fname_kml, self.fname_kmz)
            os.remove(self.fname_kml)
        
    def __compressToKMZ(self, fkml, fkmz):
        try:
            compression = zipfile.ZIP_DEFLATED
        except:
            compression = zipfile.ZIP_STORED

        zf = zipfile.ZipFile(fkmz, mode='w')
        for f in ICON_FILES:
            zf.write(f, arcname=os.path.basename(f), compress_type=compression)
        zf.write(fkml, arcname=os.path.basename(fkml), compress_type=compression)
        zf.close()

    def __incCounter(self, name):
        try:
            self.cntrs[name] += 1
        except KeyError:
            self.cntrs[name] = 1
        return self.cntrs[name]

    def __getCounter(self, name):
        try:
            return self.cntrs[name]
        except KeyError:
            return 0

    def __getPolygonsFromPath(self, path, forceCCW = False):
        if (self.is_hd):
            return MPLUtil.getHDPolygonsFromPath(path, forceCCW)
        else:
            return MPLUtil.getLDPolygonsFromPath(path, forceCCW)

    def __writeVertex(self, x, y, z):
        #self.writeLine('%.8f,%.8f,%.4f' % (x, y, z))
        self.writeLine('%s,%s,%s' % (str(x), str(y), str(z)))

    def __writePoints(self, vertices):
        outCntr = self.__getCounter('Altitude')
        z = KMLWriter.zOffset+outCntr
        for vertex in vertices:
            self.openElem('Point')
            self.openElem('coordinates')
            self.__writeVertex(vertex[0], vertex[1], z)
            self.closeElem()              # coordinates
            self.closeElem()              # LineString

    def __writeLineString(self, vertices):
        outCntr = self.__getCounter('Altitude')
        z = KMLWriter.zOffset+outCntr
        self.openElem('LineString')
        self.openElem('coordinates')
        for vertex in vertices:
            self.__writeVertex(vertex[0], vertex[1], z)
        self.closeElem()                  # coordinates
        self.closeElem()                  # LineString

    def __writeLinearRing(self, vertices, tag='outerBoundaryIs'):
        outCntr = self.__getCounter('Altitude')
        z = KMLWriter.zOffset+outCntr
        self.openElem(tag)
        self.openElem('LinearRing')
        self.openElem('coordinates')
        for vertex in vertices:
            self.__writeVertex(vertex[0], vertex[1], z)
        self.closeElem()                  # coordinates
        self.closeElem()                  # LinearRing
        self.closeElem()                  # tag

    def __writeTimePrimitive(self, time=None):
        timCntr = self.__incCounter('TimePrimitive')
        try:
            t = [ "{0}Z".format(t_.isoformat()) for t_ in time[0:2] ]
            self.openElem('TimeSpan', 'id="TimePrimitive_{0:04d}"'.format(timCntr))
            self.writeTag('begin', t[0])
            self.writeTag('end',   t[1])
            self.closeElem()        #TimeSpan
        except:
            try:
                t = "{0}Z".format(time.isoformat())
                self.openElem('TimeStamp', 'id="TimePrimitive_{0:04d}"'.format(timCntr))
                self.writeTag('when', t)
                self.closeElem()    #TimeStamp
            except:
                pass

    def __openPlacemark(self, time=None, **kwargs):
        name  = kwargs.get('name',  '')
        value = kwargs.get('value', None)

        title = ' '.join( (name, 'value=%s' % value) ) if value else name
        title = title.strip()
        descr = ''

        pmkCntr = self.__incCounter('Placemark')
        self.openElem('Placemark', 'id="Placemark_%06d"' % pmkCntr)
        if title: self.writeTag('name', title)
        if descr: self.writeTag('description', descr)
        self.writeSchemaData(**kwargs)
        if time: self.__writeTimePrimitive(time)

    def writeSchemaData(self, *args, **kwargs):
        if not kwargs: return

        self.openElem('ExtendedData')
        self.openElem('SchemaData', 'schemaUrl="#SHOP_Schema"')
        for k, v in kwargs.items():
            if k not in KMLWriter.validDataFields:
                raise ValueError('Invalid key: %s not in %s' % (k, KMLWriter.validDataFields))
            self.writeTag('SimpleData name="%s"' % k, '%s' % str(v))
        self.closeElem()                   # SchemaData
        self.closeElem()                   # ExtendedData

    def writePointColors(self, color):
        if   isinstance(color, str):
            if color in self.pointColorUniquer: return
            self.pointColorUniquer.add(color)

            self.openElem ('Style', 'id="SHOP_PointColor_%s"' % color)
            self.openElem ('LabelStyle')
            self.writeTag ('scale', 0.75)
            self.closeElem()              # LabelStyle
            self.openElem ('IconStyle')
            self.writeTag ('scale', 0.75)
            self.writeTag ('color', color)
            self.openElem ('Icon')
            self.writeTag ('href', 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png')
            self.closeElem()              # Icon
            self.closeElem()              # IconStyle

            self.closeElem()              # Style
        elif isinstance(color, collections.abc.Iterable):
            for clr in color:
                self.writePointColors(clr)
        else:
            raise ValueError('Expected one of (str, unicode) or an iterable, instead got %s' % type(color))

    def writeLineColors(self, color):
        if   isinstance(color, str):
            if color in self.lineColorUniquer: return
            self.lineColorUniquer.add(color)

            self.openElem ('Style', 'id="SHOP_LineColor_%s"' % color)

            self.openElem ('LineStyle')
            self.writeTag ('color', color)
            self.closeElem()              # LineStyle

            self.closeElem()              # Style
        elif isinstance(color, collections.abc.Iterable):
            for clr in color:
                self.writeLineColors(clr)
        else:
            raise ValueError('Expected one of (str, unicode) or an iterable, instead got %s' % type(color))

    def writePolyColors(self, color):
        if   isinstance(color, str):
            if color in self.polyColorUniquer: return
            self.polyColorUniquer.add(color)

            self.openElem ('Style', 'id="SHOP_PolyColor_%s"' % color)

            self.openElem ('PolyStyle')
            self.writeTag ('color', color)
            self.writeTag ('fill',    1)  # True
            self.closeElem()              # PolyStyle
            self.openElem ('LineStyle')
            self.writeTag ('color', color)
            self.closeElem()              # LineStyle

            self.closeElem()              # Style
        elif isinstance(color, collections.abc.Iterable):
            for clr in color:
                self.writePolyColors(clr)
        else:
            raise ValueError('Expected one of (str, unicode) or an iterable, instead got %s' % type(color))

    def writeDocumentStyle(self):
        # ---  Node style
        self.openElem ('Style', 'id="SHOP_NodeStyle"')

        self.openElem ('ListStyle')
        self.writeTag ('listItemType', 'checkHideChildren')
        self.openElem ('ItemIcon')
        self.writeTag ('state', 'open')
        self.writeTag ('href', 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png')
        self.closeElem()              # ItemIcon
        self.closeElem()              # ListStyle

        self.openElem ('IconStyle')
        self.writeTag ('scale', 0.75)
        self.writeTag ('color', 'ff8000ff')
        self.openElem ('Icon')
        self.writeTag ('href', 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png')
        self.closeElem()              # Icon
        self.closeElem()              # IconStyle

        self.closeElem()              # Style

        # ---  Path style
        self.openElem ('Style', 'id="SHOP_PathStyle"')
        self.openElem ('ListStyle')
        self.writeTag ('listItemType', 'checkHideChildren')
        self.openElem ('ItemIcon')
        self.writeTag ('state', 'open')
        self.writeTag ('href', 'images/map-location.png')
        self.closeElem()              # ItemIcon
        self.closeElem()              # ListStyle
        self.closeElem()              # Style

        # ---  Quiver style
        self.openElem ('Style', 'id="SHOP_QuiverStyle"')
        self.openElem ('ListStyle')
        self.writeTag ('listItemType', 'checkHideChildren')
        self.openElem ('ItemIcon')
        self.writeTag ('state', 'open')
        self.writeTag ('href', 'images/right-arrow.png')
        self.closeElem()              # ItemIcon
        self.closeElem()              # ListStyle
        self.closeElem()              # Style

        # ---  Ellipses style
        self.openElem ('Style', 'id="SHOP_EllipsesStyle"')
        self.openElem ('ListStyle')
        self.writeTag ('listItemType', 'checkHideChildren')
        self.openElem ('ItemIcon')
        self.writeTag ('state', 'open')
        self.writeTag ('href', 'images/ellipse.png')
        self.closeElem()              # ItemIcon
        self.closeElem()              # ListStyle
        self.closeElem()              # Style

        # ---  Annotation style
        self.openElem ('Style', 'id="SHOP_AnnotationStyle"')
        self.openElem ('ListStyle')
        self.writeTag ('listItemType', 'checkHideChildren')
        self.openElem ('ItemIcon')
        self.writeTag ('state', 'open')
        self.writeTag ('href', 'images/font.png')
        self.closeElem()              # ItemIcon
        self.closeElem()              # ListStyle
        self.closeElem()              # Style

    def writeDocumentSchema(self):
        self.openElem ('Schema', 'name="SHOP" id="SHOP_Schema"')

        self.openElem ('SimpleField', 'type="string" name="name"')
        self.writeTag ('displayName', 'name')
        self.closeElem()              # SimpleField

        self.openElem ('SimpleField', 'type="string" name="kind"')
        self.writeTag ('displayName', 'kind')
        self.closeElem()              # SimpleField

        self.openElem ('SimpleField', 'type="string" name="variable"')
        self.writeTag ('displayName', 'variable')
        self.closeElem()              # SimpleField

        self.openElem ('SimpleField', 'type="string" name="value"')
        self.writeTag ('displayName', 'value')
        self.closeElem()              # SimpleField

        self.openElem ('SimpleField', 'type="string" name="color"')
        self.writeTag ('displayName', 'color (abgr)')
        self.closeElem()              # SimpleField

        self.openElem ('SimpleField', 'type="string" name="position"')
        self.writeTag ('displayName', 'position (long, lat)')
        self.closeElem()              # SimpleField

        self.closeElem()              # Schema

    def writeDocumentRegion(self, bbox = None, lod = (128,-1)):
        if (bbox or lod):
            self.openElem ('Region')

            if (bbox):
                self.openElem ('LatLonAltBox')
                self.writeTag ('north','%.14f' % bbox[0])
                self.writeTag ('south','%.14f' % bbox[1])
                self.writeTag ('east', '%.14f' % bbox[2])
                self.writeTag ('west', '%.14f' % bbox[3])
                self.closeElem()                   # LatLonAltBox

            if (lod):
                self.openElem ('Lod')
                self.writeTag ('minLodPixels','%d' % lod[0])
                self.writeTag ('maxLodPixels','%d' % lod[1])
                self.closeElem()                   # Lod

            self.closeElem()                   # Region

    def writeDocumentOverlay(self, link, bbox, drawOrder = None):
        self.openElem ('GroundOverlay')

        if (drawOrder):
            self.writeTag ('drawOrder', drawOrder)

        icnCntr = self.__incCounter('Icon')
        self.openElem ('Icon')  ##, 'id="Icon_%06d"' % icnCntr)
        self.writeTag ('href', link)
        self.closeElem()                      # Icon

        self.openElem ('LatLonBox')
        self.writeTag ('north','%.14f' % bbox[0])
        self.writeTag ('south','%.14f' % bbox[1])
        self.writeTag ('east', '%.14f' % bbox[2])
        self.writeTag ('west', '%.14f' % bbox[3])
        self.closeElem()                   # LatLonAltBox

        self.closeElem()                   # Region

    def writeDocumentHead(self, time = None):
        docCntr = self.__incCounter('Document')

        m = self.meta
        name = m['authority']
        if m['title']:    name = ' - '.join((name, m['title']))
        if m['time']:     name = ' - '.join((name, '%sZ' % m['time'].isoformat()))

        descr  = '<![CDATA[\n'
        descr += '<table border="0" padding="1">\n'
        if m['authority']: descr += '<tr><td>Authority</td><td>%s</td></tr>\n' % m['authority']
        if m['title']:     descr += '<tr><td>Title</td>    <td>%s</td></tr>\n' % m['title']
        if m['time']:      descr += '<tr><td>Time</td>     <td>%sZ</td></tr>\n'% m['time'].isoformat()
        descr += '</table>\n'
        descr += ']]>\n'

        self.openElem('Document', 'id="Document_%06d"' % docCntr)
        self.writeTag('name', name)
        self.writeTag('description', descr)

        self.__writeTimePrimitive(time)

    #def writeDocument(self, levels = [], colors = [], time = None):
    #    self.writeDocumentHead(time)
    #    self.writeDocumentRegion(self.meta['bbox'])
    #    ##self.writeDocumentOverlay(link, self.meta['bbox'])
    #    #self.writeDocumentSchema()
    #    #self.writeDocumentStyle(colors)

    def writeHeader(self, time=None):
        XMLWriter.writeHeader(self, COPYRIGHT)
        self.openElem ('kml', 'xmlns="http://www.opengis.net/kml/2.2"')
        self.writeDocumentHead(time)
        self.writeDocumentRegion(self.meta['bbox'])

    def writeFooter(self, closeAll=True):
        if (closeAll):
            try:
                while True: self.closeElem()
            except:
                pass
        else:
            self.closeElem()      # kml

    def writeGeometryAsLineCollection(self, collections, levels, colors, time=None, title='Geometry'):
        """
        Add a LineCollection to an openend KML file
        """
        assert len(collections) == len(colors)

        for polygons, color in zip(collections, colors):
            if not polygons: continue

            self.__openPlacemark(name=title, color=color, time=time)
            self.writeTag('styleUrl', '#SHOP_LineColor_%s' % color)
            if (len(polygons) > 1): self.openElem('MultiGeometry')

            for polygon in polygons:
                self.__writeLineString(polygon)

            if (len(polygons) > 1): self.closeElem()    # MultiGeometry
            self.closeElem()                            # Placemark

    def writeNodesAsPointCollection(self, collections, levels, colors, time=None, title='Nodes'):
        """
        Add a Folder of points to an openend KML file
        """
        assert len(collections) == len(colors)

        for (polygons, color) in zip(collections, colors):

            self.openElem('Folder')
            self.writeTag('name', title)
            self.writeTag('visibility', 1)
            self.writeTag('open', 0)
            self.writeTag('styleUrl', '#SHOP_NodeStyle')
            self.writeSchemaData(name=title, kind='Nodes')

            for polygon in polygons:
                for point in polygon:
                    self.__openPlacemark()
                    self.writeTag('styleUrl', '#SHOP_PointColor_%s' % color)
                    self.writeSchemaData(kind='Node', position='%.7f, %.7f'% (point[0], point[1]))
                    self.__writePoints( (point,) )
                    self.closeElem()                    # Placemark

            self.closeElem()                            # Folder

    def writeIsoLineAsLineCollection(self, collections, levels, colors, time=None, title='Isoline'):
        """
        Add a LineCollection to an openend KML file
        """
        assert len(collections) == len(colors)
        assert len(levels) == len(colors)

        for polygons, level, color in zip(collections, levels, colors):
            if not polygons: continue

            try:
                color = color[0]
            except:
                pass
            value = '%.6e' % level # eng_string(level, sig_figs=3, si=False)

            self.__openPlacemark(name='Iso-line', value=value, color=color, time=time)
            self.writeTag('styleUrl', '#SHOP_LineColor_%s' % color)
            if (len(polygons) > 1): self.openElem('MultiGeometry')

            for polygon in polygons:
                self.__writeLineString(polygon)

            if (len(polygons) > 1): self.closeElem()    # MultiGeometry
            self.closeElem()                            # Placemark

    def writeIsovalueAsPolyCollection(self, collections, levels_, colors, time=None, title='Isovalue'):
        """
        Add a PolyCollection to an openend KML file
        """
        assert (len(collections) == len(colors))
        assert (len(levels_) == len(colors)-1 or len(levels_) == len(colors)+1)
        if (len(levels_) == len(colors)-1):
            levels = [ -1.0e99 ]
            levels.extend(levels_)
            levels.append(1.0e99)
        else:
            levels = levels_

        for polygons, lmin, lmax, color in zip(collections, levels, levels[1:], colors):
            if not polygons: continue

            value = '(%.6e, %.6e)' % (lmin, lmax)

            self.__openPlacemark(name='Iso-value', value=value, color=color, time=time)
            self.writeTag('styleUrl', '#SHOP_PolyColor_%s' % color)
            if (len(polygons) > 1): self.openElem('MultiGeometry')

            for polygon in polygons:
                self.openElem('Polygon')
                self.writeTag('altitudeMode', 'clampToGround')

                ring = polygon[0]
                self.__writeLinearRing(ring, 'outerBoundaryIs')
                for ring in polygon[1:]:
                    self.__writeLinearRing(ring, 'innerBoundaryIs')

                self.closeElem()                    # Polygon

            if (len(polygons) > 1): self.closeElem()    # MultiGeometry
            self.closeElem()                            # Placemark

    def writePathAsLineCollection(self, collections, levels, colors, time=None, title='Particle path'):
        """
        Add a LineCollection to an openend KML file
        """
        assert len(collections) == len(colors)

        for polygons, polycolors in zip(collections, colors):
            if not polygons: continue

            self.openElem('Folder')
            self.writeTag('name', title)
            self.writeTag('visibility', 1)
            self.writeTag('open', 0)
            self.writeTag('styleUrl', '#SHOP_PathStyle')
            self.writeSchemaData(name=title, kind='Particle path')

            for polygon, color in zip(polygons, polycolors):
                self.__openPlacemark()       # (title='', color=color, time=time)
                self.writeTag('styleUrl', '#SHOP_LineColor_%s' % color)
                self.__writeLineString(polygon)
                self.closeElem()                        # Placemark

            self.closeElem()                            # Folder

    def writeQuiverAsPolyCollection(self, collections, levels, colors, time=None, title='Quiver'):
        """
        Add a quiver to an openend KML file
        """
        assert len(collections) == len(colors)

        self.openElem('Folder')
        self.writeTag('name', title)
        self.writeTag('visibility', 1)
        self.writeTag('open', 0)
        self.writeTag('styleUrl', '#SHOP_QuiverStyle')
        self.writeSchemaData(name=title, kind='Quiver')

        for polyline, color in zip(collections, colors):
            self.__openPlacemark()       # (title='', color=color, time=time)
            self.writeTag('styleUrl', '#SHOP_PolyColor_%s' % color)
            self.openElem('Polygon')
            self.__writeLinearRing(polyline)
            self.closeElem()                        # Polygon
            self.closeElem()                        # Placemark

        self.closeElem()                            # Folder

    def writeEllipsesAsPolyCollection(self, collections, levels, colors, time=None, title='Diffusion ellipse'):
        """
        Add ellipses to an openend KML file
        """
        assert len(collections) == len(colors)

        self.openElem('Folder')
        self.writeTag('name', title)
        self.writeTag('visibility', 1)
        self.writeTag('open', 0)
        self.writeTag('styleUrl', '#SHOP_EllipsesStyle')
        self.writeSchemaData(name=title, kind='Ellipses')

        for polyline, color in zip(collections, colors):
            self.__openPlacemark(name=title, color=color, time=time)
            self.writeTag('styleUrl', '#SHOP_PolyColor_%s' % color)
            self.openElem('Polygon')
            self.__writeLinearRing(polyline)
            self.closeElem()                        # Polygon
            self.closeElem()                        # Placemark

        self.closeElem()                            # Folder

    def writeTextAsPointCollection(self, collections, levels, colors, time=None, title='Annotation'):
        """
        Add annotations to an openend KML file
        """
        assert len(collections) == len(colors)

        self.openElem('Folder')
        self.writeTag('name', title)
        self.writeTag('visibility', 1)
        self.writeTag('open', 0)
        self.writeTag('styleUrl', '#SHOP_AnnotationStyle')
        self.writeSchemaData(name=title, kind='Text')

        for (x, y, t), color in zip(collections, colors):
            self.__openPlacemark(name=t, color=color, time=time, position='%.7f, %.7f'% (x, y))
            self.writeTag('styleUrl', '#SHOP_PointColor_%s' % color)
            self.__writePoints( ((x,y),) )
            #self.writeTag('Legend', t)
            self.closeElem()                    # Placemark

        self.closeElem()                        # Folder

    def writeNetworkLink(self, link, name = None, bbox = None, lod = None, time = None):
        """
        Add a network link to an opened document
        """
        self.openElem('NetworkLink')
        self.writeTag('name', name if name else link)

        if (bbox or lod):
            self.openElem ('Region')

            if (bbox):
                self.openElem ('LatLonAltBox')
                self.writeTag ('north','%.8f' % bbox[0])
                self.writeTag ('south','%.8f' % bbox[1])
                self.writeTag ('east', '%.8f' % bbox[2])
                self.writeTag ('west', '%.8f' % bbox[3])
                self.closeElem()              # LatLonAltBox

            if (lod):
                self.openElem ('Lod')
                self.writeTag ('minLodPixels','%d' % lod[0])
                self.writeTag ('maxLodPixels','%d' % lod[1])
                self.closeElem()              # Lod

            self.closeElem()                  # Region

        self.__writeTimePrimitive(time)

        lnkCntr = self.__incCounter('Link')
        self.openElem ('Link') ## , 'id="Link_%06d"' % lnkCntr)
        self.writeTag ('href', link)
        self.writeTag ('viewRefreshMode', 'onRegion')
        self.closeElem()                      # Link

        self.closeElem()                      # NetworkLink

    def writeRaster(self, fname):
        pass

    def writeCamera(self, lg=0, lat=0, alt=0, hd=0, tlt=0, rl=0, am='relativeToGround'):
        camCntr = self.__incCounter('Camera')
        self.openElem('Camera', 'id="Camera_{0:04d}"'.format(camCntr))
        self.writeTag('longitude', lg)   # kml:angle180
        self.writeTag('latitude', lat)   # kml:angle90
        self.writeTag('altitude', alt)   # double
        self.writeTag('heading', hd)     # double
        self.writeTag('tilt', tlt)       # angle360
        self.writeTag('roll', rl)        # anglepos180
        self.writeTag('altitudeMode', am)
        self.closeElem()                 #Camera

# https://stackoverflow.com/questions/64780530/attributeerror-module-matplotlib-mlab-has-no-attribute-bivariate-normal
def bivariate_normal(X, Y, sigmax=1.0, sigmay=1.0,
                        mux=0.0, muy=0.0, sigmaxy=0.0):
    """
    Bivariate Gaussian distribution for equal shape *X*, *Y*.
    See `bivariate normal
    <http://mathworld.wolfram.com/BivariateNormalDistribution.html>`_
    at mathworld.
    """
    Xmu = X-mux
    Ymu = Y-muy

    rho = sigmaxy/(sigmax*sigmay)
    z = Xmu**2/sigmax**2 + Ymu**2/sigmay**2 - 2*rho*Xmu*Ymu/(sigmax*sigmay)
    denom = 2*np.pi*sigmax*sigmay*np.sqrt(1-rho**2)
    return np.exp(-z/(2*(1-rho**2))) / denom

def main():
    """
    Illustrate simple contour plotting, contours on an image with
    a colorbar for the contours, and labelled contours.
    From contour_demo.py.
    """
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt

    matplotlib.rcParams['xtick.direction'] = 'out'
    matplotlib.rcParams['ytick.direction'] = 'out'

    delta = 0.025
    x = np.arange(-3.0, 3.0+delta, delta)
    y = np.arange(-2.0, 2.0+delta, delta)
    X, Y = np.meshgrid(x, y)
    Z1 = bivariate_normal(X, Y, 1.0, 1.0,-1.0,-1.0)
    Z2 = bivariate_normal(X, Y, 1.0, 1.0, 1.0, 1.0)
    # difference of Gaussians
    Z = 10.0 * (Z2 + Z1)

    plt.figure()
    #CS = plt.contourf(X, Y, Z)
    CS = plt.contour(X, Y, Z)
    plt.title('Simplest default no labels')

    meta = KMLMeta.KMLMetaData()
    meta['authority']= 'KMLWriter'
    meta['title']    = 'Test'
    meta['bbox']     = [ 2.0, -2.0, 3.0, -3.0 ]   # n, s, e, w
    meta['quality']  = KMLMeta.Quality.low

    #poly = MPLUtil.getPolygonsFromContourSet(CS)
    poly = MPLUtil.MPLUtil.getPolylinesFromContourSet(CS)
    lvls = MPLUtil.MPLUtil.getLevelsFromContourSet(CS)
    clrs = MPLUtil.MPLUtil.getColorsFromContourSet(CS)
    """
    print(len(clrs), clrs)
    print(len(lvls), lvls)
    print(len(poly))
    for polygons,color in zip(poly, clrs):
        print(' '*3, len(polygons), color)
        for polygon in polygons:
            print(' '*6, len(polygon))
            for ring in polygon:
                print(' '*12, len(ring), type(ring))
    """

    writer = KMLWriter(meta, "a.kml")
    writer.writeHeader()
    writer.writeLineCollection(poly, lvls, clrs)
    writer.writeFooter()

    plt.show()

if __name__ == '__main__':
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath( os.path.join(selfDir, '..', '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

    main()
