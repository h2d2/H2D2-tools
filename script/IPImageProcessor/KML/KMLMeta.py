#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Meta-data pertaining to KML plots
"""

import datetime
import enum
#import logging
#import os
#import sys

Quality = enum.Enum('Quality', ('low', 'high'))

class KMLMetaData:
    '''
    KMLMetaData maintains a dictionary of key-value. Supporte keys are:
    Keywords:
        authority   (string)
        title       (string)
        srs_name    (Name of spatial reference system)
        srs_epsg    (EPSG code of spatial reference system)
        srs_wkt     (WKT of spatial reference system)
        time_start  (datetime)
        time_end    (datetime)
        time_step   (datetime)
        time        (datetime)
        bbox        (tuple)
        quality     (Quality enum)
        resolution  (float)
    '''

    keys = [
            "authority",
            "title",
            "srs_name",
            "srs_epsg",
            "srs_wkt",
            "time_start",
            "time_end",
            "time_step",
            "time",
            "bbox",
            "quality",
            "resolution",
            ]

    def __init__(self):
        import dateutil.parser
        self.values = {
            "authority" : "SHOP",
            "title"     : "",
            "srs_name"  : "",
            "srs_epsg"  : -1,
            "srs_wkt"   : "",
            "time_start": None, # datetime.datetime.now(),
            "time_end"  : None, # datetime.datetime.now(),
            "time_step" : None, # datetime.datetime.now(),
            "time"      : datetime.datetime.now(),
            "bbox"      : None,
            "quality"   : Quality.high,
            "resolution": 1.0e-9,
        }

    def __setitem__(self, key, value):
        if key in KMLMetaData.keys:
            self.values[key] = value
        else:
            raise ValueError("Invalid key: %s not in %s" % (key, KMLMetaData.keys))

    def __getitem__(self, key):
        return self.values[key]
