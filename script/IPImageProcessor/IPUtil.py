#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from __future__ import absolute_import
from __future__ import print_function

import calendar
import datetime
import dateutil.parser
import re

#----------------------------------------------------
#    Fonctions d'utilitaires de date-epoch
#----------------------------------------------------
def getDatetimeFromEpoch(epoch):
    """
    Retourne une date UTC à partir d'une epoch
    """
    return datetime.datetime.utcfromtimestamp( int(epoch) )

def getEpochFromDatetime(date):
    """
    Retourne une epoch d'une date UTC
    http://stackoverflow.com/questions/255035/converting-datetime-to-posix-time/5872022#5872022
    """
    try:
        return calendar.timegm(date.timetuple())
    except AttributeError:
        return getEpochFromDatetime( dateutil.parser.parse(date) )

#----------------------------------------------------
#    Fonctions utilitaires degré dms à degré dec
#----------------------------------------------------
def degMinSec2degDec(dms):
    expd = r'(?P<d>[0-9]{1,2})[:|°|d]'
    expm = r'(?P<m>[0-9]{1,2})[:|\']'
    exps = r'(?P<s>(?:\b[0-9]+(?:\.[0-9]*)?|\.[0-9]+\b))\"?'
    expn = r'(?P<n>[N|S|E|W|O])'
    exp = expd + expm + exps + expn
    m = re.match(exp, dms)

    vd = m.group('d')
    vm = m.group('m')
    vs = m.group('s')
    vn = m.group('n')
    v = float(vd) + float(vm)/60 + float(vs)/3600
    if vn in ['W', 'O', 'S']: v = -v
    return v

def degDec2degMinSec(deg):
    neg = False
    if (deg < 0.0):
        neg = True
        deg = -deg

    d   = int(deg)
    deg = deg - d
    m = int(deg*60)
    deg = deg - m / 60.0
    s = deg*3600
    if neg: d = -d
    return "%d\u00b0%02d'%06.4f\"" %(d, m, s)

## {{{ http://code.activestate.com/recipes/499299/ (r3)
## http://www.velocityreviews.com/forums/t681464-get-item-from-set.html

class _CaptureEq:
    'Object wrapper that remembers "other" for successful equality tests.'
    def __init__(self, obj):
        self.obj = obj
        self.match = obj
    def __hash__(self):
        return hash(self.obj)
    def __eq__(self, other):
        result = (self.obj == other)
        if result:
            self.match = other
        return result

def get_equivalent(container, item):
    '''Gets the specific container element matched by: "item in container".

    Useful for retreiving a canonical value equivalent to "item".  For example, a
    caching or interning application may require fetching a single representative
    instance from many possible equivalent instances).

    >>> get_equivalent(set([1, 2, 3]), 2.0)             # 2.0 is equivalent to 2
    2
    >>> get_equivalent([1, 2, 3], 4, default=0)
    0
    '''
    t = _CaptureEq(item)
    if t in container:
        return t.match
    raise KeyError(item)

## end of http://code.activestate.com/recipes/499299/ }}}

if __name__ == '__main__':
    dms = "72:04:58.7570W"
    deg = degMinSec2degDec(dms)
    print("dms value", dms)
    print("dec value", deg)
    print(degDec2degMinSec(deg))
    print( getEpochFromDatetime('2009-06-14 16:00:00') )
