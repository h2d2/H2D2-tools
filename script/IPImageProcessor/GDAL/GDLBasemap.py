#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2015-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
from re import T
import sys

if __name__ == "__main__":
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..', '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

GDAL_ROOT  = ''     # Shall already be in the path if not frozen
SHARE_PATH = ''
if getattr(sys, 'frozen', False):
    fpath = sys._MEIPASS
    if os.name == 'nt':
        fpath = os.path.join(fpath, 'Library')
    fpath = os.path.join(fpath, 'share')
    if os.path.isdir(fpath):
        SHARE_PATH = fpath
    else:
        raise RuntimeError('Could not locate frozen Python share directory')
    GDAL_ROOT = sys._MEIPASS
    if os.name == 'nt' and sys.version_info >= (3, 8):
        print('add_dll_directory(%s)' % GDAL_ROOT)
        os.add_dll_directory(GDAL_ROOT)
elif os.path.exists(os.path.join(sys.prefix, 'conda-meta')):
    fpath = os.path.dirname(sys.executable)
    if os.path.basename(fpath) in ['bin']:
        fpath = os.path.dirname(fpath)
    if os.name == 'nt':
        fpath = os.path.join(fpath, 'Library')
    fpath = os.path.join(fpath, 'share')
    if os.path.isdir(fpath):
        SHARE_PATH = fpath
    else:
        raise RuntimeError('Could not locate Python share directory')
if not 'GDAL_DATA' in os.environ:
    os.environ['GDAL_DATA'] = os.path.join(SHARE_PATH, 'gdal')
if not 'PROJ_LIB' in os.environ:
    os.environ['PROJ_LIB'] = os.path.join(SHARE_PATH, 'proj')


from CTCommon.CTException import CTException

from osgeo import osr
from osgeo import gdal
from osgeo import ogr
from PIL import Image
import numpy as np

import collections.abc
import json
import logging
import os
import six
import subprocess
import tempfile

LOGGER = logging.getLogger("H2D2.Tools.ImageProcessor.GDAL")

# ---  Make gdal raise exceptions
gdal.UseExceptions()

def isIterable(arg):
    """
    Return True if arg is iterable but not a string
    https://stackoverflow.com/questions/1055360/how-to-tell-a-variable-is-iterable-but-not-a-string
    """
    return isinstance(arg, collections.abc.Iterable) and not isinstance(arg, six.string_types)

def do_call_or_raise(command, **kwargs):
    """
    Return stdout of the command, or raise
    """
    def decode(s, encodings=('ascii', 'utf8', 'latin1')):
        """
        https://stackoverflow.com/questions/269060/is-there-a-python-library-function-which-attempts-to-guess-the-character-encodin
        """
        try:
            return s.decode(decode.lastEncoding)
        except:
            for encoding in encodings:
                try:
                    r = s.decode(encoding)
                    decode.lastEncoding = encoding
                    return r
                except UnicodeDecodeError:
                    pass
        return s.decode('ascii', 'ignore')

    doLog = kwargs.pop('doLog', True)
    if isIterable(command):
        cmd = ' '.join(command)
    else:
        cmd = command
    o = []
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, **kwargs)
    if doLog:
        LOGGER.dump("Output for command: %s", cmd)
    while p.poll() is None:
        l = decode(p.stdout.readline().rstrip())
        if l:
            o.append(l)
            if doLog: LOGGER.dump('   %s' % l)
    for l in p.stdout.readlines():
        l = decode(l.rstrip())
        if l:
            o.append(l)
            if doLog: LOGGER.dump('   %s' % l)
    if doLog:
        LOGGER.dump("Done command %s", cmd)
    if p.returncode != 0:
        raise subprocess.CalledProcessError(p.returncode, cmd)
        raise CTException("Command '%s' returned non-zero exit status %i" % (cmd, p.returncode))

    return '\n'.join(o)

class GDLSpatialReference(osr.SpatialReference):
    def __init__(self):
        super().__init__()
        self.__swapAxis = False

    def getModeleurPath(self):
        try:
           import winreg
        except:
           raise CTException('No Modeleur path on a non windows platform')
        root = winreg.HKEY_LOCAL_MACHINE
        skey = r'SOFTWARE\INRS-Eau\Modeleur\1.0a07'
        key = winreg.OpenKey(root, skey)
        p, t = winreg.QueryValueEx(key, r'TargetDirPath')
        winreg.CloseKey(key)
        return p

    def getModeleurGeodataString(self, path, file, entry):
        p = os.path.join(path, 'geodata', file)
        f = open(p)
        t = '<%s>' % entry.strip()

        acc = []
        doAdd = False
        doBreak = False
        for l in f:
            l = l.split('#', 1)[0].strip()
            if (doAdd and '<>' in l):
                doBreak = True
                l = l.split('<>',1)[0].strip()
            if (t in l):
                doAdd = True
                l = l.split(t, 1)[1].strip()
            if (doAdd):
                acc.extend( l.split() )
            if (doBreak): break
        l = '+%s' % ' +'.join(acc)
        return l

    def getModeleurProj(self, str):
        f, e = str.split(':')
        p = self.getModeleurPath()
        s = self.getModeleurGeodataString(p, f, e)
        return s

    def decodeMTMProj(self, p):
        if (not '+proj=mtm' in p): return p
        toks = []
        for t in p.split():
            if ('=' in t):
                var, val = t.split('=')
                if (var == '+proj'):
                    toks.append('+proj=tmerc')
                    toks.append('+lat_0=0.0')
                    toks.append('+k=0.9999')
                    toks.append('+x_0=304800.0')
                    toks.append('+y_0=0.0')
                    toks.append('+units=m')
                elif (var == '+zone'):
                    z = int(val)
                    if (z < 3 or z > 10):
                        raise CTException('Invalid MTM Zone: %s' % val)
                    z = - (3.0*(z-1) + 52.5)
                    toks.append('+lon_0=%f' % z)
                else:
                    toks.append(t)
            else:
                toks.append(t)
        n = ' '.join(toks)
        LOGGER.info('MTM projection: %s' % p)
        LOGGER.info('    decoded to: %s' % n)
        return n

    def GetAuthority(self):
        cstype = 'GEOGCS' if self.IsGeographic() else 'PROJCS'
        return self.GetAuthorityName(cstype)

    def GetEPSGCode(self):
        cstype = 'GEOGCS' if self.IsGeographic() else 'PROJCS'
        return int(self.GetAuthorityCode(cstype))

    def GetName(self):
        cstype = 'GEOGCS' if self.IsGeographic() else 'PROJCS'
        return self.GetAttrValue(cstype)

    def ImportFromFileAndGuess(self, file):
        command = ('"%s"' % os.path.join(GDAL_ROOT, 'gdalsrsinfo'),
                   '-o wkt2',
                   '"%s"' % file)
        try:
            txt = do_call_or_raise(command)
            self.ImportFromWkt(txt)
        except CTException:
            self.ImportFromModeleur(file)
        return self.Validate()

    def ImportFromModeleur(self, file):
        LOGGER.info('GDLSpatialReference: ImportFromModeleur')
        fi = open(file, 'r')
        line = fi.readline().strip()
        if (line.split('=')[1] == 'cartographique'):
            line = fi.readline()
            line = fi.readline().strip()
            tag, line = line.split('=', 2)[1:]
            if tag == 'init':
                p = self.getModeleurProj(line)
                p = self.decodeMTMProj(p)
                err = self.ImportFromProj4(p)
                if (err != 0):
                    raise CTException('Invalid proj4 sequence: %s' % p)
        elif (line.split('=')[1] == 'geographique'):
            line = fi.readline()
            line = fi.readline().strip()
        elif (line.split('=')[1] == 'cartesienne'):
            line = fi.readline().strip()
            if (int(line.split('=')[1]) == 1):
                line = fi.readline()
                line = fi.readline()
            else:
                pass
        else:
            raise CTException('Error reading Modeleur projection')
        LOGGER.debug('GDLSpatialReference: spatial reference: %s' % self.ExportToPrettyWkt())

    def isLongLat(self):
        """
        isLongLat return True if the object is a geographic projection
        and if the axis order it longitude/latitude (east/north)

        _extended_summary_
        """
        return (self.IsGeographic() != 0 and
                self.GetAxisOrientation(None, 0) == osr.OAO_East and
                self.GetAxisOrientation(None, 1) == osr.OAO_North)

    def isLatLong(self):
        """
        isLongLat return True if the object is a geographic projection
        and if the axis order it latitude/longitude (north/east)

        _extended_summary_
        """
        return (self.IsGeographic() != 0 and
                self.GetAxisOrientation(None, 0) == osr.OAO_North and
                self.GetAxisOrientation(None, 1) == osr.OAO_East)

    def getSwapAxis(self):
        """
        getSwapAxis return True is the axis shall be swapped
        """
        return self.__swapAxis

    def setSwapAxis(self, doSwap):
        """
        setSwapAxis if the axis shall be swapped
        """
        self.__swapAxis = doSwap

    swapAxis = property(getSwapAxis, setSwapAxis)

    def Validate(self):
        err = osr.SpatialReference.Validate(self)
        if err == 5: raise CTException('The SRS is not well formed')
        if err == 7: raise CTException('The SRS is well formed, but contains non-standard PROJECTION[] values')
        return err

class GDLCoordinateTransformation(osr.CoordinateTransformation):
    def __init__(self, srcCS, tgtCS):
        osr.CoordinateTransformation.__init__(self, srcCS, tgtCS)
        self.srcCS = srcCS
        self.tgtCS = tgtCS

    def getSourceCS(self):
        return self.srcCS

    def getTargetCS(self):
        return self.tgtCS

    def TransformPoint(self, x, y):
        if self.srcCS.swapAxis:
            x_, y_, z_ = super().TransformPoint(y, x)
        else:
            x_, y_, z_ = super().TransformPoint(x, y)
        if self.tgtCS.swapAxis:
            return y_, x_
        else:
            return x_, y_

    def TransformPoints(self, X, Y):
        if self.srcCS.swapAxis:
            XY = np.stack((Y, X), axis=1)
            r = super().TransformPoints(XY)
        else:
            XY = np.stack((X, Y), axis=1)
            r = super().TransformPoints(XY)
        XYZ = np.asarray(r)
        if self.tgtCS.swapAxis:
            XYZ[:,[1,0]] = XYZ[:,[0,1]]
        return XYZ[:,0], XYZ[:,1]

class GDLGeoTransform:
    """
    Affine transformation between pixel/line (P,L) raster space,
    and projection coordinates (Xp,Yp) space.

    Fetches the coefficients for transforming between pixel/line (P,L) raster space,
    and projection coordinates (Xp,Yp) space.
       Xp = padfTransform[0] + P*padfTransform[1] + L*padfTransform[2];
       Yp = padfTransform[3] + P*padfTransform[4] + L*padfTransform[5];
    In a north up image, padfTransform[1] is the pixel width, and padfTransform[5]
    is the pixel height. The upper left corner of the upper left pixel is at
    position (padfTransform[0],padfTransform[3]).

    The default transform is (0,1,0,0,0,1) and should be returned even when
    a CE_Failure error is returned, such as for formats that don't support
    transformation to projection coordinates.
    """
    def __init__(self, window = (0,0,1,1), viewport=(0,0,1,1)):
        self.window   = window
        self.viewport = viewport
        self.setupTransformation()

    def getGeoTransform(self):
        return self.gt

    def getViewport(self):
        return self.viewport

    def getViewportHeight(self):
        return self.viewport[3]-self.viewport[1]+1

    def getViewportWidth(self):
        return self.viewport[2]-self.viewport[0]+1

    def getViewportSize(self):
        return (self.getViewportWidth(), self.getViewportHeight())

    def getWindow(self):
        return self.window

    def setGeoTransform(self, gt):
        self.gt = gt

    def setWindow(self, window):
        self.window = window
        self.setupTransformation()

    def setViewportFromSize(self, size, dpi):
        self.viewport = (0, 0, int(size[0]*dpi+0.5)-1, int(size[1]*dpi+0.5)-1)
        ##ajuster window au rapport de dim
        self.setupTransformation()

    def resizeWindowToViewport(self, maximize = True):
        bbox = [float(x) for x in self.getWindow()]
        size = [float(x) for x in self.getViewportSize()]
        xmed, ymed = (bbox[2]+bbox[0])/2, (bbox[3]+bbox[1])/2
        if maximize:
            ratio = max( (bbox[2]-bbox[0])/size[0], (bbox[3]-bbox[1])/size[1] )
        else:
            ratio = min( (bbox[2]-bbox[0])/size[0], (bbox[3]-bbox[1])/size[1] )
        bw = ratio * size[0] / 2
        bh = ratio * size[1] / 2
        bbox = (xmed-bw, ymed-bh,  xmed+bw, ymed+bh)
        self.setWindow(bbox)

    def setViewport(self, viewport):
        self.viewport = viewport
        self.setupTransformation()

    def setupTransformation(self):
        ll = gdal.GCP(self.window[0], self.window[1], 0.0, self.viewport[0], self.viewport[3], 'll', '%i' % 1)
        lr = gdal.GCP(self.window[2], self.window[1], 0.0, self.viewport[2], self.viewport[3], 'lr', '%i' % 2)
        ur = gdal.GCP(self.window[2], self.window[3], 0.0, self.viewport[2], self.viewport[1], 'ur', '%i' % 3)
        ul = gdal.GCP(self.window[0], self.window[3], 0.0, self.viewport[0], self.viewport[1], 'ul', '%i' % 4)
        self.gt = gdal.GCPsToGeoTransform( [ul, ur, lr, ll])

    def toPixelLine(self, x, y):
        a0 = self.gt[1]
        b0 = self.gt[2]
        c0 = self.gt[4]
        d0 = self.gt[5]
        dj = 1.0 / (a0*d0 - b0*c0)
        a1 =  dj*d0
        b1 = -dj*b0
        c1 = -dj*c0
        d1 =  dj*a0
        x_ = x - self.gt[0]
        y_ = y - self.gt[3]
        p = a1*x_ + b1*y_
        l = c1*x_ + d1*y_
        return int(p+0.5), int(l+0.5)

    def toProjCoord(self, p, l):
        x = self.gt[0] + p*self.gt[1] + l*self.gt[2]
        y = self.gt[3] + p*self.gt[4] + l*self.gt[5]
        return x, y

#---------------------------------------------------------------
#---------------------------------------------------------------
class GDLGeoImage:
    def __init__(self,
                 file = '',
                 driver = 'MEM',
                 toSpacialReference = None,
                 toGeoTransform     = None,
                 backgroundColor    = [255,255,255,255]):   # full white
        """
        Create a GDAL/OGR object to PIL image interface
        INPUT:
            file:               if present, read from file
            toSpacialReference: target spacial reference (see osr)
            toGeoTransform:     target Geo transform (see osr)
            backgroundColor     Background color (RGBA triplet)
        """
        mode = 'r'
        if file and file.strip() == '':
            if driver == 'MEM':
                mode = 'w'
            else:
                raise ValueError("Filename can only be empty with driver set to MEM")
        if (not toGeoTransform and not toSpacialReference):
            mode = 'r'
        elif (toGeoTransform and toSpacialReference):
            mode = 'w'
        else:
            raise ValueError("Either both toGeoTransform and toSpacialReference, or None")

        self.toSpacialReference = toSpacialReference
        self.toGeoTransform     = toGeoTransform
        self.backgroundColor    = backgroundColor
        self.driver = driver
        self.file   = file
        self.mode   = mode
        self.tgt_ds = None

    def __del__(self):
        self.tgt_ds = None

    def __openDS(self):
        if self.tgt_ds: raise RuntimeError('Datastore already open')
        if self.mode == 'r':
            self.__openForRead()
        else:
            self.__openForWrite()

    def __openForWrite(self):
        w = self.toGeoTransform.getViewportWidth()
        h = self.toGeoTransform.getViewportHeight()
        driver = gdal.GetDriverByName(self.driver)
        tgt_ds = driver.Create(self.file, w, h, 4, gdal.GDT_Byte)
        if self.toSpacialReference: tgt_ds.SetProjection  (self.toSpacialReference.ExportToWkt())
        if self.toGeoTransform:     tgt_ds.SetGeoTransform(self.toGeoTransform.getGeoTransform())
        self.toSR = self.toSpacialReference
        self.toGT = self.toGeoTransform
        self.tgt_ds = tgt_ds
        for i in range(self.tgt_ds.RasterCount):
            c = self.backgroundColor[i]
            b = self.tgt_ds.GetRasterBand(i+1)
            b.SetNoDataValue(c)
            b.Fill(c)

    def __openForRead(self):
        """
        Open the file for reading, copy to memory dataset
        """
        src_ds = gdal.Open(self.file, gdal.gdalconst.GA_ReadOnly)
        self.toSR = src_ds.GetProjection  ()
        self.toGT = src_ds.GetGeoTransform()
        driver = gdal.GetDriverByName('MEM')
        mem_ds = driver.CreateCopy('', src_ds, 0)
        self.tgt_ds = mem_ds

    # ---------------
    # OGR - Vector
    # ---------------
    def addOgr(self, drivername, filename, layers=[], default_color=[0,0,0,255]):
        def addOneOgrLayer(self, layer, default_color):
            layer.ResetReading()
            fids = [ layer.GetNextFeature().GetFID() for i in range(layer.GetFeatureCount()) ]
            layer.ResetReading()
            for id in fids:
                layer.SetAttributeFilter( "FID = %d" % id )
                # ---  Extract color from Style
                # Very fragile, works with KML styles
                # kml should add attributes about values and color
                try:
                    f = layer.GetNextFeature()
                    style_lnk = f.GetStyleString()
                    clr_abgr  = style_lnk[-8:]
                    a, b, g, r = [ int(clr_abgr[i:i+2], 16) for i in range(0, len(clr_abgr), 2) ]
                    color = [r, g, b, a]
                except:
                    color = default_color
                # ---  Rasterize feature
                err = gdal.RasterizeLayer(self.tgt_ds,
                                         (1, 2, 3, 4),          # all 4 bands
                                          layer,
                                          options = ['ALL_TOUCHED=TRUE'],
                                          burn_values=color)    #, options=["BURN_VALUE_FROM=Z"])

        # ---  Check whether file exists
        if not os.path.isfile(filename):
            raise IOError('File not found: %s' % str(filename))

        # ---  Open the driver
        dr = ogr.GetDriverByName(drivername)
        if (not dr):
            raise RuntimeError("Invalid OGR driver name: %s"  % drivername)

        # ---  Open the source datastore
        ds = dr.Open(filename)
        if (not ds):
            raise RuntimeError("Could not open OGR DataStore: %s"  % filename)

        # ---  Open target datastore
        try:
            self.__openDS()
        except RuntimeError as e:
            raise RuntimeError("Adding multiple bands is not supported") from e

        # ---  Rasterize the layers
        if (type(layers) == type(' ')):
            addOneOgrLayer(self, layers, default_color=default_color)
        elif (len(layers) > 0):
            for name in layers:
                l = ds.GetLayer(name)
                addOneOgrLayer(self, l, default_color=default_color)
        else:
            for l in ds:
                addOneOgrLayer(self, l, default_color=default_color)

    def addShapeFile(self, filename, layers=[], default_color=[0,0,0,255]):
        self.addOgr('ESRI Shapefile', filename, layers, default_color=default_color)

    def addMapInfoFile(self, filename, layers=[], default_color=[0,0,0,255]):
        self.addOgr('MapInfo File', filename, layers, default_color=default_color)

    def addDXFFile(self, filename, layers=[], default_color=[0,0,0,255]):
        self.addOgr('DXF', filename, layers, default_color=default_color)

    def addKMLFile(self, filename, layers=[], default_color=[0,0,0,255]):
        self.addOgr('LIBKML', filename, layers, default_color=default_color)

    def addOSMFile(self, filename, layers=[], default_color=[0,0,0,255]):
        self.addOgr('OSM', filename, layers, default_color=default_color)

    # ---------------
    # GDAL - Raster
    # ---------------
    def addGdal(self, drivername, filename, band = None, fileSpacialReference = None):
        def addOneGdalBand(self, ds, iband=1):
            bsrc = ds.GetRasterBand(iband)
            dsrc = bsrc.ReadAsArray()
            ctbl = bsrc.GetColorTable()
            if ctbl:
                centry = ctbl.GetColorEntry(0)      # get first entry for len
                for ic in range(len(centry)):
                    cmap = [ ctbl.GetColorEntry(i)[ic] for i in range(ctbl.GetCount()) ]
                    cmap = np.array(cmap)
                    data = cmap[dsrc]
                    bdst = self.tgt_ds.GetRasterBand(ic)
                    bdst.WriteArray(data)
            else:
                dmin = np.iinfo(dsrc.dtype).min
                dmax = np.iinfo(dsrc.dtype).max
                dscl = 255.0 / (dmax - dmin)
                dsrc = dsrc.astype(np.float32)
                data = (dsrc - dmin) * dscl
                data = data.astype(np.uint8)
                for ib in range(1,4):
                    bdst = self.tgt_ds.GetRasterBand(ib)
                    bdst.WriteArray(data)

        # ---  Check whether file exists
        if not os.path.isfile(filename):
            raise IOError('File not found: %s' % str(filename))

        # ---  Open target datastore
        try:
            self.__openDS()
        except RuntimeError as e:
            raise RuntimeError("Adding multiple bands is not supported") from e

        # ---  Re-project
        srcprf, tgtprf, tgttif = '', '', ''
        if fileSpacialReference:
            fd, srcprf = tempfile.mkstemp(suffix='_src.prf', prefix='__tmp_gdalwarp_result_', text=True)
            f = os.fdopen(fd, 'w')
            f.write(fileSpacialReference.ExportToWkt ())
            f.close()
        if True:
            fd, tgtprf = tempfile.mkstemp(suffix='_tgt.prf', prefix='__tmp_gdalwarp_result_', text=True)
            f = os.fdopen(fd, 'w')
            f.write(self.tgt_ds.GetProjectionRef())
            f.close()
        if True:
            fd, tgttif = tempfile.mkstemp(suffix='_tgt.tif', prefix='__tmp_gdalwarp_result_', text=False)
            f = os.fdopen(fd, 'w')
            f.close()
        #nodata = ' '.join([ '%d' % c for c in self.backgroundColor])
        command = ( '"%s"' % os.path.join(GDAL_ROOT, 'gdalwarp'),
                    '-s_srs "%s"' % srcprf if fileSpacialReference else '',
                    '-t_srs "%s"' % tgtprf,
                    '-te %f %f %f %f' % self.toGT.getWindow(),
                    '-ts %d %d' % (self.tgt_ds.RasterXSize, self.tgt_ds.RasterYSize),
                    '-r bilinear',
                    '-overwrite',
                    #'-srcnodata "%s"' % nodata,
                    #'-dstnodata "%s"' % nodata,
                    #'-dstalpha',
                    '"%s"' % filename,
                    '"%s"' % tgttif)
        do_call_or_raise(command)

        # ---  Open the source datastore
        ds = gdal.Open(tgttif)
        if (not ds):
            raise RuntimeError("Could not open OGR DataStore: %s"  % filename)

        # ---  Copy the bands
        if band:
            addOneGdalBand(self, ds, band)
        else:
            addOneGdalBand(self, ds, 1)

        # ---  Close the datastore
        ds = None   # https://www.gdal.org/gdal_tutorial.html

        # ---  Cleanup
        for p in [srcprf, tgtprf, tgttif]:
            try:
                if p:
                    os.remove(p)
            except Exception as e:
                pass

    def addGeoTIFF(self, filename, bands=[], fileSpacialReference = None):
        self.addGdal('GTiff', filename, bands, fileSpacialReference)

    # ---------------
    # PIL - IO
    # ---------------
    def exportAsPILImage(self):
        w = self.tgt_ds.RasterXSize
        h = self.tgt_ds.RasterYSize
        imgs = []
        for i in range(1, self.tgt_ds.RasterCount+1):
            b = self.tgt_ds.GetRasterBand(i)
            r = b.ReadRaster() # (0, 0, w, h)
            a = np.asarray(r).reshape(h, w)
            img = Image.fromarray(a, 'L')
            imgs.append(img)
        img = Image.merge('RGB', imgs[0:3])
        img.putalpha(imgs[3])
        return img

    def addPILImage(self, img):
        w = self.tgt_ds.RasterXSize
        h = self.tgt_ds.RasterYSize
        # could resize instead of raising exception
        if ((w, h) != img.size):
            raise RuntimeError("Incompatible img/viewport : %s / %s"  % ( (w,h), img.size))
        d = np.asarray(img)
        for i in range(self.tgt_ds.RasterCount):
            b = self.tgt_ds.GetRasterBand(i+1)
            d = img.getdata(i)
            a = np.array(d).reshape(h, w)
            b.WriteArray(a)

#---------------------------------------------------------------
#---------------------------------------------------------------
class GDLGeoJSON:
    def __init__(self,
                 toSpacialReference = None,
                 toGeoTransform     = None):
        """
        Create a GDAL/OGR object to PIL image interface
        INPUT:
            file:               if present, read from file
            toSpacialReference: target spacial reference (see osr)
            toGeoTransform:     target Geo transform (see osr)
            backgroundColor     Background color (RGBA triplet)
        """
        if not toGeoTransform and not toSpacialReference:
            pass
        elif toGeoTransform and toSpacialReference:
            pass
        else:
            raise ValueError("Either both toGeoTransform and toSpacialReference, or None")

        self.toSpacialReference = toSpacialReference
        self.toGeoTransform     = toGeoTransform
        self.data               = []

    def clearData(self):
        self.data = []

    def addOne(self, drivername, srcfnm, layers=[]):

        # ---  Check whether file exists
        if not os.path.isfile(srcfnm):
            raise IOError('File not found: %s' % str(srcfnm))

        # ---  Converts simple features data between file formats
        tgtprf, tgtfnm = '', ''
        if True:
            fd, tgtprf = tempfile.mkstemp(suffix='_tgt.prf', prefix='__tmp_ogr2ogr_result_', text=True)
            f = os.fdopen(fd, 'w')
            f.write(self.toSpacialReference.ExportToWkt())
            f.close()
        if True:
            fd, tgtfnm = tempfile.mkstemp(suffix='_tgt.geojson', prefix='__tmp_ogr2ogr_result_', text=True)
            os.close(fd)
            os.remove(tgtfnm)
        command = ( '"%s"' % os.path.join(GDAL_ROOT, 'ogr2ogr'),
                    '-f GeoJSON',
                    '-t_srs "%s"' % tgtprf,
                    '-clipdst %f %f %f %f' % self.toGeoTransform.getWindow(),
                    '"%s"' % tgtfnm,
                    '"%s"' % srcfnm)
        do_call_or_raise(command)

        # ---  Append to known datas
        with open(tgtfnm) as ifs:
            self.data.append( json.load(ifs) )

        # ---  Cleanup
        if tgtprf:
            try:
                os.remove(tgtprf)
            except Exception as e:
                pass
        if tgtfnm:
            try:
                #gdal.Unlink(tgtfnm)
                os.remove(tgtfnm)
            except Exception as e:
                pass

    def addShapeFile(self, filename, layers=[]):
        self.addOne('ESRI Shapefile', filename, layers,)

    def addMapInfoFile(self, filename, layers=[]):
        self.addOne('MapInfo File', filename, layers)

    def addDXFFile(self, filename, layers=[]):
        self.addOne('DXF', filename, layers)

    def addKMLFile(self, filename, layers=[]):
        self.addOne('LIBKML', filename, layers)

    def addOSMFile(self, filename, layers=[]):
        self.addOne('OSM', filename, layers)

    def getData(self):
        return self.data

from deprecation import deprecated
@deprecated(details='GDLGdalOgr has been renamed to GDLGeoImage')
class GDLGdalOgr(GDLGeoImage):
    pass

if __name__ == "__main__":
    # ---  Set base Spatial Reference
    wkt = '''
    GEOGCS["WGS 84",
    DATUM["WGS_1984",
        SPHEROID["WGS 84",6378137,298.257223563,
            AUTHORITY["EPSG","7030"]],
        AUTHORITY["EPSG","6326"]],
    PRIMEM["Greenwich",0,
        AUTHORITY["EPSG","8901"]],
    UNIT["degree",0.01745329251994328,
        AUTHORITY["EPSG","9122"]],
    AUTHORITY["EPSG","4326"]]
    '''

    sp_proj = GDLSpatialReference()
    sp_proj.ImportFromWkt(wkt)

    # ---  Sizes
    bbox = (-71.29, 46.73, -71.04, 46.89)   # in base coord
    size = (12, 8)                          # in inches

    # ---  Set up screen Transformation
    screen = GDLGeoTransform()
    screen.setWindow(bbox)
    screen.setViewportFromSize(size=size, dpi=80)

    # ---  Base map
    bmap = GDLGeoImage(toSpacialReference=sp_proj, toGeoTransform=screen, backgroundColor=[255,255,255,0])
#    bmap.addShapeFile('nhn_rhn_02ph000_shp_fr\RHN_02PH000_1_0_RH_RIVE_1.shp')
#    bmap.addShapeFile('data/LSP_RHN_FINAL_M_polyline.shp')
#    bmap.addKMLFile('a.kml')
    fic = r'E:\Projets_simulation\VilleDeQuebec\Beauport\Simulation\PIO\vq_bb_20161201-3_15.tif'
    bmap.addGeoTIFF(fic)
    bImg = bmap.exportAsPILImage()
    print(bImg.getbbox())
    print(bImg.getpixel((0,0)))
    bImg.save('basemap_test.png', 'PNG')
    del bmap

    #import matplotlib.pyplot as plt
    #ds = gdal.Open(fic)
    #print(ds.GetProjection())
    #print(ds.GetGeoTransform())
    #print(ds.RasterCount)
    #band = ds.GetRasterBand(3)
    #pixel_values = band.ReadAsArray()
    #print(pixel_values)
    #plt.imshow(pixel_values)
    #plt.colorbar()
    #plt.show()

    #img = Image.open('basemap_test.png', 'r')
    #bmap = GDLGeoImage('basemap_test.tif', 'GTiff', toSpacialReference=sp_proj, toGeoTransform=screen)
    #bmap.addPILImage(img)
    #del bmap


