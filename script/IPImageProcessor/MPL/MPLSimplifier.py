# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import bisect
import math

class Vertex:
    sortOnAngle = False

    def __init__(self, i, xy):
        self.x = xy[0]
        self.y = xy[1]
        self.a = 1.0e99     # Angle
        self.d = 1.0e99     # Distance to right
        self.l = None
        self.r = None
        self.i = i

    def setRightVertex(self, v):
        self.r = v
        self.__alfa()
        self.__dst2()

    def setLeftVertex(self, v):
        self.l = v
        self.__alfa()
        self.__dst2()

    def __alfa(self):
        ''' Angle to Pi, i.e. distance to a flat angle'''
        if (self.l and self.r):
            xls = self.l.x - self.x
            yls = self.l.y - self.y
            xrs = self.r.x - self.x
            yrs = self.r.y - self.y
            d = xls*xrs + yls*yrs   # dot product
            c = xrs*yls - xls*yrs   # cross product
            b = math.atan2(c, d)    # -pi, +pi
            a = -abs(b) + math.pi
        else:
            a = 1.0e99
        self.a = a

    def __dst2(self):
        if (self.l and self.r):
            dx = self.x - self.r.x
            dy = self.y - self.r.y
            dr = dx*dx + dy*dy
            dx = self.x - self.l.x
            dy = self.y - self.l.y
            dl = dx*dx + dy*dy
            d = min(dr, dl)
        else:
            d = 1.0e99
        self.d = d

    def __lt__(self, other):
        if Vertex.sortOnAngle:
            return ((self.a < other.a) or
                    (self.a == other.a and self.x <  other.x) or
                    (self.a == other.a and self.x == other.x) and (self.y < other.y))
        else:
            return ((self.d <  other.d) or
                    (self.d == other.d and self.x <  other.x) or
                    (self.d == other.d and self.x == other.x) and (self.y < other.y))

    def __str__(self):
        return 'Vertex idx: %5d,  idl: %5d,  idr: %5d,  a: %.3e,  d: %.3e' % (self.i, self.l.i if self.l else -1, self.r.i if self.r else -1, math.degrees(self.a), self.d)

class Poly:
    tolOnPosition = 1.0e-14

    def __init__(self, v):
        self.v = tuple(v)

    @staticmethod
    # http://stackoverflow.com/questions/3854047/approximate-comparison-in-python
    def approx_equal(x, y):
        return abs(x-y) <= 0.5 * Poly.tolOnPosition * abs(x + y)

    def __eq__(self, other):
        if len(self.v) != len(other.v): return False
        for p1, p2, in zip(self.v, other.v):
            if not Poly.approx_equal(p1.x, p2.x): return False
            if not Poly.approx_equal(p1.y, p2.y): return False
        return True

    def __str__(self):
        return 'Poly size: %5d,  p1: (%f, %f)' % (len(self.v), self.v[0].x, self.v[0].y)


class Simplifier:
    def __init__(self):
        self.setPnt = set()
        self.setPlg = []
        self.dbgLvl = 0

    def printDebug(self, lvl, l):
        if (self.dbgLvl >= lvl): print(l)

    def simplifyOnAngle(self, vs, tol):
        Vertex.sortOnAngle = True
        h = [ v for v in vs  if v.i >= 0]
        h.sort()
        nIn = len(h)

        while (len(h) > 3):
            if (self.dbgLvl >= 3):
                self.printDebug(3, '[')
                for v in h: self.printDebug(3, '%s' % v)
                self.printDebug(3, ']')
            v = h.pop(0)
            self.printDebug(2, '%s %d' % (v,len(h)))
            if (v.a < tol):
                h.pop( bisect.bisect_left(h, v.l) )
                h.pop( bisect.bisect_left(h, v.r) )
                v.i = -1
                v.l.setRightVertex(v.r)
                v.r.setLeftVertex (v.l)
                bisect.insort(h, v.l)
                bisect.insort(h, v.r)
                self.setPnt.add((v.x,v.y))
            else:
                break
        self.printDebug(1, 'MPLSimplifier.simplifyOnAngle: removed %d points on %d' % (nIn-len(h), nIn))

    def simplifyOnLength(self, vs, tol):
        Vertex.sortOnAngle = False
        h = [ v for v in vs  if v.i >= 0]
        h.sort()
        nIn = len(h)

        tol2 = tol*tol
        while (len(h) > 3):
            if (self.dbgLvl >= 3):
                self.printDebug(3, '[')
                for v in h: self.printDebug(3, '%s' % v)
                self.printDebug(3, ']')
            v = h.pop(0)
            self.printDebug(2, '%s %d' % (v,len(h)))
            if (v.d < tol2):
#                self.printDebug(2, 'il: %d  ir: %d' % (bisect.bisect_left(h, v.l), bisect.bisect_left(h, v.r)))
                h.pop( bisect.bisect_left(h, v.l) )
                h.pop( bisect.bisect_left(h, v.r) )
                v.i = -1
                v.l.setRightVertex(v.r)
                v.r.setLeftVertex (v.l)
                bisect.insort(h, v.l)
                bisect.insort(h, v.r)
                self.setPnt.add((v.x, v.y))
            else:
                break
        self.printDebug(1, 'MPLSimplifier.simplifyOnLength: removed %d points on %d' % (nIn-len(h), nIn))

    def simplifyOnSet(self, vs):
        nIn = len(vs)

        n = 0
        for v in vs:
            if (v.x,v.y) in self.setPnt:
                v.i = -1
                if (v.l): v.l.setRightVertex(v.r)
                if (v.r): v.r.setLeftVertex (v.l)
                n += 1

        if ((nIn-n) > 3 and vs[0].i == -1):     # first point was removed
            vb = next((v for v in vs if v.i >= 0), None)            # begin
            assert (vb and vb.r is not None)
            ve = next( (v for v in reversed(vs) if v.i >= 0), None) # end
            assert (ve and ve.l is not None)
            vr = Vertex(len(vs)-1, (vb.x, vb.y))
            ve.setRightVertex(vr)
            vr.setLeftVertex (ve)
            vr.setRightVertex(None)
            vs[-1] = vr

        self.printDebug(1, 'MPLSimplifier.simplifyOnSet: removed %d points on %d' % (n, nIn))

    def simplify(self, vertices, tol_angle=math.radians(5), tol_length=0.1, tol_position=1.0e-14):
        Poly.tolOnPosition = tol_position

        # ---  Local list of vertices
        vs = [ Vertex(i, v) for i, v in enumerate(vertices) ]

        # ---  Detect identical polygons
        try:
            i = self.setPlg.index( Poly(vs) )
            self.printDebug(0, 'MPLSimplifier.simplify: Identical polygon detected: %d' % i)
            vs = self.setPlg[i].v
            r = [ (v.x, v.y) for v in vs[:-1] if v.i >= 0 ]
            return r
        except ValueError:
            pass

        # ---  Close poly and build connections
        vs.append( Vertex(len(vertices),vertices[0]) )
        for v, vr in zip(vs[0:-1], vs[1:]):
            v. setRightVertex(vr)
        for vl, v in zip(vs[0:-1], vs[1:]):
            v. setLeftVertex(vl)
        nIn = len(vs)
        self.dbgLvl = 0 # if (nIn == 48) else False
        self.printDebug(2, nIn)

        # ---  Simplify
        self.simplifyOnSet (vs)

        n0, n1, n2 = (-1, 0, 0)
        while ((n1-n0) > 0 or (n2-n1) > 0):
            n0 = len(self.setPnt)
            self.simplifyOnAngle (vs, tol_angle)
            n1 = len(self.setPnt)
            self.simplifyOnLength(vs, tol_length)
            n2 = len(self.setPnt)

        self.setPlg.append( Poly(vs[:-1]) )
        r = [ (v.x, v.y) for v in vs[:-1] if v.i >= 0 ]
        self.printDebug(1, 'MPLSimplifier.simplify: removed %d points on %d' % (nIn-len(r), nIn))
        return r


def main():
    r = 2.0
    c = [ (r*math.cos(math.radians(a)), r*math.sin(math.radians(a))) for a in range(0, 360, 10) ]
    print(len(c))
    for v in c: print('%6.3f %6.3f' % v)
    s = Simplifier()
    v = s.simplify(c, tol_angle=math.radians(10.0))
    print(len(v))
    for v_ in v: print('%6.3f %6.3f' % v_)

if __name__ == '__main__':
    main()
