# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import bisect
import math

class Vertex:
    tolOnPosition = 1.0e-14

    def __init__(self, xy):
        self.x = xy[0]
        self.y = xy[1]

    def getCoordinates(self):
        return (self.x, self.y)

    @staticmethod
    # http://stackoverflow.com/questions/3854047/approximate-comparison-in-python
    def approx_equal(x, y):
        return abs(x-y) <= 0.5 * Vertex.tolOnPosition * abs(x + y)

    def __eq__(self, other):
        if not Vertex.approx_equal(self.x, other.x): return False
        if not Vertex.approx_equal(self.y, other.y): return False
        return True

    def __lt__(self, other):
        if Vertex.approx_equal(self.x, other.x):
            if Vertex.approx_equal(self.y, other.y):
                return False
            else:
                return self.y < other.y
        else:
            return self.x < other.x

    def __str__(self):
        return '(%18.14f, %18.14f)' % (self.x, self.y)

class Sticher:
    '''
    Points with almost same coordinates (within tolerance) are snapped together
    '''
    def __init__(self):
        self.setPnt = []

    def stich(self, vertices, tol_position=1.0e-14):
        Vertex.tolOnPosition = tol_position

        r = []
        for v_ in vertices:
            v = Vertex(v_)
            i = bisect.bisect_left(self.setPnt, v)
            if (i < len(self.setPnt) and v == self.setPnt[i]):
                v = self.setPnt[i]
            else:
                bisect.insort_left(self.setPnt, v)
            r.append( v.getCoordinates() )

        return r

def main():
    r = 2.0
    c1 = [ (r*math.cos(math.radians(a)), r*math.sin(math.radians(a))) for a in range(0, 360, 10) ]
    r = r + 1.0e-15
    c2 = [ (r*math.cos(math.radians(a)), r*math.sin(math.radians(a))) for a in range(0, 360, 10) ]
    print(len(c1))
    for v1, v2 in zip(c1, c2): print('%s %s' % (v1, v2))
    s = Sticher()
    v = s.stich(c1, tol_position=1.0e-10)
    print(' ----------')
    v = s.stich(c2, tol_position=1.0e-10)
    for v1 in v: print(v1)

if __name__ == '__main__':
    main()