#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == '__main__':
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '../..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

from CTCommon.XMLWriter   import XMLWriter
from IPImageProcessor.KML import KMLMeta
from IPImageProcessor.MPL import MPLUtil

import collections.abc
import datetime
import enum
import math
import shapefile
import struct

copyright = \
'''\
<!-- ************************************************************************ -->
<!--  mpl2shp 18.04 -->
<!--  Copyright (c) INRS 2014-2018 -->
<!--  Institut National de la Recherche Scientifique (INRS) -->
<!--  -->
<!--  Distributed under the GNU Lesser General Public License, Version 3.0. -->
<!--  See accompanying file LICENSE.txt. -->
<!-- ************************************************************************ -->

<!-- File generated automatically, changes will be lost on next update -->
'''

isoType = enum.Enum('isoType', ('Isoline', 'Isovalue'))

# def getStyleName(color = 'ffffffff'):
#     return 'SHOP_Style_%s' % color

class SLDWriter(XMLWriter):
    ### http://docs.geoserver.org/stable/en/user/styling/sld/reference/sld.html
    def __init__(self, meta, fname = None):
        self.cntrs = {}
        self.pointColorUniquer = set()
        self.lineColorUniquer  = set()
        self.polyColorUniquer  = set()
        self.layerCntr = 0

        self.meta = meta
        sldname = fname + '.sld'
        XMLWriter.__init__(self, sldname)

    def writeLineColors(self, color):
        if   isinstance(color, str):
            if color in self.lineColorUniquer: return
            self.lineColorUniquer.add(color)

            c, a = MPLUtil.MPLUtil.kml2html(color)

            self.openElem('sld:Rule')
            self.writeTag('sld:Name', 'SHOP_LineColor_%s' % color)

            self.openElem('ogc:Filter')
            self.openElem('ogc:PropertyIsEqualTo')
            self.writeTag('ogc:PropertyName', 'STYLE')
            self.writeTag('ogc:Literal', 'SHOP_LineColor_%s' % color)
            self.closeElem()    # ogc:PropertyIsEqualTo
            self.closeElem()    # ogc:Filter
            
            self.openElem('sld:LineSymbolizer')
            self.openElem('sld:Stroke')
            self.writeTag('sld:CssParameter name="stroke"', '%s' % c)
            self.writeTag('sld:CssParameter name="fill-opacity"', '%3.2f' % (a/255.0))
            self.closeElem()    # Stroke
            self.closeElem()    # LineSymbolizer
            self.closeElem()    # Rule
        elif isinstance(color, collections.abc.Iterable):
            for clr in color:
                self.writeLineColors(clr)
        else:
            raise ValueError('Expected one of (str, unicode) or an iterable, instead got %s' % type(color))

    def writePolyColors(self, color):
        if   isinstance(color, str):
            if color in self.polyColorUniquer: return
            self.polyColorUniquer.add(color)

            a, b, g, r = ( color[i:i+2] for i in range(0, len(color), 2) )

            self.openElem('sld:Rule')
            self.writeTag('sld:Name', 'SHOP_PolyColor_%s' % color)

            self.openElem('ogc:Filter')
            self.openElem('ogc:PropertyIsEqualTo')
            self.writeTag('ogc:PropertyName', 'STYLE')
            self.writeTag('ogc:Literal', 'SHOP_PolyColor_%s' % color)
            self.closeElem()    # ogc:PropertyIsEqualTo
            self.closeElem()    # ogc:Filter

            self.openElem('sld:PolygonSymbolizer')
            self.openElem('sld:Fill')
            self.writeTag('sld:CssParameter name="fill"', '#%s%s%s' % (r, g, b))
            self.writeTag('sld:CssParameter name="fill-opacity"', '%3.2f' % (int(a, 16) / 255.0))
            self.closeElem()    # Fill
            self.closeElem()    # LineSymbolizer
            self.openElem('sld:LineSymbolizer')
            self.openElem('sld:Stroke')
            self.writeTag('sld:CssParameter name="stroke"', '#%s%s%s' % (r, g, b))
            self.writeTag('sld:CssParameter name="fill-opacity"', '%3.2f' % (int(a, 16) / 255.0))
            self.closeElem()    # Stroke
            self.closeElem()    # LineSymbolizer
            self.closeElem()    # Rule
        elif isinstance(color, collections.abc.Iterable):
            for clr in color:
                self.writePolyColors(clr)
        else:
            raise ValueError('Expected one of (str, unicode) or an iterable, instead got %s' % type(color))

    def openLayer(self, title):
        self.layerCntr += 1
        self.openElem('sld:NamedLayer')
        self.writeTag('sld:Name', '%d' % self.layerCntr)
        self.openElem('sld:UserStyle')
        self.writeTag('sld:Name', title)
        self.writeTag('sld:Title', title)
        self.openElem('sld:FeatureTypeStyle')

    def closeLayer(self):
        self.closeElem()    # FeatureTypeStyle
        self.closeElem()    # UserStyle
        self.closeElem()    # NamedLayer
        
    def writeDocumentHead(self):
        m = self.meta
        name = m['authority']
        if m['title']: name = ' - '.join((name, m['title']))
        if m['time']:  name = ' - '.join((name, '%sZ' % m['time'].isoformat()))

        descr  = ''
        if m['authority']: descr += 'Authority: %s\n' % m['authority']
        if m['title']:     descr += 'Title:     %s\n' % m['title']
        if m['time']:      descr += 'Time       %sZ\n'% m['time'].isoformat()

        self.writeTag('sld:Name', name)
        self.writeTag('sld:Title', m['title'])
        self.writeTag('sld:Abstract', descr)

    def writeHeader(self):
        XMLWriter.writeHeader(self, copyright)
        self.openElem ('sld:StyledLayerDescriptor',
                       '\n'.join(['',
                            '    version="1.0.0"',
                            '    xmlns="http://www.opengis.net/ogc"',
                            '    xmlns:sld="http://www.opengis.net/sld"',
                            '    xmlns:ogc="http://www.opengis.net/ogc"',
                            '    xmlns:gml="http://www.opengis.net/gml"',
                            '    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"',
                            '    xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"',
                            ]))
        self.writeDocumentHead()

class SHPWriter:
    zOffset = 2.0

    patchPartType = {
        'Triangle Strip': 0 ,
        'Triangle Fan'  : 1 ,
        'Outer Ring'    : 2 ,
        'Inner Ring'    : 3 ,
        'First Ring'    : 4 ,
        'Ring'          : 5 ,
    }

    # http://pygis.blogspot.ca/2012/10/pyshp-attribute-types-and-point-files.html
    recordType = {
       'string': 'C',   # is ASCII characters
       'int'   : 'N',   # is a double precision integer limited to around 18 characters in length
       'date'  : 'D',   # is for dates in the YYYYMMDD format, with no spaces or hyphens between the sections
       'float' : 'F',   # is for floating point numbers with the same length limits as N
       'bool'  : 'L',   # is for logical data which is stored in the shapefile's attribute table as a short integer as a 1 (true) or a 0 (false).
       'time'  : '@',   # xBase extension
    }

    def __init__(self, meta, fname):
        self.meta = meta
        self.is_hd = meta['quality'] == KMLMeta.Quality.high
        self.fname = fname
        self.writer = shapefile.Writer(self.fname)
        self.fields = {}

    def __writeLine(self, l):
        pass
    def openElem(self, l, t = ''):
        pass
    def closeElem(self):
        pass

    def writeTag(self, tag, txt):
        self.__writeLine('<%s>%s</%s>' % (tag, txt, tag))

    def writeDocument(self, levels = [], colors = []):
        pass

    def writeHeader(self):
        pass

    def writeFooter(self, closeAll=False):
        self.writer.close()

    ##def asgLineCollectionFields(self,
    ##                            writeLevels=True,     defaultLevel=0.0,
    ##                            writeColors=True,     defaultColor='FF000000',
    ##                            writeTimestamp=False, defaultTimeStamp=datetime.datetime.now()):
    ##    if writeTimestamp: self.fields['TIMESTAMP'] = defaultTimeStamp
    ##    if writeLevels:    self.fields['VALUE'] = defaultLevel;
    ##    if writeColors:    self.fields['COLOR'] = defaultColor,
    ##    if writeColors:    self.fields['STYLE'] = getStyleName(self.fields['COLOR'])
    ##
    ##def asgPolyCollectionFields(self,
    ##                            writeLevels=True,     defaultLevel=0.0,
    ##                            writeTimestamp=False, defaultTimeStamp=datetime.datetime.now()):
    ##    if writeTimestamp: self.fields['TIMESTAMP'] = defaultTimeStamp
    ##    if writeLevels:    self.fields['V_MIN'] = defaultLevel
    ##    if writeLevels:    self.fields['V_MAX'] = defaultLevel
    ##    #if writeColors:    self.fields['COLOR'] = defaultColor,
    ##    #if writeColors:    self.fields['STYLE'] = getStyleName(self.fields['COLOR'])

    def writeGeometryAsLineCollection(self, collections, levels_, colors_, writeTimestamp=False):
        assert (levels_ is None or len(levels_) == 0 or len(collections) == len(levels_))
        assert (colors_ is None or len(colors_) == 0 or len(collections) == len(colors_))
        hasLevels = levels_ is not None and len(levels_) > 0
        hasColors = colors_ is not None and len(colors_) > 0

        if writeTimestamp: self.writer.field('TIMESTAMP', SHPWriter.recordType['time'],   size=8)
        if hasLevels:      self.writer.field('VALUE',     SHPWriter.recordType['float'],  size=18, decimal=6)
        if hasColors:      self.writer.field('COLOR',     SHPWriter.recordType['string'], size=18)
        if hasColors:      self.writer.field('STYLE',     SHPWriter.recordType['string'], size=23)

        tstmp = None
        if writeTimestamp:
            dt = self.meta['time']
            ndays = (dt.date() - datetime.date(2000, 1, 1)).total_seconds() / (24*3600) + 2451545   # http://en.wikipedia.org/wiki/Julian_day
            nsecs = dt.time().hour*3600 + dt.time().minute*60 +  + dt.time().second
            tstmp = struct.pack('ll', int(ndays), int(nsecs)*1000)
        levels = levels_ if hasLevels else [0]*len(collections)
        colors = colors_ if hasColors else ['']*len(collections)

        for collection, level, color in zip(collections, levels, colors):
            for poly in collection:
                plines = [ [ (pnt[0],pnt[1], level) for pnt in poly ] ]

                if (len(plines) > 0):
                    self.writer.linez(plines)
                    args = []
                    if hasColors: args = [color, 'SHOP_LineColor_%s' % color] + args
                    if hasLevels: args = [level] + args
                    if tstmp: args = [tstmp] + args
                    if args: self.writer.record(*args)

    def writeIsoLineAsLineCollection(self, collections, levels_, colors_, writeTimestamp=False):
        assert (levels_ is None or len(levels_) == 0 or len(collections) == len(levels_))
        assert (colors_ is None or len(colors_) == 0 or len(collections) == len(colors_))
        hasLevels = levels_ is not None and len(levels_) > 0
        hasColors = colors_ is not None and len(colors_) > 0

        if writeTimestamp: self.writer.field('TIMESTAMP', SHPWriter.recordType['time'],   size=8)
        if hasLevels: self.writer.field('VALUE', SHPWriter.recordType['float'],  size=18, decimal=6)
        if hasColors: self.writer.field('COLOR', SHPWriter.recordType['string'], size=18)
        if hasColors: self.writer.field('STYLE', SHPWriter.recordType['string'], size=23)

        tstmp = None
        if writeTimestamp:
            dt = self.meta['time']
            ndays = (dt.date() - datetime.date(2000, 1, 1)).total_seconds() / (24*3600) + 2451545   # http://en.wikipedia.org/wiki/Julian_day
            nsecs = dt.time().hour*3600 + dt.time().minute*60 +  + dt.time().second
            tstmp = struct.pack('ll', int(ndays), int(nsecs)*1000)
        levels = levels_ if hasLevels else [0]*len(collections)
        colors = colors_ if hasColors else ['']*len(collections)

        for collection, level, color in zip(collections, levels, colors):
            try:
                color = color[0]
            except:
                pass
            args = []
            if hasColors: args = [color, 'SHOP_LineColor_%s' % color] + args
            if hasLevels: args = [level] + args
            if tstmp: args = [tstmp] + args
        
            for poly in collection:
                plines = [ [ (pnt[0],pnt[1], level) for pnt in poly ] ]
                if len(plines) > 0:
                    self.writer.linez(plines)
                    if args: self.writer.record(*args)

    def writeIsoValueAsPolyCollection(self, collections, levels_, colors, writeTimestamp=False):
        assert (len(collections) == len(colors))
        assert (len(levels_) == len(colors)-1 or len(levels_) == len(colors)+1)
        if (len(levels_) == len(colors)-1):
            levels = [ -1.0e9 ]
            levels.extend(levels_)
            levels.append(1.0e9)
        else:
            levels = levels_

        if writeTimestamp: self.writer.field('TIMESTAMP', SHPWriter.recordType['time'],   size=8)
        self.writer.field('V_MIN', SHPWriter.recordType['float'],  size=18, decimal=6)
        self.writer.field('V_MAX', SHPWriter.recordType['float'],  size=18, decimal=6)
        self.writer.field('COLOR', SHPWriter.recordType['string'], size=18)
        self.writer.field('STYLE', SHPWriter.recordType['string'], size=23)

        tstmp = None
        if writeTimestamp:
            dt = self.meta['time']
            ndays = (dt.date() - datetime.date(2000, 1, 1)).total_seconds() / (24*3600) + 2451545   # http://en.wikipedia.org/wiki/Julian_day
            nsecs = dt.time().hour*3600 + dt.time().minute*60 +  + dt.time().second
            tstmp = struct.pack('ll', int(ndays), int(nsecs)*1000)

        for polygons, lmin, lmax, color in zip(collections, levels, levels[1:], colors):
            pgons = []
            types = []

            lmed = 0.5 * (lmin+lmax)
            for polygon in polygons:
                mpoly    = [ [ (pnt[0], pnt[1], lmed) for pnt in ring ] for ring in polygon if len(ring) > 0 ]
                tpoly    = [ SHPWriter.patchPartType['Inner Ring'] * len(mpoly) ]
                tpoly[0] =   SHPWriter.patchPartType['Outer Ring']
                pgons.extend(mpoly)
                types.extend(tpoly)

            if len(pgons) > 0:
                self.writer.multipatch(pgons, types)
                args = []
                args = [color, 'SHOP_PolyColor_%s' % color] + args
                args = [lmin, lmax] + args
                if tstmp: args = [tstmp] + args
                if args: self.writer.record(*args)

    def writeQuiverAsPolyCollection(self, collections, levels, colors, writeTimestamp=False):
        """
        Add a quiver
        """
        assert len(collections) == len(colors)

        if writeTimestamp: self.writer.field('TIMESTAMP', SHPWriter.recordType['time'],   size=8)
        self.writer.field('VNORM', SHPWriter.recordType['float'],  size=18, decimal=6)
        self.writer.field('VDEGR', SHPWriter.recordType['float'],  size=18, decimal=6)
        self.writer.field('COLOR', SHPWriter.recordType['string'], size=18)
        self.writer.field('STYLE', SHPWriter.recordType['string'], size=23)

        tstmp = None
        if writeTimestamp:
            dt = self.meta['time']
            ndays = (dt.date() - datetime.date(2000, 1, 1)).total_seconds() / (24*3600) + 2451545   # http://en.wikipedia.org/wiki/Julian_day
            nsecs = dt.time().hour*3600 + dt.time().minute*60 +  + dt.time().second
            tstmp = struct.pack('ll', int(ndays), int(nsecs)*1000)

        for polygon, color in zip(collections, colors):
            if len(polygon) == 0: continue

            pnts = [ (pnt[0], pnt[1]) for pnt in polygon ]
            self.writer.poly([pnts])
            dx = pnts[3][0] - 0.5*(pnts[0][0] + pnts[6][0])
            dy = pnts[3][1] - 0.5*(pnts[0][1] + pnts[6][1])
            v = math.hypot(dx, dy)
            a = math.degrees(math.atan2(dy, dx))
            args = []
            args = [color, 'SHOP_PolyColor_%s' % color] + args
            args = [v, a] + args
            if tstmp: args = [tstmp] + args
            if args: self.writer.record(*args)

    def writePathAsLineCollection(self, collections, levels, colors, writeTimestamp=None):
        """
        Add a LineCollection
        """
        assert len(collections) == len(colors)

        if writeTimestamp: self.writer.field('TIMESTAMP', SHPWriter.recordType['time'],   size=8)
        #self.writer.field('VNORM', SHPWriter.recordType['float'],  size=18, decimal=6)
        #self.writer.field('VDEGR', SHPWriter.recordType['float'],  size=18, decimal=6)
        self.writer.field('COLOR', SHPWriter.recordType['string'], size=18)
        self.writer.field('STYLE', SHPWriter.recordType['string'], size=23)

        tstmp = None
        if writeTimestamp:
            dt = self.meta['time']
            ndays = (dt.date() - datetime.date(2000, 1, 1)).total_seconds() / (24*3600) + 2451545   # http://en.wikipedia.org/wiki/Julian_day
            nsecs = dt.time().hour*3600 + dt.time().minute*60 +  + dt.time().second
            tstmp = struct.pack('ll', int(ndays), int(nsecs)*1000)

        for polygons, polycolors in zip(collections, colors):
            if not polygons: continue

            for polygon, color in zip(polygons, polycolors):
                if len(polygon) == 0: continue
                
                pnts = [ (pnt[0], pnt[1]) for pnt in polygon ]
                self.writer.line([pnts])

                args = []
                args = [color, 'SHOP_LineColor_%s' % color] + args
                # args = [v, a] + args
                if tstmp: args = [tstmp] + args
                if args: self.writer.record(*args)

def main():
    """
    Illustrate simple contour plotting, contours on an image with
    a colorbar for the contours, and labelled contours.
    From contour_demo.py.
    """
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt
    from IPImageProcessor.KML.KMLWriter import bivariate_normal

    matplotlib.rcParams['xtick.direction'] = 'out'
    matplotlib.rcParams['ytick.direction'] = 'out'

    delta = 1.0 # 0.025
    x = np.arange(-3.0, 3.0, delta)
    y = np.arange(-2.0, 2.0, delta)
    X, Y = np.meshgrid(x, y)
    Z1 = bivariate_normal(X, Y, 1.0, 1.0,-1.0,-1.0)
    Z2 = bivariate_normal(X, Y, 1.0, 1.0, 1.0, 1.0)
    # difference of Gaussians
    Z = 10.0 * (Z2 + Z1)

    plt.figure()
    CS = plt.contour(X, Y, Z)
    #CS = plt.contourf(X, Y, Z)
    plt.title('Simplest default no labels')

    meta = KMLMeta.KMLMetaData()
    meta['authority']= 'SHPWriter'
    meta['title']    = 'Test'
    meta['bbox']     = [ 2.0, -2.0, 3.0, -3.0 ]   # n, s, e, w
    meta['quality']  = KMLMeta.Quality.high

    poly = MPLUtil.MPLUtil.getPolylinesFromContourSet(CS)
    #poly = MPLUtil.MPLUtil.getPolygonsFromContourSet(CS)
    lvls = MPLUtil.MPLUtil.getLevelsFromContourSet(CS)
    clrs = MPLUtil.MPLUtil.getColorsFromContourSet(CS)
    print(len(clrs), clrs)

    writer = SHPWriter(meta, "test_shapfile")
    writer.writeHeader()
    writer.writeIsoLineAsLineCollection(poly, lvls, clrs)
    #writer.writeIsoValueAsPolyCollection(poly, lvls, clrs)
    writer.writeFooter()

    writer = SLDWriter(meta, "test_shapfile")
    writer.writeHeader()
    writer.openLayer('test')
    writer.writeLineColors(clrs)
    #writer.writePolyColors(clrs)
    writer.closeLayer()
    writer.writeFooter()

    plt.show()

if __name__ == '__main__':
    main()