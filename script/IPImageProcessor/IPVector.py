#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Creating and plotting vector images.
"""

from __future__ import absolute_import
from __future__ import print_function

# ---  Set-up as vector backend
import sys
if 'matplotlib' not in sys.modules:
    import matplotlib
    matplotlib.use('PS')
import matplotlib.pyplot

#------------------------------------------------------------------------

# ---  Common
from CTCommon.DTData   import *
from CTCommon.FEMeshIO import *
from CTCommon.FEMesh   import *

# ---  Plotter
from .IPUtil import *

# ---  GDAL
from .GDAL.GDLBasemap import *

# ---  KML
from .KML.KMLMeta   import *
from .KML.KMLWriter import *
from .KML.KMLTiles  import *

# --- ShapeFile
from .SHP.SHPWriter import *

# ---  Plotter
from .IPPlotter import *
