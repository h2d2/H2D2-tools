:: Terminal en UTF-8
chcp 65001

:: LD_LIBRARY_PATH=/opt/anaconda3/lib:$LD_LIBRARY_PATH
set ARGS=
::set ARGS=%ARGS% --log-level=DEBUG
::set ARGS=%ARGS% --debug=imports
set ARGS=%ARGS% --clean
set ARGS=%ARGS% --noconfirm
:: set ARGS=%ARGS% --distpath pyinstaller_dist
:: set ARGS=%ARGS% --workpath pyinstaller_build    :: Dont't work! My guess, they are not passed down to the spec files
pyinstaller %ARGS% H2D2-tools.spec
