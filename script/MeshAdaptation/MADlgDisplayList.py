#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from CTCommon import CTCheckListCtrl_v2

import sys
import wx

class MADlgDisplayList(wx.Dialog):
    def __init__(self, parent):
        self.cb_on_apply = None

        style = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER | wx.MAXIMIZE_BOX | wx.MINIMIZE_BOX
        wx.Dialog.__init__(self, parent, style = style)

        self.lst = CTCheckListCtrl.CTCheckListCtrl(self)
        self.btn_check   = wx.Button(self, -1, "Check All")
        self.btn_uncheck = wx.Button(self, -1, "Uncheck All")
        self.btn_apply   = wx.Button(self, -1, "Apply")

        self.__set_properties()
        self.__do_layout()

#        aTable = wx.AcceleratorTable([ (wx.ACCEL_CTRL, ord('A'), helpID),
#                                    ])
#        self.SetAcceleratorTable(aTable)

        self.Bind(wx.EVT_BUTTON, self.on_btn_check_all,   self.btn_check)
        self.Bind(wx.EVT_BUTTON, self.on_btn_uncheck_all, self.btn_uncheck)
        self.Bind(wx.EVT_BUTTON, self.on_btn_apply,  self.btn_apply)
        self.Bind(wx.EVT_CONTEXT_MENU, self.on_mnu_popup)

    def __set_properties(self):
        self.SetTitle("Display List")
        self.SetSize((800, 800))
        self.lst.InsertColumn(0, "Item")
        self.lst.InsertColumn(1, "")

    def __do_layout(self):
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(self.lst, 1, wx.ALL | wx.EXPAND, 0)
        sizer_1.Add(sizer_2,  0, wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(self.btn_check,   0, 0, 0)
        sizer_2.Add(self.btn_uncheck, 0, 0, 0)
        sizer_2.Add(self.btn_apply,   0, 0, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()

    def fillList(self, title, data, cb_on_apply):
        self.cb_on_apply = cb_on_apply

        for i in range( len(data) ):
            v, t, c = data[i]
            try:
                self.lst.SetItem(i, 0, '  %3i' % i)
            except:
                index = self.lst.InsertItem(sys.maxsize, '  %3i' % i)
            self.lst.SetItem(i, 1, '%s' % t)
            item = self.lst.GetItem(i)
            item.SetTextColour(c)
            self.lst.SetItem(item)
            self.lst.CheckItem(i, v)

        self.SetTitle("Display List: %s" % title)
        self.lst.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        self.lst.SetColumnWidth(1, wx.LIST_AUTOSIZE)

    def on_btn_apply(self, event):
        vis = []
        clr = []
        n = self.lst.GetItemCount()
        for i in range(n):
            v = self.lst.IsChecked(i)
            c = self.lst.GetItemTextColour(i)
            h = c.GetAsString(wx.C2S_HTML_SYNTAX)
            vis.append(v)
            clr.append(h)
        self.cb_on_apply(vis, clr)

    def on_btn_uncheck_all(self, event):
        n = self.lst.GetItemCount()
        for i in range(n):
            self.lst.CheckItem(i, False)

    def on_btn_check_all(self, event):
        n = self.lst.GetItemCount()
        for i in range(n):
            self.lst.CheckItem(i)

    def on_mnu_popup(self, event):
        # Only do this part the first time so the events are only bound once
        if not hasattr(self, "id_mnu_check"):
            self.id_mnu_check   = wx.NewId()
            self.id_mnu_uncheck = wx.NewId()
            self.id_mnu_chngclr = wx.NewId()
            self.Bind(wx.EVT_MENU, self.on_mnu_check,   id=self.id_mnu_check)
            self.Bind(wx.EVT_MENU, self.on_mnu_uncheck, id=self.id_mnu_uncheck)
            self.Bind(wx.EVT_MENU, self.on_mnu_chngclr, id=self.id_mnu_chngclr)

        # ---  Create menu
        menu = wx.Menu()
        menu.Append(self.id_mnu_check,   "Check")
        menu.Append(self.id_mnu_uncheck, "Uncheck")
        menu.Append(self.id_mnu_chngclr, "Change color")

        self.PopupMenu(menu)
        menu.Destroy()

    def on_mnu_uncheck(self, event):
        i = self.lst.GetFirstSelected()
        while i != -1:
            self.lst.CheckItem(i, False)
            i = self.lst.GetNextSelected(i)

    def on_mnu_check(self, event):
        i = self.lst.GetFirstSelected()
        while i != -1:
            self.lst.CheckItem(i)
            i = self.lst.GetNextSelected(i)

    def on_mnu_chngclr(self, event):
        dlg = wx.ColourDialog(self)
        dlg.GetColourData().SetChooseFull(True)
        if dlg.ShowModal() == wx.ID_OK:
            clr = dlg.GetColourData().GetColour()
            htm = clr.GetAsString(wx.C2S_HTML_SYNTAX)

            i = self.lst.GetFirstSelected()
            while i != -1:
                self.lst.SetItemTextColour(i, htm)
                i = self.lst.GetNextSelected(i)

        dlg.Destroy()


if __name__ == "__main__":
    def on_apply(v, *args):
        print(v)
    app = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    dialog_1 = PTDlgDisplayList(None)
    lst = ("a", "b", "c")
    args = [ 'arg0', 'arg1' ]
    dialog_1.fillList(lst, on_apply, args)
    app.SetTopWindow(dialog_1)
    dialog_1.Show()
    app.MainLoop()
