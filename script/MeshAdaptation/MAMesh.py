#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import matplotlib
import os

from CTCommon import CTUtil
from CTCommon import FEMesh

class MAMeshes:
    @staticmethod
    def reset_counter():
        MAMesh.reset_counter()

    def __init__(self):
        self.__reset()

    def __len__(self):
        return len(self.meshes)

    def __iter__(self):
        return self.meshes.__iter__()

    def __reset(self):
        self.meshes = []

    def load_data_from_file(self, fname):
        c = MAMesh(fname)
        self.meshes.append(c)

    def get_color(self):
        clr = []
        for c in self.meshes:
            clr.append( c.get_color() )
        return clr

    def set_color(self, clrs):
        for c, d in zip(self.meshes, clrs):
            c.set_color(d)

    def get_stats(self):
        txt = []
        for c in self.meshes:
            txt.append( c.get_stats() )
        return txt

    def on_update(self):
        modified = False
        return modified

    def get_visibility(self):
        r = [ ]
        for c in self.meshes:
            r.append( c.get_visibility() )
        return r

    def set_visibility(self, visi):
        for c, v in zip(self.meshes, visi):
            c.set_visibility(v)

class MAMesh:
    counter = 0

    @staticmethod
    def reset_counter():
        MAMesh.counter = 0

    def __init__(self, fname, clr = None):
        self.__reset()
        self.mesh = FEMesh.FEMesh(fname)
        self.clr = clr
        if (not self.clr): self.clr = CTUtil.get_clr(MAMesh.counter)
        MAMesh.counter += 1

    def __reset(self):
        self.dsp  = True
        self.plt  = None
        self.idx  = None
        self.mesh = None

    def fill_plot(self, ax):
        if (not self.dsp): return
        bbox = self.mesh.getBbox(stretchfactor=0.01)
        X, Y = self.mesh.getCoordinates()
        K    = self.mesh.getT3XtrnConnectivities()
        ax.set_xlim(bbox[0], bbox[2])
        ax.set_ylim(bbox[1], bbox[3])
        self.plt = ax.triplot(X,
                              Y,
                              triangles = K,
                              color     = self.clr)

    def get_color(self):
        return self.clr

    def set_color(self, clr):
        self.clr = clr
        #if (self.plt):
        #    self.plt.set_color(clr)

    def get_stats(self):
        return 'NbNodes = %i; NbElem = %i' % (self.mesh.getNbNodes(), self.mesh.getNbElements())

    def get_visibility(self):
        return self.dsp

    def set_visibility(self, visibility = True):
        self.dsp = visibility

if __name__ == '__main__':
    plot = MAMesh()
    plot.load_data_from_file('p1.prb')
    plot.fill_plot(None)
