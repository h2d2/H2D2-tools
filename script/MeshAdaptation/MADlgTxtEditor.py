#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx

class MADlgTxtEditor(wx.Dialog):
    def __init__(self, *args, **kwds):
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER
        wx.Dialog.__init__(self, *args, **kwds)
        self.txt = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE)
        self.btn_OK     = wx.Button(self, wx.ID_OK, "")
        self.btn_CANCEL = wx.Button(self, wx.ID_CANCEL, "")

        self.__set_properties()
        self.__do_layout()

#        self.Bind(wx.EVT_BUTTON, self.on_btn_ok, self.btn_OK)
#        self.Bind(wx.EVT_BUTTON, self.on_btn_cancel, self.btn_CANCEL)

    def __set_properties(self):
        self.SetTitle("H2D2 code snippet Editor")
        self.SetSize((400, 300))
        self.btn_OK.SetFocus()

    def __do_layout(self):
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(self.txt, 2, wx.EXPAND, 0)
        sizer_2.Add(self.btn_OK, 0, wx.ALIGN_RIGHT | wx.ALIGN_BOTTOM, 0)
        sizer_2.Add(self.btn_CANCEL, 0, wx.ALIGN_RIGHT | wx.ALIGN_BOTTOM, 0)
        sizer_1.Add(sizer_2, 0, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()

    def setText(self, txt):
        self.txt.Clear()
        for l in txt: self.txt.WriteText(l)

    def getText(self):
        return self.txt.GetValue()

#    def on_btn_ok(self, event):
#        print "Event handler `on_btn_ok' not implemented!"
#        event.Skip()
#
#    def on_btn_cancel(self, event):
#        print "Event handler `on_btn_cancel' not implemented!"
#        event.Skip()

if __name__ == "__main__":
    app = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    txt_editor = MyDialog(None, -1, "")
    app.SetTopWindow(txt_editor)
    txt_editor.Show()
    app.MainLoop()
