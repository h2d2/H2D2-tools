# H2D2-tools ChangeLog

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## Version 2110rc1

### DADataAnalizer:

#### Enhancements-additions:
- Add: Help system embryo
- Add: Support for SMS \.2dm grid files
- Add: Import of SHP/KML as vector via GeoJSON
- Add: Polyline editor for regular grid and ruler
- Add: History cleaner

#### Bug fixes:
- Fix: 3D
- Various bug fixes

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## Version 2104rc4

### DADataAnalizer:

#### Enhancements-additions:
- Add: Enable quiver for 1D field in 2D display
- Add: improve error message for 2D display
- Add: Automatically adjust limits of plots to include min and max

#### Bug fixes:
- Fix: Interpolated values for 1D plots where not correct
- Fix: Statistics for 1D grids (regular grids or subgrids)
- Various bug fixes

