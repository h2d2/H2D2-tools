@echo off
setlocal

:: ---  Check environment
if [%TMP%] == []         echo TMP must be defined      && goto exit
if [%INRS_BLD%] == []    echo INRS_BLD must be defined && goto exit
if [%INRS_DEV%] == []    echo INRS_DEV must be defined && goto exit
if [%INRS_LXT%] == []    echo INRS_LXT must be defined && goto exit
if not exist %TMP%       echo %TMP% must exist      && goto exit
if not exist %INRS_BLD%  echo %INRS_BLD% must exist && goto exit
if not exist %INRS_DEV%  echo %INRS_DEV% must exist && goto exit
if not exist %INRS_LXT%  echo %INRS_LXT% must exist && goto exit

:: ---  Get arguments
set LCL_CPL=%1
set LCL_MPI=%2
if [%LCL_CPL%] == [] set LCL_CPL=pyinstaller
if [%LCL_MPI%] == [] set LCL_MPI=_none_

:: ---  Calls
set SCONSTRUCT=%INRS_BLD%/MUC_Scons/SConstruct
:: --debug=stacktrace --taskmastertrace=-
call scons -Q --file=%SCONSTRUCT% compilers=%LCL_CPL% mpilibs=%LCL_MPI% builds=_none_
:: second call required for pyinstaller 
call scons -Q --file=%SCONSTRUCT% compilers=%LCL_CPL% mpilibs=%LCL_MPI% builds=_none_

:exit
endlocal
