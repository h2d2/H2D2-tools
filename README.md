# H2D2-tools

H2D2-tools contains a suite of python programs:
- [PTProbe](#PTProbe)
- [CTTracer](#CTTracer)
- [CCCompare](#CCCompare)
- [DADataAnalyzer](#DADataAnalyzer)

### <a name="PTProbe"></a>PTProbe
PTProbe (Probe Tracer) plots the data from H2D2 probes.
If the simulation is still running it will follow the progress.
The program allows for simple graphic interaction such as pan and zoom.

### <a name="CTTracer"></a>CTTracer
CTTracer (Convergence Tracer) plots the convergence history of a H2D2 run.
If the simulation is still running it will follow the progress.
The program allows for simple graphic interaction such as pan and zoom
as well as data probing.

### <a name="CCCompare"></a>CCCompare
CCCompare (Convergence Compare) plots in parallel the convergence history of two H2D2 runs.
The program allows for simple graphic interaction such as pan and zoom
as well as data probing.

### <a name="DADataAnalyzer"></a>DADataAnalyzer
DADataAnalyzer started as a tool to investigate H2D2 non-stationary simulations results. It allows:
- 2d data reduction operations: for example, the maximum water level from time t1 to t2;
- 0d data extraction: for example, evolution in time at points (like probes);
- 1d data extraction: for example, the instantaneous water level along a length profile;
- ability to project data on regular 0d-1d-2d grids
- etc...

It evolved as a geo-aware finite element graphical tool, to display data as 1d-2d-3d plots.

Help could be improved (this is an understatement)!

## Downloads

### <a name="As_prepackaged_binaries"></a>As pre-packaged binaries
The [pyinstaller](https://pyinstaller.readthedocs.io/en/stable/index.html) packages are binary packages and contains all dependencies needed to run the tools. They do not rely on separate installs. They can be installed system wide or for a single user.

pyinstaller packages are available for: 
- [Windows](http://www.gre-ehn.ete.inrs.ca/H2D2/contenu_download). The Windows installer can install either system wide (as Administrator) or for the current user. Tools can then be launched:
    - from the *Start Menu* under *H2D2-tools*
    - with the Windows Run Command (Windows Key + R)
    - with Windows Search, the magnifying glass icon normally found next to the Start Icon on the task bar.
- [Ubuntu (Debian)](http://www.gre-ehn.ete.inrs.ca/H2D2/contenu_download). The *.tar.gz* archive is to be extracted either in 
a system wide directory as in */opt/H2D2-tools*, or in user space as in *$HOME/opt/H2D2-tools*. Add "installation_directory/H2D2-tools/bin" to your PATH, usually done by editing *$HOME/.bash_aliases* file.



### As python scripts with cython components:
**Warning**: This requires a valid Python3 install

[cython](https://cython.readthedocs.io/en/latest/index.html) components are binary components (compiled modules) callable from python. They accelerate critical part of DADataAnalyzer.

The Python tools require specialized libraries. The preferred configuration is a Conda distribution like [MiniConda](https://docs.conda.io/en/latest/miniconda.html) or [Anaconda](https://www.anaconda.com/download/).

The installation steps are then:
- If not already available, install *conda* for Python3
- Download file *h2d2.yml* from <http://www.gre-ehn.ete.inrs.ca/H2D2/contenu_download>
- Create the *h2d2* *conda* environment and install packages:
    <pre><code>
    conda env create -f h2d2.yml
    </code></pre>
- Install *H2D2-tools* as described in section [As pre-packaged binaries](#As_prepackaged_binaries), choosing a cython package.

To run a tool, the *conda* environment must be activated:
- Open a *conda* terminal
- Activate the environment
    <pre><code>
    conda activate h2d2
    </code></pre>

Then launch a tool in the *conda* environment:

        DADataAnalyzer


Tips:
- [Windows Terminal](https://docs.microsoft.com/en-us/windows/terminal/) is a nice tool to configure different terminals. It can be configured to automatically open a specific *conda* environment.

<!---
### As python tarball:
-->
